unit Zahlen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, Menus, Liste;

type TZahlenClass= class(TObject)
                     procedure InitAll(Input1, Input2: String);
                     function GetggT: String;
                     function GetkgV: String;
                     function GetPrimFaktorStr: String;
                     function GetDezimal: String;
                     function GetHex: String;
                     private
                       I1,I2: String;
                       ggTValue,kgVValue: LongInt;
                       PrimFaktorStr: String;
                       Dezimal, Hex: String;
                       function BinToDez(Bin: String): Longint;
                   end;

     TZahlen_Dlg = class(TForm)
       Client_Panel: TPanel;
          MainMenu: TMainMenu;
          Optionen_Menu: TMenuItem;
          Loesche_Menu: TMenuItem;
       Input_GroupBox: TGroupBox;
       Edit1: TEdit;
       Edit2: TEdit;
       Selektion_Panel: TPanel;
       Auswahl_GroupBox: TGroupBox;
       ggt_RadioButton: TRadioButton;
       kgV_RadioButton: TRadioButton;
       Faktorisierung_RadioButton: TRadioButton;
       Exit_MenuItem: TMenuItem;
       Berechne_MenuItem: TMenuItem;
       Bin_RadioButton: TRadioButton;
       Hex_RadioButton: TRadioButton;
       OutPut_Edit: TEdit;
          procedure Berechne_ButtonClick(Sender: TObject);
       procedure Exit_MenuItemClick(Sender: TObject);
       procedure Loesche_MenuClick(Sender: TObject);
       procedure ChangeSelect(Sender: TObject);
       private
        ZahlenObj: TZahlenClass;
     end;

var
  Zahlen_Dlg: TZahlen_Dlg;

implementation

uses Mathe,p_mathtool,p_Solve;

{$R *.DFM}

procedure TZahlenClass.InitAll(Input1, Input2: String);
begin
  I1:=Input1;
  I2:=Input2;
end;
function TZahlenClass.GetggT: String;
 var a2,b2,r: Integer;
begin
  a2:=Trunc(StrToFloat(I1));
  b2:=Trunc(StrToFloat(I2));
  repeat
    r:=a2 mod b2;
    a2:=b2;
    b2:=r;
  until r=0;
  ggTValue:=a2;
  GetggT:=IntToStr(ggTValue);
end;
function TZahlenClass.GetkgV: String;
begin
  kgVValue:=(Trunc(StrToFloat(I1))*Trunc(StrToFloat(I2))) div StrToInt(GetggT);
  GetkgV:=IntToStr(kgVValue);
end;
function TZahlenClass.GetPrimFaktorStr: String; (*Faktorisierung in Primzahlen*)
 var Code, Cnt: Integer;
      i: Byte;
      n, m, Temp: LongInt;
      Res, GesamtRes: String;
begin
    PrimFaktorStr:='';
    GesamtRes:='1';
    Res:='';
    Cnt:=0;
    i:=2;
    Temp:=1;
    n:=StrToInt(I1);
    m:=n;
    repeat
      while (m mod i = 0) do begin
        m:=m div i;
        Inc(Cnt);
      end;
      Res:=IntToStr(i)+'^'+IntToStr(Cnt);
      Temp:=Temp*Trunc(Pot(i,Cnt));
      if Parsen(Res,Code)<> 1 then GesamtRes:=GesamtRes+'*'+Res;
      Inc(i);
      Cnt:=0;
      Res:='';
    until (Temp = n); (*immer l�sbar, wenn Zahl gr��er +1*)
    PrimFaktorStr:=GesamtRes;
    GetPrimFaktorStr:=PrimFaktorStr;
end;
function TZahlenClass.GetDezimal: String;
begin
  Dezimal:=IntToStr(BinToDez(I1));
  GetDezimal:=Dezimal;
end;
function TZahlenClass.GetHex: String;
begin
  Hex:=String(IntToHex(StrToInt(I1),6));
  GetHex:=Hex;
end;

function TZahlenClass.BinToDez(Bin: String): Longint;
 var Zahl: Longint;
     Lv, Exp: Byte;
 function BinOk: boolean;
  var lv:byte;
  begin
    for lv:=1 to length(bin) do
     if (bin[lv] <> '0') and (bin[lv] <> '1') then begin
      BinOk:=FALSE;
      exit;
    end;
    BinOk:=True;
 end;

 function Zweier_Potenz(exp:byte):longint;
  var lv: Byte;
      zahl: Longint;
 begin
   zahl:=1;
   for lv:=1 to exp do
     zahl:=zahl*2;
   Zweier_Potenz:=zahl;
 end;

begin
  Zahl:=0;
  Exp:=0;
  for lv:=length(bin) downto 1 do begin
    if bin[lv] = '1' then
      zahl:=zahl + zweier_Potenz(exp);
    Inc (exp);
  end;
  BinToDez:=Zahl;
end;

procedure TZahlen_Dlg.Berechne_ButtonClick(Sender: TObject);
begin
  ZahlenObj:=TZahlenClass.Create;
  ZahlenObj.InitAll(Edit1.Text,Edit2.Text);
  try
    if (ggt_RadioButton.Checked) then
      OutPut_Edit.Text:=ZahlenObj.GetggT
    else if (kgV_RadioButton.Checked) then
      OutPut_Edit.Text:=ZahlenObj.GetkgV
    else if (Bin_RadioButton.Checked) then
      OutPut_Edit.Text:=ZahlenObj.GetDezimal
    else if (Hex_RadioButton.Checked) then
      OutPut_Edit.Text:=ZahlenObj.GetHex
    else OutPut_Edit.Text:=ZahlenObj.GetPrimFaktorStr;
  except
    else ShowMessage('Fehler in Eingabe!');
  end;
  ZahlenObj.Free;
end;

procedure TZahlen_Dlg.Exit_MenuItemClick(Sender: TObject);
begin
  Close;
  Main_Dlg.Visible:=True;
end;

procedure TZahlen_Dlg.Loesche_MenuClick(Sender: TObject);
begin
  Edit1.Text:='';
  Edit2.Text:='';
  OutPut_Edit.Text:='';
end;

procedure TZahlen_Dlg.ChangeSelect(Sender: TObject);
begin
  Edit1.Enabled:=True;
  Edit2.Enabled:=True;
  if (Sender = Faktorisierung_RadioButton) or (Sender = Bin_RadioButton) or
     (Sender = Hex_RadioButton) then
      Edit2.Enabled:=False;
end;

end.


