�
 TFORM1 0  TPF0TForm1Form1Left� Top4BorderStylebsDialogCaptionG A U S S  -  U N I V E R S A LClientHeight�ClientWidth�ColorclSilver
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style OnCreate
FormCreatePixelsPerInchx
TextHeight TLabelLabel1Left�TopUWidthHeight	AlignmenttaCenterCaptionx
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel2LeftOTop1WidthWHeight	AlignmenttaCenterCaptionrechte Seite  TLabelLabel3Left1Top1Width� Height	AlignmenttaCenterCaption%Koeffizientenmatrix  A  (linke Seite)  TLabelLabel5Left�TopYWidth
Height	AlignmenttaCenterCaptionb
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel6Left�Top1WidthmHeightCaptionL�sungsvektor  TLabelLabel7Left�Top� WidthHeight	AlignmenttaCenterCaption*
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel8Left;Top� Width	Height	AlignmenttaCenterCaption=
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel9LeftTopWidth� HeightCaptionAnzahl der Unbekannten  TLabelLabel10Left� TopWidthrHeightCaptionKoeffiziententyp  TLabelLabel11Left�TopWidth� HeightCaptionkomplexe KoordinatenVisible  TLabelLabel4Left
Top
WidthNHeight	AlignmenttaCenterCaption1L I N E A R E S   G L E I C H U N G S S Y S T E M
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel12LeftrTopWidth� Height	AlignmenttaCenterCaptionax1+bx2+cx3... = Resultat1
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TStringGridStringGrid1LeftTopYWidthwHeight� ColCount
RowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLine	goEditinggoTabsgoAlwaysShowEditor TabOrderOnKeyUpStringGridKeyUpOnTopLeftChangedStringGrid1TopLeftChanged
RowHeights   TStringGridStringGrid2Left�TopvWidth� Height{TabStopColorclWhiteColCountRowCount
	FixedRows OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLine 
ScrollBarsssNoneTabOrder	ColWidths@@ 
RowHeights   TStringGridStringGrid3LeftZTopvWidthwHeight{TabStopColCountRowCount
	FixedRows OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoTabsgoAlwaysShowEditor 
ScrollBars
ssVerticalTabOrder OnKeyUpStringGridKeyUp	ColWidths@@ 
RowHeights   TButtonButton1LeftTop�Width�Height2CaptionStartTabOrderOnClickButton1Click  TButtonButton2Left�Top�Width� Height2CaptionBeendenTabOrderOnClickButton2Click  TPanelPanel2Left� Top1Width� Height)
BevelInner	bvLowered
BevelOuter	bvLoweredTabOrder TRadioButtonRadioButton1Left
Top
WidthFHeightCaptionreellChecked	TabOrder TabStop	OnClickRadioButtonClick  TRadioButtonRadioButton2LeftYTop
WidthcHeightCaptionkomplexTabOrderOnClickRadioButtonClick   TPanelPanel3Left�Top1Width� Height)
BevelInner	bvLowered
BevelOuter	bvLoweredTabOrderVisible TRadioButtonRadioButton3Left
Top
WidthZHeightCaptionRechteckChecked	TabOrder TabStop	OnClickRadioButtonClick  TRadioButtonRadioButton4LeftlTop
WidthZHeightCaptionZeigerTabOrderOnClickRadioButtonClick   	TSpinEdit	SpinEdit1LeftTop1WidthZHeight
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold MaxValueMinValue
ParentFontTabOrderValue
OnChangeSpinEdit1Change   