unit Mathe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Menus,
  Dialogs, ExtCtrls, Spin, Grids, Calendar, Buttons, StdCtrls, ComCtrls;

type
  TMain_Dlg = class(TForm)
    First_MainMenu: TMainMenu;
    Datei_MainMenu: TMenuItem;
    Bearbeiten_MainMenu: TMenuItem;
    Funktionseingabe_Menu: TMenuItem;
    Algebra_MainMenu: TMenuItem;
    GleichungntenGrades_Menu: TMenuItem;
    Rechner_Menu: TMenuItem;
    Primzahlen_Menu: TMenuItem;
    Geometrie_MainMenu: TMenuItem;
    Dreieck_Menu: TMenuItem;
    Polyeder_Polyeder: TMenuItem;
    Hilfe_MainMenu: TMenuItem;
    ZumAutor_Menu: TMenuItem;
    Beenden_Menu: TMenuItem;
    First_Timer: TTimer;
    Parser_Button: TSpeedButton;
    Fcts_Button: TSpeedButton;
    Matrix_Button: TSpeedButton;
    First_OpenDialog: TOpenDialog;
    Systeminformationen_Menu: TMenuItem;
    StatusBar: TStatusBar;
    DreiD_Button: TSpeedButton;
    Tool_Menu: TMenuItem;
    Tastenbelegung_Menu: TMenuItem;
    procedure Beenden_MenuClick(Sender: TObject);
    procedure Open_MenuClick(Sender: TObject);
    procedure GleichungntenGrades_MenuClick(Sender: TObject);
    procedure Systeminformationen_MenuClick(Sender: TObject);
    procedure Parser_ButtonClick(Sender: TObject);
    procedure First_TimerTimer(Sender: TObject);
    procedure Funktionseingabe_MenuClick(Sender: TObject);
    procedure ZumAutor_MenuClick(Sender: TObject);
    procedure Polyeder_PolyederClick(Sender: TObject);
    procedure Primzahlen_MenuClick(Sender: TObject);
    procedure Dreieck_MenuClick(Sender: TObject);
    procedure Tool_MenuClick(Sender: TObject);
  end;

var
  Main_Dlg: TMain_Dlg;

implementation

uses Parser, Linequa, Sysinfo, funktionsplotter, Polyeder, Primzahlen,
     Triangle, Zahlen;

{$R *.DFM}

procedure TMain_Dlg.Beenden_MenuClick(Sender: TObject);
begin
  Close;
end;

procedure TMain_Dlg.First_TimerTimer(Sender: TObject);
 var Memory : TMEMORYSTATUS; (*Betriebssysteminternes*)
begin
  StatusBar.Panels[1].Text:='Uhrzeit: ' + TimeToStr(Time);
  GlobalMemoryStatus(Memory);
  StatusBar.Panels[0].Text:='freier Speicher: '+ IntToStr(Memory.dwAvailPhys) +' Bytes';
end;

procedure TMain_Dlg.Open_MenuClick(Sender: TObject);
var Memo1: TMemo;
    First_SaveDialog: TSaveDialog;
begin
  Memo1:=nil;
  First_SaveDialog:=nil;
  if First_OpenDialog.Execute then begin
    Memo1.Lines.LoadfromFile(First_OpenDialog.FileName);
    First_SaveDialog.Filename := First_OpenDialog.FileName;
    Caption := First_OpenDialog.FileName;
  end;
end;

procedure TMain_Dlg.GleichungntenGrades_MenuClick(Sender: TObject);
begin
  Application.CreateForm(TLinequa_Dlg, Linequa_Dlg);
  Main_Dlg.Visible:=False;
  Linequa_Dlg.Visible:=True;
end;

procedure TMain_Dlg.Systeminformationen_MenuClick(Sender: TObject);
begin
  Application.CreateForm(TSysInfo_Dlg,SysInfo_Dlg);
  Main_Dlg.Visible:=False;
  SysInfo_Dlg.Visible:=True;
end;

procedure TMain_Dlg.Parser_ButtonClick(Sender: TObject);
begin
  Application.CreateForm(TParser_Dlg, Parser_Dlg);
  Main_Dlg.Visible:=False;
  Parser_Dlg.Visible:=True;
end;

procedure TMain_Dlg.Funktionseingabe_MenuClick(Sender: TObject);
begin
  Application.CreateForm(TFunktionsplotter_Dlg, Funktionsplotter_Dlg);
  Main_Dlg.Visible:=False;
  Funktionsplotter_Dlg.Visible:=True;
end;

procedure TMain_Dlg.Polyeder_PolyederClick(Sender: TObject);
begin
  Application.CreateForm(TPolyeder_Dlg, Polyeder_Dlg);
  Main_Dlg.Visible:=False;
  Polyeder_Dlg.Visible:=True;
end;

procedure TMain_Dlg.Primzahlen_MenuClick(Sender: TObject);
begin
  Application.CreateForm(TPrimzahlen_Dlg, Primzahlen_Dlg);
  Main_Dlg.Visible:=False;
  Primzahlen_Dlg.Visible:=True;
end;

procedure TMain_Dlg.Dreieck_MenuClick(Sender: TObject);
begin
  Application.CreateForm(TTriangle_Dlg, Triangle_Dlg);
  Main_Dlg.Visible:=False;
  Triangle_Dlg.Visible:=True;
end;

procedure TMain_Dlg.ZumAutor_MenuClick(Sender: TObject);
begin
  ShowMessage('Ren� Haberland');
end;

procedure TMain_Dlg.Tool_MenuClick(Sender: TObject);
begin
  Application.CreateForm(TZahlen_Dlg, Zahlen_Dlg);
  Main_Dlg.Visible:=False;
  Zahlen_Dlg.Visible:=True;
end;

end.
