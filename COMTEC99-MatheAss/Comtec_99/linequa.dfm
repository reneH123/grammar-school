�
 TLINEQUA_DLG 0(  TPF0TLinequa_DlgLinequa_DlgLeft� TopWidthRHeight� ActiveControlKoeffizienten_StringGridBorderIcons CaptionLineares GleichungssystemColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelx_LabelLeft�TopWidthHeight	AlignmenttaCenterCaptionxFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelrechte_Seite_LabelLeft�Top Width9Height	AlignmenttaCenterCaptionrechte Seite  TLabelKoeffizientenmatrix_A_LabelLeft(Top Width� HeightHint)horizontal: Koeffizienten einer Gleichung	AlignmenttaCenterCaption%Koeffizientenmatrix  A  (linke Seite)ParentShowHintShowHint	  TLabelb_LabelLeft TopWidthHeight	AlignmenttaCenterCaptionbFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLoesungsvektor_LabelLeft`Top WidthFHeightCaptionL�sungsvektor  TLabel	Mal_LabelLeft@TopPWidthHeight	AlignmenttaCenterCaption*Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelGleich_LabelLeft�TopPWidthHeight	AlignmenttaCenterCaption=Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelAnzahl_der_Unbekannten_LabelLeft� Top� Width;HeightHint>legt fest, wieviele Unbekannte die Matrix ber�cksichtigen sollCaption
UnbekannteParentShowHintShowHint	  TLabelKoeffiziententyp_LabelLeft� Top� WidthKHeightCaptionKoeffiziententyp  TLabelkomplexe_Koordinaten_LabelLeftxTop� WidthiHeightCaptionkomplexe KoordinatenVisible  TLabel LeftTopWidthHeight  TStringGridKoeffizienten_StringGridLeftTopWidth1Height� ColCount
RowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLine	goEditinggoTabsgoAlwaysShowEditor TabOrderOnKeyUpStringGridKeyUpOnTopLeftChanged&Koeffizienten_StringGridTopLeftChanged
RowHeights   TStringGridLoesungsvektor_StringGridLeftXTop(WidthiHeightdTabStopColorclWhiteColCountRowCount
	FixedRows OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLine 
ScrollBarsssNoneTabOrder	ColWidths@@ 
RowHeights   TStringGridrechte_Seite_StringGridLeft�Top(WidthaHeightdTabStopColCountRowCount
	FixedRows OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoTabsgoAlwaysShowEditor 
ScrollBars
ssVerticalTabOrder OnKeyUpStringGridKeyUp	ColWidths@@ 
RowHeights   TButtonBerechne_ButtonLeftTop� Width2Height!HintBerechnet L�sungsvektorCaptionBerechneParentShowHintShowHintTabOrderOnClickBerechne_ButtonClick  TButtonExit_ButtonLeft@Top� Width2Height!HintKehrt zum Hauptprogramm zur�ckCaptionZur�ckParentShowHintShowHint	TabOrderOnClickExit_ButtonClick  TPanelKoeffiziententyp_PanelLeft� Top� Width� Height!
BevelInner	bvLowered
BevelOuter	bvLoweredTabOrder TRadioButtonreel_RadioButtonLeftTopWidth9HeightHintreeler DefinitionsbereichCaptionreellChecked	ParentShowHintShowHint	TabOrder TabStop	OnClickRadioButtonClick  TRadioButtonkomplex_RadioButtonLeftHTopWidthQHeightHintkomplexer DefinitionsbereichCaptionkomplexParentShowHintShowHint	TabOrderOnClickRadioButtonClick   TPanelkomplexe_Koordinaten_PanelLeftxTop� Width� Height!
BevelInner	bvLowered
BevelOuter	bvLoweredTabOrderVisible TRadioButtonRechteck_RadioButtonLeftTopWidthIHeightCaptionRechteckChecked	TabOrder TabStop	OnClickRadioButtonClick  TRadioButtonZeiger_RadioButtonLeftXTopWidthIHeightCaptionZeigerTabOrderOnClickRadioButtonClick   	TSpinEditUnbekannte_SpinEditLeftxTop� WidthIHeightColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold MaxValueMinValue
ParentFontTabOrderValueOnChangeUnbekannte_SpinEditChange   