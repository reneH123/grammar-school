unit Parser;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Menus;

{TParserClass= class(TObject)
                     procedure Init(Job: String);
                     function GetRes: EXTENDED;
                     function GetErr: Integer;
                     private
                       ErrCode: Integer;
                       Result: EXTENDED;
                   end;}
type
   TParser_Dlg = class(TForm)
    Eins: TSpeedButton;
    Zwei: TSpeedButton;
    Drei: TSpeedButton;
    Vier: TSpeedButton;
    Fuenf: TSpeedButton;
    Sechs: TSpeedButton;
    Neun: TSpeedButton;
    Berechnen: TSpeedButton;
    Eingabe: TEdit;
    Plus: TSpeedButton;
    Mal: TSpeedButton;
    Durch: TSpeedButton;
    Minus: TSpeedButton;
    Ausgabe: TPanel;
    Quadrat: TSpeedButton;
    Logarithmus_naturalis: TSpeedButton;
    dekadischer_Logarithmus: TSpeedButton;
    Quadratwurzel: TSpeedButton;
    nteWurzel: TSpeedButton;
    Sinusfkt: TSpeedButton;
    Tangensfkt: TSpeedButton;
    Sekansfkt: TSpeedButton;
    Cosinusfkt: TSpeedButton;
    briggscher_Logarithmus: TSpeedButton;
    Cotangensfkt: TSpeedButton;
    ArcusCosinusfkt: TSpeedButton;
    ArcusTangensfkt: TSpeedButton;
    ArcusSinusfkt: TSpeedButton;
    Cosekansfkt: TSpeedButton;
    ArcusCosekansfkt: TSpeedButton;
    SinusHyperbolikusfkt: TSpeedButton;
    CosinusHyperbolikusfkt: TSpeedButton;
    ArcusCotangens: TSpeedButton;
    ArcusSekansfkt: TSpeedButton;
    SekansHyperbolikusfkt: TSpeedButton;
    CotangensHyperbolikusfkt: TSpeedButton;
    AreaSinusHyperbolikusfkt: TSpeedButton;
    CosekansHyperbolikusfkt: TSpeedButton;
    TangensHyperbolikusfkt: TSpeedButton;
    AreaTangensHyperbolikusfkt: TSpeedButton;
    AreaCosekansHyperbolikusfkt: TSpeedButton;
    AreaCotangensHyperbolikusfkt: TSpeedButton;
    AreaSekansHyperbolikusfkt: TSpeedButton;
    AreaCosinusHyperbolikusfkt: TSpeedButton;
    Klammer_zu: TSpeedButton;
    andEN: TSpeedButton;
    orEN: TSpeedButton;
    Fakultaet: TSpeedButton;
    notEN: TSpeedButton;
    Klammer_auf: TSpeedButton;
    Negieren: TSpeedButton;
    Exponent_e: TSpeedButton;
    xorEN: TSpeedButton;
    Lichtjahr: TSpeedButton;
    Pi: TSpeedButton;
    Eulersche_Zahl: TSpeedButton;
    Clearen: TSpeedButton;
    Beenden: TSpeedButton;
    Hoch: TSpeedButton;
    Sieben: TSpeedButton;
    Acht: TSpeedButton;
    Null: TSpeedButton;
    Punkt: TSpeedButton;
    MainMenu1: TMainMenu;
    Datei1: TMenuItem;
    Lschealles1: TMenuItem;
    Zurck1: TMenuItem;
    procedure EinsClick(Sender: TObject);
    procedure ZweiClick(Sender: TObject);
    procedure VierClick(Sender: TObject);
    procedure FuenfClick(Sender: TObject);
    procedure NeunClick(Sender: TObject);
    procedure AchtClick(Sender: TObject);
    procedure SechsClick(Sender: TObject);
    procedure SiebenClick(Sender: TObject);
    procedure DreiClick(Sender: TObject);
    procedure BerechnenClick(Sender: TObject);
    procedure HochClick(Sender: TObject);
    procedure PlusClick(Sender: TObject);
    procedure MalClick(Sender: TObject);
    procedure DurchClick(Sender: TObject);
    procedure MinusClick(Sender: TObject);
    procedure NullClick(Sender: TObject);
    procedure nteWurzelClick(Sender: TObject);
    procedure PiClick(Sender: TObject);
    procedure Eulersche_ZahlClick(Sender: TObject);
    procedure QuadratClick(Sender: TObject);
    procedure QuadratwurzelClick(Sender: TObject);
    procedure Logarithmus_naturalisClick(Sender: TObject);
    procedure dekadischer_LogarithmusClick(Sender: TObject);
    procedure briggscher_LogarithmusClick(Sender: TObject);
    procedure ClearenClick(Sender: TObject);
    procedure SinusfktClick(Sender: TObject);
    procedure CosinusfktClick(Sender: TObject);
    procedure TangensfktClick(Sender: TObject);
    procedure CotangensfktClick(Sender: TObject);
    procedure SekansfktClick(Sender: TObject);
    procedure CosekansfktClick(Sender: TObject);
    procedure ArcusSinusfktClick(Sender: TObject);
    procedure ArcusCosinusfktClick(Sender: TObject);
    procedure ArcusTangensfktClick(Sender: TObject);
    procedure ArcusSekansfktClick(Sender: TObject);
    procedure ArcusCosekansfktClick(Sender: TObject);
    procedure ArcusCotangensClick(Sender: TObject);
    procedure SinusHyperbolikusfktClick(Sender: TObject);
    procedure CosinusHyperbolikusfktClick(Sender: TObject);
    procedure LichtjahrClick(Sender: TObject);
    procedure TangensHyperbolikusfktClick(Sender: TObject);
    procedure SekansHyperbolikusfktClick(Sender: TObject);
    procedure CosekansHyperbolikusfktClick(Sender: TObject);
    procedure CotangensHyperbolikusfktClick(Sender: TObject);
    procedure AreaSinusHyperbolikusfktClick(Sender: TObject);
    procedure AreaCosinusHyperbolikusfktClick(Sender: TObject);
    procedure AreaTangensHyperbolikusfktClick(Sender: TObject);
    procedure AreaSekansHyperbolikusfktClick(Sender: TObject);
    procedure AreaCosekansHyperbolikusfktClick(Sender: TObject);
    procedure AreaCotangensHyperbolikusfktClick(Sender: TObject);
    procedure NegierenClick(Sender: TObject);
    procedure Exponent_eClick(Sender: TObject);
    procedure FakultaetClick(Sender: TObject);
    procedure notENClick(Sender: TObject);
    procedure xorENClick(Sender: TObject);
    procedure andENClick(Sender: TObject);
    procedure Klammer_aufClick(Sender: TObject);
    procedure Klammer_zuClick(Sender: TObject);
    procedure orENClick(Sender: TObject);
    procedure PunktClick(Sender: TObject);
    procedure ExitClick(Sender: TObject);
  private
{    ParserObj: TParserClass;}
  end;

var Parser_Dlg: TParser_Dlg;

implementation

uses Mathe,p_Solve;
{$R *.DFM}

(* function Parsen(Aufgabe: String; var Code: Integer): Extended; external 'ParserLib' index 1; *)

{procedure TParserClass.Init(Job: String);
begin
  ErrCode:=0;
  Result:=Parsen(Job,ErrCode);
end;

function TParserClass.GetRes: EXTENDED;
begin
  GetRes:=Result;
end;

function TParserClass.GetErr: Integer;
begin
  GetErr:=ErrCode;
end;}

procedure TParser_Dlg.EinsClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'1';
end;
procedure TParser_Dlg.ZweiClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'2';
end;
procedure TParser_Dlg.VierClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'4';
end;
procedure TParser_Dlg.FuenfClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'5';
end;
procedure TParser_Dlg.NeunClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'9';
end;
procedure TParser_Dlg.AchtClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'8';
end;
procedure TParser_Dlg.SechsClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'6';
end;
procedure TParser_Dlg.SiebenClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'7';
end;
procedure TParser_Dlg.DreiClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'3';
end;

procedure TParser_Dlg.BerechnenClick(Sender: TObject);
 var ErrCode: Integer;
     Res: EXTENDED;
     Err: Boolean;
begin
  Res:=0; Err:=False;
  try
(*    ParserObj:=TParserClass.Create;
    ParserObj.Init(Eingabe.Text);
    Res:=ParserObj.GetRes;
    ErrCode:=ParserObj.GetErr;
    ParserObj.Free;*)
    Res:=Parsen(Eingabe.text,ErrCode);
  except
    on EZeroDivide do begin
      Ausgabe.Caption:='Fehler bei Division durch Null!';
      Err:=True;
    end;
    on EOverflow do begin
      Ausgabe.Caption:='�berlauf des Gleitkommaregisters!';
      Err:=True;
    end;
    on EMathError do begin
      Ausgabe.Caption:='Fehler bei Berechnungen mit Gleitkommazahlen!';
      Err:=True;
    end;
    on EConvertError do begin
      Ausgabe.Caption:='Konvertierungsfehler!';
      Err:=True;
    end;
    on EInOutError do begin
      Ausgabe.Caption:='Fehler bei Ein/Ausgabe!';
      Err:=True;
    end;
  end;
  if Not(Err) then Ausgabe.Caption:=FloatToStr(Res);
end;

procedure TParser_Dlg.PlusClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'+';
end;
procedure TParser_Dlg.HochClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'^';
end;
procedure TParser_Dlg.MalClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'*';
end;
procedure TParser_Dlg.DurchClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'/';
end;
procedure TParser_Dlg.MinusClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'-';
end;
procedure TParser_Dlg.NullClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'0';
end;
procedure TParser_Dlg.nteWurzelClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ROOT';
end;
procedure TParser_Dlg.PiClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'PI';
end;
procedure TParser_Dlg.Eulersche_ZahlClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'EZ';
end;

procedure TParser_Dlg.QuadratClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'SQR(';
end;
procedure TParser_Dlg.QuadratwurzelClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'SQRT(';
end;
procedure TParser_Dlg.Logarithmus_naturalisClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'LN(';
end;
procedure TParser_Dlg.dekadischer_LogarithmusClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'LG(';
end;
procedure TParser_Dlg.briggscher_LogarithmusClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'LB(';
end;
procedure TParser_Dlg.ClearenClick(Sender: TObject);
begin
  Eingabe.Text:='';
  Ausgabe.Caption:='';
end;
procedure TParser_Dlg.SinusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'SIN(';
end;
procedure TParser_Dlg.CosinusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'COS(';
end;
procedure TParser_Dlg.TangensfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'TAN(';
end;
procedure TParser_Dlg.CotangensfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'COT(';
end;
procedure TParser_Dlg.SekansfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'SEK(';
end;
procedure TParser_Dlg.CosekansfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'COSEK(';
end;
procedure TParser_Dlg.ArcusSinusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ARSIN(';
end;
procedure TParser_Dlg.ArcusCosinusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ARCCOS(';
end;
procedure TParser_Dlg.ArcusTangensfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ARCTAN(';
end;
procedure TParser_Dlg.ArcusSekansfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ARCSEK(';
end;
procedure TParser_Dlg.ArcusCosekansfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ARCCOSEK(';
end;
procedure TParser_Dlg.ArcusCotangensClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ARCCOT(';
end;
procedure TParser_Dlg.SinusHyperbolikusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'SINH(';
end;
procedure TParser_Dlg.CosinusHyperbolikusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'COSH(';
end;
procedure TParser_Dlg.LichtjahrClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'LY';
end;
procedure TParser_Dlg.TangensHyperbolikusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'TANH(';
end;
procedure TParser_Dlg.SekansHyperbolikusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'SEKH(';
end;
procedure TParser_Dlg.CosekansHyperbolikusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'COSEKH(';
end;
procedure TParser_Dlg.CotangensHyperbolikusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'COTH(';
end;
procedure TParser_Dlg.AreaSinusHyperbolikusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ARSINH(';
end;
procedure TParser_Dlg.AreaCosinusHyperbolikusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ARCOSH(';
end;
procedure TParser_Dlg.AreaTangensHyperbolikusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ARTANH(';
end;
procedure TParser_Dlg.AreaSekansHyperbolikusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ARSEKH(';
end;
procedure TParser_Dlg.AreaCosekansHyperbolikusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ARCOSEKH(';
end;
procedure TParser_Dlg.AreaCotangensHyperbolikusfktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'ARCOTH(';
end;
procedure TParser_Dlg.NegierenClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'SGN(';
end;
procedure TParser_Dlg.Exponent_eClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'EXP(';
end;
procedure TParser_Dlg.FakultaetClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'FAK(';
end;
procedure TParser_Dlg.notENClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'NOT(';
end;
procedure TParser_Dlg.xorENClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'XOR';
end;
procedure TParser_Dlg.andENClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'AND';
end;
procedure TParser_Dlg.orENClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'OR';
end;
procedure TParser_Dlg.Klammer_aufClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'(';
end;
procedure TParser_Dlg.Klammer_zuClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+')';
end;
procedure TParser_Dlg.PunktClick(Sender: TObject);
begin
  Eingabe.Text:=Eingabe.Text+'.';
end;
procedure TParser_Dlg.ExitClick(Sender: TObject);
begin
  Close;
  Main_Dlg.Visible:=True;
end;

end.
