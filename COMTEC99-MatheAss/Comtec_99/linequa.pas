unit linequa;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Grids, ExtCtrls, Spin;

const nmax = 20; (* max. Anzahl von Unbekannten *)

type
  TLinequaClass= class(TObject)
                    function GetN: Byte;
                    function GetC(Index: Byte): Extended;
                    function GetD(Index: Byte): Extended;
                    function GetA(Index1,Index2: Byte): Extended;
                    function GetB(Index1,Index2: Byte): Extended;
                    procedure SetN(Value: Byte);
                    procedure SetC(Index: Byte; Value: Extended);
                    procedure SetD(Index: Byte; Value: Extended);
                    procedure SetA(Index1,Index2: Byte; Value: Extended);
                    procedure SetB(Index1,Index2: Byte; Value: Extended);
                    procedure SetErrCode(Error: Integer);
                    procedure Init;
                    procedure TransR_Z(var x,y: Extended);
                    procedure TransZ_R(var be,ph: Extended);
                    procedure AddK(a,b,c,d: Extended; var e,f: Extended);
                    procedure MultK(a,b,c,d: Extended; var e,f: Extended);
                    procedure DivK(a,b,c,d: Extended; var e,f: Extended);
                    procedure Gauss;
                  private
                     n: Byte;                                (* Ordnung des zu l�senden GLS *)
                     Code: Integer;                          (* div. Fehlercode *)
                     (* mathematische Prozeduren: *)
                     a: array[1..nmax,1..nmax] of Extended;  (* Realteil der Koeffizientenmatrix *)
                     b: array[1..nmax,1..nmax] of Extended;  (* Imagin�rteil   "            "    *)
                     c: array[1..nmax] of Extended;          (* Realteil der rechten Seite *)
                     d: array[1..nmax] of Extended;          (* Imagin�rteil    "       "  *)
                     p: array[1..nmax] of Byte;              (* Pivotzeiger *)
                 end;

  TLinequa_Dlg = class(TForm)
    Koeffizienten_StringGrid: TStringGrid;
    Loesungsvektor_StringGrid: TStringGrid;
    rechte_Seite_StringGrid: TStringGrid;
    Berechne_Button: TButton;
    Exit_Button: TButton;
    reel_RadioButton: TRadioButton;
    komplex_RadioButton: TRadioButton;
    Rechteck_RadioButton: TRadioButton;
    Zeiger_RadioButton: TRadioButton;
    Koeffiziententyp_Panel: TPanel;
    komplexe_Koordinaten_Panel: TPanel;
    x_Label: TLabel;
    rechte_Seite_Label: TLabel;
    Koeffizientenmatrix_A_Label: TLabel;
    b_Label: TLabel;
    Loesungsvektor_Label: TLabel;
    Mal_Label: TLabel;
    Gleich_Label: TLabel;
    Anzahl_der_Unbekannten_Label: TLabel;
    Koeffiziententyp_Label: TLabel;
    komplexe_Koordinaten_Label: TLabel;
    Unbekannte_SpinEdit: TSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure StringGridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Koeffizienten_StringGridTopLeftChanged(Sender: TObject);
    procedure RadioButtonClick(Sender: TObject);
    procedure Berechne_ButtonClick(Sender: TObject);
    procedure Exit_ButtonClick(Sender: TObject);
    procedure changeGrids;
    procedure clearAll;
    procedure storeRealSystem;
    procedure displayRealSystem;
    procedure displayRealSolution;
    procedure storeComplexSystem;
    procedure displayComplexSystem;
    procedure displayComplexSolution;
    procedure Unbekannte_SpinEditChange(Sender: TObject);
    private
     GLS: TLinequaClass;
   end;

var Linequa_Dlg: TLinequa_Dlg;

implementation

{$R *.DFM}
uses Mathe, sysinfo;

function TLinequaClass.GetN: Byte;
begin
  GetN:=n;
end;

function TLinequaClass.GetC(Index: Byte): Extended;
begin
  GetC:=c[Index];
end;

function TLinequaClass.GetD(Index: Byte): Extended;
begin
  GetD:=d[Index];
end;

function TLinequaClass.GetA(Index1,Index2: Byte): Extended;
begin
  GetA:=a[Index1,Index2];
end;

function TLinequaClass.GetB(Index1,Index2: Byte): Extended;
begin
  GetB:=b[Index1,Index2];
end;

procedure TLinequaClass.SetN(Value: Byte);
begin
  n:=Value;
end;

procedure TLinequaClass.SetC(Index: Byte; Value: Extended);
begin
  c[Index]:=Value;
end;

procedure TLinequaClass.SetD(Index: Byte; Value: Extended);
begin
  d[Index]:=Value;
end;

procedure TLinequaClass.SetA(Index1,Index2: Byte; Value: Extended);
begin
  a[Index1,Index2]:=Value;
end;

procedure TLinequaClass.SetB(Index1,Index2: Byte; Value: Extended);
begin
  b[Index1,Index2]:=Value;
end;

procedure TLinequaClass.SetErrCode(Error: Integer);
begin
  Code:=Error;
end;

procedure TLinequaClass.Init;  (* Setzen auf Anfangswerte *)
var i,j: Byte;
begin
  for i:=1 to nmax do begin
    p[i]:=i;
    c[i]:=0;
    d[i]:=0;
    for j:=1 to nmax do begin
      a[i,j]:=0;
      b[i,j]:=0
    end;
  end;
end;

procedure TLinequaClass.TransR_Z(var x,y: Extended); (* Transformation Rechteck => Zeiger *)
 var a,b: Extended;
begin
  a:=x;
  b:=y;
  x:=Sqrt(a*a+b*b);    (* Betrag *)
  (* Phase in Grad *)
  if (a <> 0) then begin
    y:=180/Pi*ArcTan(b/a);
    if (a < 0) then
     y:=y+180
   end
  else begin
    if (b <> 0) then
     if (b > 0) then
      y:=90
     else
      y:=-90
  end
end;

procedure TLinequaClass.TransZ_R(var be,ph: Extended); (* Transformation Zeiger => Rechteck *)
 var x,y: Extended;
begin
  ph:=ph*Pi/180;    (* Umwandlung Grad => Bogenma� *)
  x:=be*cos(ph);
  y:= be*sin(ph);
  be:=x;
  ph:=y;
end;

procedure TLinequaClass.AddK(a,b,c,d: Extended; var e,f: Extended);
(* Addition kompl. Z. *)
begin
  e:=a+c;
  f:=b+d;
end;

procedure TLinequaClass.MultK(a,b,c,d: Extended; var e,f: Extended);
(* Multiplikation kompl.Z. *)
begin
  e:=a*c-b*d;
  f:=a*d+b*c;
end;

procedure TLinequaClass.DivK(a,b,c,d: Extended; var e,f: Extended);
(* Division kompl. Z. *)
begin
  e:=(a*c+b*d)/(c*c+d*d);
  f:=(b*c-a*d)/(c*c+d*d)
end;

procedure TLinequaClass.Gauss; (* Gau�algorithmus *)
 const p1=1E-12;
 var i,j,k,o,pv,z,p0: Byte;
     v1,v2, w1,w2: Extended;   (* Zwischenvariable *)
begin
  Code:=0;
  for k:=n downto 2 do begin   (* Elimination *)
    z:=1;
    for i:=2 to k do begin     (* Pivotsuche *)
      o:=p[z]; pv:=p[i];       (* Nr. der Pivotgleichung *)
      if (a[o,o]*a[o,o]+b[o,o]*b[o,o] < a[pv,pv]*a[pv,pv]+b[pv,pv]*b[pv,pv]) then
       z:=i
    end;
    p0:=p[z];                  (* Austausch Pivotelement *)
    p[z]:=p[k];
    p[k]:=p0;
    if (a[p0,p0]*a[p0,p0]+b[p0,p0]*b[p0,p0] < p1) then
     code:=p0;                 (* Pivotkontrolle *)
  for j:=1 to k-1 do begin
    z:=p[j];
    if (a[z,p0] <> 0) or (b[z,p0] <> 0) then begin
      DivK(a[z,p0],b[z,p0],a[p0,p0],b[p0,p0],w1,w2);
      MultK(w1,w2,c[p0],d[p0],v1,v2);
      AddK(c[z],d[z],-v1,-v2,c[z],d[z]);
      for i:=1 to k-1 do begin
        MultK(w1,w2,a[p0,p[i]],b[p0,p[i]],v1,v2);
        AddK(a[z,p[i]],b[z,p[i]],-v1,-v2,a[z,p[i]],b[z,p[i]])
      end
    end
  end
 end;                           (* Elimination ENDE*)
 for j:=1 to n do begin         (* R�cksubstitution *)
  z:=p[j];
  if (a[z,z] = 0) and (b[z,z] = 0) then
   code:=z
  else begin
    for i:=1 to j-1 do begin
      MultK(c[p[i]],d[p[i]],a[z,p[i]],b[z,p[i]],v1,v2);
      AddK(c[z],d[z],-v1,-v2,c[z],d[z])
    end;
    DivK(c[z],d[z],a[z,z],b[z,z],c[z],d[z])
  end
 end                            (* R�cksubstitution ENDE*)
end;                            (*Gauss ENDE*)

(**************************** Objektmethoden **********************************)
procedure TLinequa_Dlg.ClearAll; (* l�scht Inhalt der drei Gitter incl. Beschriftung *)
 var c,i,j: Integer;
begin
  if reel_RadioButton.Checked then
   c:=GLS.GetN
  else
   c:=2*GLS.GetN;
  for i := 0 to c do begin
    if (i < c) then begin
      LoesungsVektor_StringGrid.Cells[0,i]:='';
      LoesungsVektor_StringGrid.Cells[1,i]:='';
      rechte_Seite_StringGrid.Cells[0,i]:='';
      rechte_Seite_StringGrid.Cells[1,i]:=''
    end;
    for j := 1 to c do
      Koeffizienten_StringGrid.Cells[i,j] := ''
  end
end;

procedure TLinequa_Dlg.ChangeGrids;       (* baut Gitter neu auf *)
 var i: Integer;
begin
  ClearAll;
  if reel_RadioButton.Checked then begin  (* reelles GLS *)
    LoesungsVektor_StringGrid.RowCount:=GLS.GetN;
    rechte_Seite_StringGrid.RowCount:=GLS.GetN;
    with Koeffizienten_StringGrid do begin
      RowCount:=GLS.GetN+1;
      ColCount:=GLS.GetN+1;
      for i:=1 to GLS.GetN do begin
        Cells[i,0]:=IntToStr(i);
        Cells[0,i]:=IntToStr(i);
        Loesungsvektor_StringGrid.Cells[0,i-1]:='x'+IntToStr(i);
        rechte_Seite_StringGrid.Cells[0,i-1]:='b'+IntToStr(i);
      end
    end
   end
  else begin                              (* komplexes GLS *)
    Loesungsvektor_StringGrid.RowCount:=2*GLS.GetN;
    rechte_Seite_StringGrid.RowCount:=2*GLS.GetN;
    with Koeffizienten_StringGrid do begin
      RowCount:=2*GLS.GetN+1;
      ColCount:=GLS.GetN+1;
      for i:= 1 to GLS.GetN do begin
        Cells[i,0]:=IntToStr(i);
        Cells[0,2*i-1]:=IntToStr(i);
        Loesungsvektor_StringGrid.Cells[0,2*i-2]:='x'+IntToStr(i);
        rechte_Seite_StringGrid.Cells[0,2*i-2]:='b'+IntToStr(i)
      end
    end
  end
end;

procedure TLinequa_Dlg.StoreRealSystem;   (* reelles GLS: Anzeigegitter => Speicher *)
 var i,j: Integer;
     TempC: Extended;
     TempErrCode: Integer;
     TempA: Extended;
begin
  for i:= 1 to GLS.GetN do begin
    Val(rechte_Seite_StringGrid.Cells[1,i-1], TempC, TempErrCode);  (* rechte Seite *)
    GLS.SetC(i,TempC);
    GLS.SetErrCode(TempErrCode);
    for j:= 1 to GLS.GetN do begin
      Val(Koeffizienten_StringGrid.Cells[j,i], TempA, TempErrCode); (* linke Seite *)
      GLS.SetA(i,j,TempA);
      GLS.SetErrCode(TempErrCode);
    end;
  end
end;

procedure TLinequa_Dlg.DisplayRealSystem; (* reelles GLS: Speicher => Anzeigegitter *)
 var i,j: Integer;
     s: String;
begin
  for i:=1 to GLS.GetN do begin
    Str(GLS.GetC(i):8:4,s);
    rechte_Seite_StringGrid.Cells[1,i-1]:=s;
    for j:=1 to GLS.GetN do
      Str(GLS.GetA(i,j):8:4,s);
    Koeffizienten_StringGrid.Cells[j,i]:=s
  end;
end;

procedure TLinequa_Dlg.DisplayRealSolution;  (* reelle L�sung: Speicher => Anzeigegitter *)
 var i: Integer;
     s: String;
begin
  for i:= 1 to GLS.GetN do begin
    Str(GLS.GetC(i):8:4,s);
    Loesungsvektor_StringGrid.Cells[1,i-1]:=s
  end
end;

procedure TLinequa_Dlg.StoreComplexSystem;(* komplexes GLS: Anzeigegitter => Speicher *)
 var i,j: Integer;
     TempA,TempB,TempC,TempD: Extended;
     TempErrCode: Integer;
begin
  for i:= 1 to GLS.GetN do begin
    Val(rechte_Seite_StringGrid.Cells[1,2*(i-1)], TempC, TempErrCode);    (* rechte Seite, Realteil *)
    GLS.SetC(i,TempC);
    GLS.SetErrCode(TempErrCode);
    Val(rechte_Seite_StringGrid.Cells[1,2*i-1], TempD, TempErrCode);  (* rechte Seite, Imagin�rteil *)
    GLS.SetD(i,TempD);
    GLS.SetErrCode(TempErrCode);
    if Zeiger_RadioButton.Checked then begin
      GLS.TransZ_R(TempC,TempD);
      GLS.SetC(i,TempC);
      GLS.SetD(i,TempD);
    end;
    for j:=1 to GLS.GetN do begin
      Val(Koeffizienten_StringGrid.Cells[j,2*i-1], TempA, TempErrCode);    (* linke Seite, Realteil *)
      GLS.SetA(i,j,TempA);
      GLS.SetErrCode(TempErrCode);
      Val(Koeffizienten_StringGrid.Cells[j,2*i], TempB, TempErrCode);  (* linke Seite, Imagin�rteil *)
      GLS.SetB(i,j,TempB);
      GLS.SetErrCode(TempErrCode);
      if Zeiger_RadioButton.Checked then begin
        GLS.TransZ_R(TempA,TempB);
        GLS.SetA(i,j,TempA);
        GLS.SetB(i,j,TempB);
      end;
    end;
  end;
end;

procedure TLinequa_Dlg.DisplayComplexSystem;  (* komplexes GLS: Speicher => Anzeigegitter *)
 var i,j: Integer;
     s: String;
     x, y: Extended;
begin
  for i:=1 to GLS.GetN do begin
    x:=GLS.GetC(i);
    y:=GLS.GetD(i);
    if Zeiger_RadioButton.Checked then
     GLS.TransR_Z(x,y);
    Str(x:8:4,s);
    rechte_Seite_StringGrid.Cells[1,2*(i-1)]:=s;    (* Realteil *)
    Str(y:8:4,s);
    rechte_Seite_StringGrid.Cells[1,2*i-1]:=s;      (* Imagin�rteil *)
    for j:=1 to GLS.GetN do begin
      x:=GLS.GetA(i,j);
      y:=GLS.GetB(i,j);
      if Zeiger_RadioButton.Checked then
       GLS.TransR_Z(x,y);
      Str(x:8:4,s);
      Koeffizienten_StringGrid.Cells[j,2*i-1]:=s;    (* Realteil *)
      Str(y:8:4,s);
      Koeffizienten_StringGrid.Cells[j,2*i]:=s       (* Imagin�rteil *)
    end;
  end;
end;

procedure TLinequa_Dlg.displayComplexSolution; (* komplexe L�sung: Speicher => Gitter *)
 var i: Integer;
     s: String;
     x, y: Extended;
begin
  for i:= 1 to GLS.GetN do begin
    x:=GLS.GetC(i);
    y:=GLS.GetD(i);
    if Zeiger_RadioButton.Checked then
     GLS.TransR_Z(x,y);        (* Rechteck => Zeiger *)
    Str(x:8:4,s);
    Loesungsvektor_StringGrid.Cells[1,2*(i-1)]:=s;    (* Realteil *)
    Str(y:8:4,s);
    Loesungsvektor_StringGrid.Cells[1,2*i-1]:=s       (* Imagin�rteil *)
  end
end;

(**************************** Ereignis-Steuerung ******************************)

procedure TLinequa_Dlg.FormCreate(Sender: TObject); (* Programmstart *)
begin
  GLS:=TLinequaClass.Create;
  GLS.SetN(2);
  Koeffizienten_StringGrid.ColWidths[0]:=30;
  Loesungsvektor_StringGrid.ColWidths[0]:=30;
  Loesungsvektor_StringGrid.ColWidths[1]:=80;
  rechte_Seite_StringGrid.ColWidths[0]:=30;
  ChangeGrids
end;

procedure TLinequa_Dlg.Koeffizienten_StringGridTopLeftChanged(Sender: TObject);
(* Anpassung des Bildlaufs der beiden rechten Gitter *)
begin
  Loesungsvektor_StringGrid.TopRow:=Koeffizienten_StringGrid.TopRow-1;
  rechte_Seite_StringGrid.TopRow:=Koeffizienten_StringGrid.TopRow-1
end;

procedure TLinequa_Dlg.RadioButtonClick(Sender: TObject);
(* gemeinsame Ereignis-Handler f�r alle RadioButtons *)
begin
  if (Sender=reel_RadioButton) or (Sender=komplex_RadioButton) then begin  (* reell <=> komplex *)
    ClearAll;
    ChangeGrids;
    if Sender = reel_RadioButton then begin
      komplexe_Koordinaten_Label.Visible:=False;
      komplexe_Koordinaten_Panel.Visible:=False;
     end
    else begin
      komplexe_Koordinaten_Label.Visible:=True;
      komplexe_Koordinaten_Panel.Visible:=True;
    end
   end
  else begin  (* Rechteck <=> Zeiger *)
    DisplayComplexSystem;
    DisplayComplexSolution
  end
end;

procedure TLinequa_Dlg.StringGridKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(* bei jeder Tastatureingabe erfolgt komplette Abspeicherung *)
begin
  if reel_RadioButton.Checked then
   StoreRealSystem
  else StoreComplexSystem
end;

procedure TLinequa_Dlg.Berechne_ButtonClick(Sender: TObject); (* Start-Button *)
begin
  GLS.Init; (* Nullsetzen der Matrizen *)
  if reel_RadioButton.Checked then (* Laden der Matrizen *)
   StoreRealSystem
  else
    StoreComplexSystem;
  GLS.Gauss;                       (* L�sen des Gleichungssystems *)
  if reel_RadioButton.Checked then (* Anzeigen der L�sung *)
   DisplayRealSolution
  else begin
    DisplayComplexSolution;
    StoreComplexSystem
  end;
end;

procedure TLinequa_Dlg.Exit_ButtonClick(Sender: TObject); (* Programm beenden *)
begin
  GLS.Free;
  Close;
  Main_Dlg.Visible:=True;
end;

procedure TLinequa_Dlg.Unbekannte_SpinEditChange(Sender: TObject);
begin
  GLS.SetN(Integer(Unbekannte_SpinEdit.value)); (* Festlegen der Unbekannten *)
  ChangeGrids
end;

end.
