unit Polyeder;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Menus, Graphic, ComCtrls,
  (*eigene Units*)
  Camera, Mathe, Point;

type
  TPolyeder_Dlg = class(TForm)
    Image: TImage;
    Manual_Panel: TPanel;
    Rotation_GroupBox: TGroupBox;
    xpl_Button: TButton;
    xmin_Button: TButton;
    ypl_Button: TButton;
    ymin_Button: TButton;
    zpl_Button: TButton;
    zmin_Button: TButton;
    GroupBox_Translation: TGroupBox;
    Left_Button: TButton;
    Right_Button: TButton;
    Up_Button: TButton;
    Down_Button: TButton;
    Input_Panel: TPanel;
    Winkel_GroupBox: TGroupBox;
    Zoom_GroupBox: TGroupBox;
    Alpha_Edit: TEdit;
    Beta_Edit: TEdit;
    Gamma_Edit: TEdit;
    Alpha_Label: TLabel;
    Beta_Label: TLabel;
    Gamma_Label: TLabel;
    Zoomfaktor_Edit: TEdit;
    Zoomfaktor_Label: TLabel;
    ZoomIn_Button: TButton;
    ZoomOut_Button: TButton;
    Exit_Button: TButton;
    Axis_Image: TImage;
    procedure Exit_ButtonClick(Sender: TObject);
    procedure RotationClick(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure TranslationClick(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ZoomClick(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    private
     DrawingObject: TDrawingObject;
     Viewer: TViewer;
  end;

var
  Polyeder_Dlg: TPolyeder_Dlg;

implementation

{$R *.DFM}

procedure TPolyeder_Dlg.Exit_ButtonClick(Sender: TObject);
begin
  Viewer.Done;
  DrawingObject.Done;
  Close;
  Main_Dlg.Visible:=True;
end;

procedure TPolyeder_Dlg.FormCreate(Sender: TObject);
begin
  Viewer:=TViewer.Create;
  Viewer.Init(0,0,0); (*x:1;y:0.1;z:0.1*)
  DrawingObject:=TDrawingObject.Create;
  DrawingObject.Init(0,0,0,clBlue,Viewer);
  (*Ausrichten: (sp�ter automatisieren) *)
  DrawingObject.AddTranslate(0,-0.2,0);
  DrawingObject.AddScale(-8,-8,-8);
  DrawingObject.AddRotate(0,-0.02,0);
  DrawingObject.AddRotate(0,0.0,02);
  DrawingObject.Show(Polyeder_Dlg.Image.Canvas);
end;

procedure TPolyeder_Dlg.RotationClick(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  while Application.Active do begin
     if (DrawingObject<> nil) then begin
        with DrawingObject do begin
          Show(Polyeder_Dlg.Image.Canvas);     (*0.2*)
          if Sender=xpl_Button then AddRotate(StrToFloat(Alpha_Edit.Text),0,0)
          else if Sender=xmin_Button then AddRotate(-StrToFloat(Alpha_Edit.Text),0,0)
          else if Sender=ypl_Button then AddRotate(0,StrToFloat(Beta_Edit.Text),0)
          else if Sender=ymin_Button then AddRotate(0,-StrToFloat(Beta_Edit.Text),0)
          else if Sender=zpl_Button then AddRotate(0,0,StrToFloat(Gamma_Edit.Text))
          else if Sender=zmin_Button then AddRotate(0,0,-StrToFloat(Gamma_Edit.Text));
          Show(Polyeder_Dlg.Image.Canvas);
        end;
        with Polyeder_Dlg.Image.Canvas do begin
          Font.Color:=clRed;
          TextOut(0,0,'Viewer'+'('+FloatToStr(Viewer.GetPos.x)+'/'+
                  FloatToStr(Viewer.GetPos.y)+'/'+FloatToStr(Viewer.GetPos.z)+')');
          TextOut(0,20,'DO'+'('+FloatToStr(DrawingObject.GetPos.x)+'/'+
                  FloatToStr(DrawingObject.GetPos.y)+'/'+FloatToStr(DrawingObject.GetPos.z)+')');
        end;
     end;
     Application.ProcessMessages; (*BUG*)
  end;
end;

procedure TPolyeder_Dlg.TranslationClick(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if DrawingObject<> nil then begin
    with DrawingObject do begin
      Show(Polyeder_Dlg.Image.Canvas);
      if      Sender=Left_Button then AddTranslate(-1,0,0)
      else if Sender=Right_Button then AddTranslate(1,0,0)
      else if Sender=Up_Button then AddTranslate(0,1,0)
      else if Sender=Down_Button then AddTranslate(0,-1,0);
      Show(Polyeder_Dlg.Image.Canvas);
   end;
   with Polyeder_Dlg.Image.Canvas do begin
     Font.Color:=clRed;
     TextOut(0,0,'Viewer'+'('+FloatToStr(Viewer.GetPos.x)+'/'+
             FloatToStr(Viewer.GetPos.y)+'/'+FloatToStr(Viewer.GetPos.z)+')');
     TextOut(0,20,'DO'+'('+FloatToStr(DrawingObject.GetPos.x)+'/'+
             FloatToStr(DrawingObject.GetPos.y)+'/'+FloatToStr(DrawingObject.GetPos.z)+')');
   end;
  end;
end;

procedure TPolyeder_Dlg.ZoomClick(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if DrawingObject<> nil then begin
    with DrawingObject do begin
      Show(Polyeder_Dlg.Image.Canvas);
      if Sender=ZoomOut_Button then AddScale(-StrToFloat(Zoomfaktor_Edit.Text),
                                             -StrToFloat(Zoomfaktor_Edit.Text),
                                             -StrToFloat(Zoomfaktor_Edit.Text))
      else if Sender=ZoomIn_Button then AddScale(StrToFloat(Zoomfaktor_Edit.Text),
                                                 StrToFloat(Zoomfaktor_Edit.Text),
                                                 StrToFloat(Zoomfaktor_Edit.Text));
      Show(Polyeder_Dlg.Image.Canvas);
    end;
    with Polyeder_Dlg.Image.Canvas do begin
      Font.Color:=clRed;
      TextOut(0,0,'Viewer'+'('+FloatToStr(Viewer.GetPos.x)+'/'+
              FloatToStr(Viewer.GetPos.y)+'/'+FloatToStr(Viewer.GetPos.z)+')');
      TextOut(0,20,'DO'+'('+FloatToStr(DrawingObject.GetPos.x)+'/'+
              FloatToStr(DrawingObject.GetPos.y)+'/'+FloatToStr(DrawingObject.GetPos.z)+')');
    end;
  end;
end;

end.


