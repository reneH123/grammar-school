unit sysinfo;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Grids, StdCtrls, Buttons, ExtCtrls;

type
  TSysInfo_Dlg = class(TForm)
    StringGrid1: TStringGrid;
    BitBtn1: TBitBtn;
    Timer1: TTimer;
    procedure FormActivate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  SysInfo_Dlg: TSysInfo_Dlg;

implementation

{$R *.DFM}
uses Mathe;

procedure TSysInfo_Dlg.FormActivate(Sender: TObject);
var p     : PChar;
    zw    :string;
    dc    : hdc;
    memory : TMEMORYSTATUS;
    systeminfo : TSystemInfo;
    dw : dword;
    os : TOSVERSIONINFO;
    UINTDT, TempInt: Integer; (*32Bit-Datentyp f�r Windows-interne-Routine*)
    TempBool: Boolean;
begin

     with stringgrid1 do begin
          colwidths[0]:=clientwidth div 2;
          colwidths[1]:=clientwidth div 2;
          memory.dwLength := sizeof(memory);
          GlobalMemoryStatus(memory);
          cells[0,0]:='phys. Speicher'; cells[1,0]:=inttostr(memory.dwTotalPhys) + ' Bytes';
          cells[0,1]:='phys. Speicher frei'; cells[1,1]:=inttostr(memory.dwAvailPhys) + ' Bytes';
          cells[0,2]:='Auslagerungsspeicher'; cells[1,2]:=inttostr(memory.dwTotalPageFile) + ' Bytes';
          cells[0,3]:='Auslagerungsspeicher frei'; cells[1,3]:=inttostr(memory.dwAvailPageFile) + ' Bytes';
          P:=StrAlloc(MAX_PATH+1);
          UINTDT:=GetWindowsDirectory(P,MAX_PATH+1); (*tempor�r*)
          WriteLn(UINTDT); (*Ausgabe unsichtbar*)
          Cells[0,4]:='Windowsverzeichnis'; cells[1,4]:= p;
          StrDispose(P);
          P:=StrAlloc(MAX_PATH+1);
          UINTDT:=GetSystemDirectory(P,MAX_PATH+1); (*tempor�r*)
          cells[0,5]:='Systemverzeichnis'; cells[1,5]:= p;
          StrDispose(P);
          GetSystemInfo(systeminfo);
          cells[0,6]:='Prozessor(en)'; cells[1,6]:= inttostr(systeminfo.dwNumberOfProcessors);
          case systeminfo.dwProcessorType of
               386  : zw := 'Intel 386';
               486  : zw := 'Intel 486';
               586  : zw := 'Intel Pentium';
               860  : zw := 'Intel 860';
              2000  : zw := 'MIPS R2000';
              3000  : zw := 'MIPS R3000';
              4000  : zw := 'MIPS R4000';
             21064  : zw := 'ALPHA 21064';
               601  : zw := 'PPC 601';
               603  : zw := 'PPC 603';
               604  : zw := 'PPC 604';
               620  : zw := 'PPC 620';
          end;
          cells[0,7]:='Prozessor'; cells[1,7]:= zw;
          cells[0,8]:='Zeit'; cells[1,8]:= timetostr(time);
          cells[0,9]:='Zeit seit Systemstart'; cells[1,9]:=timetostr(getcurrenttime /60000000 );
          cells[0,10]:='Datum';cells[1,10]:=datetostr(date);
          cells[0,11]:='Grafikaufl�sung';cells[1,11]:=inttostr(screen.width)+'x'+inttostr(screen.height);
          dc:=GetDC(0);
          cells[0,12]:='Farben';
          cells[1,12]:=inttostr(1 shl GetDeviceCaps(dc,BITSPIXEL));
          TempInt:=ReleaseDC(0,dc);
          dw := 255;
          P:=StrAlloc(256);
          TempBool:=GetUserName(p,dw); (*tempor�r*)
          WriteLn(TempBool);  (*Ausgabe: nirgends sichtbar*)
          cells[0,13]:='Username';cells[1,13]:=p;
          StrDispose(P);
          P:=StrAlloc(256);
          TempBool:=GetComputerName(p,dw); (*tempor�r*)
          WriteLn(TempBool);
          cells[0,14]:='Computername';cells[1,14]:=p;
          StrDispose(P);
          os.dwOSVersionInfoSize := sizeof(os);
          TempBool:=GetVersionEx(os); (*tempor�r*)
          case os.dwPlatformId of
            VER_PLATFORM_WIN32s	        : zw := 'Win32s unter Windows 3.x';
            VER_PLATFORM_WIN32_WINDOWS	: zw := 'Windows 95';
            VER_PLATFORM_WIN32_NT	: zw := 'Windows NT';
          end;
          cells[0,15]:='Betriebssystem';cells[1,15]:=zw + ' ' + inttostr(os.dwMajorVersion) + '.' + inttostr(os.dwMinorVersion);
          WriteLn(TempBool);  (*Ausgabe: nirgends sichtbar*)
          WriteLn(TempInt);
          WriteLn(UINTDT);
     end;
end;

procedure TSysInfo_Dlg.Timer1Timer(Sender: TObject);
begin
     with stringgrid1 do begin
          cells[1,8]:= timetostr(time);
          cells[1,9]:=inttostr(getcurrenttime)+' ms';
     end;
end;

procedure TSysInfo_Dlg.BitBtn1Click(Sender: TObject);
begin
  Close;
  Main_Dlg.Visible:=True;
end;

end.
