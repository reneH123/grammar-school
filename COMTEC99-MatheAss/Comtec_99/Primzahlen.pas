unit Primzahlen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls;

type
  TPrimzahlen_Dlg = class(TForm)
    Exit_Button: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    ObereGrenze_Edit: TEdit;
    Schreibe_Button: TButton;
    SaveDialog: TSaveDialog;
    NewStatusBar: TStatusBar;
    procedure Exit_ButtonClick(Sender: TObject);
    procedure Schreibe_ButtonClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Primzahlen_Dlg: TPrimzahlen_Dlg;

implementation

uses Mathe,p_Solve;
{$R *.DFM}

procedure TPrimzahlen_Dlg.Exit_ButtonClick(Sender: TObject);
begin
  Close;
  Main_Dlg.Visible:=True;
end;

procedure TPrimzahlen_Dlg.Schreibe_ButtonClick(Sender: TObject);
 type AByte = array[0..0] of Byte;
 var obere_grenze: Integer;
     array_grenze: Integer;
     faktor, max_faktor: Integer;
     schritt, i, j, nr: Integer;
     mask: Byte;
     zahl: ^AByte;
     F: TextFile;
     Code: Integer;
begin
  if (SaveDialog.Execute) then;
  if (SaveDialog.Filename <> '') then begin
    try
       (* obere Grenze f�r Primzahlenbestimmung festlegen *)
       obere_grenze := Trunc(Parsen(ObereGrenze_Edit.Text,Code));
       (* Diese Meldung ist f�r gro�e Speichermengen (z.B. 100 MB) *)
       (* gedacht - durch die Auslagerung auf Platte kann das l�nger dauern *)
       NewStatusBar.Panels[0].Text := 'Belege Speicher ...';
       array_grenze := obere_grenze div 8 + 1;
       GetMem(zahl, array_grenze); (* Speicherplatz bereitstellen *)
       // Alle Elemente auf 0 (= nicht teilbar) initialisieren
       for i := 0 to array_grenze do
          zahl^[i] := 0;
       (* Faktoren m�ssen nur bis zur Quadratwurzel *)
       (* der oberen Grenze gepr�ft werden (Schulmathematik!) *)
       max_faktor := Trunc(Sqrt(obere_grenze));
       NewStatusBar.Panels[0].Text := 'Rechne...';
       (* Begonnen wird mit Primzahl 2 *)
       repeat
         try
           if (Parsen(Edit1.Text,Code) < 2) then faktor:=2  (* 2 *)
           else faktor := Trunc(Parsen(Edit1.Text,Code));
         except
             on EZeroDivide do begin
               ShowMessage('Division durch Null ist verboten!');
             end;
             on EConvertError do begin
               ShowMessage('Konvertierungsfehler!');
             end;
             else begin
               ShowMessage('Fehler!');
             end;
         end;
         Application.ProcessMessages;  (*BUG*)
       until (faktor >= 2) and (Code = 0);

       repeat
         (* begonnen wird mit dem Doppelten jedes Faktors *)
         (*(der Faktor selbst ist Primzahl!)              *)
         schritt := faktor shl 1;
         nr := schritt shr 3; (* nr gibt den Index im Array an *)
         mask := 1 shl (schritt and 7); (* mask zeigt das verwendete Bit *)
         (* Jede durch Faktor teilbare Zahl wird auf True gesetzt *)
         while schritt <= obere_grenze do begin
            (* Nur 1 Bit wird ver�ndert entspr. Maske *)
            zahl^[nr] := zahl^[nr] or mask;
            Inc(schritt, faktor);
            (* Index und Maske berechnen *)
            nr := schritt shr 3;
            mask := 1 shl (schritt and 7);
         end;
         (* n�chster Faktor (n�chste Primzahl) wird gesucht *)
         repeat
           Inc(faktor);
           (* auch hier Index und Maske berechnen *)
           nr := faktor shr 3;
           mask := 1 shl (faktor and 7);
         until (zahl^[nr] and mask) = 0;
       until faktor > max_faktor;

       AssignFile(F, SaveDialog.Filename);
       (* Datei erzeugen *)
       Rewrite(F);
       (* Jedes nicht auf True gesetztes Element    *)
       (* entspricht einer Primzahl und wird in die *)
       NewStatusBar.Panels[0].Text := 'Schreibe...'; (* Datei geschrieben *)
       nr := 0;
       (* Null und Eins sind keine Primzahlen *)
       zahl^[0] := zahl^[0] or 3;
       (* Ausgabe aller Primzahlen *)
       for i := 0 to array_grenze - 2 do begin
          mask := 1;
          (* Alle 8 Bits (Zahlen) pro Byte pr�fen *)
          for j := 0 to 7 do begin
              if (zahl^[i] and mask) = 0 then
                WriteLn(F, IntToStr(nr));
              Inc(nr);
              mask := mask shl 1;
          end;
       end;
       (* Letztes Byte einzeln behandeln, um nur bis obere Grenze auszugeben *)
       mask := 1;
       for j := 0 to obere_grenze and 7 do begin
           if (zahl^[array_grenze - 1] and mask) = 0 then
             WriteLn(F, IntToStr(nr));
           Inc(nr);
           mask := mask shl 1;
       end;
       CloseFile(F);
       FreeMem(zahl);
       NewStatusBar.Panels[0].Text := 'Fertig - schauen Sie in '+SaveDialog.FileName;
    except  (* Fehlerbehandlung *)
      on EConvertError do begin
        ShowMessage('Ung�ltige Eingabe!');
        ObereGrenze_Edit.SetFocus;
      end;
      on EOutOfMemory do
        ShowMessage('Nicht gen�gend Speicher - bitte kleinere Zahl verwenden!');
      on EFCreateError do begin
        ShowMessage('Konnte Datei f�r Primzahlen nicht erzeugen!');
        (* statt try-finally -> Speicher hier freigeben *)
        FreeMem(zahl);
      end;
      on EInOutError do begin
        ShowMessage('Fehler beim Schreiben der Datei!');
        FreeMem(zahl);
      end;
    end;
   end
  else NewStatusBar.Panels[4].Text:='Speichern abgebrochen';
  ShowMessage('Daten wurden in '+SaveDialog.FileName+' geschrieben!');
end;

end.
