unit Graphic;

interface

uses Classes, Graphics, Matrix, Camera, Liste, Point;

const MAX_DREIECKE=12;

type TriangleType= record
                     p1, p2, p3: Integer; (* Indizes in das Array PunktType *)
                   end;

(*   PDrawingObject= ^TDrawingObject; *)
     TDrawingObject= class
      private
        Dreiecke: array[1..MAX_DREIECKE] of TriangleType;
        Size: Word;
        Points: PListe;
        Links,Oben: Integer;
        pos: PunktType;
        matrix: MatrixType;
        A,B,G: Extended;
        ScaleX, ScaleY, ScaleZ: Extended;
        TransX, TransY, TransZ: Extended;
        Color: TColor;
        Viewer: TViewer;
        procedure RenderTriangle(var Punkt1, Punkt2, Punkt3: PunktType; Dreieck: TriangleType);
      public
       constructor Init(NewX, NewY, NewZ: Extended; NewColor: TColor; NewViewer: TViewer);
       destructor Done;
       function GetColor: TColor;
       procedure SetColor(Col: TColor);
       procedure AddRotate(Alpha, Beta, Gamma: Extended);
       procedure AddTranslate(tx,ty,tz: Extended);
       procedure AddScale(sx,sy,sz: Extended);
       procedure Show(Canvas: TCanvas);
       procedure Hide(Canvas: TCanvas);
       procedure AddPoint(Pkt: PunktType);
       function GetPoint(Number: Integer): PunktType;
       function GetPos: PunktType;
     end;

implementation

function dx2(Point: PunktType): Integer;
begin
  dx2:=Round({256}100*Point.X/Point.Z);
end;
function dy2(Point: PunktType): Integer;
begin
  dy2:=Round({256}100*Point.Y/Point.Z);
end;

constructor TDrawingObject.Init(NewX, NewY, NewZ: Extended; NewColor: TColor; NewViewer: TViewer);
 var TmpPkt: PunktType;
begin
  (* NewX, NewY, NewZ:=... *)
  Points:=nil;
  TmpPkt.x:=2; TmpPkt.y:=-2; TmpPkt.z:=-2;
  AddPoint(TmpPkt);
  TmpPkt.x:= 2; TmpPkt.y:= 2; TmpPkt.z:=-2;
  AddPoint(TmpPkt);
  TmpPkt.x:=-2; TmpPkt.y:= 2; TmpPkt.z:=-2;
  AddPoint(TmpPkt);
  TmpPkt.x:=-2; TmpPkt.y:=-2; TmpPkt.z:=-2;
  AddPoint(TmpPkt);
  TmpPkt.x:= 2; TmpPkt.y:=-2; TmpPkt.z:= 2;
  AddPoint(TmpPkt);
  TmpPkt.x:= 2; TmpPkt.y:= 2; TmpPkt.z:= 2;
  AddPoint(TmpPkt);
  TmpPkt.x:=-2; TmpPkt.y:= 2; TmpPkt.z:= 2;
  AddPoint(TmpPkt);
  TmpPkt.x:=-2; TmpPkt.y:=-2; TmpPkt.z:= 2;
  AddPoint(TmpPkt);
  TmpPkt.x:=-0.5; TmpPkt.y:=0; TmpPkt.z:=0;
  AddPoint(TmpPkt);
  (* W�rfel: Zentrum ist Koordinatenursprung *)
  (* Folgendes mu� normalerweise bei anderen Objekten gelesen werden: *)
  (* Punkte der Teildreiecke festlegen *)
  (* Dreiecke laden*)
  (*
  i:=1;
  while Not(ReadyTriangleLoad) do begin
    Read(f,String);
    Dreiecke[i].p1:=StrToInt(  String[Pos('I:')...String[Pos(';'-1)]]   );
    Dreiecke[i].p2:=StrToInt(  String[Pos('II:')...String[Pos(';'-1)]]   );
    Dreiecke[i].p3:=StrToInt(  String[Pos('III:')...String[Pos(');'-2)]]   );
    i:=i+1;
  end; *)
  Dreiecke[ 1].p1:=5; Dreiecke[ 1].p2:=1; Dreiecke[ 1].p3:=2;
  Dreiecke[ 2].p1:=5; Dreiecke[ 2].p2:=2; Dreiecke[ 2].p3:=6;
  Dreiecke[ 3].p1:=6; Dreiecke[ 3].p2:=2; Dreiecke[ 3].p3:=3;
  Dreiecke[ 4].p1:=6; Dreiecke[ 4].p2:=7; Dreiecke[ 4].p3:=3;
  Dreiecke[ 5].p1:=7; Dreiecke[ 5].p2:=3; Dreiecke[ 5].p3:=4;
  Dreiecke[ 6].p1:=7; Dreiecke[ 6].p2:=8; Dreiecke[ 6].p3:=4;
  Dreiecke[ 7].p1:=8; Dreiecke[ 7].p2:=4; Dreiecke[ 7].p3:=1;
  Dreiecke[ 8].p1:=8; Dreiecke[ 8].p2:=5; Dreiecke[ 8].p3:=1;
  Dreiecke[ 9].p1:=1; Dreiecke[ 9].p2:=2; Dreiecke[ 9].p3:=3;
  Dreiecke[10].p1:=1; Dreiecke[10].p2:=4; Dreiecke[10].p3:=3;
  Dreiecke[11].p1:=5; Dreiecke[11].p2:=6; Dreiecke[11].p3:=7;
  Dreiecke[12].p1:=5; Dreiecke[12].p2:=8; Dreiecke[12].p3:=7;
  A:=(*15*)5; B:=(*15*)0; G:=(*15*)0;
  ScaleX:=1; ScaleY:=1; ScaleZ:=1;
  TransX:=0; TransY:=0; TransZ:=0;
  Pos.X:=1; Pos.Y:=1; Pos.Z:=1;
  Size:=0;
  Links:=148;
  Oben:=151;
  Color:=NewColor;
  Viewer:=NewViewer;
end;
destructor TDrawingObject.Done;
begin
end;
function TDrawingObject.GetColor: TColor;
begin
  GetColor:=Color;
end;
procedure TDrawingObject.SetColor(Col: TColor);
begin
  Color:=Col;
end;
(* Rotation wird in den Winkeln vermerkt *)
procedure TDrawingObject.AddRotate(Alpha, Beta, Gamma: Extended);
begin
  A:=A+Alpha; B:=B+Beta; G:=G+Gamma;
end;
(* bewegt das Grafikobjekt um *)
procedure TDrawingObject.AddTranslate(tx,ty,tz: Extended);
begin
  TransX:=TransX+tx; TransY:=TransY+ty; TransZ:=TransZ+tz;
end;
(* skaliert das Grafikobjekt *)
procedure TDrawingObject.AddScale(sx,sy,sz: Extended);
begin
  ScaleX:=ScaleX+sx; ScaleY:=ScaleY+sy; ScaleZ:=ScaleZ+sz;
end;

procedure TDrawingObject.RenderTriangle(var Punkt1, Punkt2, Punkt3: PunktType; Dreieck: TriangleType);
 var MatrixX, MatrixY, MatrixZ: MatrixType;
begin
  NullMatrix(MatrixX); NullMatrix(MatrixY); NullMatrix(MatrixZ);
  MatrixX:=RotateX(A); MatrixY:=RotateY(B); MatrixZ:=RotateZ(G);
  Matrix:=MultiplyMatrix(MultiplyMatrix(MatrixX,MatrixY),MatrixZ);
  Matrix:=MultiplyMatrix(Scale(ScaleX,ScaleY,ScaleZ),Matrix);
(*  Pos.x:=Pos.x+TransX; Pos.y:=Pos.y+TransY; Pos.z:=Pos.z+TransZ; *)
  Punkt1:=GetPoint(Dreieck.p1);
  Punkt2:=GetPoint(Dreieck.p2);
  Punkt3:=GetPoint(Dreieck.p3);
  (* 1. Transformation vom lokalen in Weltkoordinatensystem: *)
  Punkt1 := Multiply(Punkt1,MatrixX);
  Punkt1 := Multiply(Punkt1,MatrixY);
  Punkt1 := Multiply(Punkt1,MatrixZ);
  Punkt2 := Multiply(Punkt2,MatrixX);
  Punkt2 := Multiply(Punkt2,MatrixY);
  Punkt2 := Multiply(Punkt2,MatrixZ);
  Punkt3 := Multiply(Punkt3,MatrixX);
  Punkt3 := Multiply(Punkt3,MatrixY);
  Punkt3 := Multiply(Punkt3,MatrixZ);

  Punkt1 := Multiply(Punkt1,Matrix);
  Punkt2 := Multiply(Punkt2,Matrix);
  Punkt3 := Multiply(Punkt3,Matrix);
  (* Folgendes nach Translate: *)
  Punkt1.X :=Punkt1.X+Pos.X;
  Punkt1.Y :=Punkt1.Y+Pos.Y;
  Punkt1.Z :=Punkt1.Z+Pos.Z;
  Punkt2.X :=Punkt2.X+Pos.X;
  Punkt2.Y :=Punkt2.Y+Pos.Y;
  Punkt2.Z :=Punkt2.Z+Pos.Z;
  Punkt3.X :=Punkt3.X+Pos.X;
  Punkt3.Y :=Punkt3.Y+Pos.Y;
  Punkt3.Z :=Punkt3.Z+Pos.Z;
  (* 2. Transformation vom Welt in Betrachterkoordinatensystem: *)
  Punkt1.X:=Punkt1.X-Viewer.GetPos.X;
  Punkt1.Y:=Punkt1.Y-Viewer.GetPos.Y;
  Punkt1.Z:=Punkt1.Z-Viewer.GetPos.Z;
  Punkt2.X:=Punkt2.X-Viewer.GetPos.X;
  Punkt2.Y:=Punkt2.Y-Viewer.GetPos.Y;
  Punkt2.Z:=Punkt2.Z-Viewer.GetPos.Z;
  Punkt3.X:=Punkt3.X-Viewer.GetPos.X;
  Punkt3.Y:=Punkt3.Y-Viewer.GetPos.Y;
  Punkt3.Z:=Punkt3.Z-Viewer.GetPos.Z;
  (* Welt vor Betrachter drehen: *)
  Punkt1:=Multiply(Punkt1,Viewer.GetMatrix);
  Punkt2:=Multiply(Punkt2,Viewer.GetMatrix);
  Punkt3:=Multiply(Punkt3,Viewer.GetMatrix);
end;

(* Gibt Grafikobjekt auf Zeichenoberfl�che: Canvas aus *)
procedure TDrawingObject.Show(Canvas: TCanvas);
var Punkt1, Punkt2, Punkt3: PunktType;
    i: Integer;
begin
  Canvas.Brush.Color:=clBlack;
  Canvas.FillRect(Rect(0, 0, 349, 357));
  for i:=Low(Dreiecke) to High(Dreiecke) do begin
    RenderTriangle(Punkt1, Punkt2, Punkt3, Dreiecke[i]);
    (* Verbindungen zwischen Punkten zeichnen: *)
    Canvas.Pen.Color:=GetColor;  (*Fehler: kein Zugriff auf GetColor!*)
    Canvas.MoveTo(dx2(Punkt1),dy2(Punkt1));
    Canvas.LineTo(dx2(Punkt2),dy2(Punkt2));
    Canvas.LineTo(dx2(Punkt3),dy2(Punkt3));
    Canvas.Pen.Color:=clGreen;
    Canvas.LineTo(dx2(Punkt1),dy2(Punkt1));
  end;
end;

(* versteckt das Grafikobjekt *)
procedure TDrawingObject.Hide(Canvas: TCanvas);
begin
  SetColor(clBlack); (*eigentliche Hintergrundfarbe...*)
  Show(Canvas);
end;
(* f�gt einen Punkt der Liste zu *)
procedure TDrawingObject.AddPoint(Pkt: PunktType);
 var h: PListe;
begin
  Size:=Size+1;
  New(h);
  h^.Next:=Points;
  h^.Data:=Pkt;
  h^.Nummer:=Size;
  Points:=h;
end;
(* liefert die Koordinaten des "Number".-Punktes der Liste *)
function TDrawingObject.GetPoint(Number: Integer): PunktType;
 var h: PListe;
     Tmp: PunktType;
begin
  h:=Points;
  Tmp.x:=0; Tmp.y:=0; Tmp.z:=0;
  GetPoint:=Tmp;
  while h<> nil do begin
    if h^.Nummer = Number then begin
      GetPoint:=h^.Data;
      EXIT;
     end
    else h:=h^.Next;
  end;
end;
function TDrawingObject.GetPos: PunktType;
begin
  GetPos:=Pos;
end;

(* Initialisierung *)
begin
end.


