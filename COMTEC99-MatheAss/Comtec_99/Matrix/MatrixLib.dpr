library MatrixLib;

uses
  SysUtils,
  Classes,
  Matrix in 'Matrix.pas';

exports Multiply index 1;
exports MultiplyMatrix index 2;
exports RotateX index 3;
exports RotateY index 4;
exports RotateZ index 5;
exports Scale index 6;
exports NullMatrix index 7;

begin
end.


