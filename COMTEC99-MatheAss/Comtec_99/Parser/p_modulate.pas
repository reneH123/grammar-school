unit p_modulate;

interface

 uses p_mathtool;

 const Myf: array[MFZahlT] of MyF_Type =
  (Plus,Minus,Mul,Divi,Pot,Root,Und,Oder,Exoder,Nicht,SQR,SQRT,Ln,Lg,
   Lb,Log,Exp,Sgn,Sinus,Cosinus,Tangens,Sekans,Cosekans,Cotangens,
   ArcusSinus,ArcusCosinus,ArcusTangens,ArcusSekans,ArcusCosekans,
   ArcusCotangens,SinusHyp,CosinusHyp,TangensHyp,SekansHyp,CosekansHyp,
   CotangensHyp,AreaSinusHyp,AreaCosinusHyp,AreaTangensHyp,AreaSekansHyp,
   AreaCosekansHyp,AreaCotangensHyp,Deg2Rad,Rad2Deg,Fak,Hanoi,ggT,kgV);

 function MyPos(Substr: String; NewLine: String): Byte; (* Sucht ein Teilwort *)
 function MyStrE(x: EXTENDED): String; (* Wandelt EXTENDED in STRING *)
 function MyEStr(x: String): EXTENDED; (* Wandelt STRING in EXTENDED *)
 function Modify(Line: String): String; (* Bereitet die Eingabezeile vor *)
 function UpCaseStr(Line: String): String; (* Wandelt alle Zeichen in gro�e um *)
 function GetConstValue(Index: Byte): EXTENDED;
 function GetConstName(Index: Byte): String;
 procedure SetConstValue(Symbol: String; Value: EXTENDED); (* �ndert den Wert der Konstante *)
 procedure SetConstName(Index: Byte; Symbol: String);

 const VarList  = ['X','Y','Z'];
 const MaxConst = 15+3; (*VarList am Ende drangeh�ngt*)
 var Constanten: array [1..MaxConst] of record
                                          Name: String[8];
                                          Wert: EXTENDED;
                                        end;

implementation

function MyPos(Substr: String; NewLine: String): Byte; (* Sucht ein Teilwort *)
 label Spr,Out;
 const Txt = ['a'..'z','A'..'Z','0'..'9','�','�','�','�','�','�','�'];
 var P: Byte;
begin
 Spr:
  P:=Pos(Substr,NewLine);
  if ((P <> 0) and not(NewLine[Pred(P)] in Txt) and not(NewLine[P+Length(Substr)] in Txt)) then begin
    goto Out;     (* Ist ein Wort *)
   end
  else if P <> 0 then begin
    NewLine[P]:=' ';
    goto Spr;
  end;
 Out:
  MyPos:=P;
end;

function MyStrE(x: EXTENDED): String; (* Wandelt EXTENDED in STRING *)
 var S: String;
begin
  Str(x,S);
  MyStrE:=S;
end;

function MyEStr(x: String): EXTENDED; (* Wandelt STRING in EXTENDED *)
 var Ex: Extended;
     Code: Integer;
begin
  Val(x,Ex,Code);
  MyEStr:=Ex;
END;

function Modify(Line: String): String; (* Bereitet die Eingabezeile vor *)
 var C: Word;
     P: BYTE;
     NewLine: String;
begin
  NewLine:=Line;
  for C:=1 to MaxConst do begin
    repeat
      P:=MyPos(Constanten[C].Name,NewLine);
      if (P <> 0) then
        NewLine:=Copy(NewLine,0,Pred(P))+MyStrE(Constanten[C].Wert)+
                 Copy(NewLine,P+Length(Constanten[C].Name),255);
    until P = 0;
  end;
  Modify:=NewLine;
end;

function UpCaseStr(Line: String): String; (* Wandelt alle Zeichen in gro�e um *)
 var i: Integer;
begin
  for i:=1 to Length(Line) do
    Line[i]:=UpCase(Line[i]);  (* Gro�schreiben *)
  UpCaseStr:=Line;
end;

procedure SetConstValue(Symbol: String; Value: EXTENDED); (* �ndert den Wert der Konstante *)
 var i: Integer;
begin
  i:=1;
  while (Constanten[i].Name <> Symbol) and (i <= High(Constanten)) do
    i:=i+1;
  if (Constanten[i].Name = Symbol) then
   Constanten[i].Wert:=Value;
end;

function GetConstValue(Index: Byte): EXTENDED;
begin
  GetConstValue:=Constanten[Index].Wert;
end;

procedure SetConstName(Index: Byte; Symbol: String);
begin
  Constanten[Index].Name:=Symbol;
end;

function GetConstName(Index: Byte): String;
begin
  GetConstName:=Constanten[Index].Name;
end;

(* Initialisierung *)
begin
  Constanten[1].Name:='DEGREE';
  Constanten[1].Wert:=0.01745329251;
  Constanten[2].Name:='CU';
  Constanten[2].Wert:=2.365882365E-4;
  Constanten[3].Name:='GRADE';
  Constanten[3].Wert:=0.01570796326;
  Constanten[4].Name:='LY';
  Constanten[4].Wert:=9.46073E+15;
  Constanten[5].Name:='PC';
  Constanten[5].Wert:=3.085678E+16;
  Constanten[6].Name:='YEAR';
  Constanten[6].Wert:=31556926;
  Constanten[7].Name:='ACRE';
  Constanten[7].Wert:=4046.8564224;
  Constanten[8].Name:='EZ';
  Constanten[8].Wert:=2.718281828459045;
  Constanten[9].Name:='PI';
  Constanten[9].Wert:=3.141592653589793238462643383;
  Constanten[10].Name:='G';
  Constanten[10].Wert:=6.67259E-11;
  Constanten[11].Name:='MILE';
  Constanten[11].Wert:=1609.344;
  Constanten[12].Name:='MPH';
  Constanten[12].Wert:=0.44704;
  Constanten[13].Name:='PS';
  Constanten[13].Wert:=735.49875;
  Constanten[14].Name:='AQ�C';
  Constanten[14].Wert:=273.15;
  Constanten[15].Name:='ROENTGEN';
  Constanten[15].Wert:=2.56E-4;
  (*VarList*)
  Constanten[16].Name:='X';
  Constanten[16].Wert:=0;
  Constanten[17].Name:='Y';
  Constanten[17].Wert:=0;
  Constanten[18].Name:='Z';
  Constanten[18].Wert:=0;
end.
