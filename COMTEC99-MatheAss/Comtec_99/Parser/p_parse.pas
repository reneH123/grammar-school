{$A+,B-,D-,E-,F-,G+,I+,L+,N+,O-,P-,Q-,R-,S+,T-,V-,X+,Y+}
unit p_parse;

interface

 uses p_mathtool;
 type  MyString = String[25];
 const MyLeer = '0';
       VarList  = ['X','Y','Z'];
       VarWert: array ['A'..'Z'] of MyString =
       ('','','','','','','','','','','','','','','','','','','','','','','','','','');

 function Rechne(Funk: FString; sa,sb: MyString; var Error: Byte): MyString;
 function MyCopy(var Aufgabe: String; Start: Byte): String;

implementation

uses p_modulate;

(* Berechnet einen Ausdruck und gibt den Fehlerstatus zur�ck *)
function Rechne(Funk: FString; sa,sb: MyString; var Error: Byte): MyString;
 var Res,a,b: RechType;
     Resultus: MyString;
     FLv: MFZahlT;
     Code: Integer;
begin
  Error:=0; (* Res:=0; *)
  Resultus:='';
  Val(sa,a,Code);
  if (Code <> 0) then begin
    Error:=1;
    EXIT;
  end;
  Val(sb,b,Code);
  if (Code <> 0) then begin
    Error:=2;
    EXIT;
  end;
  for FLv:=1 to MyFunkZahl do
    if (Funk = MyFunk[FLv]) then begin
      Res:=MyF[FLv](a,b);
      Str(Res:0:10,Resultus);
      Rechne:=Resultus;
      EXIT;
    end; (* Funktion nicht unterst�tzt: *)
  Error:=3;
end; (* Rechne *)

(* Kopiert einen umklammerten Ausdruck *)
function MyCopy(var Aufgabe: String; Start: Byte): String;
 var Tiefe, Lv: BYTE;
     HelpStr: String;
begin
  HelpStr:='';
  Tiefe:=1;
  (* Zeichen f�r Zeichen kopieren: *)
  for Lv:=Start to Length(Aufgabe) do begin
    if (Aufgabe[Lv] = '(') then
      (* Noch eine Klammerung:*)
      Inc(Tiefe)
    else if (Aufgabe[Lv] = ')') then begin
      (* Eine Klammer wird geschlossen: *)
      Dec(Tiefe);
      if (Tiefe = 0) then begin
        (* Das war die Schlu�klammer (nicht mitkopieren): *)
        MyCopy:=HelpStr;
        EXIT;
      end;
      (* Klammer mitkopieren ! *)
    end;
    (* N�chstes Zeichen: *)
    HelpStr:=HelpStr+Aufgabe[Lv];
  end;
end; (* MyCopy *)

end.
