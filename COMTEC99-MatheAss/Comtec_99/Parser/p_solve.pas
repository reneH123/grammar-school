unit p_solve;

interface

 uses p_mathtool,p_parse,p_modulate;

 function Parsen(Aufgabe: String; var Code: Integer): RechType;

implementation

function Job(Aufgabe: String; var Lv: Byte): MyString;
label Ok;
var   TmpFunk, Funk: FString;
      TmpX, x: MyString;
      TmpLv, Err: Byte;
begin    (* Job *)
  Err:=0;
  Insert(#0,Aufgabe,length(Aufgabe)+1);
  Aufgabe:=Modify(Aufgabe); (*- speziell*)
  x:='0';
  Funk:='+';
  Lv:=1;
  while (Lv <= Length(Aufgabe)) do begin
    while (Aufgabe[Lv] = ' ') do Inc(Lv);
    if (Aufgabe[Lv] in ZahlStart) then begin      (* Aufgabentyp: Zahl *)
      TmpX:='';
      while ((Aufgabe[Lv] in ZahlStart) or (Aufgabe[Lv] = 'E')) do begin
        if (Aufgabe[Lv] = 'E') then begin
          TmpX:=TmpX+Aufgabe[Lv];
          Inc(Lv)
        end;
        TmpX:=TmpX+Aufgabe[Lv];                 (* x �bergeben *)
        Inc(Lv);
      end;  (* WHILE *)
      x:=Rechne(Funk,x,TmpX,Err);
      end  (* Aufgabentyp: Zahl *)
    else if (Aufgabe[Lv] in VarList) then begin
      TmpX:=VarWert[Aufgabe[Lv]];              (* Aufgabentyp: x *)
      x:=Rechne(Funk,x,TmpX,Err);
      Inc(Lv);
      end     (* Aufgabentyp: x *)
    else if (Aufgabe[Lv] = '(') then begin       (* Aufgabentyp: Klammerung *)
      Inc(Lv);          (* �berspringen *)
      TmpLv:=1;
      x:=Rechne(Funk,x,Job(MyCopy(Aufgabe,Lv),TmpLv),Err);
      Lv:=Lv+TmpLv;
      end     (* Aufgabentyp: Klammerung *)
    else if (Aufgabe[Lv] = #0) then begin        (* Inzwischen Ende erreicht: *)
      Job:=x;
      EXIT;
      end     (* Am Ende der Aufgabe angekommen *)
    else begin
      TmpFunk:='';
      while not((Aufgabe[Lv] in [' ','(',#0]) or (Aufgabe[Lv] in ZahlStart)) do begin
        TmpFunk:=TmpFunk+Aufgabe[Lv];
        Inc(Lv);
        for TmpLv:=1 to FunkVerbZahl do
          if (TmpFunk = FunkVerb[TmpLv]) then begin     (* Aufgabentyp: +,-,... *)
            Funk:=TmpFunk; // +-  -->  -
            goto Ok;
          end;     (* Aufgabentyp: +,-,... *)
      end;    (* WHILE *)
      TmpLv:=1;
      while (TmpLv <= FunkStartZahl) do begin
        if (TmpFunk = FunkStart[TmpLv]) then begin       (* Aufgabentyp: Funk(x) *)
          while (Aufgabe[Lv] = ' ') do Inc(Lv); (* Leerzeichen �berspringen *)
          if (Aufgabe[Lv] = '(') then (* Klammern m�ssen sein ! *)
            Inc(Lv)
          else begin
            WriteLn('Klammer-Fehler !');
            HALT;
          end;    (* IF *)
          TmpLv:=1;
          TmpX:=Rechne(TmpFunk,Job(MyCopy(Aufgabe,Lv),TmpLv),MyLeer,Err);
          Lv:=Lv+TmpLv;     (* �berspringen *)
          x:=Rechne(Funk,x,TmpX,Err);
          goto Ok;
         end;     (* Aufgabentyp: Funk(x) *)
         Inc(TmpLv);
      end;
      WriteLn('Funktion nicht bekannt !');
      HALT;
     Ok:
    end;     (* IF Aufgabe[Lv] *)
  end;     (* WHILE Lv *)
  Job:=x;
end;     (* Job *)

function Parsen(Aufgabe: String; var Code: Integer): RechType;
 var NewResult: Extended;
     MainLv: Byte;
begin
  Aufgabe:=UpCaseStr(Aufgabe);  (* Gro�schreiben *)
  MainLv:=1;
  Val(Job(Aufgabe,MainLv),NewResult, Code); (* Fehlerstatus in Code *)
  Parsen:=NewResult;
end;

(* Initialisierung *)
begin
  Randomize;
end.
