library ParserLib;

uses SysUtils,
  Classes,
  p_modulate in 'p_modulate.pas',
  p_parse in 'p_parse.pas',
  p_mathtool in 'p_mathtool.pas',
  p_solve in 'p_solve.pas';

exports
  (*Test*)
  Parsen index 1,  (*eigentliche Hauptroutine*)
  (*Modulate*)
  MyPos index 2,   (* Sucht ein Teilwort *)
  MyStrE index 3,  (* Wandelt EXTENDED in STRING *)
  MyEStr index 4,  (* Wandelt STRING in EXTENDED *)
  Modify index 5,  (* Bereitet die Eingabezeile vor *)
  UpCaseStr index 6, (* Wandelt alle Zeichen in gro�e um *)

  ggT index 59,
  kgV index 60,
  SetConstValue index 58, (* �ndert den Wert der Konstante *)
  GetConstValue Index 57,
  SetConstName index 56,
  GetConstName index 55,

  (*Parser*)
  Rechne index 7,
  MyCopy index 8,
  (*Mathe:*)
  Plus index 9,
  Minus index 10,
  Mul index 11,
  Divi index 12,
  Pot index 13,
  Root index 14,
  Und index 15,
  Oder index 16,
  ExOder index 17,
  Nicht index 18,
  SQR index 19,
  SQRT index 20,
  Ln index 21,
  Lg index 22,
  Lb index 23,
  Log index 24,
  Exp index 25,
  Sgn index 26,
  Sinus index 27,
  Cosinus index 28,
  Tangens index 29,
  Sekans index 30,
  Cosekans index 31,
  Cotangens index 32,
  ArcusSinus index 33,
  ArcusCosinus index 34,
  ArcusTangens index 35,
  SekansHyp index 36,
  ArcusSekans index 37,
  ArcusCosekans index 38,
  ArcusCotangens index 39,
  SinusHyp index 40,
  CosinusHyp index 41,
  TangensHyp index 42,
  CosekansHyp index 43,
  CotangensHyp index 44,
  AreaSinusHyp index 45,
  AreaCosinusHyp index 46,
  AreaTangensHyp index 47,
  AreaSekansHyp index 48,
  AreaCosekansHyp index 49,
  AreaCotangensHyp index 50,
  Deg2Rad index 51,
  Rad2Deg index 52,
  Fak index 53,
  Hanoi index 54;

(* Initialisierung *)
begin
end.
