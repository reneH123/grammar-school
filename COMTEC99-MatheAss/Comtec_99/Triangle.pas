(*Funktionen: 1. Werte erg�nzen
              2. Werte vergleichen und ggf. berichtigen mit Anzeige, ob ja/nein!*)
(*in Skizze Umkreisradius zeigen + in Hilfe erl�utern*)
(*Beachten: Teilstrecken p und q werden nicht errechnet!*)
unit Triangle;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Menus;

type
  TTriangle_Dlg = class(TForm)
    Grundseiten_GroupBox: TGroupBox;
    a_Label: TLabel;
    a_Edit: TEdit;
    b_Edit: TEdit;
    c_Edit: TEdit;
    b_Label: TLabel;
    c_Label: TLabel;
    Hoehen_GroupBox1: TGroupBox;
    ha_Edit: TEdit;
    hb_Edit: TEdit;
    hc_Edit: TEdit;
    ha_Label: TLabel;
    hb_Label: TLabel;
    hc_Label: TLabel;
    Seitenhalbierende_GroupBox: TGroupBox;
    sc_Label: TLabel;
    sb_Label: TLabel;
    sa_Label: TLabel;
    sa_Edit: TEdit;
    sb_Edit: TEdit;
    sc_Edit: TEdit;
    Winkel_GroupBox: TGroupBox;
    Alpha_Label: TLabel;
    Beta_Label: TLabel;
    Alpha_Edit: TEdit;
    Beta_Edit: TEdit;
    Gamma_Edit: TEdit;
    Gamma_Label: TLabel;
    Zusatz_GroupBox: TGroupBox;
    Umfang_Label: TLabel;
    Flaeche_Label: TLabel;
    Flaeche_Edit: TEdit;
    Umfang_Edit: TEdit;
    Ru_Label: TLabel;
    Ru_Edit: TEdit;
    MainMenu1: TMainMenu;
    Exit_MainMenu: TMenuItem;
    Operationen_MainMenu: TMenuItem;
    Berechne_MenuItem: TMenuItem;
    Check_MenuItem: TMenuItem;
    GroupBox1: TGroupBox;
    p_Edit: TEdit;
    q_Edit: TEdit;
    p_Label: TLabel;
    q_Label: TLabel;
    Image: TImage;
    Bemerkungen_GroupBox: TGroupBox;
    Winkel_Label: TLabel;
    Seiten_Label: TLabel;
    Allgemein_Label: TLabel;
    procedure Akt_Input(Sender: TObject);
    procedure Exit_MainMenuClick(Sender: TObject);
    procedure Berechne_MenuItemClick(Sender: TObject);
  end;

var
  Triangle_Dlg: TTriangle_Dlg;

implementation

{$R *.DFM}

uses Mathe,p_Solve;

type TTriAngleEquation = class(TObject)
                            procedure InitAll(a_,b_,c_,Alpha_,Beta_,Gamma_,
                                              ha_,hb_,hc_,sa_,sb_,sc_,p_,q_,
                                              Flaecheninhalt_,U_,r_: Extended);
                            function Get_a(var Bool: Boolean): Extended;
                            function Get_b(var Bool: Boolean): Extended;
                            function Get_c(var Bool: Boolean): Extended;
                            function Get_Alpha(var Bool: Boolean): Extended;
                            function Get_Beta(var Bool: Boolean): Extended;
                            function Get_Gamma(var Bool: Boolean): Extended;
                            function Get_ha(var Bool: Boolean): Extended;
                            function Get_hb(var Bool: Boolean): Extended;
                            function Get_hc(var Bool: Boolean): Extended;
                            function Get_sa(var Bool: Boolean): Extended;
                            function Get_sb(var Bool: Boolean): Extended;
                            function Get_sc(var Bool: Boolean): Extended;
                            function Get_p(var Bool: Boolean): Extended;
                            function Get_q(var Bool: Boolean): Extended;
                            function Get_Flaecheninhalt(var Bool: Boolean): Extended;
                            function Get_U(var Bool: Boolean): Extended;
                            function Get_r(var Bool: Boolean): Extended;
                          private
                            a,b,c,                    (*Grundseiten*)
                            Alpha,Beta,Gamma,         (*Winkel*)
                            ha,hb,hc,                 (*H�hen*)
                            sa,sb,sc,                 (*Seitenhalbierende*)
                            p,q,                      (*Teilstrecken auf c*)
                            Flaecheninhalt,U,         (*Fl�cheninhalt / Umfang*)
                            r: Extended;              (*Umkreisradius*)
                            (*Gleichungen:*)
                            Eq_a: array[1..15] of String;
                            Eq_b: array[1..14] of String;
                            Eq_c: array[1..14] of String;
                            Eq_Alpha: array[1..11] of String;
                            Eq_Beta: array[1..10] of String;
                            Eq_Gamma: array[1..10] of String;
                            Eq_ha: array[1..5] of String;
                            Eq_hb: array[1..4] of String;
                            Eq_hc: array[1..4] of String;
                            Eq_sa: array[1..1] of String;
                            Eq_sb: array[1..1] of String;
                            Eq_sc: array[1..1] of String;
                            Eq_Flaecheninhalt: array[1..13] of String;
                            Eq_U: array[1..1] of String;
                            Eq_Ru: array[1..4] of String
                            (*gesamt: 108 Gleichungen*)
                        end;

var TriAngleEquation: TTriAngleEquation;

procedure TTriAngleEquation.InitAll(a_,b_,c_,Alpha_,Beta_,Gamma_,ha_,hb_,hc_,
                            sa_,sb_,sc_,p_,q_,Flaecheninhalt_,U_,r_: Extended);
begin
  a:=a_;
  b:=b_;
  c:=c_;
  Alpha:=Alpha_;
  Beta:=Beta_;
  Gamma:=Gamma_;
  ha:=ha_;
  hb:=hb_;
  hc:=hc_;
  sa:=sa_;
  sb:=sb_;
  sc:=sc_;
  p:=p_;
  q:=q_;
  Flaecheninhalt:=Flaecheninhalt_;
  U:=U_;
  r:=r_;
  (*Gleichungen:*)
  Eq_a[1]:='u-b-c';
  Eq_a[2]:='sin(Alpha)*b/sin(Beta)';
  Eq_a[3]:='sin(Alpha)*c/sin(Gamma)';
  Eq_a[4]:='sqrt(sqr(b)+sqr(c)-2*b*c*cos(Alpha))';
  Eq_a[5]:='(A*2)/(b*sin(Gamma))';
  Eq_a[6]:='(A*2)/(c*sin(Beta))';
  Eq_a[7]:='sqrt((2*A*sin(Alpha))/(sin(Beta)*sin(Gamma)))';
  Eq_a[8]:='hb/sin(Gamma)';
  Eq_a[9]:='hc/sin(Beta)';
  Eq_a[10]:='2*r*sin(Alpha)';
  Eq_a[11]:='b*hb/ha';
  Eq_a[12]:='sqrt(2*(sqr(b)+sqr(c))-4*sqr(sa))';
  Eq_a[13]:='A*2/ha';
  Eq_a[14]:='(A*4*r)/(b*c)';
  Eq_a[15]:='sqrt((sqr(A)*4/sqr(c))+sqr(q))';
  Eq_b[1]:='u-a-c';
  Eq_b[2]:='sin(Beta)*a/sin(Alpha)';
  Eq_b[3]:='sin(Beta)*c/sin(Gamma)';
  Eq_b[4]:='sqrt(sqr(a)+sqr(c)-2*a*c*cos(Beta))';
  Eq_b[5]:='(A*2)/(a*sin(Gamma))';
  Eq_b[6]:='(A*2)/(c*sin(Alpha))';
  Eq_b[7]:='sqrt((2*A*sin(Beta))/(sin(Alpha)*sin(Gamma)))';
  Eq_b[8]:='ha/sin(Gamma)';
  Eq_b[9]:='hc/sin(Alpha)';
  Eq_b[10]:='2*r*sin(Beta)';
  Eq_b[11]:='ha*a/hb';
  Eq_b[12]:='sqrt(2*(sqr(a)+sqr(c))-4*sqr(sb))';
  Eq_b[13]:='sqr(A)/hb';
  Eq_b[14]:='(A*4*r)/(a*c)';
  Eq_c[1]:='u-a-b';
  Eq_c[2]:='sin(Gamma)*b/sin(Beta)';
  Eq_c[3]:='sin(Gamma)*a/sin(Alpha)';
  Eq_c[4]:='sqrt(sqr(a)+sqr(b)-2*a*b*cos(Gamma))';
  Eq_c[5]:='(A*2)/(b*sin(Alpha))';
  Eq_c[6]:='(A*2)/(a*sin(Beta))';
  Eq_c[7]:='ha/sin(Beta)';
  Eq_c[8]:='hb/sin(Alpha)';
  Eq_c[9]:='2*r*sin(Gamma)';
  Eq_c[10]:='sqrt(2*(sqr(a)+sqr(b))-4*sqr(sc))';
  Eq_c[11]:='p+q';
  Eq_c[12]:='A*2/ha';
  Eq_c[13]:='(A*4*r)/(a*b)';
  Eq_c[14]:='sqrt((sqr(A)*4)/(sqr(a)-sqr(q)))';
  Eq_Alpha[1]:='180-Beta-Gamma';
  Eq_Alpha[2]:='arcsin(sin(Beta)*a/b)';
  Eq_Alpha[3]:='arcsin(sin(Gamma)*a/c)';
  Eq_Alpha[4]:='arccos((sqr(b)+sqr(c)-sqr(a))/(2*b*c))';
  Eq_Alpha[5]:='arcsin((A*2)/(b*c))';
  Eq_Alpha[6]:='arcsin((sin(Beta)*sin(Gamma)*sqr(a))/(A*2))';
  Eq_Alpha[7]:='arcsin((A*2*sin(Beta))/(sqr(b)*sin(Gamma)))';
  Eq_Alpha[8]:='arcsin((A*2*sin(Gamma))/(sqr(c)*sin(Beta)))';
  Eq_Alpha[9]:='arcsin(hb/c)';
  Eq_Alpha[10]:='arcsin(hc/b)';
  Eq_Alpha[11]:='a/(2*r)';
  Eq_Beta[1]:='arcsin(sin(Alpha)*b/a)';
  Eq_Beta[2]:='arcsin(sin(Gamma)*b/c)';
  Eq_Beta[3]:='arccos((sqr(a)+sqr(c)-sqr(b))/(2*a*c))';
  Eq_Beta[4]:='arcsin((A*2)/(a*c))';
  Eq_Beta[5]:='arcsin((A*2*sin(Alpha))/(sqr(a)*sin(Gamma)))';
  Eq_Beta[6]:='arcsin((sin(Alpha)*sin(Gamma)*sqr(b))/(A*2))';
  Eq_Beta[7]:='arcsin((A*2*sin(Gamma))/(sqr(c)*sin(Alpha)))';
  Eq_Beta[8]:='arcsin(ha/c)';
  Eq_Beta[9]:='arcsin(hc/a)';
  Eq_Beta[10]:='arcsin(b/(2*r))';
  Eq_Gamma[1]:='arcsin(sin(Beta)*c/b)';
  Eq_Gamma[2]:='arcsin(sin(Alpha)*c/a)';
  Eq_Gamma[3]:='arccos((sqr(a)+sqr(b)-sqr(c))/(2*a*b))';
  Eq_Gamma[4]:='arcsin((A*2)/(a*b))';
  Eq_Gamma[5]:='arcsin((A*2*sin(Alpha))/(sqr(a)*sin(Beta)))';
  Eq_Gamma[6]:='arcsin((A*2*sin(Beta))/(sqr(b)*sin(Alpha)))';
  Eq_Gamma[7]:='arcsin((sin(Alpha)*sin(Beta)*c)/(A*2))';
  Eq_Gamma[8]:='arcsin(ha/b)';
  Eq_Gamma[9]:='arcsin(hb/a)';
  Eq_Gamma[10]:='arcsin(c/(2*r))';
  Eq_ha[1]:='b*hb/a';
  Eq_ha[2]:='hb*c/a';
  Eq_ha[3]:='A*2/a';
  Eq_ha[4]:='b*sin(Gamma)';
  Eq_ha[5]:='c*sin(Beta)';
  Eq_hb[1]:='a*ha/b';
  Eq_hb[2]:='A*2/b';
  Eq_hb[3]:='a*sin(Gamma)';
  Eq_hb[4]:='c*sin(Alpha)';
  Eq_hc[1]:='sqrt(sqr(b)-sqr(a))';
  Eq_hc[2]:='A*2/c';
  Eq_hc[3]:='b*sin(Alpha)';
  Eq_hc[4]:='a*sin(Beta)';
  Eq_sa[1]:='0.5*sqrt(2*(sqr(b)+sqr(c))-sqr(a))';
  Eq_sb[1]:='0.5*sqrt(2*(sqr(a)+sqr(c))-sqr(b))';
  Eq_sc[1]:='0.5*sqrt(2*(sqr(a)+sqr(b))-sqr(c))';
  Eq_Flaecheninhalt[1]:='0.5*a*ha';
  Eq_Flaecheninhalt[2]:='0.5*b*hb';
  Eq_Flaecheninhalt[3]:='0.5*c*hc';
  Eq_Flaecheninhalt[4]:='(a*b*c)/(4r)';
  Eq_Flaecheninhalt[5]:='0.5*c*sqrt(sqr(a)-sqr(q))';
  Eq_Flaecheninhalt[6]:='sqrt(((a+b+c)/2)*((a+b+c)/2)-a)*((a+b+c)/2)-b)*((a+b+c)/2)-c))';
  Eq_Flaecheninhalt[7]:='0.5*a*b*sin(Gamma)';
  Eq_Flaecheninhalt[8]:='0.5*b*c*sin(Alpha)';
  Eq_Flaecheninhalt[9]:='0.5*a*c*sin(Beta)';
  Eq_Flaecheninhalt[10]:='2*sqr(r)*sin(Alpha)*sin(Beta)*sin(Gamma)';
  Eq_Flaecheninhalt[11]:='(sqr(a)/2)*((sin(Beta)*sin(Gamma))/sin(Alpha))';
  Eq_Flaecheninhalt[12]:='(sqr(b)/2)*((sin(Alpha)*sin(Gamma))/sin(Beta))';
  Eq_Flaecheninhalt[13]:='(sqr(c)/2)*((sin(Alpha)*sin(Beta))/sin(Gamma))';
  Eq_U[1]:='a+b+c';
  Eq_Ru[1]:='a/(2*sin(Alpha))';
  Eq_Ru[2]:='b/(2*sin(Beta))';
  Eq_Ru[3]:='c/(2*sin(Gamma))';
  Eq_Ru[4]:='(a*b*c)/(A*4)';
end;

function Substitute(s: String): String;
 const MaxVars = 16;
       Vars: array[1..MaxVars] of String =
 ('Alpha','Beta','Gamma','sa','sb','sc','ha','hb','hc','r','p','q','A','a','b','c');
 (*A=a*)
 var Lv: Byte;
begin
  (*Ziel: so weit ersetzen; *)
  for Lv:=1 to Length(s) do begin
(*    if Pos(Vars[Lv],s) > 0 then *)
  end;
end;

function TTriAngleEquation.Get_a(var Bool: Boolean): Extended;
 var i: Integer;
     Temp: String;
     Code: Integer;
     Res: Extended;
begin
  Bool:=False;
  for i:=Low(Eq_a) to High(Eq_a) do begin
  (*Kopie vom gesamten a_-Feld erstellen und danach wieder freigeben*)
     Temp:=Eq_a[i];
     Temp:=Substitute(Temp); (*Ersetzen...*)
     Res:=Parsen(Temp,Code);
     if (Code = 0) then begin (*1.bestes Ergebnis zur�ckliefern und Rechnen einstellen*)
       Bool:=True;
       Get_a:=Res;
       EXIT;
     end;
    (*Auswerten: Ersetzen (Pr�fen ob Wert leer) der Variablen durch Werte*)
  end;
end;
function TTriAngleEquation.Get_b(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_c(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_Alpha(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_Beta(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_Gamma(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_ha(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_hb(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_hc(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_sa(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_sb(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_sc(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_p(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_q(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_Flaecheninhalt(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_U(var Bool: Boolean): Extended;
begin
end;
function TTriAngleEquation.Get_r(var Bool: Boolean): Extended;
begin
end;

procedure TTriangle_Dlg.Akt_Input(Sender: TObject);
begin
  if      Sender=a_Edit       then a_Edit.Font.Color:=      clRed
  else if Sender=b_Edit       then b_Edit.Font.Color:=      clRed
  else if Sender=c_Edit       then c_Edit.Font.Color:=      clRed
  else if Sender=ha_Edit      then ha_Edit.Font.Color:=     clRed
  else if Sender=hb_Edit      then hb_Edit.Font.Color:=     clRed
  else if Sender=hc_Edit      then hc_Edit.Font.Color:=     clRed
  else if Sender=sa_Edit      then sa_Edit.Font.Color:=     clRed
  else if Sender=sb_Edit      then sb_Edit.Font.Color:=     clRed
  else if Sender=sc_Edit      then sc_Edit.Font.Color:=     clRed
  else if Sender=Alpha_Edit   then Alpha_Edit.Font.Color:=  clRed
  else if Sender=Beta_Edit    then Beta_Edit.Font.Color:=   clRed
  else if Sender=Gamma_Edit   then Gamma_Edit.Font.Color:=  clRed
  else if Sender=Flaeche_Edit then Flaeche_Edit.Font.Color:=clRed
  else if Sender=Umfang_Edit  then Umfang_Edit.Font.Color:= clRed
  else if Sender=Ru_Edit      then Ru_Edit.Font.Color:=     clRed
end;

procedure TTriangle_Dlg.Exit_MainMenuClick(Sender: TObject);
begin
  Close;
  Main_Dlg.Visible:=True;
end;

procedure TTriangle_Dlg.Berechne_MenuItemClick(Sender: TObject);
begin
  TriAngleEquation:=TTriAngleEquation.Create;
  TriAngleEquation.InitAll(StrToFloat(a_Edit.Text),StrToFloat(b_Edit.Text),
    StrToFloat(c_Edit.Text),StrToFloat(Alpha_Edit.Text),StrToFloat(Beta_Edit.Text),
    StrToFloat(Gamma_Edit.Text),StrToFloat(ha_Edit.Text),StrToFloat(hb_Edit.Text),
    StrToFloat(hc_Edit.Text),StrToFloat(sa_Edit.Text),StrToFloat(sb_Edit.Text),
    StrToFloat(sc_Edit.Text),StrToFloat(p_Edit.Text),StrToFloat(q_Edit.Text),
    StrToFloat(Flaeche_Edit.Text),StrToFloat(Umfang_Edit.Text),
    StrToFloat(Ru_Edit.Text));

(* Farbe der errechneten Wert ist schwarz *)
(* Farbe der eingegebenen Werte ist rot   *)
  TriAngleEquation.Free;
end;

end.
