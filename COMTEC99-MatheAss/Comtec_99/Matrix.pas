unit Matrix;

interface

uses Graphics, Classes, Point;

(* Matrix (3x3): 2-dimensionales Feld *)
type MatrixType= array[1..3{4},1..3{4}] of Extended;

function Multiply(Point: PunktType; Mat: MatrixType): PunktType;
function MultiplyMatrix(Mat1, Mat2: MatrixType): MatrixType;
function RotateX(Alpha: Extended): MatrixType;
function RotateY(Beta: Extended): MatrixType;
function RotateZ(Gamma: Extended): MatrixType;
function Scale(sx,sy,sz: Extended): MatrixType;
procedure NullMatrix(var Mat: MatrixType);

//function Multiply(Point: PunktType; Mat: MatrixType): PunktType; external 'MatrixLib.dll' index 1;
//function MultiplyMatrix(Mat1, Mat2: MatrixType): MatrixType; external 'MatrixLib.dll' index 2;
//function RotateX(Alpha: Extended): MatrixType;  external 'MatrixLib.dll' index 3;
//function RotateY(Beta: Extended): MatrixType;   external 'MatrixLib.dll' index 4;
//function RotateZ(Gamma: Extended): MatrixType;  external 'MatrixLib.dll' index 5;
//function Scale(sx,sy,sz: Extended): MatrixType; external 'MatrixLib.dll' index 6;
//procedure NullMatrix(var Mat: MatrixType); external 'MatrixLib.dll' index 7;


implementation

(* multipliziert Punkt mit Matrix = Punkt *)
function Multiply(Point: PunktType; Mat: MatrixType): PunktType;
 var Tmp: PunktType;
begin
  with Point do begin
    Tmp.x:=(x*Mat[1][1])+(y*Mat[2][1])+(z*Mat[3][1]){+(1*Mat[4][1])};
    Tmp.y:=(x*Mat[1][2])+(y*Mat[2][2])+(z*Mat[3][2]){+(1*Mat[4][2])};
    Tmp.z:=(x*Mat[1][3])+(y*Mat[2][3])+(z*Mat[3][3]){+(1*Mat[4][3])};
  end;
  Multiply:=Tmp;
end;

(* nur zur Multiplikation von 3{4}x{4}3 Matrizen g�ltig ! *)
function MultiplyMatrix(Mat1, Mat2: MatrixType): MatrixType;
 var M: MatrixType;
     i, j : Integer;
begin
  for i:=1 to 3{4} do
    for j:=1 to 3{4} do
      M[i,j]:= (Mat1[i,1] * Mat2[1,j])
             + (Mat1[i,2] * Mat2[2,j])
             + (Mat1[i,3] * Mat2[3,j])
{             + (Mat1[i,4] * Mat2[4,j])};
  MultiplyMatrix:=M;
end;

(* neuer Punkt = alter Punkt*Rotationsmatrix *)
function RotateX(Alpha: Extended): MatrixType;
 var M: MatrixType;
begin
  M[1][1]:=0; M[1][2]:=0; M[1][3]:=0;{  M[1][4]:=0;}
  M[2][1]:=0; M[2][2]:=0; M[2][3]:=0;{  M[2][4]:=0;}
  M[3][1]:=0; M[3][2]:=0; M[3][3]:=0;{  M[3][4]:=0;}
{  M[4][1]:=0; M[4][2]:=0;  M[4][3]:=0;  M[4][4]:=0;}
  if Alpha <> 0 then begin (*x-Achse: *)
    M[1][1]:=1; M[1][2]:=0;          M[1][3]:=0;{    M[1][4]:=0;}
    M[2][1]:=0; M[2][2]:=Cos(Alpha); M[2][3]:=-(Sin(Alpha));{    M[2][4]:=0;}
    M[3][1]:=0; M[3][2]:=Sin(Alpha); M[3][3]:=Cos(Alpha);{   M[3][4]:=0;
    M[4][1]:=0; M[4][2]:=Sin(Alpha); M[4][3]:=Cos(Alpha); M[4][4]:=1;}
  end;
  RotateX:=M;
end;
function RotateY(Beta: Extended): MatrixType;
 var M: MatrixType;
begin
  M[1][1]:=0; M[1][2]:=0; M[1][3]:=0;{  M[1][4]:=0;}
  M[2][1]:=0; M[2][2]:=0; M[2][3]:=0;{  M[2][4]:=0;}
  M[3][1]:=0; M[3][2]:=0; M[3][3]:=0;{  M[3][4]:=0;
  M[4][1]:=0; M[4][2]:=0; M[4][3]:=0; M[4][4]:=0;}
  if Beta <> 0 then begin (*y-Achse: *)
    M[1][1]:=Cos(Beta);    M[1][2]:=0; M[1][3]:=Sin(Beta);{   M[1][4]:=0;}
    M[2][1]:=0;            M[2][2]:=1; M[2][3]:=0;        {   M[2][4]:=0;}
    M[3][1]:=-(Sin(Beta)); M[3][2]:=0; M[3][3]:=Cos(Beta);{   M[3][4]:=0;
    M[4][1]:=0; M[4][2]:=0; M[4][3]:=0; M[4][4]:=1;}
  end;
  RotateY:=M;
end;
function RotateZ(Gamma: Extended): MatrixType;
 var M: MatrixType;
begin
  M[1][1]:=0; M[1][2]:=0; M[1][3]:=0;{  M[1][4]:=0;}
  M[2][1]:=0; M[2][2]:=0; M[2][3]:=0;{  M[2][4]:=0;}
  M[3][1]:=0; M[3][2]:=0; M[3][3]:=0;{  M[3][4]:=0;
  M[4][1]:=0; M[4][2]:=0; M[4][3]:=0; M[4][4]:=0;}
  if Gamma <> 0 then begin (*z-Achse: *)
    M[1][1]:=Cos(Gamma); M[1][2]:=-(Sin(Gamma)); M[1][3]:=0;{    M[1][4]:=0;}
    M[2][1]:=Sin(Gamma); M[2][2]:=Cos(Gamma);    M[2][3]:=0;{    M[2][4]:=0;}
    M[3][1]:=0;          M[3][2]:=0;             M[3][3]:=1;{    M[3][4]:=0;
    M[4][1]:=0; M[4][2]:=0; M[4][3]:=0; M[4][4]:=1;}
  end;
  RotateZ:=M;
end;

(* neuer Punkt = alter Punkt * Skalierungsmatrix *)
function Scale(sx,sy,sz: Extended): MatrixType;
 var M: MatrixType;
begin
  M[1][1]:=sx; M[1][2]:=0;   M[1][3]:=0;{    M[1][4]:=0;}
  M[2][1]:=0;  M[2][2]:=sy;  M[2][3]:=0;{    M[2][4]:=0;}
  M[3][1]:=0;  M[3][2]:=0;   M[3][3]:=sz;{    M[3][4]:=0;
  M[4][1]:=0;  M[4][2]:=0;  M[4][3]:=0;  M[4][4]:=1;}
  if (sx<>0) and (sy<>0) and (sz<>0) then begin
    M[1][1]:=1/sx; M[1][2]:=0;    M[1][3]:=0;{  M[1][4]:=0;}
    M[2][1]:=0;    M[2][2]:=1/sy; M[2][3]:=0;{  M[2][4]:=0;}
    M[3][1]:=0;    M[3][2]:=0;    M[3][3]:=1/sz;{  M[3][4]:=0;
    M[4][1]:=0; M[4][2]:=0; M[4][3]:=0; M[4][4]:=1;}
  end;
  Scale:=M;
end;

procedure NullMatrix(var Mat: MatrixType);
 var i,j: Integer;
begin
  for i:=1 to 3 do
    for j:=1 to 3 do
      Mat[i][j]:=0;
end;


(* Initialisierung *)
begin
end.
