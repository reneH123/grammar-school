unit Camera;

interface

uses Matrix, Point;

type TViewer= class
       private
         Pos: PunktType;
         Matrix: MatrixType;
       public
         constructor Init(x_,y_,z_: Extended);
         destructor Done;
         function GetPos: PunktType;
         function GetMatrix: MatrixType;
       end;

implementation

constructor TViewer.Init(x_,y_,z_: Extended);
begin
  Pos.x:=x_; Pos.y:=y_; Pos.z:=z_;
  Matrix[1][1]:=1; Matrix[1][2]:=0; Matrix[1][3]:=0;{ Matrix[1][4]:=0;}
  Matrix[2][1]:=0; Matrix[2][2]:=1; Matrix[2][3]:=0;{ Matrix[2][4]:=0;}
  Matrix[3][1]:=0; Matrix[3][2]:=0; Matrix[3][3]:=1;{ Matrix[3][4]:=0;}
{  Matrix[4][1]:=0; Matrix[4][2]:=0; Matrix[4][3]:=1; Matrix[4][4]:=1;}
end;
destructor TViewer.Done;
begin
end;
function TViewer.GetPos: PunktType;
begin
  GetPos:=Pos;
end;
function TViewer.GetMatrix: MatrixType;
begin
  GetMatrix:=Matrix;
end;

(* Initialisierung *)
begin
end.

