unit Funktionsplotter;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Menus, ComCtrls, Buttons;

type
  TFunktionsplotter_Dlg = class(TForm)
    Output_Panel: TPanel;
    Main_Image: TImage;
    MainMenu1: TMainMenu;
    Zurueck_Menu: TMenuItem;
    Eingabe_Edit: TEdit;
    Zeichne_Button: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    MinZ_Label: TLabel;
    MaxZ_Label: TLabel;
    ZAnz_Label: TLabel;
    EditMinX: TEdit;
    EditMaxX: TEdit;
    EditMinY: TEdit;
    EditMaxY: TEdit;
    EditXAnz: TEdit;
    EditYAnz: TEdit;
    EditMinZ: TEdit;
    EditMaxZ: TEdit;
    EditZAnz: TEdit;
    CheckBox1: TCheckBox;
    Clear_Button: TButton;
    CheckLoesche: TCheckBox;
    ProgressBar: TProgressBar;
    procedure Beenden_MenuClick(Sender: TObject);
    procedure Clear_ButtonClick(Sender: TObject);
    procedure Zeichne_ButtonClick(Sender: TObject);
    procedure Zeichne_2D_ButtonClick(Sender: TObject);
    procedure Zeichne_3D_ButtonClick(Sender: TObject);
    procedure CheckLoescheClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Funktionsplotter_Dlg: TFunktionsplotter_Dlg;

implementation

uses Mathe,p_modulate,p_Solve;

(* Globale Constanten und (vordefinierte) Variablen: *)
const X_0 = 30;
      Y_0 = 30;
      GlobXAnz: WORD = 300;
      GlobYAnz: WORD = 300;
      GlobZAnz: WORD = 300;
      GlobMinX: EXTENDED = -10.0;
      GlobMaxX: EXTENDED = 10.0;
      GlobMinY: EXTENDED = -10.0;
      GlobMaxY: EXTENDED = 10.0;
      GlobMinZ: EXTENDED = -10.0;
      GlobMaxZ: EXTENDED = 10.0;
      GlobXStep: EXTENDED = (10.0-(-10.0))/300;
      GlobYStep: EXTENDED = (10.0-(-10.0))/300;
      GlobZStep: EXTENDED = (10.0-(-10.0))/300;

var FirstTime: Boolean;

{$R *.DFM}

procedure TFunktionsplotter_Dlg.Beenden_MenuClick(Sender: TObject);
begin
  Close;
  Main_Dlg.Visible:=True;
end;

procedure MoveTo3D(X,Y,Z: WORD); (* Umrechnung von 3D nach 2D *)
begin
  Funktionsplotter_Dlg.Canvas.MoveTo(X_0+X+(Y div 2),Y_0+Z+(Y div 2));
end;

procedure LineTo3D(X,Y,Z: Word);
 var ColorSave: TColor;
     XMitte, YMitte, ZMitte: Word;
     a: EXTENDED;
begin
  ColorSave:=Funktionsplotter_Dlg.Canvas.Pen.Color;
  a:=255.0/(GlobMaxX-GlobMinX);
  XMitte:=Round(a*(X*GlobXStep+GlobMinX)-a*GlobMinX);
  a:=255.0/(GlobMaxY-GlobMinY);
  YMitte:=Round(a*(Y*GlobYStep+GlobMinY)-a*GlobMinY);
  a:=255.0/(GlobMaxZ-GlobMinZ);
  ZMitte:=Round(a*(Z*GlobZStep+GlobMinZ)-a*GlobMinZ);
  Funktionsplotter_Dlg.Canvas.Pen.Color:=$02000000+(XMitte shl 16)+(ZMitte shl 8)+YMitte;
  Funktionsplotter_Dlg.Canvas.LineTo(X_0+X+(Y div 2),Y_0+Z+(Y div 2));
  Funktionsplotter_Dlg.Canvas.Pen.Color:=ColorSave;
end;

procedure TFunktionsplotter_Dlg.Clear_ButtonClick(Sender: TObject);
begin
  Funktionsplotter_Dlg.Canvas.FillRect(Funktionsplotter_Dlg.Canvas.ClipRect);
end;

procedure TFunktionsplotter_Dlg.Zeichne_3D_ButtonClick(Sender: TObject);
 var MinX,MaxX,MinY,MaxY,MinZ,MaxZ,StepX,StepY,Tmp,MyResult,MinZA,MaxZA: EXTENDED;
     LvX,LvY,LvZ,XAnz,YAnz,ZAnz: Word;
     MinZAuto, MaxZAuto,Weiter,NoError: Boolean;
     Aufgabe: String;
     Code: Integer;
BEGIN
  (*Vorbereitung:*)
  Weiter:=True;
  Aufgabe:=Eingabe_Edit.Text;
  Aufgabe:=UpCaseStr(Aufgabe);
  (*Eingabe:*)
  XAnz:=0;
  YAnz:=0;
  ZAnz:=0;
  MinX:=0;
  MaxX:=0;
  MinY:=0;
  MaxY:=0;
  MinZ:=0;
  MaxZ:=0;
  MinZA:= 1.1E4932(*MaxRechType*);
  MaxZA:=-1.1E4932(*MinRechType*);
(* MinZAuto:=FALSE;*)
(* MaxZAuto:=FALSE;*)
  (*Eingabe:*)
  try
    XAnz:=StrToInt(EditXAnz.Text); (*XAnz: Word*)
    YAnz:=StrToInt(EditYAnz.Text); (*YAnz: Word*)
    ZAnz:=StrToInt(EditZAnz.Text); (*ZAnz: Word*)
    MinX:=StrToFloat(EditMinX.Text);     (*MinX*)
    MaxX:=StrToFloat(EditMaxX.Text);     (*MaxX*)
    MinY:=StrToFloat(EditMinY.Text);     (*MinY*)
    MaxY:=StrToFloat(EditMaxY.Text);     (*MaxY*)
  except
    on EZeroDivide do begin
      Weiter:=FALSE;
      ShowMessage('Division durch Null ist verboten!');
    end;
    on EOverflow do begin
      Weiter:=False;
      ShowMessage('�berlauf des Gleitkommaregisters!');
    end;
    on EMathError do begin
      Weiter:=False;
      ShowMessage('Fehler bei Berechnungen mit Gleitkommazahlen!');
    end;
    on EConvertError do begin
      Weiter:=False;
      ShowMessage('Konvertierungsfehler!');
    end;
    else begin
      Weiter:=False;
      ShowMessage('Fehler!');
    end;
  end;
  if (XAnz <= 0) or (YAnz <= 0) or (ZAnz <= 0) then begin
    Weiter:=False;
    if (XAnz <= 0) then
      ShowMessage('XAnz ist kleiner 1 !')
    else if (YAnz <= 0) then
      ShowMessage('YAnz ist kleiner 1 !')
    else
      ShowMessage('ZAnz ist kleiner 1 !');
  end;
  try
    MinZ:=StrToFloat(EditMinZ.Text);     (*MinZ*)
    MinZAuto:=False;
  except
    MinZAuto:=True
  end;
  try
    MaxZ:=StrToFloat(EditMaxZ.Text);     (*MaxZ*)
    MaxZAuto:=False;
  except
    MaxZAuto:=True
  end;
  (*Fehlervorbeugung:*)
  if (MaxX = MinX) then
   Weiter:=False;
  if (MaxX < MinX) then begin
    Tmp:=MaxX;
    MaxX:=MinX;
    MinX:=Tmp;
  end;
  StepX:=(MaxX-MinX)/XAnz;
  if (MaxY = MinY) then
   Weiter:=False;
  if (MaxY < MinY) then begin
    Tmp:=MaxY;
    MaxY:=MinY;
    MinY:=Tmp;
  end;
  StepY:=(MaxY-MinY)/YAnz;
  if (MaxZ = MinZ) then
   Weiter:=False;
  if (MaxZ < MinZ) then begin
    Tmp:=MaxZ;
    MaxZ:=MinZ;
    MinZ:=Tmp;
  end;
  (*Automatisierung:*)
  if (Weiter and (MinZAuto OR MaxZAuto)) then begin
    LvY:=0;
    while (MinY+LvY*StepY <= MaxY) do begin
      SetConstValue('Y',MinY+LvY*StepY);
      LvX:=0;
      while (MinX+LvX*StepX <= MaxX) do begin
        SetConstValue('X',MinX+LvX*StepX);
        MyResult:=Parsen(Aufgabe,Code);
        if (Code = 0) then begin
          if (MyResult < MinZA) then
           MinZA:=MyResult;
          if (MyResult > MaxZA) then
           MaxZA:=MyResult;
        end;
        Inc(LvX);
      end;
      Inc(LvY);
    end;
  end;
  if MinZAuto then
    MinZ:=MinZA;
  if MaxZAuto then
    MaxZ:=MaxZA;
  EditMinX.Text:=Format('%g',[MinX]);
  EditMaxX.Text:=Format('%g',[MaxX]);
  EditMinY.Text:=Format('%g',[MinY]);
  EditMaxY.Text:=Format('%g',[MaxY]);
  EditMinZ.Text:=Format('%g',[MinZ]);
  EditMaxZ.Text:=Format('%g',[MaxZ]);
  if Weiter then begin
    GlobXAnz:=XAnz;
    GlobYAnz:=YAnz;
    GlobZAnz:=ZAnz;
    GlobMinX:=MinX;
    GlobMaxX:=MaxX;
    GlobMinY:=MinY;
    GlobMaxY:=MaxY;
    GlobMinZ:=MinZ;
    GlobMaxZ:=MaxZ;
    GlobXStep:=StepX;
    GlobYStep:=StepY;
    GlobZStep:=(MaxZ-MinZ)/ZAnz;
    (* Berechnung: *)
    if Weiter then begin
      (* Vorbereitung: *)
      if CheckLoesche.Checked then
        Clear_ButtonClick(Funktionsplotter_Dlg);
      ProgressBar.Position:=0;
      LvY:=0;
      while (GlobMinY+LvY*GlobYStep <= GlobMaxY) do begin
        SetConstValue('Y',GlobMinY+LvY*GlobYStep);
        FirstTime:=TRUE;
        LvX:=0;
        while (GlobMinX+LvX*GlobXStep <= GlobMaxX) do begin
          SetConstValue('X',GlobMinX+LvX*GlobXStep);
          MyResult:=-1.1E4932{MyError};
          try
            MyResult:=Parsen(Aufgabe,Code);
          except
            Code := 1;
          end;
          if (Code <> 0) then
            MyResult:=-1.1E4932{MyError};
          if MyResult = -1.1E4932{MyError} then
            NoError:=FALSE
          else
            NoError:=TRUE;
          if (MyResult < GlobMinZ) then begin
            MyResult:=GlobMinZ;
            FirstTime:=TRUE;
          end;
          if (MyResult > GlobMaxZ) then begin
            MyResult:=GlobMaxZ;
            FirstTime:=TRUE;
          end;
          (* Darstellung: *)
          LvZ:=Round((MyResult-GlobMinZ)/GlobZStep);
          if FirstTime then begin
            if NoError then
              FirstTime:=FALSE;
            MoveTo3D(LvX,LvY,LvZ);
            end
          else
            LineTo3D(LvX,LvY,LvZ);
          Inc(LvX);
        end;
        Inc(LvY);
        ProgressBar.Position:=100*LvY div YAnz;
      end;
      Application.ProcessMessages; (*auf Windowsereignisse bereit sein*)
      ProgressBar.Position:=0;
    end;
  end;
end;

procedure TFunktionsplotter_Dlg.Zeichne_2D_ButtonClick(Sender: TObject);
 var MinX,MaxX,MinY,MaxY,StepX,Tmp: EXTENDED;
     LvX,LvY,XAnz,YAnz: Word;
     MinYAuto,MaxYAuto,Weiter,NoError: Boolean;
     MinYA,MaxYA,MyResult: EXTENDED;
     Aufgabe: String;
     Code: Integer;
begin
  (*Vorbereitung:*)
  Weiter:=True;
  Aufgabe:=Eingabe_Edit.Text;
  Aufgabe:=UpCaseStr(Aufgabe);
  (*Eingabe:*)
  XAnz:=0;
  YAnz:=0;
  MinX:=0;
  MaxX:=0;
  MinY:=0;
  MaxY:=0;
  MinYA:=1.1E4932;                (*MaxRechType*)
  MaxYA:=-1.1E4932;               (*MinRechType*)
(*MinYAuto:=FALSE;*)
(*MaxYAuto:=FALSE; *)
  (* Eingabe: *)
  try
    XAnz:=StrToInt(EditXAnz.Text); (*XAnz: Word *)
    YAnz:=StrToInt(EditYAnz.Text); (*YAnz: Word *)
    MinX:=StrToFloat(EditMinX.Text);    (* MinX *)
    MaxX:=StrToFloat(EditMaxX.Text);    (* MaxX *)
  except
    on EZeroDivide do begin
      Weiter:=FALSE;
      ShowMessage('Division durch Null ist verboten!');
    end;
    on EOverflow do begin
      Weiter:=FALSE;
      ShowMessage('�berlauf des Gleitkommaregisters!');
    end;
    on EMathError do begin
      Weiter:=FALSE;
      ShowMessage('Fehler bei Berechnungen mit Gleitkommazahlen!');
    end;
    on EConvertError do begin
      Weiter:=FALSE;
      ShowMessage('Konvertierungsfehler!');
    end;
    else begin
      Weiter:=FALSE;
      ShowMessage('Fehler!');
    end;
  end;
  if (XAnz <= 0) or (YAnz <= 0) then begin
    Weiter:=FALSE;
    if (XAnz <= 0) then
      ShowMessage('XAnz ist kleiner 1 !')
    else
      ShowMessage('YAnz ist kleiner 1 !');
  end;
  try
    MinY:=StrToFloat(EditMinY.Text);     {MinY}
    MinYAuto:=FALSE;
  except
    MinYAuto:=TRUE
  end;
  try
    MaxY:=StrToFloat(EditMaxY.Text);     {MaxY}
    MaxYAuto:=FALSE;
  except
    MaxYAuto:=TRUE
  end;
  (*Fehlervorbeugung:*)
  if (MaxX = MinX) then
   Weiter:=False;
  if (MaxX < MinX) then begin
    Tmp:=MaxX;
    MaxX:=MinX;
    MinX:=Tmp;
  end;
  StepX:=(MaxX-MinX)/XAnz;
  if (MaxY = MinY) then
   Weiter:=FALSE;
  if (MaxY < MinY) then begin
    Tmp:=MaxY;
    MaxY:=MinY;
    MinY:=Tmp;
  end;
  (*Automatisierung:*)
  if (Weiter and (MinYAuto or MaxYAuto)) then begin
    LvX:=0;
    while (MinX+LvX*StepX <= MaxX) do begin
      SetConstValue('X',MinX+LvX*StepX);
      MyResult:=Parsen(Aufgabe,Code);
      if (Code = 0) then begin
        if (MyResult < MinYA) then
         MinYA:=MyResult;
        if (MyResult > MaxYA) then
         MaxYA:=MyResult;
      end;
      Inc(LvX);
    end;
  end;
  if MinYAuto then
    MinY:=MinYA;
  if MaxYAuto then
    MaxY:=MaxYA;
  EditMinX.Text:=Format('%g',[MinX]);
  EditMaxX.Text:=Format('%g',[MaxX]);
  EditMinY.Text:=Format('%g',[MinY]);
  EditMaxY.Text:=Format('%g',[MaxY]);
  if Weiter then begin (*Koordinatensystem zeichnen*)
    GlobXAnz:=XAnz;
    GlobYAnz:=YAnz;
    GlobMinX:=MinX;
    GlobMaxX:=MaxX;
    GlobMinY:=MinY;
    GlobMaxY:=MaxY;
    GlobXStep:=StepX;
    GlobYStep:=(MaxY-MinY)/YAnz;
    (* Berechnung: *)
    if Weiter then begin
      (* Vorbereitung: *)
      if CheckLoesche.Checked then
        Clear_ButtonClick(Funktionsplotter_Dlg);
      ProgressBar.Position:=0;
      FirstTime:=TRUE;
      LvX:=0;
      while (GlobMinX+LvX*GlobXStep <= GlobMaxX) do begin
        SetConstValue('X',GlobMinX+LvX*GlobXStep);
        MyResult:=-1.1E4932;      (*MyError*)
        try
          MyResult:=Parsen(Aufgabe,Code); (*kann Exception verursachen*)
        except
          Code := 1;                      (*Behandlung der Exception*)
        end;
        if (Code <> 0) then
          MyResult:=-1.1E4932;           (*MyError*)
        if (MyResult = -1.1E4932) then   (*MyError*)
          NoError:=False
        else
          NoError:=True;
        if (MyResult < GlobMinY) then begin
          MyResult:=GlobMinY;
          FirstTime:=True;
        end;
        if (MyResult > GlobMaxY) then begin
          MyResult:=GlobMaxY;
          FirstTime:=True;
        end;
        (* Darstellung: *)
        LvY:=Round((MyResult-GlobMinY)/GlobYStep);
        with Funktionsplotter_Dlg.Canvas do
          if FirstTime then begin
            if NoError then
              FirstTime:=False;
            MoveTo(X_0+LvX,Y_0+YAnz-LvY);
            end
          else begin
            LineTo(X_0+LvX,Y_0+YAnz-LvY);
          end;
        Application.ProcessMessages; (*BUG*)
        Inc(LvX);
        ProgressBar.Position:=100*LvX div XAnz;
      end;
      ProgressBar.Position:=0;
    end;
  end;
end;

procedure TFunktionsplotter_Dlg.Zeichne_ButtonClick(Sender: TObject);
begin
  if (CheckBox1.Checked) then
    Zeichne_3D_ButtonClick(Sender)
  else
    Zeichne_2D_ButtonClick(Sender);
end;

procedure TFunktionsplotter_Dlg.CheckLoescheClick(Sender: TObject);
 var Rect1: TRect;
     XAnz, YAnz: Word;
     Code: Integer;
     Weiter: Boolean;
begin
  Weiter:=True;
  Val(EditXAnz.Text,XAnz,Code);
  if (Code <> 0) then
   Weiter:=False;
  Val(EditYAnz.Text,YAnz,Code);
  if (Code <> 0) then
   Weiter:=False;
  if Weiter then begin
    Rect1:=Rect(X_0-1,Y_0-1,X_0+XAnz+1,Y_0+YAnz+1);
    Canvas.FillRect(Rect1);
  end;
end;

procedure TFunktionsplotter_Dlg.CheckBox1Click(Sender: TObject);
begin
  EditMinZ.Enabled := Not(EditMinZ.Enabled);
  EditMaxZ.Enabled := Not(EditMaxZ.Enabled);
  EditZAnz.Enabled := Not(EditZAnz.Enabled);
  MinZ_Label.Enabled := Not(MinZ_Label.Enabled);
  MaxZ_Label.Enabled := Not(MaxZ_Label.Enabled);
  ZAnz_Label.Enabled := Not(ZAnz_Label.Enabled);
end;

end.
