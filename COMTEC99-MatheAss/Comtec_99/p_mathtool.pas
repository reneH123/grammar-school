{$A+,B-,D-,E-,F-,G+,I+,L+,N+,O-,P-,Q-,R-,S+,T-,V-,X+,Y+}
unit p_mathtool;

interface

 const FunkVerbZahl  = 9;
       FunkStartZahl = 39;
       MyFunkZahl = FunkVerbZahl+FunkStartZahl;
       ZahlStart = ['0'..'9','.'];
 type  FString  = String[8];
       RechType = EXTENDED;
       MFZahlT  = 1..MyFunkZahl;
       MyF_Type = function (a,b: RechType): RechType;
 const MyFunk: array[MFZahlT] of FString =
         ('+','-','*','/','^','ROOT','AND','OR','XOR',
          'NOT','SQR','SQRT','LN','LG','LB','LOG','EXP','SGN',
          'SIN','COS','TAN','SEK','COSEK','COT',
          'ARCSIN','ARCCOS','ARCTAN','ARCSEK','ARCCOSEK','ARCCOT',
          'SINH','COSH','TANH','SEKH','COSEKH','COTH',
          'ARSINH','ARCOSH','ARTANH','ARSEKH','ARCOSEKH','ARCOTH',
          'DEG2RAD','RAD2DEG','FAK','HANOI','GGT','KGV');
        FunkVerb:  array[1..FunkVerbZahl] of FString  =
         ('+','-','*','/','^','ROOT','AND','OR','XOR');
        FunkStart: array[1..FunkStartZahl] of FString =
        ('NOT','SQR','SQRT','LN','LG','LB','LOG','EXP','SGN',
         'SIN','COS','TAN','SEK','COSEK','COT',
         'ARCSIN','ARCCOS','ARCTAN','ARCSEK','ARCCOSEK','ARCCOT',
         'SINH','COSH','TANH','SEKH','COSEKH','COTH',
         'ARSINH','ARCOSH','ARTANH','ARSEKH','ARCOSEKH','ARCOTH',
         'DEGRAD','RADDEG','FAK','HANOI','GGT','KGV');

 function Plus(a,b: RechType): RechType; far;
 function Minus(a,b: RechType): RechType; far;
 function Mul(a,b: RechType): RechType; far;
 function Divi(a,b: RechType): RechType; far;
 function Pot(a,b: RechType): RechType; far;
 function Root(a,b: RechType): RechType; far;
 function Und(a,b: RechType): RechType; far;
 function Oder(a,b: RechType): RechType; far;
 function ExOder(a,b: RechType): RechType; far;
 function Nicht(a,b: RechType): RechType; far;
 function SQR(a,b: RechType): RechType; far;
 function SQRT(a,b: RechType): RechType; far;
 function Ln(a,b: RechType): RechType; far;
 function Lg(a,b: RechType): RechType; far;
 function Lb(a,b: RechType): RechType; far;
 function Log(a,b: RechType): RechType; far;
 function Exp(a,b: RechType): RechType; far;
 function Sgn(a,b: RechType): RechType; far;
 function Sinus(a,b: RechType): RechType; far;
 function Cosinus(a,b: RechType): RechType; far;
 function Tangens(a,b: RechType): RechType; far;
 function Sekans(a,b: RechType): RechType; far;
 function Cosekans(a,b: RechType): RechType; far;
 function Cotangens(a,b: RechType): RechType; far;
 function ArcusSinus(a,b: RechType): RechType; far;
 function ArcusCosinus(a,b: RechType): RechType; far;
 function ArcusTangens(a,b: RechType): RechType; far;
 function SekansHyp(a,b: RechType): RechType; far;
 function ArcusSekans(a,b: RechType): RechType; far;
 function ArcusCosekans(a,b: RechType): RechType; far;
 function ArcusCotangens(a,b: RechType): RechType; far;
 function SinusHyp(a,b: RechType): RechType; far;
 function CosinusHyp(a,b: RechType): RechType; far;
 function TangensHyp(a,b: RechType): RechType; far;
 function CosekansHyp(a,b: RechType): RechType; far;
 function CotangensHyp(a,b: RechType): RechType; far;
 function AreaSinusHyp(a,b: RechType): RechType; far;
 function AreaCosinusHyp(a,b: RechType): RechType; far;
 function AreaTangensHyp(a,b: RechType): RechType; far;
 function AreaSekansHyp(a,b: RechType): RechType; far;
 function AreaCosekansHyp(a,b: RechType): RechType; far;
 function AreaCotangensHyp(a,b: RechType): RechType; far;
 function Deg2Rad(a,b: RechType):RechType; far;
 function Rad2Deg(a,b: RechType): RechType;  far;
 function Fak(a,b: RechType): RechType; far;
 function Hanoi(a,b: RechType): RechType;  far;
 function ggT(a,b: RechType): RechType; far;
 function kgV(a,b: RechType): RechType; far;

implementation

function MySgn(x: EXTENDED): EXTENDED;
   begin
     if x > 0 then
       MySgn:=1
     else if x < 0 then
       MySgn:=-1
     else
       MySgn:=0;
   end;
   function Round(x: EXTENDED): RechType;
   begin
     Round:=(x);
   end;
   function Plus(a,b: RechType): RechType; far;
   begin
     Plus:=a + b;
   end;
   function Minus(a,b: RechType): RechType; far;
   begin
     Minus:=a - b;
   end;
   function Mul(a,b: RechType): RechType; far;
   begin
     Mul:=a * b;
   end;
   function Divi(a,b: RechType): RechType; far;
   begin
     Divi:=a / b;
   end;
   function Pot(a,b: RechType): RechType; far;
     var a2, b2: EXTENDED;
   begin
     a2:=a;
     b2:=b;
     Pot:=Round(System.Exp(System.Ln(a2)*b2));
   end;
   function Root(a,b: RechType): RechType; far;
     var a2, b2: EXTENDED;
   begin
     a2:=a;
     b2:=b;
     Root:=Round(System.Exp(System.Ln(b2)/a2));
   end;
   function Und(a,b: RechType): RechType; far;
   begin
     Und:=System.Round(a) and System.Round(b);
   end;
   function Oder(a,b: RechType): RechType; far;
   begin
     Oder:=System.Round(a) or System.Round(b);
   end;
   function ExOder(a,b: RechType): RechType; far;
   begin
     ExOder:=System.Round(a) xor System.Round(b);
   end;
   (* Negiert eine Zahl *)
   function Nicht(a,b: RechType): RechType; far;
   begin
     Nicht:=not(System.Round(a));
   end;
   function SQR(a,b: RechType): RechType; far;
   begin
     SQR:=a * a;
   end;
   function SQRT(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     SQRT:=Round(System.SQRT(a2));
   end;
   (* Berechnet den nat�rlichen Logarithmus einer Zahl *)
   function Ln(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     if (a = 1) then begin
       Ln:=0;
       EXIT;
     end;
     a2:=a;
     Ln:=Round(System.Ln(a2));
   end;
   (* Berechnet den dekadischen Logarithmus einer Zahl **)
   function Lg(a,b: RechType): RechType; far;
     CONST Ln10 = 2.302585092994;
     var   a2: EXTENDED;
   begin
     if (a = 1) then begin
       Lg:=0;
       EXIT;
     end;
     a2:=a;
     Lg:=Round(System.Ln(a2)/Ln10);
   end;
   (* Berechnet den bin�ren Logarithmus einer Zahl *)
   function Lb(a,b: RechType): RechType; far;
     const Ln2 = 0.6931471805599;
     var   a2: EXTENDED;
   begin
     if (a = 1) then begin
       Lb:=0;
       EXIT;
     end;
     a2:=a;
     Lb:=Round(System.Ln(a2)/Ln2);
   end;
   (* Berechnet den Logarithmus einer Zahl zur Basis b *)
   function Log(a,b: RechType): RechType; far;
     var a2, b2: EXTENDED;
   begin
     Log:=0;
     if (a = 1) then begin
       Log:=0;
       EXIT;
     END
     else if (a>0) and (b>0) then begin
     a2:=a;
     b2:=b;
     Log:=Round(System.Ln(a2)/System.Ln(b2));
     end;
   end;
   (* Berechnet den Exp einer Zahl zur Basis e *)
   function Exp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     Exp:=Round(System.Exp(a2));
   end;
   (* �bergibt das Vorzeichen einer Zahl *)
   function Sgn(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     Sgn:=Round(MySgn(a2));
   end;
   function Sinus(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     Sinus:=Sin(a2);
   end;
   function Cosinus(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     Cosinus:=Round(System.Cos(a2));
   end;
   function Tangens(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     Tangens:=Round(System.Sin(a2)/System.Cos(a2));
   end;
   function Sekans(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     Sekans:=Round(1 / System.Cos(a2));
   end;
   function Cosekans(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     Cosekans:=Round(1 / System.Sin(a2));
   end;
   function Cotangens(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     Cotangens:=Round(System.Cos(a2)/System.Sin(a2));
   end;
   function ArcusSinus(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     ArcusSinus:=System.ArcTan(a2 / System.Sqrt(1-a2*a2));
   end;
   function ArcusCosinus(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     ArcusCosinus:=Round((pi/2)- System.ArcTan(a2 / System.Sqrt(1.0-a2*a2)));
   end;
   function ArcusTangens(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     ArcusTangens:=Round(System.ArcTan(a2));
   end;
   function ArcusSekans(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     ArcusSekans:=Round(System.ArcTan(System.Sqrt(a2*a2-1)));
   end;
   function ArcusCosekans(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     ArcusCosekans:=Round(System.ArcTan(1 / System.Sqrt(a2*a2-1)));
   end;
   function ArcusCotangens(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     ArcusCotangens:=Round(1.57096-System.ArcTan(a2));
   end;
   function SinusHyp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     SinusHyp:=Round((System.exp(a2) - System.exp(-a2)) / 2);
   end;
   function CosinusHyp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     CosinusHyp:=Round((System.exp(a2) + System.exp(-a2)) / 2);
   end;
   function TangensHyp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     TangensHyp:=Round((System.exp(a2) - System.exp(-a2)) / (System.exp(a2) + System.exp(-a2)));
   end;
   function SekansHyp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     SekansHyp:=Round(2 / (System.Exp(a2)+System.Exp(-a2)));
   end;
   function CosekansHyp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     CosekansHyp:=Round(2 / (System.Exp(a2)-System.Exp(-a2)));
   end;
   function CotangensHyp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     CotangensHyp:=Round((System.exp(a2) + System.exp(-a2)) / (System.exp(a2) - System.exp(-a2)));
   end;
   (* Berechnet den AreaSinusHyperbolikus einer Zahl *)
   function AreaSinusHyp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     AreaSinusHyp:=Round(System.Ln(a2+System.Sqrt(a2*a2+1)));
   end;
   (* Berechnet den AreaCosinusHyperbolikus einer Zahl *)
   function AreaCosinusHyp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     AreaCosinusHyp:=Round(System.Ln(a2+System.Sqrt(a2*a2-1)));
   end;
   (* Berechnet den AreaTangensHyperbolikus einer Zahl *)
   function AreaTangensHyp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     AreaTangensHyp:=Round(System.Ln((1+a2)/(1-a2)) / 2);
   end;
   (* Berechnet den AreaSekansHyperbolikus einer Zahl *)
   function AreaSekansHyp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     AreaSekansHyp:=Round(System.Ln((System.Sqrt(-a2*a2+1)+1) / a2));
   end;
   (* Berechnet den AreaCosekansHyperbolikus einer Zahl *)
   function AreaCosekansHyp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     AreaCosekansHyp:=Round(System.Ln((MySgn(a2)*System.Sqrt(a2*a2+1)+1) / a2));
   end;
   (* Berechnet den AreaCotangensHyperbolikus einer Zahl *)
   function AreaCotangensHyp(a,b: RechType): RechType; far;
     var a2: EXTENDED;
   begin
     a2:=a;
     AreaCotangensHyp:=Round(System.Ln((a2+1)/(a2-1)) / 2);
   end;
  (* Wandelt Grad in Bogenma� um *)
  function Deg2Rad(a,b: RechType):RechType; far;
    {Umwandlung Grad --> Bogenmass}
  begin
    Deg2rad:=Round(PI*(a/180));
  end;
  (* Wandelt Bogenma� in Grad um *)
  function Rad2Deg(a,b: RechType): RechType;  far;
  begin
    Rad2deg:=Round(a*180)/PI;
  end;
  (* Liefert die Fakult�t von n  *)
  function Fak(a,b: RechType): RechType; far;
  var Tmp: LongInt;
   function Rechne(a: LongInt): LongInt;
   begin
     Rechne:=0;
     if a=0 then Rechne:=1;
     if a>0 then Rechne:=a*Rechne(a-1);
   end;
  begin
    Tmp:=Trunc(a);
    Fak:=Rechne(Tmp);
  end;
  (* Liefert die Anzzahl der Verschiebungen zur�ck, wobei n Steine da sind *)
  function Hanoi(a,b: RechType): RechType;  far;
  type NeuType= 0..2139999999;
  const Tmp: NeuType= 0;
  var a2: Integer;
   procedure Trans(n: Integer; AHS,HS,ZS: CHAR);
   begin
     if (n > 1) then Trans(n-1,AHS,HS,ZS);
     if (n > 1) then Trans(n-1,HS,ZS,AHS);
     TMP:=TMP+1;
   end;
  begin
    a2:=Trunc(a);
    Trans(a2,'a','b','c'); (* wenn ben�tigt sind ''.. Position der Stangen: A B C*)
    Hanoi:=Tmp;
  end;
  function ggT(a,b: RechType): RechType;  far;
   var a2,b2,r: Integer;
  begin
    a2:=Trunc(a);
    b2:=Trunc(b);
    repeat
      r:=a2 mod b2;
      a2:=b2;
      b2:=r;
    until r=0;
    ggT:=a2;
  end;
  function kgV(a,b: RechType): RechType;  far;
  begin
    kgV:=(Trunc(a)*Trunc(b)) div Trunc((ggT(a,b)));
  end;

 const Myf: array[MFZahlT] of MyF_Type =
  (Plus,Minus,Mul,Divi,Pot,Root,Und,Oder,Exoder,Nicht,SQR,SQRT,Ln,Lg,
   Lb,Log,Exp,Sgn,Sinus,Cosinus,Tangens,Sekans,Cosekans,Cotangens,
   ArcusSinus,ArcusCosinus,ArcusTangens,ArcusSekans,ArcusCosekans,
   ArcusCotangens,SinusHyp,CosinusHyp,TangensHyp,SekansHyp,CosekansHyp,
   CotangensHyp,AreaSinusHyp,AreaCosinusHyp,AreaTangensHyp,AreaSekansHyp,
   AreaCosekansHyp,AreaCotangensHyp,Deg2Rad,Rad2Deg,Fak,Hanoi,ggT,kgV);

end.
