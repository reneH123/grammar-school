(define (max a b)
  (cond ((> a b) a)
        (else b)))

(define (fak n)
  (cond ((zero? n) 1)
        (else (* n (fak (- n 1))))))

(define (qs zahl)
  (cond ((zero? zahl) 0)
        (else (+ (qs (quotient zahl 10)) (modulo zahl 10)))))

(define (zwischen? w1 m w2)
  (cond ((and (< w1 m) (> w2 m)) #T)
        (else #F)))

(define (z? w1 m w2)
  (cond ((or (<= w1 m w2) (>= w1 m w2)) #T)
        (else #F)))
