(define Liste '(1 2 3 (2 4) 5 3))

(define (lilaenge l1)
  (cond ((null? l1) 0)
        (else (+ (lilaenge (cdr l1)) 1))))

(define (enthalten el liste)
  (cond ((null? liste) #F)
        ((equal? el (car liste)) #T)
        (else (enthalten el (cdr liste)))))
