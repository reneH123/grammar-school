(define (htrans v n)
  (write "Transportiere von ")
  (write v)
  (write " nach ")
  (write n)
  (newline))
  
(define (hanoi n vturm zturm hturm)
  (cond ((> n 0) (hanoi (- n 1) vturm hturm zturm)
                 (htrans vturm zturm)
                 (hanoi (- n 1) hturm zturm vturm)))) 

(define (h n)
  (hanoi n "turm1" "turm2" "turm3"))
  