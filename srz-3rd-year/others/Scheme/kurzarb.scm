(define (hoch b e)
  (cond ((= e 0) 1)
        (else (* b (hoch b (- e 1))))))

(define wort '(r e n t n e r))

(define (letztes l)
  (cond ((null? (cdr l)) (car l))
        (else (letztes (cdr l)))))

(define (ohneletztes l)
  (cond ((null? (cdr l)) ())
        (else (cons (car l) (ohneletztes (cdr l))))))

(define (palindrom? w)
  (cond ((null? w) #T)
        ((null? (cdr w)) #T)
        ((equal? (car w) (letztes w)) (palindrom? (cdr (ohneletztes w))))
        (else #F)))
