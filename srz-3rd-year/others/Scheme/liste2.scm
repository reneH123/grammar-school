(define Liste '(1 2 3 (2 4) 5 3))

(define (lilaenge l1)
  (cond ((null? l1) 0)
        (else (+ (lilaenge (cdr l1)) 1))))

(define (enthalten el liste)
  (cond ((null? liste) #F)
        ((equal? el (car liste)) #T)
        (else (enthalten el (cdr liste)))))

(define (nummer el liste)
  (cond ((null? liste) 0)
        ((equal? el (car liste)) 1)
        (else (+ (nummer el (cdr liste)) 1))))

(define (umkehr liste)
  (cond ((null? liste) ())
        (else (append (umkehr (cdr liste)) (list (car liste))))))

(define (streich el liste)
  (cond ((null? liste) ())
        ((equal? el (car liste)) (streich el (cdr liste)))
        (else (cons (car liste) (streich el (cdr liste))))))

(define (summe liste)
  (cond ((null? liste) 0)
        (else (+ (car liste) (summe (cdr liste))))))

(define baum '(3 (5 (2) (8)) (4 (1 (7) (3)) 9)))
(define b1 '(+ (- 3 (/ 8 4)) (* 4 (- 3 1))))

(define (summerek liste)
  (cond ((null? liste) 0)
        ((list? (car liste)) (+ (summerek (car liste)) 
                                (summerek (cdr liste))))
        (else (+ (car liste) (summerek (cdr liste))))))

