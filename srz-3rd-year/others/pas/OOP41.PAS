program oop41;

type
   ppunkt = ^tpunkt;
   tpunkt = object
              x,y : integer;
              constructor init(x_neu,y_neu : integer);
              procedure verstecke; virtual;
              procedure zeige; virtual;
              procedure verschiebe(x_ver,y_ver : integer);
            end;
   pkreis = ^tkreis;
   tkreis = object(tpunkt)
              r : integer;
              constructor init(x_neu,y_neu,r_neu : integer);
              procedure verstecke; virtual;
              procedure zeige; virtual;
            end;
    tgraf = object
              constructor init;
              procedure run; virtual;
              procedure pause;
              procedure done;
            end;

constructor tpunkt.init(x_neu,y_neu : integer);

begin
  x:=x_neu;
  y:=y_neu
end;

procedure tpunkt.zeige;

begin
  writeln('Punkt zeigt sich.')
end;

procedure tpunkt.verstecke;

begin
  writeln('Punkt versteckt sich.')
end;

procedure tpunkt.verschiebe(x_ver,y_ver : integer);

begin
  verstecke;
  x:=x+x_ver;
  y:=y+y_ver;
  zeige
end;

constructor tkreis.init(x_neu,y_neu,r_neu : integer);

begin
  tpunkt.init(x_neu,y_neu);
  r:=r_neu
end;

procedure tkreis.zeige;

begin
  writeln('Kreis zeigt sich.')
end;

procedure tkreis.verstecke;

begin
  writeln('Kreis versteckt sich.')
end;

constructor tgraf.init;

begin
  { Hier k�nnte der Grafikmodus initialisiert werden }
end;

procedure tgraf.run;

var z1,z2 : ppunkt;

begin
  z1:=new(ppunkt,init(120,120));
  z1^.zeige;
  pause;
  z1^.verschiebe(20,0);
  pause;
  z2:=new(pkreis,init(50,50,30));
  z2^.zeige;
  pause;
  z2^.verschiebe(0,30);
  pause;
end;

procedure tgraf.pause;

begin
  readln
end;

procedure tgraf.done;

begin
			{ Hier wieder auf Textmodus zur�ckstellen }
end;

var grafedi : tgraf;

begin                                            { Hauptprogramm }
  grafedi.init;
  grafedi.run;
  grafedi.done
end.
