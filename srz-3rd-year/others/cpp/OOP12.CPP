#include <oop12.h>

tpunkt::tpunkt(int x_neu, int y_neu)
{
  x=x_neu;
  y=y_neu;
}

int tpunkt::getx()
{
  return x;
}

int tpunkt::gety()
{
  return y;
}

tkreis::tkreis(int x_neu, int y_neu, int r_neu) : tpunkt(x_neu, y_neu)
{
  r=r_neu;
}

int tkreis::getr()
{
  return r;
}

void main()
{
  int vx,vy,vr;
  tpunkt punkt1(10,10);
  vx=punkt1.getx();
  vy=punkt1.gety();
  tkreis kreis1(30,40,50);
  vx=kreis1.getx();
  vy=kreis1.gety();
  vr=kreis1.getr();
}
