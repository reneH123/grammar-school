uses Crt;

     var Path: ARRAY[0..2] OF String;

begin
  ClrsCr;
  GetDir(0,Path[0]); { 0 = Current drive }
  Path[1]:=Path[0]+'\BGI\';
  Path[2]:=Path[0]+'\PICTURES\';

  WriteLn('aktuelles Verzeichnis: ',Path[0]);

  {$I-}
  ChDir(Path[1]);
  {$I+}
  IF IOResult = 0 THEN WriteLn('Verzeichnis: ',Path[1],' wurde nicht gefunden!');
  {$I-}
  ChDir(Path[2]);
  {$I+}
  IF IOResult = 0 THEN WriteLn('Verzeichnis: ',Path[2],' wurde nicht gefunden!');

{  ChDir(Path[0]);}
  WriteLn('aktuelles Verzeichnis: ',Path[0]);
  Readkey;
end.
