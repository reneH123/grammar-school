{$A+,B-,D+,E+,F-,G-,I+,L+,N-,O-,P-,Q+,R+,S+,T-,V-,X+}
{$M 16384,0,655360}
USES Crt, Graph;
{CONST Max = 100;}
CONST MaxO = 3;
TYPE PPunkt = ^TPunkt;
     TPunkt= OBJECT
       x,y: WORD;
       f: BYTE;
       CONSTRUCTOR Init(x_,y_: WORD; f_: BYTE);
       PROCEDURE Zeige_Dich;
       PROCEDURE Verstecke_Dich;
       PROCEDURE Bewege_Dich(x_,y_: integer); VIRTUAL;
       PROCEDURE Bewege_Dich_Random; VIRTUAL;
       DESTRUCTOR Kill;
     END;
     Ple= ^TLE;
     TLE = RECORD
             Data: PPunkt;
             Next: PLE;
           END;
     TListClass= OBJECT
                   PROCEDURE AddAll();
                   PROCEDURE Add();
                   PROCEDURE Delete();
                   PROCEDURE DeleteAll()
                 END;
     TKreis= OBJECT(TPunkt)
       Radius: WORD;
       CONSTRUCTOR Init(x_,y_: WORD; f_: BYTE; R: WORD);
       PROCEDURE Zeige_Dich;
       PROCEDURE Verstecke_Dich;
       PROCEDURE Bewege_Dich(x_,y_: integer); VIRTUAL;
       PROCEDURE Bewege_Dich_Random; VIRTUAL;
     END;
     PKreis = ^TKreis;
     PRechteck = ^TRechteck;
     TRechteck= OBJECT(TPunkt)
       x2,y2: WORD;
       CONSTRUCTOR Init(x_,y_,x2_,y2_: WORD; f_: BYTE);
       PROCEDURE Zeige_Dich;
       PROCEDURE Verstecke_Dich;
       PROCEDURE Bewege_Dich(x_,y_: integer); VIRTUAL;
       PROCEDURE Bewege_Dich_Random; VIRTUAL;
     END;
     PNutzer = ^TNutzer;
     TNutzer= OBJECT
{       Punkt: PPunkt;
       Kreis: PKreis;
       Rechteck: PRechteck;}
       Anker: Ple;
       CONSTRUCTOR Init;
       PROCEDURE Run;
       DESTRUCTOR Done;
     END;
VAR Nutzer: PNutzer;

CONSTRUCTOR TPunkt.Init(x_,y_: WORD; f_: BYTE);
BEGIN
  x:=x_;
  y:=y_;
  f:=f_;
END;
PROCEDURE TPunkt.Zeige_Dich;
BEGIN
  SetColor(f);
  PutPixel(x,y,f);
END;
PROCEDURE TPunkt.Verstecke_Dich;
BEGIN
  SetColor(Black);
  PutPixel(x,y,0);
END;
PROCEDURE TPunkt.Bewege_Dich(x_,y_: integer);
BEGIN
  Verstecke_Dich;
  x:=x+x_;
  y:=y+y_;
  Zeige_Dich;
END;
PROCEDURE TPunkt.Bewege_Dich_Random;
 VAR TmpX,TmpY: INTEGER;
BEGIN
  WHILE NOT Keypressed DO BEGIN
    TmpX:=INTEGER(Random(3))-1;
    TmpY:=INTEGER(Random(3))-1;
    IF (x+TmpX >0) AND (x+TmpX < GetMaxX) AND (y+TmpY >0) AND
       (y+TmpY < GetMaxY) THEN Bewege_Dich(TmpX,TmpY);
    Delay(10);
  END;
END;
DESTRUCTOR TPunkt.Kill;
BEGIN
END;
CONSTRUCTOR TKreis.Init(x_,y_: WORD; f_: BYTE; R: WORD);
BEGIN
  Radius:=R;
  INHERITED Init(x_,y_,f_);
END;
PROCEDURE TKreis.Zeige_Dich;
BEGIN
  SetColor(f);
  Circle(x,y,Radius);
END;
PROCEDURE TKreis.Verstecke_Dich;
BEGIN
  SetColor(Black);
  Circle(x,y,Radius);
END;
PROCEDURE TKreis.Bewege_Dich(x_,y_: integer);
BEGIN
  Verstecke_Dich;
  x:=x+x_;
  y:=y+y_;
  Zeige_Dich;
END;
PROCEDURE TKreis.Bewege_Dich_Random;
 VAR TmpX,TmpY: WORD;
BEGIN
  {$R-} {$Q-}
  WHILE NOT Keypressed DO BEGIN
    TmpX:=INTEGER(Random(3))-1;
    TmpY:=INTEGER(Random(3))-1;
    IF (x+TmpX+Radius >0) AND (x+TmpX+Radius< GetMaxX) AND
       (y+TmpY+Radius >0) AND (y+TmpY+Radius < GetMaxY) THEN
      Bewege_Dich(TmpX,TmpY);
    Delay(10);
  END;
END;
CONSTRUCTOR TRechteck.Init(x_,y_,x2_,y2_: WORD; f_: BYTE);
BEGIN
  x2:=x2_;
  y2:=y2_;
  INHERITED Init(x_,y_,f_);
END;
PROCEDURE TRechteck.Zeige_Dich;
BEGIN
  SetColor(f);
  Rectangle(x,y,x2,y2)
END;
PROCEDURE TRechteck.Verstecke_Dich;
BEGIN
  SetColor(Black);
  Rectangle(x,y,x2,y2);
END;
PROCEDURE TRechteck.Bewege_Dich(x_,y_: integer);
 VAR TmpX, TmpY: Integer;
BEGIN
  Verstecke_Dich;
  x2:=x_+x2;
  y2:=y_+y2;
  x:=x+x_;
  y:=y+y_;
  Zeige_Dich;
END;
PROCEDURE TRechteck.Bewege_Dich_Random;
 VAR TmpX,TmpY: WORD;
BEGIN
  {$R-} {$Q-}
  WHILE NOT Keypressed DO BEGIN
    TmpX:=INTEGER(Random(3))-1;
    TmpY:=INTEGER(Random(3))-1;
    IF (x+TmpX >0) AND (x+TmpX+x2< GetMaxX) AND
       (y+TmpY > 0) AND (y+TmpY+y2 < GetMaxY) THEN
    Bewege_Dich(TmpX,TmpY);
    Delay(10);
  END;
END;
CONSTRUCTOR TNutzer.Init;
CONST GraphDrive: STRING ={'c:\sprachen\bp\bgi'}'p:\turbo7\bgi';
VAR gd,gm: INTEGER;
BEGIN
  gd:=detect;
  InitGraph(gd,gm,GraphDrive);
  WITH Nutzer^ DO BEGIN
{    Anker^.Init();
    Punkt^.Init(320,240,15);
    Kreis^.Init(320,240,3,20);
    Rechteck^.Init(100,100,50,50,3);}
  END;
  Randomize;
END;
PROCEDURE TNutzer.Run;
 VAR i: Integer;
BEGIN
  SetColor(Red);
  FOR i:=1 TO 3 DO BEGIN
    ClearDevice;
    OutTextXY(0,0,'Test Punkt setzen:');
    WITH Nutzer^ DO
{      CASE i OF
        1: Punkt:=New(PPunkt,Init(300,300,Red));
        2: Punkt:=New(PKreis,Init(300,300,Red,30));
        3: Punkt:=New(PRechteck,Init(100,100,200,200,Red));
      END;}
{    Nutzer^.Punkt^.Bewege_Dich_Random;}
    ReadKey;
  END;
END;
DESTRUCTOR TNutzer.Done;
BEGIN
  CloseGraph;
  WriteLn('Testprogramm ENDE');
END;

PROCEDURE CreateEl(VAR Root: PLE);
 VAR i: Integer;
BEGIN
  i:=0;
  Root:=NIL;
  New(Root);
  WHILE (i <> MaxO) AND (Root <> NIL) DO BEGIN
    Root:=Root^.p;
    New(PLE, Init);
    Inc(i); aa
  END;
END;
PROCEDURE DestroyEl(VAR Root: PLE);
 VAR i: Integer;
BEGIN
  Root:=NIL;
  Dispose(Root);
  WHILE (i <> MaxO) AND (Root <> NIL) DO BEGIN
    Root:=Root^.p;
    Dispose(Root^.w);
    Inc(i);
  END;
END;
(*Hauptprogramm*)
BEGIN
  Nutzer:=New(PNutzer,Init);
  CreateEl(Nutzer^.Anker);
  Nutzer^.Run;
{  Dispose(Nutzer^.Punkt,Kill);
  Dispose(Nutzer^.Kreis,Kill);
  Dispose(Nutzer^.Rechteck,Kill);}
  Dispose(Nutzer, Done);
  DestroyEl(Nutzer^.Anker);
END.
