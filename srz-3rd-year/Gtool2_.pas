{$A+,B-,D+,E+,F-,G-,I+,L+,N-,O-,P-,Q+,R+,S+,T-,V-,X+}
{$M 16384,0,655360}
PROGRAM Graphikelemente;
USES Crt, Graph;
CONST Max = 100;
TYPE TAttr= RECORD x1,y1,x2,y2: WORD; Radius, Color: BYTE; END;
TYPE TPunkt= OBJECT
       x,y: WORD;
       f: BYTE;
       CONSTRUCTOR Init(Attr: TAttr);
       PROCEDURE Zeige_Dich;
       PROCEDURE Verstecke_Dich;
       PROCEDURE Bewege_Dich(x_,y_: WORD);
       PROCEDURE Bewege_Dich_Random; VIRTUAL;
       DESTRUCTOR Kill;
     END;
     TKreis= OBJECT(TPunkt)
       Radius: WORD;
       CONSTRUCTOR Init(Attr: TAttr);
       PROCEDURE Zeige_Dich;
       PROCEDURE Verstecke_Dich;
       DESTRUCTOR Kill;
     END;
     TRechteck= OBJECT(TPunkt)
       x2,y2: WORD;
       CONSTRUCTOR Init(Attr: TAttr);
       PROCEDURE Zeige_Dich;
       PROCEDURE Verstecke_Dich;
       DESTRUCTOR Kill;
     END;
     TNutzer= OBJECT
       Choose: CHAR;
       MaxObjekte: BYTE; {= 1;}
       EXX: ARRAY[1..Max] OF TAttr;
       PROCEDURE Init;
       PROCEDURE Run;
       PROCEDURE Done;
     END;
VAR Nutzer: TNutzer;
    Punkt: TPunkt;
    Rechteck: TRechteck;
    Kreis: TKreis;
    Choose: BYTE;

CONSTRUCTOR TPunkt.Init(Attr: TAttr);
BEGIN
  x:=Attr.x1;
  y:=Attr.y1;
  f:=Attr.Color;
END;
DESTRUCTOR TPunkt.Kill;
BEGIN
END;
PROCEDURE TPunkt.Zeige_Dich;
BEGIN
 SetColor(f);
 Circle(x,y,2);
END;
PROCEDURE TPunkt.Verstecke_Dich;
BEGIN
  SetColor(Black);
  Circle(x,y,2);
END;
PROCEDURE TPunkt.Bewege_Dich(x_,y_: WORD);
BEGIN
  Verstecke_Dich;
  x:=x+x_;
  y:=x+y_;
  Zeige_Dich;
END;
PROCEDURE TPunkt.Bewege_Dich_Random;
 VAR Zuf: WORD;
     x_,y_: INTEGER;
BEGIN
  REPEAT
    x_:=Integer(Random(3))-1;
    y_:=Integer(Random(3))-1;
    x:=x+x_;
    y:=x+y_;
    IF (x>=0) AND (x<=640) AND (y>=0) AND (y<=480) THEN BEGIN
      Zeige_Dich;
      Delay(30);
      Verstecke_Dich;
    END;
  UNTIL Keypressed;
END;
CONSTRUCTOR TKreis.Init(Attr: TAttr);
BEGIN
  Punkt.Init(Attr);
  Radius:=Attr.Radius;
END;
PROCEDURE TKreis.Zeige_Dich;
BEGIN
  SetColor(f);
  Circle(x,y,Radius);
END;
PROCEDURE TKreis.Verstecke_Dich;
BEGIN
  SetColor(Black);
  Circle(x,y,Radius);
END;
DESTRUCTOR TKreis.Kill;
BEGIN
END;
CONSTRUCTOR TRechteck.Init(Attr: TAttr);
BEGIN
  Punkt.Init(Attr);
  x2:=Attr.x2;
  y2:=Attr.y2;
END;
DESTRUCTOR TRechteck.Kill;
BEGIN
END;
PROCEDURE TRechteck.Zeige_Dich;
BEGIN
  SetWriteMode(NormalPut);
  SetColor(f);
  Rectangle(x,y,x2,y2);
END;
PROCEDURE TRechteck.Verstecke_Dich;
BEGIN
  SetColor(Black);
  Rectangle(x,y,x2,y2);
END;
PROCEDURE TNutzer.Init;
CONST GraphPath: STRING = 'p:\turbo7\BGI'{'C:\sprachen\bp\bgi'};
VAR gd,gm: INTEGER;
    i: INTEGER;
BEGIN
  ClrScr;
  Randomize;
  WriteLn('Womit m�chten Sie arbeiten ((R)echteck, (K)reis, (P)unkt?)');
  Read(Choose);
  Write('Wieviel mal: ');
  Read(MaxObjekte);
  FOR i:=1 TO MaxObjekte DO BEGIN
    CASE Choose OF
      'r','R': BEGIN (*Rechteck*)
           Write('Geben Sie die erste X-Koordinate des ',i,'. Rechtecks ein: ');
           ReadLn(EXX[i].x1);
           Write('Geben Sie die erste Y-Koordinate des ',i,'. Rechtecks ein: ');
           ReadLn(EXX[i].y1);
           Write('Geben Sie die zweite X-Koordinate des ',i,'. Rechtecks ein: ');
           ReadLn(EXX[i].x2);
           Write('Geben Sie die zweite Y-Koordinate des ',i,'. Rechtecks ein: ');
           ReadLn(EXX[i].y2);
           Write('Geben Sie die Farbe des ',i,'. Rechtecks ein: ');
           ReadLn(EXX[i].Color);
         END;
      'k','K': BEGIN (*Kreis*)
           Write('Geben Sie die X-Koordinate des ',i,'. Kreises ein: ');
           ReadLn(EXX[i].x1);
           Write('Geben Sie die Y-Koordinate des ',i,'. Kreises ein: ');
           ReadLn(EXX[i].y1);
           Write('Geben Sie den Radius des ',i,', Kreises ein: ');
           ReadLn(EXX[i].Radius);
           Write('Geben Sie die Farbe des ',i,'. Rechtecks ein: ');
           ReadLn(EXX[i].Color);
         END;
      'p','P': BEGIN (*Punkt*)
           Write('Geben Sie die X-Koordinate des ',i,'. Punktes ein: ');
           ReadLn(EXX[i].x1);
           Write('Geben Sie die Y-Koordinate des ',i,'. Punktes ein: ');
           ReadLn(EXX[i].y1);
           Write('Geben Sie die Farbe des ',i,'. Punktes ein: ');
           ReadLn(EXX[i].Color);
         END;
      ELSE BEGIN (*keine genaue Aussuche*)
        ClrScr;
        WriteLn('Eingabe ist nicht akzeptabel!');
        Halt(1);
      END;
  END; (*FOR*)
  END;
  FOR i:=1 TO MaxObjekte DO BEGIN
    Rechteck.Init(EXX[i]);
    Kreis.Init(EXX[i]);
    Punkt.Init(EXX[i]);
  END;
  gd:=detect;
  InitGraph(gd,gm,GraphPath);
END;
PROCEDURE TNutzer.Run;
VAR i: Integer;
BEGIN
  OutTextXY(0,0,'Objekte werden der Reihenfolge nach gezeichnet');
  ReadKey;
  ClearDevice;
  FOR i:=1 TO MaxObjekte DO BEGIN
    CASE Choose OF
      'r','R': BEGIN
           WITH EXX[i] DO BEGIN
              Rechteck.Zeige_Dich;
              Rechteck.Bewege_Dich_Random;
           END;
         END;
      'k','K': BEGIN
      WITH EXX[i] DO BEGIN
         Kreis.Zeige_Dich;
         Kreis.Bewege_Dich_Random;
      END;
         END;
      'p','P': BEGIN
           WITH EXX[i] DO BEGIN
              Punkt.Zeige_Dich;
              Punkt.Bewege_Dich_Random;
           END;
         END;
      ELSE EXIT;(*nichts*)
    END; (*CASE*)
      OutTextXY(0,40,'Taste dr�cken');
      ReadKey;
      CASE Choose OF
        'r','R': Rechteck.Verstecke_Dich;
        'k','K': Kreis.Verstecke_Dich;
        'p','P': Punkt.Verstecke_Dich;
      END;
      ClearDevice;
  END; (*FOR*)
END;
PROCEDURE TNutzer.Done;
BEGIN
  CloseGraph;
END;

BEGIN
  Nutzer.Init;
  Nutzer.Run;
  Nutzer.Done;
END.