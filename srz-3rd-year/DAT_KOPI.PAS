PROGRAM CopyFile;
USES Crt;
CONST Temp_Dat= 'Default.txt';
VAR  f1, f2, f3: file;
  NumRead, NumWritten: Word;
  Buf: ARRAY [1..2048] OF Char;
  Von_Dat, Nach_Dat: String;
  c: Char;

PROCEDURE Byte_Lesen(VAR fi: File);
  BEGIN
    BlockRead(fi, Buf, SizeOf(Buf), NumRead);
  END;

PROCEDURE Byte_Schreiben(VAR fi: File);
  BEGIN
    Reset(fi, 1);
    BlockWrite(fi, Buf, NumRead, NumWritten);
  END;

FUNCTION Fileexist(VAR fi: File; Filename: String): Boolean;
  BEGIN
    Fileexist:=TRUE;
    {$I-}
    Reset(fi);
    {$I+}
    IF IOResult <> 0 THEN BEGIN
      Fileexist:=FALSE;
      Halt(1);
    END
    ELSE BEGIN
      Close(fi);
      Fileexist:=TRUE;
    END;
  END;

BEGIN
  ClrScr;
  Write('Die zu kopierende Datei hei�t: ');
    ReadLn(Von_Dat);
  Assign(f1, Von_Dat);
  IF Fileexist(f1, Von_Dat) <> TRUE THEN WriteLn('Datei nicht vorhanden: ', Von_Dat);

  Reset(f1, 1); {einzelne Bl�cke}
  Write('In welche Datei soll kopiert werden: ');
    ReadLn(Nach_Dat); {Zu welcher Datei erzeugen}

  Assign(f2, Nach_Dat); {zu erstellende Datei}
  IF Fileexist(f2, Nach_Dat) = TRUE THEN BEGIN
  Reset(f2, 1);
    WriteLn('Datei schon vorhanden ',Nach_Dat);
    WriteLn('Soll diese �berschrieben werden: j/n');
    IF ReadKey=#106 THEN BEGIN
      {EIGENDLICHES Kopierprogramm}
      Assign(f3, Temp_Dat);
{      IF Fileexist(f3, Temp_Dat)= FALSE THEN Rewrite(f3, 1);}
      Rewrite(f2, 1);
      Writeln('Kopiert ', FileSize(f1), ' Bytes...');
      REPEAT
        Byte_Lesen(f1);
        {in tempor�re Datei}
{        Byte_Schreiben(f3);
        Byte_Lesen(f3);}
        {Ende von Temp}
        Byte_Schreiben(f2);
      UNTIL (NumRead = 0) OR (NumWritten <> NumRead);
{      Close(f3);
      Erase(f3);}
    END
    ELSE IF ReadKey=#110 THEN BEGIN
      WriteLn('Kopieren der Datei: ',Von_Dat,' abgebrochen');
      Close(f1); Close(f2);
      Halt(1);
    END;
  END;

  Close(f1);
  Close(f2);
END.
