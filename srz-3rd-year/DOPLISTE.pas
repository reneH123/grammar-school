Program doppeltliste;
Uses Crt;
type tinhalt=Integer;
     ple=^tle;
     tle=record
           inhalt:tinhalt;
           zeili,zeire:ple;
         end;
var ankerli,ankerre:ple;

procedure ausg(ali,are:ple);
var ch : char;
begin
  clrscr;
  write('Vor- oder R�ckw�rts? ');
  ch:=readkey;
  clrscr;
  if ch = 'v' then
  repeat
    writeln(ali^.inhalt);
    ali:=ali^.zeire;
  until ali = nil;
  if ch = 'r' then
  repeat
    writeln(are^.inhalt);
    are:=are^.zeili;
  until are = nil;
end;

Procedure eingabe(var ali,are:ple);
Var h:ple;
    zahl:tinhalt;
Begin
  ClrScr;
  new(ali);
  h:=ali;
  Repeat
    write('Zahl: ');
    readln(zahl);
    if zahl <> 0 then
    Begin
      ali^.inhalt:=zahl;
      ali:=ali;
      new(ali);
    End;
  Until zahl = 0;
  ali:=h;
End;

Procedure loeschen(var ali:ple);
Begin
  if ali <> NIL then
  Begin
    loeschen(ali^.zeire);
    dispose(ali);
  End;
End;

Begin
  eingabe(ankerli,ankerre);
  ausg(ankerli,ankerre);
  loeschen(ankerli);
End.