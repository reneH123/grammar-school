PROGRAM Test;
USES Crt;
TYPE T_B= Record
    Titel:String;
    Autor: String[30];
    Seiten: Word;
    Preis: Real;
    ausgeliehen: Char;
End;
VAR T_Buch: T_B;

PROCEDURE Frage;
VAR wahr: Boolean;
BEGIN
 WITH T_Buch DO BEGIN
  Write('Der Titel des Buches hei�t: ');
  ReadLn(Titel);
  Write('Der Autor des Buches hei�t: ');
  ReadLn(Autor);
  Write('Das Buch hat Seiten: ');
  ReadLn(Seiten);
  Write('Das Buch kostet DM: ');
  ReadLn(Preis);
  Write('Ist das Buch ausgeliehen (j)a / (n)ein: ');
  ReadLn(ausgeliehen);
  CASE ausgeliehen OF
   'j': WriteLn('Buch ist leider ausgeliehen!');
   'n': WriteLn('Buch ist noch da.')
   ELSE WriteLn('Sie haben sich vertippt');
  END;
 END;
END;

PROCEDURE Antwort;
BEGIN
 WITH T_Buch DO BEGIN
  WriteLn('Der Titel des Buches hei�t: ',Titel);
  WriteLn('Der Autor des Buches hei�t: ',Autor);
  WriteLn('Das Buch hat ',Seiten,' Seiten');
  WriteLn('Das Buch kostet ',Preis:0:2,' DM');
 END;
END;

BEGIN
 ClrScr;
 GoToXY(58,1);
 WriteLn('St�dtische Bibliothek');
 Frage;
 WriteLn;
 Antwort;
 Readln;
END.