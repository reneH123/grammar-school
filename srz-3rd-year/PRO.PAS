
UNIT Pro;

INTERFACE

USES GTool, List, Graph;

TYPE TProject = OBJECT
                  PROCEDURE Init;
                  PROCEDURE Run;
                  PROCEDURE Done;
                END;
VAR Project: TProject;


IMPLEMENTATION

VAR GrafikListe: PDynListClass;

PROCEDURE TProject.Init;
BEGIN
  Randomize;
  InitGraph(GraphicDriver,GraphicMode,GraphicPath);
  GrafikListe^.L:=NIL; GrafikListe:=NIL;
  GrafikListe:=New(PDynListClass,Init);
  New(GrafikListe^.L);
  GrafikListe^.Erstelle;
END;

PROCEDURE TProject.Run;
BEGIN
  GrafikListe^.Zeige;
END;

PROCEDURE TProject.Done;
BEGIN
  CloseGraph;
  Dispose(GrafikListe^.L);
  Dispose(GrafikListe,Done);
END;

END.

