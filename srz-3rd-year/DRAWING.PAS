USES Crt, Graph;
TYPE PDrawingObject = ^TDrawingObject;
     TDrawingObject = OBJECT
                        x,y: WORD;
                        Color: BYTE;
                        CONSTRUCTOR Init(iX,iY: WORD; iColor: BYTE);
                        DESTRUCTOR Done;
                        PROCEDURE Show; VIRTUAL;
                        PROCEDURE Hide; VIRTUAL;
                        PROCEDURE Move(DivX, DivY: WORD); VIRTUAL;
                      END;
     PSE = ^TSE;
     TSE = RECORD
             Data: PDrawingObject;
             Next: PSE;
             Count: WORD;
           END;
     PPoint = ^TPoint;
     TPoint = OBJECT(TDrawingObject)
                CONSTRUCTOR Init(iX,iY: WORD; iColor: BYTE);
                DESTRUCTOR Done;
                PROCEDURE Show; VIRTUAL;
                PROCEDURE Hide; VIRTUAL;
              END;
     PCircle = ^TCircle;
     TCircle = OBJECT(TPoint)
                 Radius: WORD;
                 CONSTRUCTOR Init(iX, iY, iRadius: WORD; iColor: BYTE);
                 DESTRUCTOR Done;
                 PROCEDURE Show; VIRTUAL;
                 PROCEDURE Hide; VIRTUAL;
                 PROCEDURE Move(DivX, DivY: WORD); VIRTUAL;
               END;
     PLine = ^TLine;
     TLine = OBJECT(TDrawingObject)
               x2,y2: WORD;
               CONSTRUCTOR Init(iX1,iY1,iX2,iY2: WORD; iColor: BYTE);
               DESTRUCTOR Done;
               PROCEDURE Show; VIRTUAL;
               PROCEDURE Hide; VIRTUAL;
               PROCEDURE Move(DivX, DivY: WORD); VIRTUAL;
             END;
     PRectangle = ^TRectangle;
     TRectangle = OBJECT(TDrawingObject)
                    x2,y2: WORD;
                    CONSTRUCTOR Init(iX1,iY1,iX2,iY2: WORD; iColor: BYTE);
                    DESTRUCTOR Done;
                    PROCEDURE Show; VIRTUAL;
                    PROCEDURE Hide; VIRTUAL;
                    PROCEDURE Move(DivX, DivY: WORD); VIRTUAL;
                  END;
     PQuadrat = ^TQuadrat;
     TQuadrat = OBJECT(TDrawingObject)
                  a: WORD;
                  CONSTRUCTOR Init(iX,iY: WORD; iA:WORD; iColor: BYTE);
                  DESTRUCTOR Done;
                  PROCEDURE Show; VIRTUAL;
                  PROCEDURE Hide; VIRTUAL;
                  PROCEDURE Move(DivX, DivY: WORD); VIRTUAL;
                END;
     PDrawingObjectList = ^TDrawingObjectList;
     TDrawingObjectList = OBJECT(TDrawingObject)
                            PRIVATE
                              Root: PSE;
                            PUBLIC
                              CONSTRUCTOR Init;
                              DESTRUCTOR Done;
                              PROCEDURE Add(Elem: PDrawingObject);
                              PROCEDURE Show; VIRTUAL;
                              PROCEDURE Hide; VIRTUAL;
                              FUNCTION GetObject(Nummer: WORD): PDrawingObject;
                          END;
     PMain = ^TMain;
     TMain = OBJECT
               List: PDrawingObjectList;
               CONSTRUCTOR Init;
               DESTRUCTOR Done;
               PROCEDURE Run;
             END;
VAR Main: PMain;

CONSTRUCTOR TDrawingObject.Init(iX,iY: WORD; iColor: BYTE);
BEGIN
  X:=iX;
  Y:=iY;
  Color:=iColor;
END;
DESTRUCTOR TDrawingObject.Done;
BEGIN
END;
PROCEDURE TDrawingObject.Show;
BEGIN
  Write(#7); (*darf nicht hierher*)
END;
PROCEDURE TDrawingObject.Hide;
BEGIN
  Write(#7); (*darf nicht hierher*)
END;
PROCEDURE TDrawingObject.Move(DivX, DivY: WORD);
BEGIN
  X:=X+DivX;
  Y:=Y+DivY;
END;
CONSTRUCTOR TPoint.Init(iX,iY: WORD; iColor: BYTE);
BEGIN
  INHERITED Init(iX, iY, iColor);
END;
DESTRUCTOR TPoint.Done;
BEGIN
END;
PROCEDURE TPoint.Show;
BEGIN
  PutPixel(X,Y,Color);
END;
PROCEDURE TPoint.Hide;
BEGIN
  PutPixel(X,Y,Black);
END;
CONSTRUCTOR TCircle.Init(iX, iY, iRadius: WORD; iColor: BYTE);
BEGIN
  INHERITED Init(iX,iY,iColor);
  Radius:=iRadius;
END;
DESTRUCTOR TCircle.Done;
BEGIN
END;
PROCEDURE TCircle.Show;
BEGIN
  SetColor(Color);
  Circle(X,Y,Radius);
END;
PROCEDURE TCircle.Hide;
BEGIN
  SetColor(Black);
  Circle(X,Y,Radius);
END;
PROCEDURE TCircle.Move(DivX, DivY: WORD);
BEGIN
  INHERITED Move(DivX,DivY);
END;
CONSTRUCTOR TLine.Init(iX1,iY1,iX2,iY2: WORD; iColor: BYTE);
BEGIN
  INHERITED Init(iX1,iY1,iColor);
  X2:=iX2;
  Y2:=iY2;
END;
DESTRUCTOR TLine.Done;
BEGIN
END;
PROCEDURE TLine.Show;
BEGIN
  SetColor(Color);
  Line(X,Y,X2,Y2);
END;
PROCEDURE TLine.Hide;
BEGIN
  SetColor(Black);
  Line(X,Y,X2,Y2);
END;
PROCEDURE TLine.Move(DivX, DivY: WORD);
BEGIN
  INHERITED Move(DivX,DivY);
  X2:=X2+DivX;
  Y2:=Y2+DivY;
END;
CONSTRUCTOR TRectangle.Init(iX1,iY1,iX2,iY2: WORD; iColor: BYTE);
BEGIN
  INHERITED Init(iX1,iY1,iColor);
  X2:=iX2;
  Y2:=iY2;
END;
DESTRUCTOR TRectangle.Done;
BEGIN
END;
PROCEDURE TRectangle.Show;
BEGIN
  SetColor(Color);
  Rectangle(X,Y,X2,Y2);
END;
PROCEDURE TRectangle.Hide;
BEGIN
  SetColor(Black);
  Rectangle(X,Y,X2,Y2);
END;
PROCEDURE TRectangle.Move(DivX, DivY: WORD);
BEGIN
  INHERITED Move(DivX,DivY);
  X2:=X2+DivX;
  Y2:=Y2+DivX;
END;
CONSTRUCTOR TQuadrat.Init(iX,iY: WORD; iA:WORD; iColor: BYTE);
BEGIN
  INHERITED Init(iX,iY,iColor);
  A:=iA;
END;
DESTRUCTOR TQuadrat.Done;
BEGIN
END;
PROCEDURE TQuadrat.Show;
BEGIN
  SetColor(Color);
  Rectangle(X,Y,X+A,Y+A);
END;
PROCEDURE TQuadrat.Hide;
BEGIN
  SetColor(Black);
  Rectangle(X,Y,X+A,Y+A);
END;
PROCEDURE TQuadrat.Move(DivX, DivY: WORD);
BEGIN
  INHERITED Move(DivX, DivY);
END;
CONSTRUCTOR TDrawingObjectList.Init;
BEGIN
  Root:=NIL;
END;
DESTRUCTOR TDrawingObjectList.Done;
 VAR h: PSE;
BEGIN
  WHILE Root<>NIL DO BEGIN
    h:=Root;
    Dispose(h^.Data,Done);
    Root:=h^.Next;
    Dispose(h)
  END
END;
PROCEDURE TDrawingObjectList.Add(Elem: PDrawingObject);
 VAR TmpRoot: PSE;
BEGIN
  New(TmpRoot);
  TmpRoot^.Next:=Root;
  TmpRoot^.Data:=Elem;
  Root:=TmpRoot
END;
PROCEDURE TDrawingObjectList.Show;
BEGIN
  WHILE Root <> NIL DO BEGIN
    Root^.Data^.Show;
    Root:=Root^.Next;
  END;
END;
PROCEDURE TDrawingObjectList.Hide;
BEGIN
  WHILE Root <> NIL DO BEGIN
    Root^.Data^.Hide;
    Root:=Root^.Next;
  END;
END;
FUNCTION TDrawingObjectList.GetObject(Nummer: WORD): PDrawingObject;
BEGIN
  WHILE Root <> NIL DO BEGIN
    IF Nummer = Root^.Count THEN
     GetObject:=Root^.Data
    ELSE Root:=Root^.Next;
  END;
END;
CONSTRUCTOR TMain.Init;
 VAR gd,gm: Integer;
BEGIN
  List:=New(PDrawingObjectList, Init);
  gd:=Detect;
  InitGraph(gd,gm,'');
END;
DESTRUCTOR TMain.Done;
BEGIN
  Dispose(List,Done);
  CloseGraph;
END;
PROCEDURE TMain.Run;
 VAR Sun, SnowMan, House, Ground: PDrawingObjectList;
BEGIN
  Sun:=New(PDrawingObjectList,Init);
  SnowMan:=New(PDrawingObjectList,Init);
  House:=New(PDrawingObjectList,Init);
  Ground:=New(PDrawingObjectList,Init);
  List^.Add(Sun);
  Sun^.Add(New(PCircle,Init(140,120,20,Yellow)));
  Sun^.Add(New(PLine,Init(140,40,140,60,Red))); (*Nord-Strahl*)
  Sun^.Add(New(PLine,Init(60,120,80,120,Red))); (*West-Strahl*)
  Sun^.Add(New(PLine,Init(140,180,140,200,Red))); (*S�d-Strahl*)
  Sun^.Add(New(PLine,Init(200,120,220,120,Red))); (*Ost-Strahl*)
  List^.Add(SnowMan);
  SnowMan^.Add(New(PCircle,Init(260,300,20,White))); (*Kopf*)
  SnowMan^.Add(New(PCircle,Init(260,350,30,White))); (*Oberk�rper*)
  SnowMan^.Add(New(PCircle,Init(260,420,40,White))); (*Unterk�rper*)
  SnowMan^.Add(New(PCircle,Init(310,350,20,White))); (*rechte Hand*)
  SnowMan^.Add(New(PCircle,Init(210,350,20,White))); (*linke Hand*)
  SnowMan^.Add(New(PLine,Init(310,300,310,380,Brown))); (*Haupt-Stock*)
  SnowMan^.Add(New(PLine,Init(290,290,310,320,Brown))); (*Nebenstock-links*)
  SnowMan^.Add(New(PLine,Init(330,290,310,320,Brown))); (*Nebenstock-rechts*)
  SnowMan^.Add(New(PRectangle,Init(240,260,280,280,DarkGray))); (*Hut*)
  SnowMan^.Add(New(PLine,Init(220,280,300,280,DarkGray))); (*Hutschirm*)
  List^.Add(House);
  House^.Add(New(PRectangle,Init(440,320,580,460,LightGray))); (*Hauptteil*)
  House^.Add(New(PQuadrat,Init(460,360,40,Green))); (*Fenster*)
  House^.Add(New(PLine,Init(440,320,540,260,LightGray))); (*linke Dachseite*)
  House^.Add(New(PLine,Init(540,260,580,320,LightGray))); (*rechte Dachseite*)
  House^.Add(New(PLine,Init(460,308,460,260,LightGray))); (*Schornstein li*)
  House^.Add(New(PLine,Init(460,260,480,260,LightGray))); (*Schornstein oben*)
  House^.Add(New(PLine,Init(480,260,480,290,LightGray))); (*Schornstein re*)
  List^.Add(Ground);
  Ground^.Add(New(PLine,Init(0,470,220,470,Green))); (*linke Bodenh�lfte*)
  Ground^.Add(New(PLine,Init(220,470,640,475,Green)));
  List^.Show;
  ReadLn;
END;

BEGIN
  Main:=New(PMain,Init);
  Main^.Run;
  Dispose(Main,Done);
END.
