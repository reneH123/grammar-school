{$A+,B-,D-,E-,F-,G+,I+,L+,N+,O-,P-,Q-,R-,S+,T-,V-,X+,Y+}
{$M 16384,0,655360}
UNIT Mathe;

INTERFACE
 CONST FunkVerbZahl  = 9;
       FunkStartZahl = 33;
       MyFunkZahl = FunkVerbZahl+FunkStartZahl;
       ZahlStart = ['0'..'9','.'];
 TYPE  FString  = STRING[8];
       RechType = EXTENDED;
       MFZahlT  = 1..MyFunkZahl;
       MyF_Type = FUNCTION (a,b: RechType): RechType;
 CONST MyFunk:    ARRAY[MFZahlT] OF FString =
       ('+','-','*','/','^','ROOT','AND','OR','XOR',
        'NOT','SQR','SQRT','LN','LG','LB','LOG','EXP','SGN',
        'SIN','COS','TAN','SEK','COSEK','COT',
        'ARCSIN','ARCCOS','ARCTAN','ARCSEK','ARCCOSEK','ARCCOT',
        'SINH','COSH','TANH','SEKH','COSEKH','COTH',
        'ARSINH','ARCOSH','ARTANH','ARSEKH','ARCOSEKH','ARCOTH');
       FunkVerb:  ARRAY[1..FunkVerbZahl] OF FString  =
       ('+','-','*','/','^','ROOT','AND','OR','XOR');
       FunkStart: ARRAY[1..FunkStartZahl] OF FString =
       ('NOT','SQR','SQRT','LN','LG','LB','LOG','EXP','SGN',
        'SIN','COS','TAN','SEK','COSEK','COT',
        'ARCSIN','ARCCOS','ARCTAN','ARCSEK','ARCCOSEK','ARCCOT',
        'SINH','COSH','TANH','SEKH','COSEKH','COTH',
        'ARSINH','ARCOSH','ARTANH','ARSEKH','ARCOSEKH','ARCOTH');
 VAR    MyF: ARRAY[MFZahlT] OF MyF_Type;

IMPLEMENTATION

 FUNCTION MySgn(x: EXTENDED): EXTENDED;
 BEGIN
   IF x > 0 THEN
     MySgn:=1
   ELSE IF x < 0 THEN
     MySgn:=-1
   ELSE
     MySgn:=0;
 END;
 FUNCTION Round(x: EXTENDED): RechType;
 BEGIN
   Round:=(x);
 END;
 FUNCTION Plus(a,b: RechType): RechType; FAR;
 BEGIN
   Plus:=a + b;
 END;
 FUNCTION Minus(a,b: RechType): RechType; FAR;
 BEGIN
   Minus:=a - b;
 END;
 FUNCTION Mul(a,b: RechType): RechType; FAR;
 BEGIN
   Mul:=a * b;
 END;
 FUNCTION Divi(a,b: RechType): RechType; FAR;
 BEGIN
   Divi:=a / b;
 END;
 FUNCTION Pot(a,b: RechType): RechType; FAR;
   VAR a2, b2: EXTENDED;
 BEGIN
   a2:=a;
   b2:=b;
   Pot:=Round(System.Exp(System.Ln(a2)*b2));
 END;
 FUNCTION Root(a,b: RechType): RechType; FAR;
   VAR a2, b2: EXTENDED;
 BEGIN
   a2:=a;
   b2:=b;
   Root:=Round(System.Exp(System.Ln(b2)/a2));
 END;
 FUNCTION Und(a,b: RechType): RechType; FAR;
 BEGIN
   Und:=System.Round(a) AND System.Round(b);
 END;
 FUNCTION Oder(a,b: RechType): RechType; FAR;
 BEGIN
   Oder:=System.Round(a) OR System.Round(b);
 END;
 FUNCTION ExOder(a,b: RechType): RechType; FAR;
 BEGIN
   ExOder:=System.Round(a) XOR System.Round(b);
 END;
  { *** Negiert eine Zahl *** }
 FUNCTION Nicht(a,b: RechType): RechType; FAR;
 BEGIN
   Nicht:=NOT(System.Round(a));
 END;
 FUNCTION SQR(a,b: RechType): RechType; FAR;
 BEGIN
   SQR:=a * a;
 END;
 FUNCTION SQRT(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   SQRT:=Round(System.SQRT(a2));
 END;
  { *** Berechnet den nat�rlichen Logarithmus einer Zahl *** }
 FUNCTION Ln(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   IF a = 1 THEN BEGIN
     Ln:=0;
     EXIT;
   END;
   a2:=a;
   Ln:=Round(System.Ln(a2));
 END;
  { *** Berechnet den decadischen Logarithmus einer Zahl *** }
 FUNCTION Lg(a,b: RechType): RechType; FAR;
   CONST Ln10 = 2.302585092994;
   VAR   a2: EXTENDED;
 BEGIN
   IF a = 1 THEN BEGIN
     Lg:=0;
     EXIT;
   END;
   a2:=a;
   Lg:=Round(System.Ln(a2)/Ln10);
 END;
  { *** Berechnet den bin�ren Logarithmus einer Zahl *** }
 FUNCTION Lb(a,b: RechType): RechType; FAR;
   CONST Ln2 = 0.6931471805599;
   VAR   a2: EXTENDED;
 BEGIN
   IF a = 1 THEN BEGIN
     Lb:=0;
     EXIT;
   END;
   a2:=a;
   Lb:=Round(System.Ln(a2)/Ln2);
 END;
  { *** Berechnet den Logarithmus einer Zahl zur Basis b *** }
 FUNCTION Log(a,b: RechType): RechType; FAR;
   VAR a2, b2: EXTENDED;
 BEGIN
   IF a = 1 THEN BEGIN {log a B ; a ist immer 1 ???????}
     Log:=0;
     EXIT;
   END;
   IF a = a THEN BEGIN
     Log:=1;
     EXIT;
   END
   ELSE IF (a2>0) and (b2>0) THEN BEGIN
   a2:=a;
   b2:=b;
   Log:=Round(System.Ln(a2)/System.Ln(b2));
   END;
 END;
  { *** Berechnet den Exp einer Zahl zur Basis e *** }
 FUNCTION Exp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   Exp:=Round(System.Exp(a2));
 END;
  { *** �bergibt das Vorzeichen einer Zahl *** }
 FUNCTION Sgn(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   Sgn:=Round(MySgn(a2));
 END;
 FUNCTION Sinus(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   Sinus:=Sin(a2);
 END;
 FUNCTION Cosinus(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   Cosinus:=Round(System.Cos(a2));
 END;
 FUNCTION Tangens(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   Tangens:=Round(System.Sin(a2)/System.Cos(a2));
 END;
 FUNCTION Sekans(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   Sekans:=Round(1 / System.Cos(a2));
 END;
 FUNCTION Cosekans(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   Cosekans:=Round(1 / System.Sin(a2));
 END;
 FUNCTION Cotangens(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   Cotangens:=Round(System.Cos(a2)/System.Sin(a2));
 END;
 FUNCTION ArcusSinus(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   ArcusSinus:=System.ArcTan(a2 / System.Sqrt(1-a2*a2));
 END;
 FUNCTION ArcusCosinus(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   ArcusCosinus:=Round((pi/2){1.570796} - System.ArcTan(a2 / System.Sqrt(1.0-a2*a2)));
 END;
 FUNCTION ArcusTangens(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   ArcusTangens:=Round(System.ArcTan(a2));
 END;
 FUNCTION ArcusSekans(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   ArcusSekans:=Round(System.ArcTan(System.Sqrt(a2*a2-1)));
 END;
 FUNCTION ArcusCosekans(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   ArcusCosekans:=Round(System.ArcTan(1 / System.Sqrt(a2*a2-1)));
 END;
 FUNCTION ArcusCotangens(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   ArcusCotangens:=Round(1.57096-System.ArcTan(a2));
 END;
 FUNCTION SinusHyp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   SinusHyp:=Round((System.exp(a2) - System.exp(-a2)) / 2);
 END;
 FUNCTION CosinusHyp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   CosinusHyp:=Round((System.exp(a2) + System.exp(-a2)) / 2);
 END;
 FUNCTION TangensHyp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   TangensHyp:=Round((System.exp(a2) - System.exp(-a2)) / (System.exp(a2) + System.exp(-a2)));
 END;
 FUNCTION SekansHyp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   SekansHyp:=Round(2 / (System.Exp(a2)+System.Exp(-a2)));
 END;
 FUNCTION CosekansHyp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   CosekansHyp:=Round(2 / (System.Exp(a2)-System.Exp(-a2)));
 END;
 FUNCTION CotangensHyp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   CotangensHyp:=Round((System.exp(a2) + System.exp(-a2)) / (System.exp(a2) - System.exp(-a2)));
 END;
  { *** Berechnet den AreaSinusHyperbolikus einer Zahl *** }
 FUNCTION AreaSinusHyp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   AreaSinusHyp:=Round(System.Ln(a2+System.Sqrt(a2*a2+1)));
 END;
  { *** Berechnet den AreaCosinusHyperbolikus einer Zahl *** }
 FUNCTION AreaCosinusHyp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   AreaCosinusHyp:=Round(System.Ln(a2+System.Sqrt(a2*a2-1)));
 END;
  { *** Berechnet den AreaTangensHyperbolikus einer Zahl *** }
 FUNCTION AreaTangensHyp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   AreaTangensHyp:=Round(System.Ln((1+a2)/(1-a2)) / 2);
 END;
  { *** Berechnet den AreaSekansHyperbolikus einer Zahl *** }
 FUNCTION AreaSekansHyp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   AreaSekansHyp:=Round(System.Ln((System.Sqrt(-a2*a2+1)+1) / a2));
 END;
  { *** Berechnet den AreaCosekansHyperbolikus einer Zahl *** }
 FUNCTION AreaCosekansHyp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   AreaCosekansHyp:=Round(System.Ln((MySgn(a2)*System.Sqrt(a2*a2+1)+1) / a2));
 END;
  { *** Berechnet den AreaCotangensHyperbolikus einer Zahl *** }
 FUNCTION AreaCotangensHyp(a,b: RechType): RechType; FAR;
   VAR a2: EXTENDED;
 BEGIN
   a2:=a;
   AreaCotangensHyp:=Round(System.Ln((a2+1)/(a2-1)) / 2);
 END;

{ *** Initialisierung *** }
BEGIN
  {'+','-','*','/','^','AND','OR','XOR':}
  MyF[ 1]:=Plus;
  MyF[ 2]:=Minus;
  MyF[ 3]:=Mul;
  MyF[ 4]:=Divi;
  MyF[ 5]:=Pot;
  MyF[ 6]:=Root;
  MyF[ 7]:=Und;
  MyF[ 8]:=Oder;
  MyF[ 9]:=ExOder;
  {'NOT','SQR','SQRT','LN','LG','LB','LOG','EXP','SGN':}
  MyF[10]:=Nicht;
  MyF[11]:=SQR;
  MyF[12]:=SQRT;
  MyF[13]:=Ln;
  MyF[14]:=Lg;
  MyF[15]:=Lb;
  MyF[16]:=Log;
  MyF[17]:=Exp;
  MyF[18]:=Sgn;
  {Trigonometrische Funktionen:}
  MyF[19]:=Sinus;
  MyF[20]:=Cosinus;
  MyF[21]:=Tangens;
  MyF[22]:=Sekans;
  MyF[23]:=Cosekans;
  MyF[24]:=Cotangens;
  MyF[25]:=ArcusSinus;
  MyF[26]:=ArcusCosinus;
  MyF[27]:=ArcusTangens;
  MyF[28]:=ArcusSekans;
  MyF[29]:=ArcusCosekans;
  MyF[30]:=ArcusCotangens;
  {Hyperbolische Funktionen:}
  MyF[31]:=SinusHyp;
  MyF[32]:=CosinusHyp;
  MyF[33]:=TangensHyp;
  MyF[34]:=SekansHyp;
  MyF[35]:=CosekansHyp;
  MyF[36]:=CotangensHyp;
  MyF[37]:=AreaSinusHyp;
  MyF[38]:=AreaCosinusHyp;
  MyF[39]:=AreaTangensHyp;
  MyF[40]:=AreaSekansHyp;
  MyF[41]:=AreaCosekansHyp;
  MyF[42]:=AreaCotangensHyp;
END.