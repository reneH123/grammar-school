========================================================================
       WIN32-ANWENDUNG : socket
========================================================================


Diese socket-Anwendung hat der Anwendungs-Assistent f�r Sie erstellt.  

Diese Datei enth�lt eine Zusammenfassung dessen, was Sie in jeder der Dateien
finden, die Ihre socket-Anwendung bilden.

socket.cpp
    Dieses ist die Hauptquellcodedatei der Anwendung.

socket.dsp
    Diese Datei (Projektdatei) enth�lt Informationen auf Projektebene und wird zur
    Erstellung eines einzelnen Projekts oder Teilprojekts verwendet. Andere Benutzer k�nnen
    die Projektdatei (.dsp) gemeinsam nutzen, sollten aber die Makefiles lokal exportieren.
	

/////////////////////////////////////////////////////////////////////////////
Der Anwendungs-Assistent hat die folgenden Ressourcen erstellt:

socket.rc
    Dies ist die Liste aller Microsoft Windows-Ressourcen, die das Programm verwendet.
    Sie umfa�t die Symbole, Bitmaps und Cursor, die im Unterverzeichnis RES
    gespeichert werden.  Diese Datei kann direkt in Microsoft Visual C++
	bearbeitet werden.

res\socket.ico
    Dies ist eine Symboldatei, die als Anwendungssymbol (32x32) verwendet wird.
    Dies Symbol ist in der Hauptressourcendatei socket.rc enthalten.

small.ico
    %%Dies ist eine Symboldatei, die eine kleinere Version (16x16)
	des Anwendungssymbols enth�lt. Dieses Symbol ist in der
	Hauptressourcendatei socket.rc enthalten.

/////////////////////////////////////////////////////////////////////////////
Weitere Standarddateien:

StdAfx.h, StdAfx.cpp
    Diese Dateien werden zum Erstellen einer vorkompilierten Header-Datei (PCH) namens
    socket.pch und einer vorkompilierten Typdatei namens StdAfx.obj verwendet.

Resource.h
    Dies ist die Standard-Header-Datei, die neue Ressourcen-IDs definiert.
    Microsoft Visual C++ liest und aktualisiert diese Datei.

/////////////////////////////////////////////////////////////////////////////
Weitere Hinweise:

Der Anwendungs-Assistent verwendet "ZU ERLEDIGEN:", um Bereiche des Quellcodes zu
kennzeichnen, die Sie hinzuf�gen oder anpassen sollten.


/////////////////////////////////////////////////////////////////////////////
