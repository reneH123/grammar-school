#include <math.h>

class Vector3D
{
public:
	float x;
	float y;
	float z;
	Vector3D(){
		x=y=z=0;
	}
	Vector3D(float x, float y, float z){
		this->x = x;
		this->y = y;
		this->z = z;
	}
	Vector3D& operator= (Vector3D v){
		x = v.x;
		y = v.y;
		z = v.z;
		return *this;
	}
	Vector3D operator+ (Vector3D v){
		return Vector3D(x + v.x, y + v.y, z + v.z);
	}
	Vector3D operator- (Vector3D v){
		return Vector3D(x - v.x, y - v.y, z - v.z);
	}
	Vector3D operator* (float value){
		return Vector3D(x * value, y * value, z * value);
	}
	float operator* (Vector3D v){
		return x*v.x+y*v.y+z*v.z;
	}
	Vector3D operator% (Vector3D v){
		return Vector3D(y*v.z-v.y*z,z*v.x-x*v.z,x*v.y-v.x*y);
	}
	Vector3D operator/ (float value){
		return Vector3D(x / value, y / value, z / value);
	}
	Vector3D& operator+= (Vector3D v){
		x += v.x;
		y += v.y;
		z += v.z;
		return *this;
	}
	Vector3D& operator-= (Vector3D v){
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return *this;
	}
	Vector3D& operator*= (float value){
		x *= value;
		y *= value;
		z *= value;
		return *this;
	}
	Vector3D& operator/= (float value){
		x /= value;
		y /= value;
		z /= value;
		return *this;
	}
	Vector3D operator- (){
		return Vector3D(-x, -y, -z);
	}
	float length(){
		return sqrtf(x*x + y*y + z*z);
	};			   		
	void unitize(){
		float length = this->length();

		if (length == 0)
			return;

		x /= length;
		y /= length;
		z /= length;
	}
	Vector3D unit(){
		float length = this->length();

		if (length == 0)
			return *this;
		
		return Vector3D(x / length, y / length, z / length);
	}
};

class Mass
{
public:
	float m;
	Vector3D pos;
	Vector3D vel;
	Vector3D force;

	Mass(float m){
		this->m = m;
	}
	void applyForce(Vector3D force){
		this->force += force;
	}
	void simulate(float dt){
		vel += (force / m)*dt; 
		pos += vel * dt;
	}
};