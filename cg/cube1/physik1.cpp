#include <iostream.h>
#include <conio.h>
#include <GL/glut.h>

#include "cube.hpp"

Cube *cube,*cube2;

/* liefert 0 f�r <= 0
 *         1 f�r >0 zur�ck
 */
inline int sign(float a){
	return (a>0)?1:0;
}

void idle(){
	Vector3D v,n,pos_t0,pos_t1;

	// gegenseitige Kollisionen
    v=cube->pos-cube2->pos;
	float dist=v.length();
	if (dist<=2*1.4142){
		cout<<"Kollision mit anderem Objekt aufgetreten! dist="<<dist<<endl;
		cube2->applyForce(cube->force);
	}
	// Untergrund-NV
	n=Vector3D(0,1,0); 
	// W�rfel 1; Kollision mit Boden
	pos_t0=cube->pos;
	cube->simulate(0.008);
	pos_t1=cube->pos;
	if (sign((pos_t0-Vector3D(0,2,0))*n)!=sign((pos_t1-Vector3D(0,2,0))*n)){
		cout<<"Kollision aufgetreten!"<<endl;
		cube->pos=pos_t1-Vector3D(0,0.1,0); // besser iteratives Ann�hern
		v=cube->vel;
		cube->vel=(((n*(((-v)*n)/(n*n)))+v)*2)-v;
	}
	// W�rfel 2
	pos_t0=cube2->pos;
	cube2->simulate(0.008);
	pos_t1=cube2->pos;
	if (sign((pos_t0-Vector3D(0,2,0))*n)!=sign((pos_t1-Vector3D(0,2,0))*n)){
		cube2->pos=pos_t1-Vector3D(0,0.1,0);
		v=cube2->vel;
		cube2->vel=(((n*(((-v)*n)/(n*n)))+v)*2)-v;
	}

	glutPostRedisplay();
}

void reshape(int w,int h)
{
	glViewport(0,0,(GLsizei)w,(GLsizei)h);
	glMatrixMode(GL_PROJECTION);  //....
	glLoadIdentity();
	gluPerspective(40.0,1.0,1.5,1200.0);
	glMatrixMode(GL_MODELVIEW); //
}

void keyboard(unsigned char key, int x, int y){
   // alle Nicht-Funktionstasten
   switch (key){
		case 27: exit(0); 
		// g bedeutet Gravitation an
		case 103:
			cube->applyForce(Vector3D(0,cube->m*9.81,0));
			cube2->applyForce(Vector3D(0,cube2->m*9.81,0));
		break;
        // k bedeutet kollidieren lassen (=Krafteinwirkung)
		case 107: 
			cube->applyForce(Vector3D(0.3,0,0));
		break;
		// l
		case 108: 
			cube->applyForce(Vector3D(-0.3,0,0));
		break;
		case 114:  // "r"eset
			cube->setpos(3,0,0);
			cube->force=cube->vel=Vector3D();
			cube2->setpos(-3,0,0);
			cube2->force=cube2->vel=Vector3D();
		break;
		default: cout<<"Es wurde "<<key << (int)key<<" gedr�ckt!"<<endl; break;
   }
   glutPostRedisplay();
}

void special(int k, int x, int y)
{
  switch (k) {
  case GLUT_KEY_UP:
	  glTranslatef(0,0,1);
    break;
  case GLUT_KEY_DOWN:
	  glTranslatef(0,0,-1);
    break;
  case GLUT_KEY_LEFT:
	  glTranslatef(1,0,0);
    break;
  case GLUT_KEY_RIGHT:
	  glTranslatef(-1,0,0);
    break;
  case GLUT_KEY_PAGE_UP:
	  glRotatef(2,1,0,0);
	break;
  case GLUT_KEY_PAGE_DOWN:
	  glRotatef(-2,1,0,0);
	break;
  default:
    return;
  }
}

void display(){
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	// Untergrund
	glBegin(GL_TRIANGLE_STRIP);
	 glVertex3f(-10,-3,-10);
	 glVertex3f(10,-3,-10);
	 glVertex3f(10,-3,10);
	 glVertex3f(-10,-3,10);
	glEnd();
	// W�rfel
	cube->display();
	cube2->display();
	glFlush(); 
	glutSwapBuffers();
}

void init(){
	glTranslatef(0.0f,-2.0f,-20.0f);	
	glEnable(GL_TEXTURE_2D);
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glFrontFace(GL_CW);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	cube=new Cube(3,-7,0,0.3,0.2,0.4,0.8,"b.bmp");
	cube->applyForce(Vector3D(0,9.81*cube->m,0));
	cube2=new Cube(-3,-6,0,0,0.1,0,2000,"Crate.bmp");
}

void done(){
	delete cube;
	delete cube2;
}

void main(int argc, char** argv){
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DEPTH|GLUT_DOUBLE|GLUT_RGBA);
	glutCreateWindow("Physik1 Simulator");
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(special);
	init();
	glutIdleFunc(idle);
	glutMainLoop();
	done();
}