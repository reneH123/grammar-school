#include <stdio.h>
#include <GL/glaux.h>

#include "physics.hpp"

AUX_RGBImageRec *LoadBMP(char *fname){
	FILE *f;
	if ((f=fopen(fname,"r"))==NULL){
		return NULL;
	}
	fclose(f);
	return auxDIBImageLoad(fname);
}

class Cube: public Mass{
	private:
		GLuint texture[1];
		GLfloat alpha,beta,gamma;
		GLfloat	xrot,yrot,zrot;

	int LoadGLTextures(char *fname){
		int status=FALSE;
		AUX_RGBImageRec *TextureImage[1];
		memset(TextureImage,0,sizeof(void*));
		if (TextureImage[0]=LoadBMP(fname)){
			status=TRUE;
			glGenTextures(1,&texture[0]);
			glBindTexture(GL_TEXTURE_2D,texture[0]);
			glTexImage2D(GL_TEXTURE_2D,0,3,TextureImage[0]->sizeX,TextureImage[0]->sizeY,0,GL_RGB,GL_UNSIGNED_BYTE,TextureImage[0]->data);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);	// Linear Filtering
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);	// Linear Filtering
		}
		if (TextureImage[0])							// If Texture Exists
		{
			if (TextureImage[0]->data)					// If Texture Image Exists
				free(TextureImage[0]->data);				// Free The Texture Image Memory
			free(TextureImage[0]);						// Free The Image Structure
		}
		return status;
	}

	public:
	Cube(GLfloat posx, GLfloat posy, GLfloat posz,
		 GLfloat alpha,GLfloat beta,GLfloat gamma, 
		 float masse, char *fname): Mass(masse){
		if (!LoadGLTextures(fname))
			exit(-11);
		this->alpha=alpha;
		this->beta=beta;
		this->gamma=gamma;
		setpos(posx,posy,posz);
		xrot=yrot=zrot=0;
	}
	void setpos(GLfloat posx, GLfloat posy, GLfloat posz){
		this->pos=Vector3D(posx,posy,posz);
	}
	void display(){
		glPushMatrix();		
		 glTranslatef(-pos.x,-pos.y,-pos.z);
		 glRotatef(xrot,1.0f,0.0f,0.0f);						// Rotate On The X Axis
		 glRotatef(yrot,0.0f,1.0f,0.0f);						// Rotate On The Y Axis
		 glRotatef(zrot,0.0f,0.0f,1.0f);
		 xrot+=alpha;
		 yrot+=beta;
		 zrot+=gamma;

		 glBindTexture(GL_TEXTURE_2D, texture[0]);				// Select Our Texture
		 // Front Face
		 glBegin(GL_QUADS);
		  glNormal3f(1, 0, 0);
		  glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);	// Bottom Left Of The Texture and Quad
		  glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);	// Bottom Right Of The Texture and Quad
		  glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  1.0f);	// Top Right Of The Texture and Quad
		  glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  1.0f);	// Top Left Of The Texture and Quad
		 glEnd();
		 glBegin(GL_LINES);
		  glVertex3f(0,0,1);
		  glVertex3f(0,0,2);
		 glEnd();
		 // Back Face
		 glBegin(GL_QUADS);
		  glNormal3f(-1, 0, 0);
		  glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f, -1.0f);	// Bottom Right Of The Texture and Quad
		  glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);	// Top Right Of The Texture and Quad
		  glTexCoord2f(0.0f, 1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);	// Top Left Of The Texture and Quad
		  glTexCoord2f(0.0f, 0.0f); glVertex3f( 1.0f, -1.0f, -1.0f);	// Bottom Left Of The Texture and Quad
		 glEnd();
		 // Top Face
		 glBegin(GL_QUADS);
		  glNormal3f(1, 0, 0);
		  glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);	// Top Left Of The Texture and Quad
		  glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f,  1.0f,  1.0f);	// Bottom Left Of The Texture and Quad
		  glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f,  1.0f,  1.0f);	// Bottom Right Of The Texture and Quad
		  glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);	// Top Right Of The Texture and Quad
		 glEnd();
		 // Bottom Face
		 glBegin(GL_QUADS);
		  glNormal3f(-1, 0, 0);
		  glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f, -1.0f, -1.0f);	// Top Right Of The Texture and Quad
		  glTexCoord2f(0.0f, 1.0f); glVertex3f( 1.0f, -1.0f, -1.0f);	// Top Left Of The Texture and Quad
		  glTexCoord2f(0.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);	// Bottom Left Of The Texture and Quad
		  glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);	// Bottom Right Of The Texture and Quad
		 glEnd();
		 // Right face
		 glBegin(GL_QUADS);
		  glNormal3f(1, 0, 0);
		  glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f, -1.0f, -1.0f);	// Bottom Right Of The Texture and Quad
		  glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);	// Top Right Of The Texture and Quad
		  glTexCoord2f(0.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  1.0f);	// Top Left Of The Texture and Quad
		  glTexCoord2f(0.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);	// Bottom Left Of The Texture and Quad
		 glEnd();
		 // Left Face
		 glBegin(GL_QUADS);
		  glNormal3f(-1, 0, 0);
		  glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f, -1.0f);	// Bottom Left Of The Texture and Quad
		  glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);	// Bottom Right Of The Texture and Quad
		  glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  1.0f);	// Top Right Of The Texture and Quad
 		  glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);	// Top Left Of The Texture and Quad
 		 glEnd();
		 glTranslatef(pos.x,pos.y,pos.z);
		glPopMatrix();
	}
};