// = detect.h
#ifndef DetectDef
#define DetectDef

// gamatypes.h
#ifndef HEADER_GamaTypes_HEADER
#define HEADER_GamaTypes_HEADER

#include <climits>

	//
	// 8 bit types.
	//
	typedef signed   char int8_t;
	typedef unsigned char uint8_t;

	//
	// 16 bit types.
	//
#if	UINT_MAX == 0xffffU
	// The compiler has 16 bit integers.
	typedef 		 int int16_t;
	typedef unsigned int uint16_t;

#elif USHRT_MAX == 0xffffU
	// The compiler has 16 bit short integers.
	typedef 		 short int int16_t;
	typedef unsigned short int uint16_t;
#else
	#error Cannot define 16 bit types: int16_t, uint16_t.
#endif

	//
	// 32 bit types.
	//
#if	UINT_MAX == 0xffffffffUL
	// The compiler has 32 bit integers.
	typedef			 int int32_t;
	typedef unsigned int uint32_t;

#elif ULONG_MAX == 0xffffffffUL
	// The compiler has 32 bit long integers.
	typedef			 long int32_t;
	typedef unsigned long uint32_t;

#else
	#error Cannot define 32 bit types: int32_t, uint32_t.
#endif

	//
	// 64 bit types
	//
#if defined(_MSC_VER)
	// Microsoft compilers have built-in MS specific 64 bit types.
	typedef          __int64 int64_t;
	typedef unsigned __int64 uint64_t;

#else
	#error Cannot define 64 bit types: int64_t, uint64_t.
#endif
//
	// Generic types.
	//
	typedef unsigned int uint_t;

#endif
// ENDE

#include "string"		// Standard string

//==============================================================================
#define CPU_CPUID		0x00000001		//set if the processor supports CPUID instruction
#define CPU_FPUPRESENT	0x00000002		//set if the processor as an FPU
#define	CPU_CACHEINFO	0x00000004		//set if the processor can give cache info
#define CPU_MMX			0x00000008		//set is MMX is present
#define CPU_CMOV		0x00000010		//set if CMOV instructions are present
#define CPU_APIC		0x00000020		//set if APIC is present
#define CPU_RDTSC		0x00000040		//set if RDTSC and CR4 enable are supported
#define CPU_CMPXCHG8B	0x00000080		//set if the CMPXCHG8B instruction is supported
#define CPU_DEBUG		0x00000200		//set if debug extensions are present
#define CPU_VME			0x00000400		//set if virtual mode extensions are present
#define CPU_PSE			0x00000800		//set if page size extensions are present
#define CPU_MSR			0x00001000		//set if Model Specific Registers are present
#define CPU_PAE			0x00002000		//set if Physical Address Extensions are present
#define CPU_SEP			0x00004000		//set if Fast system calls are present
#define CPU_GPE			0x00008000		//set if global paging extentions are present
#define CPU_MCA			0x00010000		//set if machine check architecture is present
#define CPU_PAT			0x00020000		//set if page attribute table feature is present
#define CPU_MCE			0x00040000		//set if machine check exceptions are present
#define CPU_MTRR		0x00080000		//set if memory type range registers are present
#define CPU_SIMD        0x00100000      //set if SIMD instructions are present
#define CPU_SIMD_ENABLE 0x00200000      //set if the SIMD instructions are enabled and the OS supports them
#define CPU_SIMD_EXCEPT 0x00400000      //set if the SIMD exceptions are enabled and the OS supports them
#define CPU_SNR         0x00800000      //set if the processor has a serial number
#define CPU_PSE36		0x01000000		//set if the processor can access above 4GB

#define CPU_PENTIUM		0x10000000		// processor will run Pentium code
#define CPU_PENTIUMII	0x20000000		// processor will run Pentium pro code (this includes MMX)
#define CPU_PENTIUMIII	0x80000000		// processor will run Streaming SIMD code
#define CPU_3DNOW		0x40000000		// processor will run 3DNow code

//==============================================================================
// Remarks:
//  Class to detect the make, model and type of all processors present in the
//  machine.
class detect
{
public:
    //------------------------------------------------------------------------------
    // Remarks:
    //  A private structure to hold the 4 resulting registers from a CPUID call
    struct Registers
    {
        uint32_t    RegEAX;
        uint32_t    RegEBX;
        uint32_t    RegEDX;
        uint32_t    RegECX;
    };


	//------------------------------------------------------------------------------
	// Remarks:
	//	Enum of known x86 processor manufacturers.
	enum ProcessorManufacturer
	{
		Intel						=	0,
		AMD							=	1,
		Cyrix						=	2,
		IDT							=	3,
		Unknown_Manufacturer		=	0xffffffff
	};


    //------------------------------------------------------------------------------
    // Remarks:
    //  Enum of all known processor families
    enum ProcessorFamily
    {
        Generic_Architecture3   = 3,
        Generic_Architecture4   = 4,
        Generic_Architecture5   = 5,
        Generic_Architecture6   = 6,
        Intel_80386             = 100,      // This will never get returned
        Intel_80486             = 101,
        Intel_80486_SL          = 102,
        Intel_80486_DX2         = 103,
        Intel_80486_DX4         = 104,
        Intel_Pentium           = 105,
        Intel_PentiumMMX        = 106,
        Intel_PentiumPro        = 107,
        Intel_PentiumII         = 108,
        Intel_PentiumIIXeon     = 109,
        Intel_PentiumIII        = 110,
        Intel_PentiumIIIXeon    = 111,
        Intel_Celeron           = 112,
        Intel_CeleronA          = 113,
        Intel_Unknown           = 199,
        AMD_AM486               = 200,
        AMD_K5					= 201,
        AMD_K6					= 202,
        AMD_K62					= 203,
        AMD_K63					= 204,
        AMD_K7					= 205,
        AMD_Unknown				= 299,
        Cyrix_MediaGX           = 300,
        Cyrix_GXm               = 301,
        Cyrix_686				= 302,
        Cyrix_686MX				= 303,
        Cyrix_Unknown			= 399,
        IDT_WINCHIPC6			= 400,
        IDT_WINCHIP2			= 401,
        IDT_Unknown				= 499,
        Unknown_Family			= 0xffffffff
    };

    //------------------------------------------------------------------------------
    // Remarks:
    //  Structure to hold info on the level 1 and 2 caches as well as the TLBs. Cache
    //  and TLB code elements are only set if the particular cache type is not unified.
    struct ProcessorCacheInfo
    {
        bool        UnifiedLevel1;
        bool        UnifiedLevel2;
        bool        UnifiedTLB;

        // Level 1 data cache info
        union
        {
            uint32_t    Level1DataCacheSize;
            uint32_t    Level1UnifiedCacheSize;
        };
        union
        {
            uint32_t    Level1DataCacheLineWidth;
            uint32_t    Level1UnifiedCacheLineWidth;
        };
        union
        {
            uint32_t    Level1DataCacheAssoc;
            uint32_t    Level1UnifiedCacheAssoc;
        };

        // Level 1 code cache info, these elements are unsed if the
        // level 1 cache is unifies
        uint32_t    Level1CodeCacheSize;
        uint32_t    Level1CodeCacheLineWidth;
        uint32_t    Level1CodeCacheAssoc;

        // Level 2 cache info
        union
        {
            uint32_t    Level2DataCacheSize;
            uint32_t    Level2UnifiedCacheSize;
        };
        union
        {
            uint32_t    Level2DataCacheLineWidth;
            uint32_t    Level2UnifiedCacheLineWidth;
        };
        union
        {
            uint32_t    Level2DataCacheAssoc;
            uint32_t    Level2UnifiedCacheAssoc;
        };

        // Level 2 code cache info, unused if level 2 is unified
        uint32_t    Level2CodeCacheSize;
        uint32_t    Level2CodeCacheLineWidth;
        uint32_t    Level2CodeCacheAssoc;

        //Data TLB
        union
        {
            uint32_t    DataTLBEntries4K;
            uint32_t    UnifiedTLBEntries4K;
        };
        union
        {
            uint32_t    DataTLBAssoc4K;
            uint32_t    UnifiedTLBAssoc4K;
        };
        union
        {
            uint32_t    DataTLBEntries4M;
            uint32_t    UnifiedTLBEntries4M;
        };
        union
        {
            uint32_t    DataTLBAssoc4M;
            uint32_t    UnifiedTLBAssoc4M;
        };

        //Code TLB
        uint32_t    CodeTLBEntries4K;
        uint32_t    CodeTLBAssoc4K;
        uint32_t    CodeTLBEntries4M;
        uint32_t    CodeTLBAssoc4M;
    };

    //------------------------------------------------------------------------------
    // Remarks:
    //  A structure to hold the 96bit serial number
    struct SerialNumber
    {
        uint32_t    Lower;
        uint32_t    Middle;
        uint32_t    Upper;
    };

    //------------------------------------------------------------------------------
    detect
    (
    );
    // Description:
    //  Standard constructor


    //------------------------------------------------------------------------------
    ~detect
    (
    );
    // Description:
    //  Standard destructor


    //------------------------------------------------------------------------------
    bool get_number_of_processors
    (
        uint32_t&   number_of_processors,
        uint32_t&   processors_available
    );
    // Description:
    //  Gets the total number of processor available within the machine
    //
    // Returns:
    //  'true' if there was no errors, otherwise false.
    //
    // Post Conditions:
    //  number_of_processors is filled with the number of processors.
    //  processors_available is filled with the number of processors available to
    //  the current process.


    //------------------------------------------------------------------------------
    bool get_cpuid_limits
    (
        uint32_t 			processor, 
        uint32_t& 			normal, 
        uint32_t& 			extended
    );
    // Description:
    //  Gets the normal and extended CPUID ranges for the specified processor
    //
    // Returns:
    //  'true' if there was no errors, otherwise false.
    //
    // Post Conditions:
    //  'normal' is filled with the maximum standard CPUID function
    //  'extended' is filled with the maximum extended CPUID function or zero if
    //  extended CPUID functions are not supported.


    //------------------------------------------------------------------------------
    bool get_cpuid_data
    (
        uint32_t 			processor, 
        uint32_t 			cmd, 
        Registers&	        result
    );
    // Description:
    //  Gets the result of the specified CPUID command on the speicied processor
    //
    // Returns:
    //  'true' if there was no errors, otherwise false.
    //
    // Post Conditions:
    //  'result' is filled with the registers of the CPUID function


    //------------------------------------------------------------------------------
	bool get_manufacturer
	(
		uint32_t 				processor, 
		ProcessorManufacturer& 	manufacturer
	);
	// Description:
	//	Gets the manufacturer of the specified processor if it is known.
	//
    // Returns:
    //  'true' if there was no errors, otherwise false.
    //
    // Post Conditions:
    //  'manufacturer' is filled with the maufacrurer of the specified processor, the
	//	available manufacturers are list in the ProcessorManufacturer above.


    //------------------------------------------------------------------------------
	bool get_features
	(
		uint32_t 				processor, 
		uint32_t&				features
	);
	// Description:
	//	Gets the features of the specified processor
	//
    // Returns:
    //  'true' if there was no errors, otherwise false.
    //
    // Post Conditions:
	// 'features' is filled with the feature bit array, see the CPU_*** defines above

    //------------------------------------------------------------------------------
	bool get_signature_components
	(
		uint32_t 				processor, 
		uint32_t&				family,
		uint32_t&				model,
		uint32_t&				stepping,
		uint32_t&				type
	);
	// Description:
	//	Gets the signature broken down into components
	//
    // Returns:
    //  'true' if there was no errors, otherwise false. 'false' will be returned if
	//	the CPUID instruction is not supported.
    //
    // Post Conditions:
	// 'family'. 'model', 'stepping' and 'type' are filled with the relevent components
	//	of the processor signature.

    //------------------------------------------------------------------------------
	bool get_family
	(
		uint32_t 				processor, 
		ProcessorFamily&		family
	);
	// Description:
	//	Gets the family of the specified processor
	//
    // Returns:
    //  'true' if there was no errors, otherwise false.
    //
    // Post Conditions:
	// 'family' is filled with an entry in the ProcessorFamily enum above


    //------------------------------------------------------------------------------
	bool get_cache_info
	(
		uint32_t 				processor, 
		ProcessorCacheInfo&		cache
	);
	// Description:
	//	Gets info on the processor caches and TLBs
	//
    // Returns:
    //  'true' if there was no errors, otherwise false.
    //
    // Post Conditions:
	// 'cache' is filled with all known info about the various caches, see structure
	// contents. An unknown item in the structure is set to zero and a full associative
	// item is set to 0xffffffff.


    //------------------------------------------------------------------------------
	bool get_processor_name
	(
		uint32_t 			processor, 
		std::string& 		name
	);
	// Description:
	//	Gets a textual name for the processor in readable form
	//
    // Returns:
    //  'true' if there was no errors, otherwise false.
    //
    // Post Conditions:
	// 'string' is filled with the name of the processor.


    //------------------------------------------------------------------------------
	bool get_clock_frequency
	(
		uint32_t 			processor, 
		float&       		freq
	);
	// Description:
	//	Gets the clock speed of the processor in MHz
	//
    // Returns:
    //  'true' if there was no errors, otherwise false.
    //
    // Post Conditions:
	// 'freq' is filled with the clock speed in MHz


    //------------------------------------------------------------------------------
    bool get_serial_number
    (
        uint32_t            processor, 
        SerialNumber&       snr
    );
    // Description:
    //  Gets the serial number if one is present
    //
    // Returns:
    //  'true' if there was no errors, otherwise false. 'false' is returned if the
    //  processor has no serial number
    //
    // Post Conditions:
    //  'snr' is filled with the processors 96bit serial number


    //------------------------------------------------------------------------------
    bool get_serial_number
    (
        uint32_t            processor, 
        std::string&        snr_string
    );
    // Description:
    //  Gets the serial number for the processor as a string in the Intel recommeded
    //  format.
    //
    // Returns:
    //  'true' if there was no errors, otherwise false.
    //
    // Post Conditions:
    //  'snr_string' is filled with the serial number in string format.


private:

    //------------------------------------------------------------------------------
    // Remarks:
    //  A private structure to hold a variable amount of CPUID data.
    struct ProcCPUID
    {
        uint32_t    MaxStandardCPUID;
        uint32_t    MaxExtendedCPUID;
        Registers*  StandardCPUIDRegisters;
        Registers*  ExtendedCPUIDRegisters;
    };

    //------------------------------------------------------------------------------
    // Remarks:
    //  A private structure that holds decoded info about each processor
    struct ProcInfo
    {
        bool                    CPUIDPresent;   // true if the instruction is present
        uint32_t                NoCPUIDFamily;  // If cpuid is not present this is the family
        ProcessorManufacturer   NoCPUIDMake;    // manufacturer of the no cpuid processor
        float                   ClockSpeed;     // Speed in MHz of this processor
    };


    //------------------------------------------------------------------------------
    static uint32_t __stdcall detect_thread_entry_stub
    (
        void*   param
    );
    // Description:
    //  Static member called by Windows for the thread entry.
    //
    // Returns:
    //  Result of class_thread_entry() function

    //------------------------------------------------------------------------------
    uint32_t count_bits
    (
        uint32_t    data
    );
    // Description:
    //  Counts the number of bits set in a 32 bit value
    //
    // Returns:
    //  Returns the number of bits set in 'data'

    //------------------------------------------------------------------------------
    float measure_clock_speed
    (
    );
    // Description:
    //  Gets the clock speed of the processor. On a multiple processor machine this
    //  should only be called once the calling thread affinity has been set to a
    //  single processor.
    //
    // Returns:
    //  Clock speed in MHz.

    //------------------------------------------------------------------------------
    bool detect_cpuid_instruction
    (
        uint32_t&                   family,
        ProcessorManufacturer&      make
    );
    // Description:
    //  Detects the presence of the CPUID instruction
    //
    // Returns:
    //  'true' if the instruction is present.
    //
    // Post Conditions:
    //  'family' will hold the family of the processor if CPUID is not supported, ie,
    //  the function returned false. 'make' is filled with the manufacturer of the
    //  the processor if it can be determined. If the CPUID instruciton is supported the
    //  'family' and 'make' references is undefined.

  
    //------------------------------------------------------------------------------
    void get_cpuid_info
    (
    );
    // Description:
    //  Allocates memory and calls every CPUID function on the processor and stores
    //  it locally.

	//------------------------------------------------------------------------------
	bool detect_fpu
	(
	);
	// Description:
	//	Detects the presence of an fpu without using CPUID, this is used on processors
	//	where CPUID is not present.
	//
	// Returns:
	//	'true' if an FPU is present and working, otherwise false.

	//------------------------------------------------------------------------------
    bool detect_simd_exception_support
    (
    );
	// Description:
	//	Detects the ability of the operating system to detect SIMD exceptions.
	//
	// Returns:
	//	'true' if SIMD exceptions are supported, otherwise 'false' if SIMD exceptions
    //  are reported as invalid op-codes.

	//------------------------------------------------------------------------------
	uint32_t get_extended_cpuid_info
	(
	);
	// Description:
	//	Detects the presence of the extended CPUID functions.
	//
	// Returns:
	//	The highest supported extended CPUID function or zero.
	//
	// Notes:
	//	Call this function from within a try/except block just in case in generates
	//	an exception.

	//------------------------------------------------------------------------------
	void detect_standard_features
	(
		uint32_t	processor,
		uint32_t&	features
	);
	// Description:
	//	Gets the standard features of the processor and puts them into 'features'


	//------------------------------------------------------------------------------
	void detect_extended_features
	(
 		uint32_t	processor,
		uint32_t&	features
	);
	// Description:
	//	Gets the extended features of the processor and puts them into 'features'


	//------------------------------------------------------------------------------
	void detect_simd_features
	(
 		uint32_t	processor,
		uint32_t&	features
	);
	// Description:
	//	Gets the the SIMD features and relevent OS support


    //------------------------------------------------------------------------------
	void decode_intel_signature
	(
		uint32_t			processor,
		ProcessorFamily&	family
	);
    // Description:
    //  This function will decode the signature of an Intel procesor.


    //------------------------------------------------------------------------------
	void decode_amd_signature
	(
		uint32_t			processor,
		ProcessorFamily&	family
	);
    // Description:
    //  This function will decode the signature of an AMD procesor.


    //------------------------------------------------------------------------------
	void decode_cyrix_signature
	(
		uint32_t			processor,
		ProcessorFamily&	family
	);
    // Description:
    //  This function will decode the signature of an Cyrix procesor.


    //------------------------------------------------------------------------------
	void decode_idt_signature
	(
		uint32_t			processor,
		ProcessorFamily&	family
	);
    // Description:
    //  This function will decode the signature of an IDT procesor.


    //------------------------------------------------------------------------------
    uint32_t class_thread_entry
    (
    );
    // Description:
    //  Class based thread entry function, called by the above.
    //
    // Returns:
    //  returns non zero if all went well, otherwise zero is returned.

    // data

    bool        Setup;                  // 'true' if all setup went well.
    uint32_t    Processors;             // number of processors in the machine
    uint32_t    ProcessProcessors;      // number of processors available to this process
    uint32_t    ActiveProcessorMask;    // bit mask of the available system processors
    uint32_t    ProcessAffinityMask;    // bit mask of processors available to this process

    ProcCPUID*  ProcessorData[32];      // an array of pointer to processor data blocks containing raw cpuid data
    ProcInfo*   ProcessorInfo[32];      // disseminated cpuid data returned by the calling functions
    uint32_t    CurrentProcessor;       // used by the thread detect function
    uint32_t    CurrentProcessorElement;// used by the thread detect function

    friend uint32_t __stdcall detect_thread_entry_stub(void*);
};


#endif