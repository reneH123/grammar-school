#include "mathlib.hpp"
#include "detect.hpp"
#include "../Bin/common.hpp"

typedef struct _var{
	char * name;
	char * value;
	char * defvalue;
	int flags;
	_var * next;
}var;
var *keys;
int numkeys;

void CreateKey(char *name, char *value, int flags){
	var *v;
	v= new var;
	v->next=keys;
	keys=v;
	keys->name=new char[strlen(name)+1];
	strcpy(keys->name,name);
	keys->value=new char[strlen(value)+1];
	strcpy(keys->value,value);
	keys->defvalue=new char[strlen(value)+1];
	strcpy(keys->defvalue,value);
	keys->flags=flags;
	numkeys++;
}

void SetKeyValue(char *name, char *value, int flags){
	for(var *k=keys;k;k=k->next){
		if(!strcmp(k->name,name)){
			delete k->value;
			k->value=new char[strlen(value)+1];
			strcpy(k->value,value);
			k->flags=flags;
			return;
		}
	}
	CreateKey(name, value, flags);
}

char *ValueForKey(char *name){
	for(var *k=keys;k;k=k->next){
		if(!strcmp(k->name,name)){
			return k->value;
		}
	}
	return "";
}

void ProcessorDetect(tlist<char *> &subtle)
//  < Zahlen stehen immer am Ende einer gedachten Zeile!>
{
	detect  processors;     // Create a detect class
    // Display the number of processors available to the system and the current process
    //
    uint32_t    system_processors;
    uint32_t    processors_available;

    processors.get_number_of_processors(system_processors,processors_available);
	
	subtle.push_back(">--- Number of system processors: ");
	subtle.push_back(Convert->float2string((float)system_processors));
	subtle.push_back(">--- Number of available process processors: ");
	subtle.push_back(Convert->float2string((float)processors_available));
    //
    // Display info on each processor available to this process
    //
    uint32_t    current_processor = 0;
    while (current_processor<processors_available){
        
		subtle.push_back(">--- Processor: ");
		subtle.push_back(Convert->float2string((float)current_processor+1));
		//
		// Get the manufacturer
		//
		detect::ProcessorManufacturer	manu;
        if (!processors.get_manufacturer(current_processor,manu)){
			subtle.push_back("*** Unexpected Error");
            current_processor++;
            continue;
        }

		switch (manu) {
		case detect::Intel:
			subtle.push_back(">--- Manufacturer: Intel");
			break;
		case detect::AMD:
			subtle.push_back(">--- Manufacturer: AMD");
			break;
		case detect::Cyrix:
			subtle.push_back(">--- Manufacturer: Cyrix");
			break;
		case detect::IDT:
			subtle.push_back(">--- Manufacturer: IDT");
			break;
		case detect::Unknown_Manufacturer:
		default:
			subtle.push_back(">*** Manufacturer: Unknown");
			break;
		}
		//
		// get the signature
		//
		uint32_t	family;
		uint32_t	model;
		uint32_t	stepping;
		uint32_t	type;

		if (!processors.get_signature_components(current_processor,family,model,stepping,type)){
			subtle.push_back(">*** Unexpected Error");
            current_processor++;
            continue;
		}
		subtle.push_back(">--- Signature: family = ");
		subtle.push_back(Convert->float2string((float)family));

		subtle.push_back(">-------------- model = ");
		subtle.push_back(Convert->float2string((float)model));

		subtle.push_back(">-------------- stepping = ");
		subtle.push_back(Convert->float2string((float)stepping));

		subtle.push_back(">-------------- type = ");
		subtle.push_back(Convert->float2string((float)type));
		//
		// get the processor name and family
		//
		//std::string					name;
		detect::ProcessorFamily		proc_family;

//		if (!processors.get_processor_name(current_processor,name)){
			//subtle.push_back(">*** Unexpected Error");
            //current_processor++;
            //continue;
		//}
		if (!processors.get_family(current_processor,proc_family)){
			subtle.push_back(">*** Unexpected Error");
            current_processor++;
            continue;
		}
		subtle.push_back(">--- Processor family ID: ");
		subtle.push_back(Convert->float2string((float)uint32_t(proc_family)));
//		subtle.push_back(concatestrstr("--- Processor name: ", name.c_str()));
		//
		// get the clock speed
		//
        float clock_speed;
        if (!processors.get_clock_frequency(current_processor,clock_speed)){
			subtle.push_back(">*** Unexpected Error");
            current_processor++;
            continue;
        }
		subtle.push_back(">--- Clock speed (MHz): ");
		subtle.push_back(Convert->float2string((float) clock_speed));
		//
		// Get the available features
		//
		uint32_t	features;
        if (!processors.get_features(current_processor,features)){
			subtle.push_back(">*** Unexpected Error");
            current_processor++;
            continue;
        }
		if(features&CPU_MMX) SetKeyValue("mmx", "true",0);
		else SetKeyValue("mmx", "false", 0);
		subtle.push_back(">--- MMX Technology: ");
		subtle.push_back(ValueForKey("mmx"));
		if(features&CPU_3DNOW) SetKeyValue("3dnow", "true", 0);
		else SetKeyValue("3dnow", "false", 0);
		subtle.push_back(">--- 3D-NOW Extensions: ");
		subtle.push_back(ValueForKey("3dnow"));
		if(features&CPU_PENTIUMIII) SetKeyValue("kmi", "true", 0);
		else SetKeyValue("kmi", "false", 0);
		subtle.push_back(">--- KMI Instructions: ");
		subtle.push_back(ValueForKey("kmi"));
        current_processor++;
    }
}