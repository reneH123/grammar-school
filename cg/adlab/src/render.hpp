#ifndef RenderDef
#define RenderDef

#include "vertex.hpp"
#include "window.hpp"
#include "clip.hpp"
#include "common.hpp"

class render: virtual public DDRAW
{
public:
	render(void)
	{
		PixelList = NULL;
		VertexList = NULL;
		Clipper = new clipper;
		PixelList = new tlist<PIXEL>;
	}
	~render(void)
	{
		delete PixelList;
		delete Clipper;
	}
	void SetViewPort(const VERTEX* vp)	// Kameraposition einstellen
	{
		ViewPort = *vp;
	}
	void SetVertexList(tlist<VERTEX>* vl)  // 3D-Punktlist setzen
	{
		VertexList = vl;
	}
	void RenderVertexList(void)		 // Komplette Berechnung + Darstellung
	{
		if (visiblepoly())
		{
			Clipper->SetViewPort(&ViewPort);
			Clipper->SetVertexList(VertexList);
			Clipper->NearPlane3D();		// Clipping 3D: Punkte.z < 0 angepa�t!
			VertexList = Clipper->GetVertexList();
			if (VertexList)
			{
				if (VertexList->getroot())
				{
					TransformVertices2Pixels();
					if (PolygonState() != POLYGON_IN) 
					{
						Clipper->SetPixelList(PixelList);
						Clipper->Polygon();
						PixelList = Clipper->GetPixelList();
					}
					if (PixelList->getroot()!=NULL)
				/*	#ifdef POINTS
					DrawGons();
					#endif
					#ifdef WIREFRAME
					DrawPolygon();
					#endif
					#ifdef MONOCHROMTEXTURED*/
					FillPoly();
/*					#endif
					#ifdef TEXTUREDMAPPED
					FillTexturedPoly();
					#endif*/
				}
			}
		}
	}
	void SetActualVidMem(const vidmem &vm)	// aktuellen Zeichenbereich zuweisen
	{
		VideoMemory = vm;
	}
private:
	VERTEX ViewPort;					 // Betrachter
	tlist<VERTEX>* VertexList;			 // 3D-Punktliste
	tlist<PIXEL>* PixelList;			 // 2D-Punktliste
	clipper* Clipper;					 // 2D & 3D Polygonclipper
	bool visiblepoly(void)				 // ist Polygon sichtbar
	{
		VERTEX v1 = VertexList->getroot()->data,
			   v2 = VertexList->getroot()->nxt->data,
			   v3 = VertexList->getroot()->nxt->nxt->data;
		VERTEX a = v3-v2;
		VERTEX b = v1-v2;
		VERTEX u = a%b;
		float D = u*v2;
	    u.z = -u.z;
		return ((((u*ViewPort)+D)>0)?false:true);
	}
	void TransformVertices2Pixels(void)  // Transformation 3D in 2D Koordinaten
	{
		while (!PixelList->empty())
		{
			PixelList->pop_front();
		}
		tle<VERTEX> *begin = VertexList->getroot();
		while (begin!=NULL)
		{
			float zrecip = 1/(begin->data.z-ViewPort.z);
			PIXEL p = {(int)(((RESX*(ViewPort.x+begin->data.x))*zrecip)+MIDX),
					   (int)(((RESX*(ViewPort.y+begin->data.y))*zrecip)+MIDY)};
			PixelList->push_back(p);
			begin = begin->nxt;
		}
	}
	int PolygonState(void)				 // Polygonlagestatus
	{
		tle<PIXEL> *begin = PixelList->getroot();
		unsigned state = 0, stateold = Clipper->code(PixelList->getroot()->data);
		while (begin!=NULL)
		{
			state|=(Clipper->code(begin->data)!=0)?POLYGON_OUT:POLYGON_IN;
			if ((state!=0)|(state!=stateold)) return POLYGON_OUT;
			begin = begin->nxt;
		}
		return state;
	}
	void FillPoly(void)                  // f�llt Polygon mit Farbe
	{
		tle<PIXEL> *beginpxl = PixelList->getroot();
		PIXEL ymax = beginpxl->data, ymin = ymax;
		tlist<tlist<int>*> pixeledgelist; // Liste der Polygonkanten: Werte f�r je ein X-Wert
		tlist<int> *newpxl;				  // nur tempor�r
		int yminindex = 0;
		// Bestimme max Y / min Y + Erstelle Kantenliste
		int i = 0; // tempor�re Variable nur durch zum Z�hlen der momentane Position in Liste
		while (beginpxl!=NULL)
		{
			if (beginpxl->data.y > ymax.y) ymax = beginpxl->data;					 // Y Max
			if (beginpxl->data.y < ymin.y) { yminindex = i; ymin = beginpxl->data; } // Y Min
			PIXEL point1, point2;
			if (beginpxl->nxt!=NULL) { point1 = beginpxl->data;	point2 = beginpxl->nxt->data; }
			else { point1 = beginpxl->data;	point2 = PixelList->getroot()->data; } // Linie zwischen letztem und erstem Punkt
			// Start: Berechnen aller X-Werte einer Kante
			int divY = point2.y-point1.y, divX = point2.x-point1.x;
			float m = (float)(divY)/(divX); // Achtung div 0 m�glich!
			m = ((m>0.0f)?m:-m);
	        divY = ((divY<0)?-divY:divY);
			newpxl = new tlist<int>;
			for (int y=0; y<(divY+1); y++)
			{
				int newpixelpos = (divX>0)?(point1.x+(int)(y/m)):(point1.x-(int)(y/m));
				newpxl->push_back(newpixelpos);
			}
			pixeledgelist.push_back(newpxl);
			// Ende der Kanten-X-Wert-Berechnungen
			beginpxl = beginpxl->nxt;
			i++;
		}
		int sizeoflist = i-1; // wie lang ist die durchlaufene Liste	
		// Kanten mit gleichen X-Werten verbinden
		int acty = ymin.y;				// Z�hler f�r HorLine
		int pindexleft  = pred(yminindex,sizeoflist),
			pindexright = succ(yminindex,sizeoflist);
		// Kantenlisten laden
		tle<int> *leftedge = pixeledgelist[succ(pindexleft,sizeoflist)]->getroot(); // Zeiger ein Element vorher pos.
		tle<int> *rightedge = pixeledgelist[pindexright]->getreversedroot();        // Daten der Liste von hinten gebraucht
		// jede Zeile hintereinanderweg zeichnen
		while (acty < ymax.y)
		{
			// y-Wert vom linken Punkt
			tle<PIXEL> *test = PixelList->getroot();
			int j = 0;
			for (j=0; j<pindexleft; j++, test = test->nxt) {}
			int leftpointy =  test->data.y;				  // Y-Wert der Rand-Eck-punkte
			// y-Wert vom rechten Punkt
			test = PixelList->getroot();
			for (j=0; j<pindexright; j++, test = test->nxt) {}
			int rightpointy = test->data.y;
			// y-Werte der Zwischenbereiche ermitteln
			int partialy = ((leftpointy<rightpointy)?leftpointy:rightpointy); // 1.Abschnitt
			int furthery = ((partialy==leftpointy)?rightpointy:leftpointy);   // 2.Abschnitt
			// Verbinden der X-Werte der beiden Kantenlisten
			while (acty < (partialy+1))		// Achtung: Zugriff auf Null m�glich!
			{
				int x1 = leftedge->data, x2 = rightedge->data;
				linehor(x1,x2,acty,25015);
				leftedge = leftedge->nxt;
				rightedge = rightedge->nxt;
				acty++;
			}
			// Indizes aktualisieren
			if (partialy==rightpointy)  // wenn rechte Seite die Teilgrenze darstellt
			{
				pindexright = succ(pindexright,sizeoflist);
				rightedge = pixeledgelist[pindexright]->getreversedroot(); // neue Kante laden // -1 ?
			}
			else						// wenn linke Seite die Teilgrenze darstellt
			{
				pindexleft = pred(pindexleft,sizeoflist);
				leftedge = pixeledgelist[succ(pindexleft,sizeoflist)]->getroot();
			}
		}
		delete newpxl;
	}
	void DrawGons(void)				 // Ecken der Polygone zeichnen
	{
		tle<PIXEL> *beginpxl = PixelList->getroot();
		while (beginpxl!=NULL)
		{
			putpixel(beginpxl->data.x,beginpxl->data.y,255);
			beginpxl = beginpxl->nxt;
		}
	}
	void DrawPolygon(void)				 // Polygoneecken miteinander verbinden
	{
		tle<PIXEL> *beginpxl = PixelList->getroot();
		while (beginpxl->nxt!=NULL)
		{
			line(beginpxl->data,beginpxl->nxt->data);
			beginpxl = beginpxl->nxt;
		}
		line(beginpxl->data,PixelList->getroot()->data);
	}
    void FillTexturedPoly(void)			 // texturiertex Polygon zeichnen
	{
	}
	void linehor(int x1, int x2, int y, long value)	// zeichnet eine horizontale Linie
	{
		int temp;
		if ((x2-x1) < 0) swap(x2,x1);
		int x=x1;
		for (;x<=x2;x++,putpixel(x,y,value)) {}
	}
	void line(PIXEL &p1,PIXEL &p2)			 // ~ beliebige Linie (BRESENHAM)
	{
		int error,x,y,dx,dy,xincr,yincr;
		dx = p2.x-p1.x; dy = p2.y-p1.y;
		if (abs(dx) > abs(dy))
		{
			if (dx<0)
			{
				swapPIXEL(&p1,&p2);
				dx = p2.x-p1.x;
			}
			if (p2.y>p1.y) yincr = 1;
			else yincr = -1;
			error = -abs(dx)/2;
			for (x=p1.x,y=p1.y; x<=p2.x; x++)
			{
				putpixel(x,y,255);
				error += abs(dy);
				if (error>=0)
				{
					y+=yincr;
					error-=dx;
				}
			}
		}
		else
		{
			if (dy<0)
			{
				swapPIXEL(&p2,&p1);
				dy = p2.y-p1.y;
			}
			if (p2.x>p1.x) xincr = 1;
			else xincr = -1;
			error = -abs(dy)/2;
			for (y=p1.y,x=p1.x; y<=p2.y; y++)  
			{
				putpixel(x,y,255);
				error+=abs(dx);
				if (error>=0)
				{
					x+=xincr;
					error-=dy;
				}
			}
		}
	}
//void linetexturehor(TEXTUR *txt, TRIANGLEVERTEX *tv, int x1, int x2, int y); // horizontale Linie aus Textur	
	
/*
void linetexturehor(TEXTUR *txt, TRIANGLEVERTEX *tv, int x1, int x2, int y)
{
  VERTEX tp1 = tv->tv1;
  VERTEX tp2 = tv->tv2;
  VERTEX tp3 = tv->tv3;
  VERTEX a = tp1;
  VERTEX p( (tp2.x-tp1.x), (tp2.y-tp1.y), (tp2.z-tp1.z) );
  VERTEX q( (tp3.x-tp1.x), (tp3.y-tp1.y), (tp3.z-tp1.z) );
  VERTEX b( MIDX, MIDY, RESX*DIST );
  int temp;
  if( x2 < x1 ) swap(x2,x1);
  float  pyqz = p.y*q.z, pzqx = p.z*q.x, qypz = q.y*p.z, qzpx = q.z*p.x,
		 ayqz = a.y*q.z, azqx = a.z*q.x, qyaz = q.y*a.z, qzax = q.z*a.x,
		 pyaz = p.y*a.z, axpz = a.x*p.z, aypz = a.y*p.z, azpx = a.z*p.x,
		 pxqybz = p.x*q.y*b.z, qxpybz = q.x*p.y*b.z, axqybz = a.x*q.y*b.z,
		 qxaybz = q.x*a.y*b.z, pxaybz = p.x*a.y*b.z, axpybz = a.x*p.y*b.z;
  for (int i=x1; (i<x2); i++)
  {
	 b.x  = (float)i-MIDX;
	 b.y  = (float)y-MIDY;
	 float D  = ((b.x*pyqz)+(pxqybz)+(b.y*pzqx))-((qxpybz)+(b.x*qypz)+(b.y*qzpx));
	 float Du = ((b.x*ayqz)+(axqybz)+(b.y*azqx))-((qxaybz)+(b.x*qyaz)+(b.y*qzax));
	 float Dv = ((b.x*pyaz)+(pxaybz)+(b.y*axpz))-((axpybz)+(b.x*aypz)+(b.y*azpx));
	 float drawx = (float)(150.0*Du/D);
	 float drawy = (float)(150.0*Dv/D);
	 if (drawx < 0) drawx = -drawx;
	 if (drawy < 0) drawy = -drawy;
	 int dr_x = int(drawx)%(txt->width),
	     dr_y = int(drawy)%(txt->height);
	 putpixel(i,y,txt->data[(txt->width)*(dr_y)+(dr_x)]);
  }
}
*/
};

#endif