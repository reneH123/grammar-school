#include "window.hpp"
#include "common.hpp"
#include "../Resources/resource.h"

// Determine DX-Stuff
typedef HRESULT(WINAPI * DIRECTDRAWCREATE) (GUID *, LPDIRECTDRAW *, IUnknown *);

void DDRAW::GetDXVersion(LPDWORD pdwDXVersion, LPDWORD pdwDXPlatform)
{
    HRESULT                 hr;
    HINSTANCE               DDHinst = 0;
    HINSTANCE               DIHinst = 0;
    LPDIRECTDRAW            pDDraw = 0;
    LPDIRECTDRAW2           pDDraw2 = 0;
    DIRECTDRAWCREATE        DirectDrawCreate = 0;
    OSVERSIONINFO           osVer;
    LPDIRECTDRAWSURFACE     pSurf = 0;
	LPDIRECTDRAWSURFACE3    pSurf3 = 0;
//	LPDIRECTDRAWSURFACE4    pSurf4 = 0;
	char szError[MAX_LOADSTRING];

    // First get the windows platform
    osVer.dwOSVersionInfoSize = sizeof(osVer);
    if (!GetVersionEx(&osVer))
    {
        *pdwDXVersion = 0;
        *pdwDXPlatform = 0;
        return;
    }
    if (osVer.dwPlatformId == VER_PLATFORM_WIN32_NT)
    {
        *pdwDXPlatform = VER_PLATFORM_WIN32_NT;
        // NT is easy... NT 4.0 is DX2, 4.0 SP3 is DX3, 5.0 is DX5
        // and no DX on earlier versions.
        if (osVer.dwMajorVersion < 4)
        {
            *pdwDXPlatform = 0; // No DX on NT3.51 or earlier
            return;
        }
        if (osVer.dwMajorVersion == 4)
        {
             // NT4 up to SP2 is DX2, and SP3 onwards is DX3, so we are at least DX2
            *pdwDXVersion = 0x200;
            // We're not supposed to be able to tell which SP we're on, so check for dinput
            // It must be NT4, DX2
            *pdwDXVersion = 0x300;  // DX3 on NT4 SP3 or higher
            return;
        }
        // Else it's NT5 or higher, and it's DX5a or higher:
        // Drop through to Win9x tests for a test of DDraw (DX6 or higher)
    }
    else
    {
        // Not NT... must be Win9x
        *pdwDXPlatform = VER_PLATFORM_WIN32_WINDOWS;
    }
    // Now we know we are in Windows 9x (or maybe 3.1), so anything's possible.
    // First see if DDRAW.DLL even exists.
    DDHinst = LoadLibrary("DDRAW.DLL");
    if (DDHinst == 0)
    {
        *pdwDXVersion = 0;
        *pdwDXPlatform = 0;
        FreeLibrary(DDHinst);
        return;
    }
    //  See if we can create the DirectDraw object.
	LoadString(Manager->GetInstance(), IDS_DirectDrawCreate, szError, MAX_LOADSTRING);
    DirectDrawCreate = (DIRECTDRAWCREATE)GetProcAddress(DDHinst, szError);
    if (DirectDrawCreate == 0)
    {
        *pdwDXVersion = 0;
        *pdwDXPlatform = 0;
        FreeLibrary(DDHinst);
		LoadString(Manager->GetInstance(), IDS_LL_DDraw_failed, szError, MAX_LOADSTRING);
        OutputDebugString(szError);
        return;
    }
    hr = DirectDrawCreate(NULL, &pDDraw, NULL);
    if (FAILED(hr))
    {
        *pdwDXVersion = 0;
        *pdwDXPlatform = 0;
        FreeLibrary(DDHinst);
		LoadString(Manager->GetInstance(), IDS_Create_DDraw_failed, szError, MAX_LOADSTRING);
        OutputDebugString(szError);
        return;
    }
	//  So DirectDraw exists.  We are at least DX1.
    *pdwDXVersion = 0x100;
    //  Let's see if IID_IDirectDraw2 exists.
    hr = pDDraw->QueryInterface(IID_IDirectDraw2, (LPVOID *) & pDDraw2);
    if (FAILED(hr))
    {
        // No IDirectDraw2 exists... must be DX1
        pDDraw->Release();
        FreeLibrary(DDHinst);
		LoadString(Manager->GetInstance(), IDS_QI_DDraw2_failed, szError, MAX_LOADSTRING);
        OutputDebugString(szError);
        return;
    }
    // IDirectDraw2 exists. We must be at least DX2
    pDDraw2->Release();
    *pdwDXVersion = 0x200;
    // DirectInputCreate exists. That's enough to tell us that we are at least DX3
    *pdwDXVersion = 0x300;
     // Checks for 3a vs 3b?
     // We can tell if DX5 is present by checking for the existence of IDirectDrawSurface3.
     // First we need a surface to QI off of.
    DDSURFACEDESC desc;

    ZeroMemory(&desc, sizeof(desc));
    desc.dwSize = sizeof(desc);
    desc.dwFlags = DDSD_CAPS;
    desc.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;

    hr = pDDraw->SetCooperativeLevel(NULL, DDSCL_NORMAL);
    if (FAILED(hr))
    {
        // Failure. This means DDraw isn't properly installed.
        pDDraw->Release();
        FreeLibrary(DDHinst);
        *pdwDXVersion = 0;
		LoadString(Manager->GetInstance(), IDS_Set_coop_level_failed, szError, MAX_LOADSTRING);
        OutputDebugString(szError);
        return;
    }
    hr = pDDraw->CreateSurface(&desc, &pSurf, NULL);
    if (FAILED(hr))
    {
        // Failure. This means DDraw isn't properly installed.
        pDDraw->Release();
        FreeLibrary(DDHinst);
        *pdwDXVersion = 0;
		LoadString(Manager->GetInstance(), IDS_CreateSurface_failed, szError, MAX_LOADSTRING);
        OutputDebugString(szError);
        return;
    }
    // Try for the IDirectDrawSurface3 interface. If it works, we're on DX5 at least
    if (FAILED(pSurf->QueryInterface(IID_IDirectDrawSurface3, (LPVOID *) & pSurf3)))
    {
        pDDraw->Release();
        FreeLibrary(DDHinst);
        return;
    }
/*     // QI for IDirectDrawSurface3 succeeded. We must be at least DX5
    *pdwDXVersion = 0x500;
    // Try for the IDirectDrawSurface4 interface. If it works, we're on DX6 at least
    if (FAILED(pSurf->QueryInterface(IID_IDirectDrawSurface4, (LPVOID *) & pSurf4)))
    {
        pDDraw->Release();
        FreeLibrary(DDHinst);
        return;
    } 
*/
    // QI for IDirectDrawSurface4 succeeded. We must be at least DX6
    *pdwDXVersion = 0x600;
    pSurf->Release();
    pDDraw->Release();
    FreeLibrary(DDHinst);
    return;
}

void DDRAW::DirectXDetect(char *platform, char *version)
{
	DWORD   dwDXVersion;
    DWORD   dwDXPlatform;

    GetDXVersion(&dwDXVersion, &dwDXPlatform);
    switch (dwDXPlatform)
    {
        case VER_PLATFORM_WIN32_WINDOWS:
			LoadString(Manager->GetInstance(), IDS_OS_WIN_9x_n, platform, MAX_LOADSTRING);
            break;
        case VER_PLATFORM_WIN32_NT:
			LoadString(Manager->GetInstance(), IDS_OS_WIN_NT_n, platform, MAX_LOADSTRING);
            break;
        default:
			LoadString(Manager->GetInstance(), IDS_OS_Error_n, platform, MAX_LOADSTRING);
    }
	switch (dwDXVersion)
    {
        case 0x000:
			LoadString(Manager->GetInstance(), IDS_DX_NotInstalled, version, MAX_LOADSTRING);
            break;
        case 0x100:
			LoadString(Manager->GetInstance(), IDS_DXVER_1, version, MAX_LOADSTRING);
            break;
        case 0x200:
			LoadString(Manager->GetInstance(), IDS_DXVER_2, version, MAX_LOADSTRING);
            break;
        case 0x300:
			LoadString(Manager->GetInstance(), IDS_DXVER_3, version, MAX_LOADSTRING);
            break;
		case 0x400:	
			LoadString(Manager->GetInstance(), IDS_DXVER_4, version, MAX_LOADSTRING);
			break;
        case 0x500:
			LoadString(Manager->GetInstance(), IDS_DXVER_5, version, MAX_LOADSTRING);
            break;
        case 0x600:
			LoadString(Manager->GetInstance(), IDS_DXVER_6, version, MAX_LOADSTRING);
            break;
        default:
			LoadString(Manager->GetInstance(), IDS_DXVER_Error, version, MAX_LOADSTRING);
    }
}

void DDRAW::InitDDRAW(void)
{
	resx = 640;
	resy = 480;
	depthcolor = 32;
	fpsactive = true;
}

void DDRAW::DoneDDRAW(void)
{
	finiObjects();
}

void DDRAW::PutPix(PIXEL pixel, RGBCOLOR rgb)
{
	VideoMemory.lpSurface[(4*pixel.x) + (VideoMemory.lPitch*pixel.y) + 0] = rgb.r;
	VideoMemory.lpSurface[(4*pixel.x) + (VideoMemory.lPitch*pixel.y) + 1] = rgb.g;
	VideoMemory.lpSurface[(4*pixel.x) + (VideoMemory.lPitch*pixel.y) + 2] = rgb.b;
}

void DDRAW::putpixel(int x, int y, long c)
{
//ACHTUNG: Bereichs�berpr�fung langsam: sicher ist sicher!
	if ( (x > 0) && (x < (long)VideoMemory.dwWidth) && (y > 0) && (y < (long)VideoMemory.dwHeight) )
	{
		// 4 ist f�r entsprechende Farbtiefe + Aufl�sung
		VideoMemory.lpSurface[(4*x) + (VideoMemory.lPitch*y) + 0] = (unsigned char)c;
		VideoMemory.lpSurface[(4*x) + (VideoMemory.lPitch*y) + 1] = (unsigned char)c;
		VideoMemory.lpSurface[(4*x) + (VideoMemory.lPitch*y) + 2] = (unsigned char)c; // c/2 halber Blau-Wert
	}
}

vidmem* DDRAW::GetVideoMemory(void)
{
	return &VideoMemory;
}

void DDRAW::CheckFPS(void)
{
	fpsactive=!fpsactive;
}

bool DDRAW::CheckedFPS(void)
{
	return fpsactive;
}

int DDRAW::restorePrimary(void)
{
    int result = lpDDSPrimary->Restore();
    return ((result!=DD_OK)?false:true);
}

void DDRAW::restoreSurfaces(void)
{
	if (lpDDSPrimary)		// Check/restore the primary surface
		if( lpDDSPrimary->IsLost() )
			lpDDSPrimary->Restore();
	if (lpDDSBack)			// Check/restore the back buffer
		if( lpDDSBack->IsLost() )
			lpDDSBack->Restore();
}

void DDRAW::finiObjects(void)
{
	if (lpDD != NULL) 
	{
		if(lpDDSPrimary != NULL)	// gibt die Oberfl�che frei
		{
			lpDDSPrimary->Release();
			lpDDSPrimary = NULL;
		}
		lpDD->Release();			// gibt das IDirectDraw object frei
		lpDD = NULL;
	}
}

void DDRAW::printmessage(int x, int y, char *string)
{
	HDC hdc;		       // Handle f�r Surface
	if (lpDDSPrimary->GetDC(&hdc) != DD_OK)
	{
		char lpText[MAX_LOADSTRING], lpCaption[MAX_LOADSTRING];
		GetResourceString(IDS_Err_GetHDC,lpText);
		GetResourceString(IDS_ERROR,lpCaption);
		MessageBox(NULL,lpText,lpCaption,MB_ICONERROR);
		exit(0);			// bei Fehler sofort Programm beenden
	}
	SetBkMode(hdc, TRANSPARENT);					// Durchsichtig
	SelectObject(hdc, hfont);						// "Schrift" selektieren
	SetTextColor(hdc, RGB(255, 255, 255));			// Text wei�
	TextOut(hdc, x, y, string, strlen(string));
	lpDDSPrimary->ReleaseDC(hdc);
}

void DDRAW::SettingFailed(const char* errmsg)
{
	MessageBox(NULL, errmsg, 0, MB_OK);
	finiObjects();
}

bool DDRAW::SetFullScreen(HWND window)
{
	DDSURFACEDESC       ddsd;
	DDSCAPS				ddscaps;
	HRESULT             ddrval;
	// DirectDraw (version 1.0) interface holen und in "lpDD" abspeichern
	ddrval = DirectDrawCreate( NULL, &lpDD, NULL );
	if( ddrval != DD_OK ) 
	{
		char szError[MAX_LOADSTRING];
		GetResourceString(IDS_Err_DirectDrawCreate,szError);
		SettingFailed(szError);
		return false;
	};
	// Bildmodus einstellen (hier: Vollbild - Achtung: Dadurch kein Debugging m�glich)
	ddrval = lpDD->SetCooperativeLevel(window, DDSCL_FULLSCREEN | DDSCL_EXCLUSIVE );
	if( ddrval != DD_OK ) 
	{
		char szError[MAX_LOADSTRING];
		GetResourceString(IDS_Err_SetCooperativeLevel,szError);
		SettingFailed(szError);
		return false; 
	};
	ddrval = lpDD->SetDisplayMode(resx, resy, depthcolor);
	if( ddrval != DD_OK ) 
	{ 
		char szError[MAX_LOADSTRING];
		GetResourceString(IDS_Err_SetDisplayMode,szError);
		SettingFailed(szError);
		return false; 
	};
	ZeroMemory(&ddsd, sizeof(ddsd));
	ddsd.dwSize         = sizeof(ddsd);
	ddsd.dwFlags        = DDSD_CAPS | DDSD_BACKBUFFERCOUNT;
	ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE | DDSCAPS_FLIP | DDSCAPS_COMPLEX;	ddsd.dwBackBufferCount = 1;
	ddrval = lpDD->CreateSurface(&ddsd, &lpDDSPrimary, NULL);
	ddrval = lpDD->SetDisplayMode(resx, resy, depthcolor);
	if( ddrval != DD_OK )
	{	
		char szError[MAX_LOADSTRING];
		GetResourceString(IDS_Err_CreateSurface,szError);
		SettingFailed(szError); 
		return false; 
	};
	// Der Backbuffer wurde bereits im vorigen Aufruf mit erzeugt (=> BackBufferCount = 1)
	// Jetzt wird nur noch des Interface darauf vom DirectX geholt und in 
	// "lpDDSBack" gespeichert.
	ddscaps.dwCaps = DDSCAPS_BACKBUFFER;
	ddrval = lpDDSPrimary->GetAttachedSurface (&ddscaps, &lpDDSBack); // &lpDDSBack kein Zugriff!?
	if( ddrval != DD_OK ) 
	{ 
		char szError[MAX_LOADSTRING];
		GetResourceString(IDS_Err_GetAttachedSurface,szError);
		SettingFailed(szError); 
		return false; 
	};
	// und noch ein paar Infos zum Bildspeicher (Breite, H�he und Byte pro Pixelzeile)
	ZeroMemory(&ddsd, sizeof(ddsd));
	ddsd.dwSize = sizeof(ddsd);
	ddrval = lpDDSPrimary->GetSurfaceDesc( &ddsd );
	if( ddrval != DD_OK ) 
	{	
		char szError[MAX_LOADSTRING];
		GetResourceString(IDS_Err_GetSurfaceDesc,szError);
		SettingFailed(szError); 
		return false; 
	};
	VideoMemory.dwHeight = ddsd.dwHeight;
	VideoMemory.dwWidth = ddsd.dwWidth;
	VideoMemory.lPitch = ddsd.lPitch;
	return true;
}

bool DDRAW::SetWindowedMode(HWND window)
{
	DDSURFACEDESC       ddsd;
	HRESULT             ddrval;
	char lpText[MAX_LOADSTRING];
	// DirectDraw (version 1.0) interface holen und in "lpDD" abspeichern
	ddrval = DirectDrawCreate( NULL, &lpDD, NULL );
	if( ddrval != DD_OK ) 
	{
		GetResourceString(IDS_Err_DirectDrawCreate,lpText);
		SettingFailed(lpText);
		return false;
	};
	LPDIRECTDRAWCLIPPER pClipper;
	// Get normal windowed mode
	ddrval = lpDD->SetCooperativeLevel(window, DDSCL_NORMAL );
	if( ddrval != DD_OK )
	{
		GetResourceString(IDS_Err_SetCooperativeLevel,lpText);
		SettingFailed(lpText);
		return false;
	}
	// Get the dimensions of the viewport and screen bounds
    GetClientRect(window, &g_rcViewport);
    GetClientRect(window, &g_rcScreen);
    ClientToScreen(window, (POINT*)&g_rcScreen.left);
    ClientToScreen(window, (POINT*)&g_rcScreen.right);
	// Create the primary surface
    ZeroMemory(&ddsd, sizeof(ddsd));
    ddsd.dwSize = sizeof(ddsd);
    ddsd.dwFlags = DDSD_CAPS;
    ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;
	ddrval = lpDD->CreateSurface(&ddsd, &lpDDSPrimary, NULL);
	if( ddrval != DD_OK )
	{
		GetResourceString(IDS_Err_SetCooperativeLevel,lpText);
		SettingFailed(lpText);
		return false;
	}
	// Create a clipper object since this is for a Windowed render
    ddrval = lpDD->CreateClipper(0, &pClipper, NULL);
	if( ddrval != DD_OK )
	{
		GetResourceString(IDS_Fail_CreateClipper,lpText);
		SettingFailed(lpText);
		return false;
	}
    // Associate the clipper with the window
    pClipper->SetHWnd(0, window);
    lpDDSPrimary->SetClipper(pClipper);
    pClipper->Release();
    pClipper = NULL;
    // Get the backbuffer. For fullscreen mode, the backbuffer was created
    // along with the primary, but windowed mode still needs to create one.
    ddsd.dwFlags        = DDSD_WIDTH | DDSD_HEIGHT | DDSD_CAPS;
    ddsd.dwWidth        = 640;
    ddsd.dwHeight       = 480;
    ddsd.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN;
    ddrval = lpDD->CreateSurface(&ddsd, &lpDDSBack, NULL);
	if( ddrval != DD_OK )
	{
		GetResourceString(IDS_Fail_CreateSurface2,lpText);
		SettingFailed(lpText);
		return false;
	}
	return true;
}

int DDRAW::FullScreenFramePrepare(void)
{
	HRESULT				ddrval;
	DDSURFACEDESC       ddsd;
	DDBLTFX				ddbltfx;
	ZeroMemory (&ddbltfx, sizeof(ddbltfx));	// Bildschirm l�schen
	ddbltfx.dwSize = sizeof (ddbltfx);
	ddbltfx.dwFillColor = RGB( 0, 0, 0 );
    ddrval = lpDDSBack->Blt( NULL, NULL, NULL, DDBLT_COLORFILL | DDBLT_WAIT, &ddbltfx );
	// Zugriff auf Videospeicher
	ZeroMemory (&ddsd, sizeof(ddsd));
	ddsd.dwSize = sizeof (ddsd);
	while ((ddrval = lpDDSBack->Lock( NULL, &ddsd, 0, NULL )) == DDERR_WASSTILLDRAWING);
	if (ddrval != DD_OK) return ddrval;
	// Zeiger auf Bilddaten merken
	VideoMemory.lpSurface = (unsigned char*)ddsd.lpSurface;
	return true;  // All done
}

int DDRAW::FullScreenFrameEnd(void)
{
	HRESULT				ddrval;
	ddrval = lpDDSBack->Unlock(VideoMemory.lpSurface); // Zugriff auf Videospeicher wieder freigeben
	if (ddrval != DD_OK) { return false; }
	while (1)				// Backbuffer mit Frontbuffer vertauschen
	{
		ddrval = lpDDSPrimary->Flip( NULL, 0 );
		if (ddrval == DD_OK) break;
		if (ddrval == DDERR_SURFACELOST) { if (!restorePrimary()) return false; }
		if (ddrval != DDERR_WASSTILLDRAWING) { if (ddrval != DD_OK) { return false; } }
	}
	return true;  // All done
}

int DDRAW::WindowedModeFramePrepare(HWND hWnd)
{
	// Blit the stuff for the next frame
    g_rcScreen.left = 0;
    g_rcScreen.top = 0;
    g_rcScreen.right = 128;
    g_rcScreen.bottom = 128;
	GetClientRect(hWnd, &g_rcWindow);
    if (g_rcWindow.right < 128)
        g_rcWindow.right = 64;
    if (g_rcWindow.bottom < 64)
        g_rcWindow.bottom = 64;
// POINT pt;   pt.x = pt.y = 0;
//    ClientToScreen(hMainWnd, &pt);
//    OffsetRect(&g_rcWindow, pt.x, pt.y);
	return true;
}

int DDRAW::WindowedModeFrameEnd(void)
{
    HRESULT ddrval;
	while (TRUE)
    {
        ddrval = lpDDSPrimary->Blt(&g_rcWindow, lpDDSBack, &g_rcScreen, 0, NULL);
        if (ddrval == DD_OK) return true;
        if (ddrval == DDERR_SURFACELOST)
			restoreSurfaces();
        if (ddrval != DDERR_WASSTILLDRAWING)
            return false;
    }
    if (ddrval != DD_OK) return true;
}
