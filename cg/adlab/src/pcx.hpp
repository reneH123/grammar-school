#ifndef PCXDef
#define PCXDef

#include <stdlib.h>

typedef struct {
  char manufacturer;
  char version;
  char encoding;
  char bits_per_pixel;
  short int xmin, ymin;
  short int xmax, ymax;
  short int hres;
  short int vres;
  char palette[48];
  char reserved;
  char colour_planes;
  short int bytes_per_line;
  short int palette_type;
  char filles[58];
} pcxheader;

class pcx
{
	private:
		unsigned char pcxpalette[768]; 
		pcxheader header;				  // Headerinformationen	
		unsigned short pcxwidth, pcxheight, pcxbytes, pcxbits;	// wichtige Bildattribute
		FILE *fp;						// Zeiger auf interne Dateivariable
		char *file;						// Dateiname
		char *data;						// Zeiger auf PCX-Daten
		char *errormsg;					// Fehlermeldung
		void UnpackPcxFile(FILE * fp, char *data);		// gesamten Bildinhalt in data speichern
		int ReadPcxLine(FILE *fp, char *data);			// eine vollständige Zeile in data speichern
	public:
		pcx(char *pszFileName);				// Konstruktor
		~pcx(void);    						// Destruktor
		char *loadpcx(void);
		void copydata(char **ptr);			// gleiche wie loadPCX, nur als Variablenparameter		
		char *geterrormsg(void);			// zeigt Fehlernachricht während des Ladens an
		void getheader(pcxheader *header_);
		unsigned short getwidth(void) const;
		unsigned short getheight(void) const;
		unsigned short getbytes(void) const;
		unsigned short getbits(void) const;
};

int pcx::ReadPcxLine(FILE *fp, char* data) 
{
  int n = 0;
  do 
  {
	char c = fgetc(fp) & 0xff;
	if ((c & 0xc0) == 0xc0) 
	{
		short i = c & 0x3f;                           
		c = fgetc(fp);
		while (i--) 
		{
		   *data++ = c;
			n++;
		}
    }
    else 
	{
      *data++ = c;
      n++;
    }
  } while (n < pcxwidth);
  return n;
}

void pcx::UnpackPcxFile(FILE * fp, char *data) 
{
  for (int i = 0; i < pcxheight; i++) 
    ReadPcxLine(fp,data+i*pcxwidth);
}

pcx::pcx(char *pszFileName)		// Konstruktor
{
  pcxheight = 0;
  pcxwidth = 0;
  file = pszFileName;	
  errormsg = "";
  data=NULL;
  if ((fp = fopen(file, "rb")) == NULL)
   errormsg = "Fehler beim Öffnen der Datei!";

  if (errormsg == "")
  {
	if (fread((char *) &header, 1, sizeof(pcxheader), fp) != sizeof(pcxheader))
		errormsg = "Falsche PCX-Headergröße!";
  }

  if (header.manufacturer != 0x0a || header.version != 5)
    errormsg = "Nichtunterstützte PCX-Version!";

  if (header.bits_per_pixel == 1) pcxbits = header.colour_planes;
  else pcxbits = header.bits_per_pixel;

  if (pcxbits != 8) errormsg = "Fehler in Anzahl der Bits pro Pixel (bpp)";
  //FEHLT!: Palette umstellen:8->24 Bit
  if (errormsg == "")	
  {
		if (! fseek(fp, -769L, SEEK_END)) 
		{
			if (fgetc(fp) != 0x0c || fread(pcxpalette, 1, 768, fp) != 768)
				return;
		}
		else return;

  }
  if (errormsg == "") 
  {
	  fseek(fp, 128L, SEEK_SET);
	  pcxwidth = (header.xmax - header.xmin) + 1;
	  pcxheight = (header.ymax - header.ymin) + 1;
	  pcxbytes = header.bytes_per_line;
	  (char *)data =  (char *)malloc(pcxwidth * pcxheight);
	  UnpackPcxFile(fp, data);
	  fclose(fp);
  }

}

pcx::~pcx(void)		// andere Daten wieder freigeben...
{
}		

char *pcx::loadpcx(void) 
{
  return (char *)data;
}

void pcx::copydata(char **ptr)
{
	(char *)*ptr = (char *)data;
}

char *pcx::geterrormsg(void)
{
	return errormsg;
}

void pcx::getheader(pcxheader *header_)
{
	*header_ = header;
}

unsigned short pcx::getwidth(void) const
{
	return pcxwidth;
}

unsigned short pcx::getheight(void) const
{
	return pcxheight;
}

unsigned short pcx::getbytes(void) const
{
	return pcxbytes;
}

unsigned short pcx::getbits(void) const
{
	return pcxbits;
}

#endif