#ifndef MatrixDef
#define MatrixDef

#include "vertex.hpp"
#include "common.hpp"

class Matrix
{
public:
	// Konstruktoren
	Matrix()								   // Initialisierung mit "0"
	{
		x(0,0,0);
		y(0,0,0); 
		z(0,0,0);
	}
	Matrix(const VERTEX& a, const VERTEX& b, const VERTEX& c)
	{
		x=a;
		y=b;
		z=c;
	}
	// Werte setzen
	inline Matrix& diagonal(float a, float b, float c)	// diagonale Matrix
	{
		return Matrix(x=VERTEX(a,0,0),y=VERTEX(0,b,0),z=VERTEX(0,0,c));
	}
	inline Matrix& identity(void)						// Einheitsmatrix
	{
		return Matrix(x=VERTEX(1,0,0),y=VERTEX(0,1,0),z=VERTEX(0,0,1));
	}
	inline Matrix& zero(void)							// alle Komponenten = "0"
	{
		return Matrix(x=VERTEX(0,0,0),y=VERTEX(0,0,0),z=VERTEX(0,0,0));
	}
	inline Matrix& operator() (const VERTEX& a, const VERTEX& b, const VERTEX& c)	// neue Reihen setzen
	{
		return Matrix(x=a,y=b,z=c);
	}
	// Komponentenzugriffe
	inline VERTEX& operator[] (int i)					// Vektorreihe[i]
	{
		return ((i==0)?x:((i==1)?y:z));
	}
	inline VERTEX operator[] (int i) const
	{
		return ((i==0)?x:((i==1)?y:z));
	}
	inline VERTEX& column(int j) const					// Vektorzeile[j]
	{
		return VERTEX((j==0)?(x.x,y.x,z.x):((j==1)?x.y,y.y,z.y:x.z,y.z,z.z));
	}
	// Komponentenweises Zuweisen
	inline Matrix& operator+=(const Matrix& B)
	{
		return Matrix(x+B.x,y+B.y,z+B.z);
	}
	inline Matrix& operator-=(const Matrix& B)
	{
		return Matrix(x-B.x,y-B.y,z-B.z);
	}
	inline Matrix& operator*=(float d)
	{
		return Matrix(x*d,y*d,z*d);
	}
	inline Matrix& operator/=(float d)
	{
		return Matrix(x/d,y/d,z/d);
	}
	VERTEX x,y,z;
// semant. Ende
/*	void inverse_rot_matrix(Rot_matrix M, Rot_matrix InvM)		// Matrixinversion
	{
		register short i, j;
		for (i=0;i<3;i++)
		for (j=0;j<3;j++)
			InvM[i][j] = M[j][i];
	}
	void fill_rot_matrixX(Rot_matrix& M, float angle)			// XRotationsmatrix: formal
	{
		register float s, c;  // for reasons of speed 
		s=(float)sin(angle); c=(float)cos(angle);
		memset(M,0,sizeof(Rot_matrix));
		M[0][0]=1; M[1][1]=c; M[1][2]=s; M[2][1]=-s; M[2][2]=c; 
	}
	void fill_rot_matrixY(Rot_matrix& M, float angle)			// YRotationsmatrix
	{
		register float s, c;  // for reasons of speed 
		s=(float)sin(angle); c=(float)cos(angle);
		memset(M,0,sizeof(Rot_matrix));
		M[0][0]=c; M[0][2]=-s; M[1][1]=1; M[2][0]=s; M[2][2]=c; 
		return;
	}
	void fill_rot_matrixZ(Rot_matrix& M, float angle)			// ZRotationsmatrix
	{
		register float s, c;  // for reasons of speed 
	    s=(float)sin(angle); c=(float)cos(angle);
		memset(M,0,sizeof(Rot_matrix));
		M[0][0]=c; M[0][1]=s; M[1][0]=-s; M[1][1]=c; M[2][2]=1;
		return;
	}
	void vec_mult_matrix(VERTEX& V, Rot_matrix M)	// Multiplikation(Matrix,Vektor)
	{
		VERTEX tmp = V;
		V.x = (M[0][0]*tmp.x)+(M[1][0]*tmp.y)+(M[2][0]*tmp.z);
		V.y = (M[0][1]*tmp.x)+(M[1][1]*tmp.y)+(M[2][1]*tmp.z);
		V.z = (M[0][2]*tmp.x)+(M[1][2]*tmp.y)+(M[2][2]*tmp.z);
	}
	void identity_matrix(Rot_matrix& M)
	{
		memset(M,0,sizeof(Rot_matrix));
		M[0][0]=1; M[1][1]=1; M[2][2]=1; 
	}
	void matrix_translate(matrix_t * m1, real x, real y, real z) 
	{
		m1->el[0][3] = x;
		m1->el[1][3] = y;
		m1->el[2][3] = z;
	}
	void matrix_scale(matrix_t * m1, real scaleX, real scaleY, real scaleZ) 
	{
		m1->el[0][0] *= scaleX;
		m1->el[1][1] *= scaleY;
		m1->el[2][2] *= scaleZ;
	}*/
};

// Matrizen-Arithmetik:

Matrix operator-(const Matrix& A);						// un�res Minus
Matrix operator+(const Matrix& A, const Matrix& B);		// Summe
Matrix operator-(const Matrix& A, const Matrix& B);		// Differenz
Matrix operator*(float d, const Matrix& A);				// Skalares Multiplizieren
Matrix operator*(const Matrix& A, float d);
Matrix operator/(const Matrix& A, float d);				// Skalares Dividieren
VERTEX operator*(const Matrix& A, const VERTEX& b);		// Punktprodukt: Matrizen mit Vektor
VERTEX operator*(const VERTEX& b, const Matrix& A);
Matrix operator*(const Matrix& A, const Matrix& B);		// Matrixprodukt
//Matrix operator~(const Matrix& A);					// transponierte Matrix
//Matrix inverse(const Matrix& A);
float det(const Matrix& A);								// Determinante
float trace(const Matrix& A);							// Quersumme
int operator== (const Matrix& A, const Matrix& B);		// Gleichheit
inline Matrix operator-(const Matrix& A)
{
	return Matrix(-A.x,-A.y,-A.z);
}
inline Matrix operator+(const Matrix& A, const Matrix& B)
{
	return Matrix(A.x+B.x,A.y+B.y,A.z+B.z);
}
inline Matrix operator-(const Matrix& A, const Matrix& B)
{
	return Matrix(A.x-B.x,A.y-B.y,A.z-B.z);
}
inline Matrix operator*(float d, const Matrix& A)
{
	return Matrix(d*A.x,d*A.y,d*A.z);
}
inline Matrix operator*(const Matrix& A, float d)
{
	return Matrix(A.x*d,A.y*d,A.z*d);
}
inline Matrix operator/(const Matrix& A, float d)
{
	return Matrix(A.x/d,A.y/d,A.z/d);
}
inline VERTEX operator*(const Matrix& A, const VERTEX& b)
{
	return VERTEX(A.column(0)*b,A.column(1)*b,A.column(2)*b);
}
inline VERTEX operator*(const VERTEX& b, const Matrix& A)
{
	return VERTEX(A.column(0)*b,A.column(1)*b,A.column(2)*b);
}
inline Matrix operator*(const Matrix& A, const Matrix& B)
{
	return Matrix(VERTEX(A.column(0)*B[0],A.column(1)*B[0],A.column(2)*B[0]),
				  VERTEX(A.column(0)*B[1],A.column(1)*B[1],A.column(2)*B[1]),
				  VERTEX(A.column(0)*B[2],A.column(1)*B[2],A.column(2)*B[2]));
}
/*inline Matrix operator~(const Matrix& A)
{
	return ;
}
inverse*/
inline float det(const Matrix& A)
{
	return (A[0].x*A[1].y*A[2].z)+(A[0].z*A[1].x*A[2].y)+(A[0].y*A[1].z*A[2].x)
	-(A[0].x*A[1].z*A[2].y)-(A[0].y*A[1].x*A[2].z)-(A[0].z*A[1].y*A[2].x);
}
inline float trace(const Matrix& A)
{
	return A[0].x+A[1].y+A[2].z;
}
inline int operator== (const Matrix& A, const Matrix& B)
{
	return ((A.x==B.x)&(A.y==B.y)&(A.z==B.z)?true:false);
}

#endif
