#ifndef WINDOWDef
#define WINDOWDef

#include <ddraw.h>
#include <windows.h>
#include "list.hpp"
#include "vertex.hpp"
#include "common.hpp"
  
class DDRAW
{
protected:	
	vidmem VideoMemory;
public:
	void InitDDRAW(void);						// Ersatzkonstruktor: Initialisierung & Anlegen v. Speicherbereichen
	void DoneDDRAW(void);						// Ersatzdestruktor: Freigeben aller ben�tigten Sourcen
	void putpixel(int,int,long);				// Zeichnet ein Pixel in Frontbuffer
	// access in derived classes forbiden !		
	vidmem* GetVideoMemory(void);				// Zeiger auf VideoMem-Daten
	void PutPix(PIXEL, RGBCOLOR);				// Pixel auf den Bildschirm schreiben
	int restorePrimary(void);					// Herstellen des Frontbuffers
	void restoreSurfaces(void);				    // Herstellen aller Surfaces
	void printmessage(int,int,char*);			// Ausgabe positionierten Textes
	bool SetFullScreen(HWND);					// Setzen des Vollbildschirmmodi
	bool SetWindowedMode(HWND);					// Setzen des Fenstermodi
	static void DirectXDetect(char*,char*);		// Strings: DX-Version + Platform
	void CheckFPS(void);						// (de)aktiviert FPS-Anzeige
	bool CheckedFPS(void);						// Zustand der FPS-Anzeige
	int FullScreenFramePrepare(void);		    // aktuelle Frame der abgebildeten Welt zeichnen
	int FullScreenFrameEnd(void);				// nach Abbilden Speicherbereiche dereferenzieren
	int WindowedModeFramePrepare(HWND);			// s. F.S.FramePrepare
	int WindowedModeFrameEnd(void);				// s. F.S.FrameEnd
private:												// Anzahl Pixel pro Zeile und Zeilen pro Bild
	LPDIRECTDRAW            lpDD;               // DirectDraw object
	LPDIRECTDRAWSURFACE     lpDDSPrimary;       // DirectDraw primary surface
	LPDIRECTDRAWSURFACE     lpDDSBack;          // DirectDraw back surface
	HFONT hfont;								// handle f�r Font auf Bildschirm
	// Clipper Equipment
	RECT g_rcWindow;							// Saves the window size & pos.
	RECT g_rcViewport;							// Pos. & size to blt from
	RECT g_rcScreen;                            // Screen pos. for blt
	int resx, resy, depthcolor;
	bool fpsactive;								// managt FPS-Anzeige
	void SettingFailed(const char* errmsg);		// Fehlernachricht in DialogFenster ausgeben
	void finiObjects(void);						// Alle angeforderten Speicherbereiche und Objekte wieder freigeben
	static void GetDXVersion(LPDWORD, LPDWORD); // ermittle aktuelle DX-Version + OS
};

void AppPause(HWND hWnd, BOOL f);

typedef struct tagMenuItemActive{
	UINT ID;
	bool activated;
}MenuItemActive;

class MENU
{
public:
	MENU(void)
	{
		Menu = NULL;
	}
	~MENU(void)
	{
		DestroyMenu(Menu);
	}
	void Check(UINT id)
	{
		bool check = (GetMenuState(Menu,id,MF_BYCOMMAND) == MF_CHECKED)?false:true;
		if (!RegisteredID(id))
		{
			MenuItemActive newid;
			newid.activated = check;
			newid.ID=id;
			MenuItemList.push(newid);
		}
		if (check) CheckMenuItem(Menu,id,MF_CHECKED);
		else CheckMenuItem(Menu,id,MF_UNCHECKED);
	}
	void SetMenuHandle(HWND window)
	{
		Menu = GetMenu(window);
	}
	HMENU GetMenuHandle(void) const
	{
		return Menu;
	}
	void SetupRenderLayout(int);
private:
	HMENU Menu;
	tlist<MenuItemActive> MenuItemList;
	bool RegisteredID(UINT id)
	{
		tle<MenuItemActive>* node = MenuItemList.getroot();
		int count = 0;
		for (;node!=NULL;count++,node = node->nxt)
		{
			if (node->data.ID == id) return true;
		}
	    return false;
	}
};

class WINDOW
{
	friend LRESULT CALLBACK WindowProc(HWND,UINT,WPARAM,LPARAM);
	friend BOOL CALLBACK AddDlgProc(HWND,UINT,WPARAM,LPARAM);
	friend BOOL CALLBACK AddPripropDlgProc(HWND,UINT,WPARAM,LPARAM);
	friend BOOL CALLBACK AddScriptDlgProc(HWND,UINT,WPARAM,LPARAM);
public:
	int Run(void);
	static void Register(void);					// Fenstereinstellungen registrieren
	static void Unregister(void);				// ~ r�ckg�ngig machen
	LRESULT Create(LPCTSTR);					// Fenster erzeugen
	void AppPause(bool f);						// System anhalten -> Systembars aktualisieren
	void Show(void);							// Fenster anzeigen
	bool SetScreen(void);						// Graphikmodus setzen
	void UnSetScreen(void);						// vom Graphiksystem verabschieden
	void LoadAccelerator(HINSTANCE,LPCTSTR);	// Laden des Accelerators aus den Resourcen
	HWND GetWndHandle(void) const;				// Window-Handle
	MENU* GetMenuObj(void) const;				// Zeiger auf Menu-Objekt
	DDRAW* GetDDrawObj(void) const;				// Zeiger auf DDraw-Objekt
private:
	LRESULT Destroy(HWND,UINT,WPARAM,LPARAM);	// Fenster zerst�ren
	HWND Window;								// "Windows"-Fenster
	HACCEL AccelTable;							// Handle zum Accelerator
	MENU* Menu;									// Handle zum Hauptmenu	
	DDRAW* DDraw;								// Schnittstelle zum Graphiksystem
	void WriteGraphBuffer(void);
	void SetupRenderLayout(int layout);
};

#endif