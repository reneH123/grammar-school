#ifndef BMPDef
#define BMPDef

#include <stdio.h>
#include "vertex.hpp"

TEXTUR *loadbmp(const unsigned BMPWIDTH, const unsigned BMPHEIGHT, char *pszFileName)
{
	long *pic1 = new long; //long pic1[BMPWIDTH][BMPHEIGHT];
	char s1;
	unsigned int i,h;
	unsigned char red, green, blue;
	FILE *f;
	f=fopen(pszFileName,"rt");
	for (int io=0;io<54;io++,s1=getc(f)){}
	for (i=0;i<BMPHEIGHT;i++)
		for (h=0;h<BMPWIDTH;h++)
		{
			blue  = unsigned (getc(f));
			green = unsigned (getc(f));
			red   = unsigned (getc(f));
			//pic1[h][i] = long((red<<16)+(green<<8)+(blue));
			*pic1++ = long((red<<16)+(green<<8)+(blue));
		}
	fclose(f);
	
	TEXTUR *tmp = new TEXTUR;
	tmp->width  = (int)BMPWIDTH;
	tmp->height = (int)BMPHEIGHT;
	tmp->data   = pic1;
	return tmp;
}

#endif