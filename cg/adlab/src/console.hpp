#ifndef ConsoleDef
#define ConsoleDef

#include <stdio.h>
#include <string.h>
#include "common.hpp"
#include "List.hpp"

class console				// Verwalten von Messages/Commands
{
	private:
		tlist<char*> errmsg;
	public:
		console()
		{
			errmsg.push_back(">(C)opyright Ren� Haberland, 1999/2000");
			errmsg.push_back(">Creating console-parser:");
		}
		~console(void){};
		void Insert(char *msg)					  // Nachricht in Console eintragen
		{
			errmsg.push_back(msg);
		}
		void InsertReportList(tlist<char *> &rl)  // tempor�re Nachrichtenliste anh�ngen
		{
			tle<char *> *end = errmsg.getlast();  // Ende der Hauptliste
			end->nxt = rl.getroot();			  // Ende = Anfang der anzuf�genden Liste
			errmsg.setlast(rl.getlast());	      // Ende der Hauptliste = Ende der anz. Liste
		}
		bool LogToFile(char *fname)               // Schreibe Log-Liste in Datei f
		{
			FILE *fp;
			if ((fp=fopen(fname,"w")) == NULL) return false;
			tle<char*> *begin = errmsg.getroot();
			char buffer[MAX_LOADSTRING];
			strcpy(buffer,"");
			while (begin!=NULL)
			{
				while (begin->nxt!=NULL)
				{
					if ((begin->nxt->data[0] == '>'))
					{
						strcat(buffer,begin->data);
						break;
					}
					else 
					{
						strcat(buffer,begin->data);
					}
					begin = begin->nxt;
				}
				strcat(buffer,"\n");	// Absatz einf�gen
				int len = strlen(buffer);
				fwrite(buffer,1,len,fp);
				strcpy(buffer,"");
				begin = begin->nxt;
			}
			fclose(fp);
			return true;
		}
		tle<char*> *GetRootMsgList(void)          // Anfang der Nachrichtenliste
		{
			return errmsg.getroot();
		}
		int GetCntMessages(void) 		          // Anzahl der Nachrichten
		{
			return errmsg.count_items();
		}
		/*
		void console_t::toggle_dummy()
		{
			pleCHAR h = errmsg->getroot();  // Hauptliste
			pleCHAR g = dummy->getroot();	// Teilliste
			while (h!=NULL)
			{
				if (h->nxt == NULL)
				{
					h->nxt = g;
					break;
				}
				h = h->nxt;
			}
			dummy = NULL;	//
			//
			// - Teilliste zeigt auf NULL (bezugslos)
			// - errmsg hat jetzt alle Nachrichten in sich vereint
		}*/
};

#endif
