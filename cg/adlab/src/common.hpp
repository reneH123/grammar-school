#ifndef CommonDef
#define CommonDef

#include <windows.h>
#include "vertex.hpp"

struct convert{
	char* float2string(float numb)
    {
		char* mainstr = new char;
        gcvt(numb,5,mainstr);
        return mainstr;
    }
	char* int2string(int numb)
    {
	    char *mainstr = new char;
	    itoa(numb,mainstr,10);
	    return mainstr;
    }
};
extern convert* Convert;

// structure to manage Video-Memory and belonging surfaces	
struct vidmem{
	unsigned char *lpSurface;					// Zeiger auf Bilddaten
	unsigned long dwHeight, dwWidth;			// Height und Width der Surface
	long lPitch;								// Anzahl von Bytes pro Pixelzeile
};

void swapPIXEL(PIXEL *p1, PIXEL *p2);

// Macros:
#define pred(d,max) (((d==max)?0:d+1))	// Nachfolger
#define succ(d,max) (((d==0)?max:d-1))	// Vorg�nger
#define Min(x,y) ((x)<(y)? (x):(y))		// Minimum
#define Max(x,y) ((x)>(y)? (x):(y))		// Maximum
#define Sign(x)  ((x)<  0?(-1):(1))		// Vorzeichen
#define swap(a,b) (temp=a, a=b, b=temp)	// Dreieckstausch
// Konstanten:
#define ERRCODE 0xFFFFFFFF
#define MAX_LOADSTRING 255
#define PI 3.141592654
#define POLYGON_IN  0
#define POLYGON_OUT	1
#define RESX 640		// PC-Einstellung:    1024x768x32; Laptop-Einstellung: 640x480x32
#define RESY 480
#define MIDX RESX/2
#define MIDY RESY/2
#define TEXTLINEWIDTH 20
#define TEXTLINETOP 0
#define TEXTLINEBOTTOM  RESY-TEXTLINEWIDTH
#define TEXTLINELEFT 0
#define TEXTLINERIGHT RESX
#define DEPTHCOLOR 32
#define MAXSIZEX		16384
#define MAXSIZEY		4096
#define MAXSIZEZ		8192
#define DIST 0.25 // Distance
// Render-Properties:
#ifdef POINTS				// Punkt-Darstellung definiert
#undef POINTS
#endif
#ifndef WIREFRAME
#define WIREFRAME
#endif
#ifdef MONOCHROMTEXTURED
#undef MONOCHROMTEXTURED
#endif
#ifdef TEXTUREDMAPPED
#undef TEXTUREDMAPPED
#endif
#define pointlayout             0
#define wireframelayout         1
#define monochromtexturedlayout 2
#define texturedmapped          3

#define FULLSCREEN

typedef float Rot_matrix[3][3];
typedef char char_[MAX_LOADSTRING];

void GetResourceString(UINT uID, LPTSTR test);

// Manager-Definition
class MANAGER
{
public:
	MANAGER() {}
	~MANAGER() {}
	void SetInstance(HINSTANCE instance)
	{
		Instance = instance;
	}
	HINSTANCE GetInstance(void) const
	{
		return Instance;
	}
private:
	HINSTANCE Instance;	
};

extern MANAGER* Manager;

#endif