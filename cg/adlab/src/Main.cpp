#include "Main.hpp"      

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,
			LPSTR lpCmdLine, int nCmdShow)
{
	Convert = new (convert);
	Manager = new (MANAGER);
	Manager->SetInstance(hInstance);
	WINDOW::Register();
	WINDOW window;		    // Initialisiere die Anwendung
	if (window.Create("Test Program") == FALSE)
	{
		char_ wnd_creatingerr;
		GetResourceString(IDS_WND_CreatingError,wnd_creatingerr);
		MessageBox(NULL,wnd_creatingerr,0,MB_OK);
		return ERRCODE;
	}
	window.Show();
	if (!window.SetScreen())
	{
		char_ setscreen;
		GetResourceString(IDS_SETSCREEN,setscreen);
		MessageBox(NULL,setscreen,0,MB_OK);
		return ERRCODE;
	}
	window.LoadAccelerator(Manager->GetInstance(),(LPCTSTR)IDR_ACCELERATORMAIN);
	int res = window.Run();
	window.UnSetScreen();
	delete Manager;
	delete Convert;
	return  res;
}