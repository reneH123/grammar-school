#ifndef PolygonDef
#define PolygonDef

#include "vertex.hpp"
#include "List.hpp"

struct node
{
	tlist<VERTEX> *data;				  		// 3D Koordinaten
	VERTEX normal;							    // Normale der Fl�che
	node *nxt;
};

class polylist
{
private:
protected:
	node *head;									//  Polygondefinition: Index auf 3d Koordinaten
public:
	polylist(void);								//	constructor
	~polylist(void);							//	destructor
	void AddToList(tlist<VERTEX> *poly);		//	add a polygon to the list
	void Pop(void);							    //	pop the head from the list
	int Empty(void) const;						//	return whether or not the list is empty
	int Items(void) const;						//  counts items
	node *getroot(void) const;					//  root of head
	node *operator[](int n)const;				//  i = numb of poly in list
};				

polylist::polylist(void)	//	constructor
{
	head = 0;				//	the list is empty
}

polylist::~polylist(void)  //	destructor
{
	node *h;
	while (head)			//	while the list is not empty
	{
	    h = head;			//	save the head element for a moment
	    head = head->nxt;	//	advance the head element
		delete h;			//	kill the former head
	}
}

void polylist::AddToList(tlist<VERTEX> *poly)	//	add a polygon to the root of list
{
	if (poly->getroot()!=NULL)
	{
		if (poly->getroot()->nxt!=NULL)
		{
			node *add = new node;						//	create a new node
			add->data = poly;							//	copy the polygon pointer
			//add->normal = poly->first() % poly->getroot()->nxt->data;			//  determine normale
			add->normal = (poly->getroot()->nxt->nxt->data - poly->getroot()->nxt->data) %
						  (poly->getroot()->data - poly->getroot()->nxt->data);			//  determine normale
			add->nxt  = head;							//	set the new node at the head of the list
			head = add;									//	install the new head
		}
	}
}

void polylist::Pop(void)
{											
	node *h;
	h = head;
	head = h->nxt;
	delete h;
}

int polylist::Empty(void) const
{
	return head->data->empty();
}

int polylist::Items(void) const
{
	node *h = head;
	for(int i = 0; h!=NULL; i++)
	    h = h->nxt;
	return i;
}

node *polylist::getroot(void) const
{
	return head;
}

node *polylist::operator[](int n) const
{
	node *h = head;
	for(int i = 0; i < n; i++)
	    h = h->nxt;
	return h;
}

#endif