#include "frame.hpp"
#include "window.hpp"
#include "../Resources/resource.h"

void WINDOW::AppPause(bool f)
{
    if (f)
    {
       // bActive = FALSE;
        DrawMenuBar(Window);
        RedrawWindow(Window, NULL, NULL, RDW_FRAME);
    }
    //else  bActive = TRUE;
}

void WINDOW::Register(void)
{
	WNDCLASS windowclass;
	LPCTSTR icon,cursor;
	HBRUSH brush;
	
	icon = MAKEINTRESOURCE(IDI_ICONINIT);
	cursor = MAKEINTRESOURCE(IDC_ARROW);
	brush = (HBRUSH)GetStockObject(BLACK_BRUSH);
	// Registrieren des Hauptfensters
	windowclass.lpszClassName = "AdLabClassName";//GetResourceString(IDS_WND_ClassName);
	windowclass.hInstance = Manager->GetInstance();
	windowclass.lpfnWndProc = WindowProc;	
	windowclass.style = CS_HREDRAW|CS_VREDRAW;
	windowclass.lpszMenuName = MAKEINTRESOURCE(IDR_MAINMENU);
	windowclass.hIcon = LoadIcon(Manager->GetInstance(),icon);
	windowclass.hCursor = LoadCursor(NULL,cursor);
	windowclass.hbrBackground = brush;
    windowclass.cbClsExtra = 0;
    windowclass.cbWndExtra = 4;
    RegisterClass(&windowclass);
}

void WINDOW::Unregister(void)
{
	UnregisterClass("AdLabClassName"/*GetResourceString(IDS_WND_ClassName)*/,Manager->GetInstance());
}

int WINDOW::Run(void)
{
	MSG msg;
	while (1)
	{
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) { break; }
			if (!Window || !TranslateAccelerator(Window, AccelTable, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}			
		}
		else
		{
			#ifdef FULLSCREEN
			if (!DDraw->FullScreenFramePrepare()) DDraw->printmessage(100,100,"Err: DrawFrame");
			WriteGraphBuffer();
			if (!DDraw->FullScreenFrameEnd()) DDraw->printmessage(100,100,"Err: DrawFrame");
			#else
			if (!DDraw->WindowedModeFramePrepare(Window)) DDraw->printmessage(100,100,"Err: DrawFrame");
			WriteGraphBuffer();
			if (!DDraw->FullScreenFrameEnd()) DDraw->printmessage(100,100,"Err: DrawFrame");
			#endif
			//}
			//else WaitMessage();
		}
	}
	return msg.wParam;
}

LRESULT WINDOW::Create(LPCTSTR title)
{
	DWORD style = WS_POPUP | WS_SYSMENU | WS_OVERLAPPEDWINDOW | WS_SIZEBOX;
	HWND parent = NULL;
	LPVOID params = this;
	// Hauptfenster erzeugen (Parameter mal bitte in der Hilfe nachschauen)
	Menu = new (MENU);
	Window = CreateWindow("AdLabClassName"/*GetResourceString(IDS_WND_ClassName)*/,
						  title/*GetResourceString(IDS_WND_WindowName)*/,
						  style/*WS_POPUP | WS_SYSMENU | WS_OVERLAPPED | WS_SIZEBOX*/,
						  CW_USEDEFAULT,
						  CW_USEDEFAULT,
						  /*RESX, RESY,*/640,480, parent/*NULL*/, Menu->GetMenuHandle(), Manager->GetInstance(),params);
	Menu->SetMenuHandle(Window);
	// Probe, ob Fenster tats�chlich erstellt wurde
	return ((!Window)?FALSE:TRUE);
}

void WINDOW::Show(void)
{
	ShowWindow(Window,SW_SHOW);
}

bool WINDOW::SetScreen(void)
{
	DDraw = new (DDRAW);
	DDraw->InitDDRAW();
	Framework = new (framework);
	#ifdef FULLSCREEN 
	if (!DDraw->SetFullScreen(Window))
	#else             
	if (!DDraw->SetWindowedMode(Window))
	#endif 
		return false;
	return true;
}

void WINDOW::UnSetScreen(void)
{
	delete Framework;
	DDraw->DoneDDRAW();
	delete DDraw;
}

LRESULT WINDOW::Destroy(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
	delete Menu;
	PostQuitMessage(0);
	return 0;
}

void WINDOW::LoadAccelerator(HINSTANCE instance,LPCTSTR lpTableName)
{
	AccelTable = LoadAccelerators(instance, lpTableName);
}

HWND WINDOW::GetWndHandle(void) const
{
	return Window;
}

MENU* WINDOW::GetMenuObj(void) const
{
	return Menu;
}

DDRAW* WINDOW::GetDDrawObj(void) const
{
	return DDraw;
}

/**************************************************************************** 
* 
*     PURPOSE:  Dialog Procedure for "Add ~" dialog 
* 
*     PARAMS:   HWND hWnd     - This dialog's window handle 
*               UINT Msg      - Which Message? 
*               WPARAM wParam - message parameter 
*               LPARAM lParam - message parameter 
* 
*     RETURNS:  BOOL - TRUE for OK, FALSE for Cancel 
* 
\****************************************************************************/ 

BOOL CALLBACK AddDlgProc(HWND window, UINT Msg, WPARAM wParam, LPARAM lParam) 
{ 
	switch( Msg ) 
    { 
        case WM_CLOSE: 
            PostMessage(window, WM_COMMAND, IDCANCEL, 0l); 
        break;
        // Messages from user items - checkboxes etc.
        case WM_COMMAND: 
            switch( LOWORD(wParam) ) 
            { 
                case IDOK:		  // Time to cancel 
                    EndDialog(window, FALSE); 
                break;
            } 
        break;
		case WM_DESTROY:
			EndDialog(window, FALSE);
		break;
        default: 
            return FALSE; 
        break; 
    } 
    return TRUE; 
}

BOOL CALLBACK AddPripropDlgProc(HWND window, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	switch( Msg ) 
    { 
        case WM_CLOSE: 
            PostMessage(window, WM_COMMAND, IDCANCEL, 0l); 
        break;
        // Messages from user items - checkboxes etc.
        case WM_COMMAND: 
            switch( LOWORD(wParam) ) 
            { 
                case IDOK:		  // Time to cancel 
                    EndDialog(window, FALSE ); 
                break;
				case IDCANCEL:
				break;
				//TB_BOTTOM
				//TB_TOP
            } 
        break;
		case WM_DESTROY:
			EndDialog(window, FALSE );
		break;
        default: 
            return FALSE; 
        break; 
    } 
    return TRUE; 
}

BOOL CALLBACK AddScriptDlgProc(HWND window, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	switch(Msg)
    { 
        // Dialog is being initialized 
        case WM_INITDIALOG: 
        { 
  		    HWND hListBox = NULL;
			char linesequence[255]; 
			strcpy(linesequence,"");
			// Get the windows we need.
			hListBox = GetDlgItem(window, IDC_MAINLIST);
			// Put the strings in the list box.
			tle<char*> *nodemsg = Framework->csl->GetRootMsgList();
			int cT=0, n = Framework->csl->GetCntMessages();
			for (;(cT<n)&(nodemsg!=NULL);cT++)
			{
				while (nodemsg->nxt)
				{
					if ((nodemsg->nxt->data[0] == '>'))
					{
						strcat(linesequence,nodemsg->data);
						break;
					}
					else 
					{
						strcat(linesequence,nodemsg->data);
					}
					nodemsg=nodemsg->nxt;
				}
				SendMessage(hListBox, LB_ADDSTRING, 0, (LPARAM)linesequence);
				strcpy(linesequence,"");
				nodemsg=nodemsg->nxt;
			}
			// We are passed a pointer to a LPICONIMAGE in lParam, save it 
            // Set the range and position of the sliders 
        } 
        break;
        // Time to say 'goodbye'
        case WM_CLOSE: 
            PostMessage(window, WM_COMMAND, IDCANCEL, 0l); 
        break;
        // Messages from user items - checkboxes etc.
        case WM_COMMAND: 
            switch( LOWORD(wParam) ) 
            { 
                // Time to cancel 
                case IDOK: 
                    EndDialog(window, FALSE); 
                break;
            } 
        break;
		case WM_DESTROY:
			EndDialog(window, FALSE);
		break;
        default: 
            return FALSE; 
        break; 
    } 
    return TRUE; 
}

void WINDOW::SetupRenderLayout(int layout)
{
	Framework->ChangeRenderLayout(layout);
}

void WINDOW::WriteGraphBuffer(void)
{
	// alle Grafikausgaben des Frontbuffers t�tigen:
	char posstring[MAX_LOADSTRING];
	VERTEX* campos = Framework->cam->getpos();
	strcpy(posstring,"");
	strcat(posstring,Convert->float2string(campos->x));
	strcat(posstring," ");
	strcat(posstring,Convert->float2string(campos->y));
	strcat(posstring," ");
	strcat(posstring,Convert->float2string(campos->z));
	if (DDraw->CheckedFPS()) DDraw->printmessage(TEXTLINELEFT,TEXTLINEBOTTOM,Convert->int2string(Framework->time->fps()));
	DDraw->printmessage(TEXTLINERIGHT-100,TEXTLINEBOTTOM,posstring);
	Framework->DrawFrame(*(DDraw->GetVideoMemory()));
	// Grafikausgabe komplett
}

// Fensterbehandlungsroutine f�r das Hauptfenster. Hier werden die von 
// Windows gesendeten Nachrichten ausgewertet. Wichtige Nachrichten sind z.B.
LRESULT CALLBACK WindowProc(HWND window, UINT message, WPARAM wParam, LPARAM lParam )
{
	static LPARAM lpii;
	if (message == WM_CREATE)
	{
		CREATESTRUCT* createstruct;
		createstruct =(CREATESTRUCT*)lParam;
		SetWindowLong(window,0,(LONG)(createstruct->lpCreateParams));
		return 0;
	}
	WINDOW* object = (WINDOW*)GetWindowLong(window,0);
	if (object)
	{
		switch( message ) 
		{
			case WM_DESTROY:
			{
				return object->Destroy(window,message,wParam,lParam);
			}
			break;
			case WM_ACTIVATEAPP:
				//DDraw->restoreSurfaces();
			break;
			case WM_ENTERMENULOOP:
				object->AppPause(true);
			break;
			case WM_EXITMENULOOP:
				object->AppPause(false);
			break;
			break;
			case WM_KEYDOWN:
				switch( wParam ) 
				{
					case VK_ESCAPE:
					case VK_F12:
						PostMessage(window, WM_CLOSE, 0, 0);
					break;
				}
			break;
			/*
			case WM_MOVE:
			break;
			case WM_SIZE:
			{
				GetWindowRect(hMainWnd, &g_rcWindow);
				LPPOINT newmouse = GetActMousePos(),
					    oldmouse = GetOldMousePos();
				int winwidth  = (int)(g_rcWindow.right - g_rcWindow.left),
					winheight = (int)(g_rcWindow.bottom- g_rcWindow.top );
				MoveWindow(hMainWnd,g_rcWindow.left+(newmouse->x-oldmouse->x),
					                g_rcWindow.top +(newmouse->y-oldmouse->y),
									winwidth,winheight,TRUE);
				UpdateWindow(hMainWnd);
			}
			break;
			// Maus-Steuerung:
			case WM_MOUSEMOVE:							//	on a mouse moved
				ActualizeMouse();							
			break;
			case WM_LBUTTONDOWN:		//	if the left mouse button is clicked down
				Beep(4000,500);
			break;
			case WM_LBUTTONUP:			//	left mouse button lifted
				Beep(4000,500);
			break;
			*/
			// Tastatur-Steuerung:
			// Tastenabfrage: 'Hauptmen�'
			case WM_COMMAND:	
				switch(LOWORD(wParam))
				{
					// Menu-Punkte
					case ID_FILE_EXIT:
						PostMessage(window, WM_CLOSE, 0, 0);
						return 0L;
					case ID_FILE_SHOWCONSOLE:
						if (DialogBoxParam(Manager->GetInstance(),MAKEINTRESOURCE(IDD_ScriptConsole),object->GetWndHandle(),AddScriptDlgProc,(LPARAM)(&lpii))) 
						{ return FALSE; }
					break;
					case ID_FILE_SHOWFRAMESPERSE:
						{
							object->GetMenuObj()->Check(ID_FILE_SHOWFRAMESPERSE);
							object->GetDDrawObj()->CheckFPS();
						}
					break;
					case ID_Support:
						if (DialogBoxParam(Manager->GetInstance(),MAKEINTRESOURCE(IDD_ABOUT),object->GetWndHandle(),AddDlgProc,(LPARAM)(&lpii))) 
						{ return FALSE; }
					break;
					case ID_WINDOWSETTINGS:
						if (DialogBoxParam(Manager->GetInstance(),MAKEINTRESOURCE(IDD_WindowPriorities),object->GetWndHandle(),AddPripropDlgProc,(LPARAM)(&lpii))) 
						{ return FALSE; }
					break;
					case ID_OPTIONS_LAYOUT_POINTS:
						{
							object->GetMenuObj()->Check(ID_OPTIONS_LAYOUT_POINTS);						
							object->SetupRenderLayout(pointlayout);
						}
					break;
					case ID_OPTIONS_LAYOUT_WIREFRAME:
						{
							object->GetMenuObj()->Check(ID_OPTIONS_LAYOUT_WIREFRAME);						
							object->SetupRenderLayout(wireframelayout);
						}
					break;
					case ID_OPTIONS_LAYOUT_TEXTUREMAPPED:
						{
							object->GetMenuObj()->Check(ID_OPTIONS_LAYOUT_TEXTUREMAPPED);						
							object->SetupRenderLayout(monochromtexturedlayout);
						}
					break;
					case ID_OPTIONS_LAYOUT_SOLIDMAPPED:
						{
							object->GetMenuObj()->Check(ID_OPTIONS_LAYOUT_SOLIDMAPPED);						
							object->SetupRenderLayout(texturedmapped);
						}
					break;
	/*
					case ID_SETTINGS_SOUND:
					break;
					case ID_SETTINGS_GRAPHICS_RESOLUTION:
					break;
					case ID_SETTINGS_GRAPHICS_DRIVER:
					break;
					case ID_FILE_RELOAD:
					break;
					case ID_SETTINGS_MOUSE:
					break;
					case ID_SETTINGS_MOUSE_LOOKUP:
					break;
					case ID_SETTINGS_MOUSE_SENSITY_LOW:
					break;
					case ID_SETTINGS_MOUSE_SENSITY_MEDIUM:
					break;
					case ID_SETTINGS_MOUSE_SENSITY_HIGH:
					break;
	*/
					case IDM_QUIT:
						PostMessage(window, WM_CLOSE, 0, 0);
					break;
					// interaktive Steuerung
					case IDM_LEFT:
						Framework->cam->rotateAlpha(-0.017f);
					break;
					case IDM_RIGHT:
						Framework->cam->rotateAlpha(0.017f);
					break;
					case IDM_UP:
						Framework->cam->translateY(-0.5f);
					break;
					case IDM_DOWN:
						Framework->cam->translateY(0.5f);
					break;
					case IDM_STEPLEFT:
						Framework->cam->translateX(-0.5f);
					break;
					case IDM_STEPRIGHT:
						Framework->cam->translateX(0.5f);
					break;
					case IDM_CENTERVIEW:
						Framework->cam->setcenterview();
					break;
					case IDM_ZOOMIN:
						//cam->addscale(0.9f);
					break;
					case IDM_ZOOMOUT:
						//cam->addscale(1.1f);
					break;
					case IDM_FORWARD:
						Framework->cam->translateZ(-1.0f);
					break;
					case IDM_BACKWARD:
						Framework->cam->translateZ(1.0f);
					break;
					case IDM_ABOUT:
						if (DialogBoxParam(Manager->GetInstance(),MAKEINTRESOURCE(IDD_ABOUT),object->GetWndHandle(),AddDlgProc,(LPARAM)(&lpii))) 
						{ return FALSE; }
					break;
		}
	}
	}
	return DefWindowProc(window, message, wParam, lParam);
}
/*
//TrackBar-Properties:
//===================
SendMessage(hwndTrack, TBM_SETRANGE, 
        (WPARAM) TRUE,                   // redraw flag 
        (LPARAM) MAKELONG(iMin, iMax));  // min. & max. positions 
    SendMessage(hwndTrack, TBM_SETPAGESIZE, 
        0, (LPARAM) 4);                  // new page size 
 
    SendMessage(hwndTrack, TBM_SETSEL, 
        (WPARAM) FALSE,                  // redraw flag 
        (LPARAM) MAKELONG(iSelMin, iSelMax); 
    SendMessage(hwndTrack, TBM_SETPOS, 
        (WPARAM) TRUE,                   // redraw flag 
        (LPARAM) iSelMin); 
*/