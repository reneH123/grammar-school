#ifndef FRAMEDef
#define FRAMEDef

#include "common.hpp"
#include "bsp.hpp"
#include "console.hpp"
#include "mouse.hpp"
#include "timer.hpp"
#include "camera.hpp"
#include "vertex.hpp"
//#ifdef WIN32DDRAW9X	
#include "../ProcessorDetect/mathlib.hpp"
//#endif
#include "../Resources/resource.h"

#define MaxWorldItems 1
const VERTEX FIXEDCAMERAPOS(5,3,-7);

const VERTEX poss[MaxWorldItems] = {(-1.0f,-1.0f,5.0f)}; //,(-2.0f,-2.0f,-2.0f)};

class framework 
{
public:
// Error/Processor-Klassen fehlen!
	console *csl;		// Nachrichtenkonsole
	timer *time;
	camera *cam;
	bsptree *world;
	mouse *lmouse;
	//tlist<DO*> models; // alle Weltmodelle zusammen
	framework(void)
	{
		csl = new console();
		// String - Variablen f�r Err/Console/List:
		char_ cmd_initfs,cmd_errpriclass,cmd_err_threadclass,cmd_highpriclass,cmd_highthreadclass,
			  cmd_initmath,cmd_setmouse,cmd_settime,cmd_initgraph,cmd_resx,cmd_resy,cmd_bpp,
			  cmd_initcam,cmd_camx,cmd_camy,cmd_camz, cmd_initdo,cmd_faces,cmd_vertices,
			  cmd_initbsp,cmd_bspdepthleft,cmd_bspdepthright,dx_platform,dx_version;
		GetResourceString(IDS_CMD_INITFS,cmd_initfs);
		csl->Insert(cmd_initfs);
		if (!SetPriorityClass(GetCurrentProcess(), IDLE_PRIORITY_CLASS/*HIGH_PRIORITY_CLASS*/))	// set thread priority to one level below real-time
		{ 
			GetResourceString(IDS_CMD_ERR_PRICLASS,cmd_errpriclass);
			csl->Insert(cmd_errpriclass);
			csl->LogToFile("cmd.log");
		}
		if (!SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_IDLE/*THREAD_PRIORITY_HIGHEST*/))
		{ 
			GetResourceString(IDS_CMD_ERR_THREADCLASS,cmd_err_threadclass);
			csl->Insert(cmd_err_threadclass);
			csl->LogToFile("cmd.log");
		}
		GetResourceString(IDS_CMD_HIGHPRICLASS,cmd_highpriclass);
		csl->Insert(cmd_highpriclass);
		GetResourceString(IDS_CMD_HIGHTHREADCLASS,cmd_highthreadclass);
		csl->Insert(cmd_highthreadclass);
		GetResourceString(IDS_CMD_INITMATH,cmd_initmath);
		csl->Insert(cmd_initmath);	
//		#ifdef WIN32DDRAW9X
		tlist<char *> *ProcessorIndicator;			// darf nie gel�scht werden!
		ProcessorIndicator = new tlist<char *>;
		ProcessorDetect(*ProcessorIndicator);
		csl->InsertReportList(*ProcessorIndicator);	// concate temp an csl->errmsg
//		#endif
		csl->Insert(">Initializing DirectX:");
		DDRAW::DirectXDetect(dx_platform, dx_version);//DDraw->DirectXDetect(DXPlatform, DXVersion);
		csl->Insert(">--- Version: ");   
		csl->Insert(dx_version);
		csl->Insert(">--- OS Platform: ");
		csl->Insert(dx_platform);
		lmouse = new mouse;
		GetResourceString(IDS_CMD_SETMOUSE,cmd_setmouse);
		csl->Insert(cmd_setmouse);
		time = new timer();
		GetResourceString(IDS_CMD_SETTIME,cmd_settime);
		csl->Insert(cmd_settime);
		GetResourceString(IDS_CMD_INITGRAPH,cmd_initgraph);
		csl->Insert(cmd_initgraph);
		GetResourceString(IDS_CMD_RESX,cmd_resx);
		csl->Insert(cmd_resx);
		csl->Insert(Convert->int2string(RESX));
		GetResourceString(IDS_CMD_RESY,cmd_resy);
		csl->Insert(cmd_resy); 
		csl->Insert(Convert->int2string(RESY));
		GetResourceString(IDS_CMD_BPP,cmd_bpp);
		csl->Insert(cmd_bpp);  
		csl->Insert(Convert->int2string(DEPTHCOLOR));
		GetResourceString(IDS_CMD_INITCAM,cmd_initcam);
		csl->Insert(cmd_initcam);
		cam = new camera(FIXEDCAMERAPOS);
		GetResourceString(IDS_CMD_CAMX,cmd_camx);
		csl->Insert(cmd_camx); 
		csl->Insert(Convert->float2string(cam->getpos()->x));
		GetResourceString(IDS_CMD_CAMY,cmd_camy);
		csl->Insert(cmd_camy);   
		csl->Insert(Convert->float2string(cam->getpos()->y));
		GetResourceString(IDS_CMD_CAMZ,cmd_camz);
		csl->Insert(cmd_camz);   
		csl->Insert(Convert->float2string(cam->getpos()->z));
		csl->Insert(";}");
		GetResourceString(IDS_CMD_INITDOs,cmd_initdo);
		csl->Insert(cmd_initdo);
		tlist<tle<VERTEX>*> *verticesList = new tlist<tle<VERTEX>*>;	// tempor�re Liste
		tlist<tle<TRIANGLE>*> *facesList = new tlist<tle<TRIANGLE>*>;	// tempor�re Liste
		LoadWorld(verticesList,facesList);
		world = new bsptree(verticesList,facesList);
		delete verticesList;
		delete facesList;
		GetResourceString(IDS_CMD_VERTICES,cmd_vertices);
		csl->Insert(cmd_vertices);
		csl->Insert(Convert->int2string(world->CountVertices()));
		GetResourceString(IDS_CMD_FACES,cmd_faces);
		csl->Insert(cmd_faces);
		csl->Insert(Convert->int2string(world->CountFaces()));
		GetResourceString(IDS_CMD_INITBSP,cmd_initbsp);
		csl->Insert(cmd_initbsp);
		int leftdepth,rightdepth;
		world->depthtree(leftdepth,rightdepth); // Tiefe des Baumes ermitteln
		GetResourceString(IDS_CMD_BSPDEPTHLEFT,cmd_bspdepthleft);
		csl->Insert(cmd_bspdepthleft);
		csl->Insert(Convert->int2string(leftdepth));
		GetResourceString(IDS_CMD_BSPDEPTHRIGHT,cmd_bspdepthright);
		csl->Insert(cmd_bspdepthright);
		csl->Insert(Convert->int2string(rightdepth));
		csl->Insert("end of console");
		bool res = csl->LogToFile("cmd.log"); // Schreiben der Log-Eintr�ge
		
	//	SetMessageQueue(csl->GetRootMsgList());
	//	tle<char*>* test = GetMessageQueue();
// Auswertung von res!
	}
	~framework(void)
	{
		delete time;
		delete csl;
		delete world;
		delete lmouse;
		delete cam;
	}
	void DrawFrame(const vidmem &VideoMemory)
	{
		world->RenderTree(cam->getpos(),VideoMemory);
	}
	void ChangeRenderLayout(int designid)
	{
		switch(designid)
		{
			case pointlayout:
			{
				#define POINTS
				#undef WIREFRAME
				#undef MONOCHROMTEXTURED
				#undef TEXTUREDMAPPED
			}
			break;
			case wireframelayout:
				#undef POINTS
				#define WIREFRAME
				#undef MONOCHROMTEXTURED
				#undef TEXTUREDMAPPED
			break;
			case monochromtexturedlayout:
				#undef POINTS
				#undef WIREFRAME
				#define MONOCHROMTEXTURED
				#undef TEXTUREDMAPPED
			break;
			case texturedmapped:
				#undef POINTS
				#undef WIREFRAME
				#undef MONOCHROMTEXTURED
				#define TEXTUREDMAPPED
			break;
		}
	}
private:
	void LoadWorld(tlist<tle<VERTEX>*> *vertices, tlist<tle<TRIANGLE>*> *faces)			// Einlesen aller Daten von HDD
	{
		char* errmsg;
		FILE *f;
		VERTEX v; TRIANGLE t;		// Einlesemuster
		char gobjname[256];         // name of geometry object -> objlist
		int ct,                     // Counter
		    AB,BC,CA,               // indices of vertices
            SMOOTH,MTLID;           // properties for faces (unused!)

		if (((f=fopen("haus.dat","r")) == NULL))
		{ errmsg = "Error occured while open file"; }
		char buffer[256];
		fscanf(f,"%s\n",buffer);
		if (strcmp(buffer,"Scene:haus.dat(C)opyright;Haberland,Ren�;1982-2000")!=0)
		{ errmsg = "corrupt copyright!"; }
		while (!feof(f))
		{
			// Einlesen eines GEOMOBJECT`s:
			fscanf(f,"%s\n",buffer);
			fscanf(f,"%s\n",buffer);
			sscanf(buffer,"*GEOMOBJECT::""%s""", &gobjname);   // optional
			fscanf(f,"%s\n",buffer);
			fscanf(f,"%s\n",buffer);                           //   "*MESH_VERTEX_LIST {"
			// read verticies
			tlist<VERTEX>* vqueue = new tlist<VERTEX>;
			fscanf(f,"%s\n",buffer);
			do{
				sscanf(buffer,"*MESH_VERTEX%d:%f;%f;%f",&ct,&v.x,&v.y,&v.z);
				vqueue->push_back(v);		// an Vertex-Liste anf�gen!
				fscanf(f,"%s\n",buffer);
			} while (strcmp(buffer,"}")!=0);
			vertices->push_back(vqueue->getroot());
			// read faces
			tlist<TRIANGLE>* fqueue = new tlist<TRIANGLE>;
			fscanf(f,"%s\n",buffer);                 //   "*MESH_FACE_LIST {"
			fscanf(f,"%s\n",buffer);			     //   leerer Puffer
			do{
				sscanf(buffer,"*MESH_FACE%d:A:%d;B:%d;C:%d;AB:%d;BC:%d;CA:%d;*MESH_SMOOTHING:%d;*MESH_MTLID:%d\n",
						&ct,&t.p1,&t.p2,&t.p3,&AB,&BC,&CA,&SMOOTH,&MTLID);
				fqueue->push_back(t);		// an Face-Liste anf�gen!
				fscanf(f,"%s\n",buffer);
			} while (strcmp(buffer,"}")!=0);
			faces->push_back(fqueue->getroot()); 
			fscanf(f,"%s\n",buffer);      // Abschlussklammerung f�r GEOMOBJECT
		}
		//Ende des Einlesens eines GEOMOBJECT`s
		if (fclose(f)!=NULL)
		{ errmsg = "Error while closing file occured!"; };
		errmsg = "";  // '' statt ""
// FEHLT!!: Resultat in errmsg zur�ckliefern
	}

};

framework* Framework;

#endif