#ifndef ClipDef
#define ClipDef

#include "vertex.hpp"
#include "list.hpp"

class clipper
{
public:
	clipper(void){}				            // Clipper-Konstruktor
	~clipper(void){}					    // Clipper-Destruktor
	void SetViewPort(const VERTEX* Cam)	    // setzt Betrachter
	{
		ViewPort = *Cam;
	}
	void SetVertexList(tlist<VERTEX>* vl)	// setzt 3D-Punkt-Liste
	{
		VertexList = vl;
	}
	tlist<VERTEX>* GetVertexList(void)
	{
		return VertexList;
	}
	void SetPixelList(tlist<PIXEL>* pl)  	// setzt 2D-Punkt-Liste
	{
		PixelList = pl;
	}
	tlist<PIXEL>* GetPixelList(void)
	{
		return PixelList;
	}
	unsigned code(const PIXEL& p)			// ClipCode f�r Punktposition nach Cohen
	{
		unsigned res = 0;
		if (p.x<0) res |= 1;
		if (p.y<0) res |= 2;
		if (p.x>/*RESX*/640) res |= 4;
		if (p.y>/*RESY*/480) res |= 8;
		return res;
	}
	void NearPlane3D(void)					// Clippen an (z=0) ~ Betrachter-Nah-Ebene
	{
		VERTEX intersect; // Schnittpunkt mit XY-Ebene
		tlist<VERTEX> *res = new tlist<VERTEX>;
		tle<VERTEX> *begin = VertexList->getroot();
		while  (begin!=NULL)
		{
			VERTEX a = begin->data - ViewPort,
				   b = ((begin->nxt!=NULL)?begin->nxt->data:VertexList->getroot()->data) - ViewPort;
			bool a_before = ((a.z>0)?true:false),
				 b_before = ((b.z>0)?true:false);
			if ( a_before & !b_before )
			{
				intersect = XYInterSect(&a,&b);
				res->push_back(a);
				res->push_back(intersect);
			}
			else if ( !a_before & b_before )
			{
				intersect = XYInterSect(&b,&a);
				res->push_back(intersect);
			}
			else if ( a_before )
			{
				res->push_back(a);
			}
			begin = begin->nxt;
		}
		VertexList = res;
//		delete res;
	}
	// clipt Polygon an Fenstergrenzen des Bildschirmes: nach Sutherland-Hodgman
	// !! Clipsequenz einhalten -> Gegenuhrzeigersinn-Indizierung !!!
	void Polygon(void)			  // Clippen des Polygons an physikal. Bildschirm
	{
		tlist<PIXEL> *res = new tlist<PIXEL>;	// Zwischenresultatspolygon
		res->setroot(PixelList->getroot());
		tle<PIXEL> *begin; 
		// 1. MaxX clipping ( plane : X = ResX )
		begin = res->getroot();
		PixelList->setroot(begin);
		res->setroot(NULL);
		res->setlast(NULL);
		while (begin!=NULL)						
		{
			PIXEL p1 = begin->data,								// Punkte f�r Linie laden
				  p2 = ((begin->nxt)?begin->nxt->data:PixelList->getroot()->data); 
			unsigned c1 = code(p1),						// deren Statute
					 c2 = code(p2);				
			if (((c1&4)!=4)&((c2&4)!=4)) res->push_back(p2);	// IN ->IN;  save the last one.
			else if	(((c1&4)!=4)&((c2&4)==4))					// IN ->OUT; save the intersect point
			{
				PIXEL newp1 = p1;
				maxx(newp1,p2);
				res->push_back(newp1);
			}
			else if (((c1&4)==4)&((c2&4)!=4))					// OUT->IN;  save the intersect point + and the vertex that is in
			{
				PIXEL newp1 = p1;
				maxx(newp1,p2);
				res->push_back(newp1);
				res->push_back(p2);
			}
			begin = begin->nxt;
		}
		// 2. MinY clipping ( plane : Y = 0    )
		begin = res->getroot();
		PixelList->setroot(begin);
		res->setroot(NULL);
		res->setlast(NULL);
		while (begin!=NULL)						
		{
			PIXEL p1 = begin->data,								// Punkte f�r Linie laden
				  p2 = ((begin->nxt)?begin->nxt->data:PixelList->getroot()->data); 
			unsigned c1 = code(p1),						// deren Statute
					 c2 = code(p2);				
			if (((c1&2)!=2)&((c2&2)!=2)) res->push_back(p2);	// IN ->IN;  save the last one.
			else if	(((c1&2)!=2)&((c2&2)==2))					// IN ->OUT; save the intersect point
			{
				PIXEL newp1 = p1;
				miny(newp1,p2);
				res->push_back(newp1);
			}	
			else if (((c1&2)==2)&((c2&2)!=2))					// OUT->IN;  save the intersect point + and the vertex that is in
			{
				PIXEL newp1 = p1;
				miny(newp1,p2);
				res->push_back(newp1);
				res->push_back(p2);
			}
			begin = begin->nxt;
		}
		// 3. MinX clipping ( plane : X = 0    )
		begin = res->getroot();
		PixelList->setroot(begin);
		res->setroot(NULL);
		res->setlast(NULL);
		while (begin!=NULL)						
		{
			PIXEL p1 = begin->data,								// Punkte f�r Linie laden
		 		  p2 = ((begin->nxt)?begin->nxt->data:PixelList->getroot()->data); 
			unsigned c1 = code(p1),						// deren Statute
					 c2 = code(p2);				
			if (((c1&1)!=1)&((c2&1)!=1)) res->push_back(p2);	// IN ->IN;  save the last one.
			else if	(((c1&1)!=1)&((c2&1)==1))					// IN ->OUT; save the intersect point
			{
				PIXEL newp1 = p1;
				minx(newp1,p2);
				res->push_back(newp1);
			}
			else if (((c1&1)==1)&((c2&1)!=1))					// OUT->IN;  save the intersect point + and the vertex that is in
			{
				PIXEL newp1 = p1;
				minx(newp1,p2);
				res->push_back(newp1);
				res->push_back(p2);
			}
			begin = begin->nxt;
		}
		// 4. MaxY clipping ( plane : Y = ResY )
		begin = res->getroot();
		PixelList->setroot(begin);
		res->setroot(NULL);
		res->setlast(NULL);
		while (begin!=NULL)						
		{
			PIXEL p1 = begin->data,								// Punkte f�r Linie laden
				  p2 = ((begin->nxt)?begin->nxt->data:PixelList->getroot()->data); 
			unsigned c1 = code(p1),						// deren Statute
					 c2 = code(p2);				
			if (((c1&8)!=8)&((c2&8)!=8)) res->push_back(p2);	// IN ->IN;  save the last one.
			else if	(((c1&8)!=8)&((c2&8)==8))					// IN ->OUT; save the intersect point
			{
				PIXEL newp1 = p1;
				maxy(newp1,p2);
				res->push_back(newp1);
			}
			else if (((c1&8)==8)&((c2&8)!=8))					// OUT->IN;  save the intersect point + and the vertex that is in
			{
				PIXEL newp1 = p1;
				maxy(newp1,p2);
				res->push_back(newp1);
				res->push_back(p2);
			}
			begin = begin->nxt;
		}
		*PixelList = *res; // liefere geclipptes Polygon zur�ck
	}
	// f�r weitere Klassen, zu erbende Eigenschaften:
	//unsigned clip_code(const PIXEL &p);  // Status des Punktes in Bezug auf Umgabung
	//void clipline(PIXEL *p1, PIXEL *p2); // Clipping nach Cohen & Sutherland
private:
	VERTEX ViewPort;
	tlist<VERTEX>* VertexList;			 // 3D-Punktliste
	tlist<PIXEL>* PixelList;			 // 2D-Punktliste
	void minx(PIXEL& p1,const PIXEL& p2)	// MinX clipping ( plane : X = 0    )
	{
		p1.y = (int)(-( float(p2.y-p1.y) / (p2.x-p1.x)*p1.x ) + p1.y);
		p1.x = 0;
	}
	void miny(PIXEL& p1, const PIXEL& p2 )	// MinY clipping ( plane : Y = 0    )
	{
		p1.x = (int)(-( float(p2.x-p1.x) / (p2.y-p1.y)*p1.y ) + p1.x);
		p1.y = 0;
	}
	void maxx(PIXEL& p1,const PIXEL& p2)	// MaxX clipping ( plane : X = ResX )
	{
		p1.y = (int)(-( float(p2.y-p1.y) / (p2.x-p1.x)*(p1.x-/*RESX*/640) ) + p1.y);
		p1.x = /*RESX*/640;
	}
	void maxy(PIXEL& p1,const PIXEL& p2)	// MaxY clipping ( plane : Y = ResY )
	{
		p1.x = (int)(-( float(p2.x-p1.x) / (p2.y-p1.y)*(p1.y-/*RESY*/480) ) + p1.x);
		p1.y = /*RESY*/480;
	}	
	VERTEX XYInterSect(const VERTEX* a,const VERTEX* b)	// Schnittpunkt: Ebene (z=0) + Gerade zw. 2 Punkten
	{
		float t = -(a->z)/(a->z-b->z);
		VERTEX S;
		S.x = a->x+(t*(a->x-b->x));
		S.y = a->y+(t*(a->y-b->y));
		S.z = 0;
		return S;
	}
};

#endif