#ifndef TimerDef
#define TimerDef

#include <windows.h>

class timer
{
   private:
     int framerate;
	 int framecount;
	 int framecount0;
	 DWORD frametime;
	 DWORD frametime0;	
   public:
 	 timer(void)
	 {
		 framerate = 0;
	     framecount = 0;
	     framecount0 = 0;
	 }
	 ~timer(void)
	 {
	 }
     int fps( void )		// aktuelle fps
	 {
		 int savedframes = framerate;
		framecount++;
		frametime = timeGetTime();
		if( ( frametime-frametime0 )>1000 ) 
		{
			framerate = ( framecount-framecount0 )*1000 / ( frametime-frametime0 );
			frametime0 = frametime;
			framecount0 = framecount;
		}
		return savedframes;
	 }
};

#endif