#ifndef VertexDef
#define VertexDef

// Compiler-Optimiert: const VERTEX& statt VERTEX übergeben!

#include <math.h>

#define Dot_product(a, b)\
       ((a).x * (b).x + (a).y * (b).y + (a).z * (b).z)
#define Length(v)\
       (sqrt(((a).x*(a).x)+((a).y*(a).y)+((a).z*(a).z)))

class VERTEX
{   
public:
	VERTEX(float x_ = 0, float y_ = 0, float z_ = 0) // Konstruktor
	{
		x = x_;
		y = y_;
		z = z_; 
	}
	inline VERTEX& operator() (float a, float b, float c)
	{
		return VERTEX(x = a,y = b,z = c);
	}
	inline VERTEX& operator+=(const VERTEX& b)				 // komponentenweises Zuweisen
	{
		return VERTEX(x+b.x,y+b.y,z+b.z);
	}
	inline VERTEX& operator-=(const VERTEX& b)				
	{
		return VERTEX(x-b.x,y-b.y,z-b.z);
	}
	inline VERTEX& operator*=(float b)
	{
		return VERTEX(x*b,y*b,z*b);
	}
	inline VERTEX& operator/=(float b)
	{
		return VERTEX(x/b,y/b,z/b);
	}
	inline VERTEX& norm(void)								// auf Einheitslänge skalieren
	{
		return VERTEX(x=1,y=1,z=1);
	}
	inline float abs(const VERTEX& a)
	{
		return (float)Length(a);
	}
	float x,y,z;									// Komponenten
};
// dazugehörige überladene Operatoren:
const VERTEX operator-(const VERTEX& a);					// unäres Minus
const VERTEX operator+(const VERTEX& a, const VERTEX& b);	// Differenz
const VERTEX operator-(const VERTEX& a, const VERTEX& b);	// Summe
const VERTEX operator*(float a, const VERTEX& b);			// Skalares Multiplizieren
const VERTEX operator*(const VERTEX& a, float b);			
const VERTEX operator/(const VERTEX& a, float b);			// Skalares Dividieren
float operator*(const VERTEX& a, const VERTEX& b);	// Kreuzprodukt
const VERTEX operator%(const VERTEX& a, const VERTEX& b); // Punktprodukt
float abs(const VERTEX& a);							// Euklidische Normale
int operator==(const VERTEX& a, const VERTEX& b);	// Gleichheit

inline const VERTEX operator-(const VERTEX& a)
{
	return VERTEX(-a.x,-a.y,-a.z);
}
inline const VERTEX operator+(const VERTEX& a, const VERTEX& b)
{
	return VERTEX(a.x+b.x,a.y+b.y,a.z+b.z);
}
inline const VERTEX operator-(const VERTEX& a, const VERTEX& b)
{
	return VERTEX(a.x-b.x,a.y-b.y,a.z-b.z);
}
inline const VERTEX operator*(float a, const VERTEX& b)
{
	return VERTEX(a*b.x,a*b.y,a*b.z);
}
inline const VERTEX operator*(const VERTEX& a, float b)
{
	return VERTEX(a.x*b,a.y*b,a.z*b);
}
inline const VERTEX operator/(const VERTEX& a, float b)
{
	return VERTEX(a.x/b,a.y/b,a.z/b);
}
inline float operator*(const VERTEX& a, const VERTEX& b)	
{
	 return Dot_product(a,b);
}
inline const VERTEX operator%(const VERTEX& a, const VERTEX& b)	
{ 
    return VERTEX(a.y*b.z-a.z*b.y, a.z*b.x-a.x*b.z, a.x*b.y-a.y*b.x);
}
inline int operator==(const VERTEX& a, const VERTEX& b)
{
	return (((a.x==b.x)&(a.y==b.y)&(a.z==b.z))?true:false);
}

typedef struct tagPIXEL{	// Bildschirmkoordinatenangabe
	int x,y;
} PIXEL;

typedef struct tagTRIANGLE  // Struktur zur Verwaltung der Linien zwischen Punkten
{
	int p1;
	int p2;
	int p3;
} TRIANGLE;

typedef struct tagTRIANGLEVERTEX{	// Dreieckskoodinaten 3d
	VERTEX tv1;
	VERTEX tv2;
	VERTEX tv3;
} TRIANGLEVERTEX;

typedef struct tagTRIANGLEPIXEL{	// Dreieckskoodinaten 2d
	PIXEL tp1, tp2, tp3;
} TRIANGLEPIXEL;

typedef	struct tagRGBCOLOR{
	unsigned char	r,g,b;
} RGBCOLOR;

typedef struct tagTEXTUR{
	int width;
	int height;
	long *data;
} TEXTUR;

#endif