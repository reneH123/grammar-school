#ifndef BSPDef
#define BSPDef

#include "vertex.hpp"
#include "List.hpp"
#include "polygon.hpp"
#include "render.hpp"
#include "common.hpp"

struct BSPNODE{
	BSPNODE* left;
	BSPNODE* right;
	tlist<VERTEX>* n;
};

class bsptree
{
private:
	tlist<VERTEX>* vertices;
	tlist<TRIANGLE>* faces;
	BSPNODE* bspnode;														// BSP-Baum
	polylist* plist;														// list for polygon
	VERTEX CamPos;															// temp. Variable zum Rendern
	render* Render;
	void SplitPolygon(tlist<VERTEX>*,tlist<VERTEX>*,tlist<VERTEX>*,tlist<VERTEX>*); // splittet ein bestehendes Polygon in ev. 2
	void createbsptree(polylist*,BSPNODE*);							        // Aufbau des Baumes
	void DetBeforeBehind(tlist<VERTEX>*,polylist*,polylist*,polylist*);	    // teilt ein in davor | dahinter
	void destroybsptree(BSPNODE*);									        // Zerst�ren des Baumes
	void OutTree(BSPNODE*,int&,int&);										// rekursives Durchqueren des Baumes
	float ClassifyPoint(tlist<VERTEX>*);									// Schnittpunkt einer Gerade AB und einer Ebene
	VERTEX GetInterSection(tle<VERTEX>*,VERTEX);							// intersection between plane and "delta"vector
	void traverse_render(const BSPNODE*);									// Baum durchlaufen & rendern
	void MakeInputFormate(tlist<tle<VERTEX>*>*,tlist<tle<TRIANGLE>*>*);		// Eingabeparameter f�r BSP jetzt vorhanden
	void bspinit(void);														// initialisiere bin�ren Baum
public:
	bsptree(tlist<tle<VERTEX>*>*,tlist<tle<TRIANGLE>*>*);					// Konstruktor
	~bsptree();																// Destruktor
	BSPNODE* GetBspNode(void) const;										// bsp node
	VERTEX* GetPlaneIntersect(tle<VERTEX>*,const VERTEX&,const VERTEX&) const; // Schnittpunkt: Polygon & Ebene
	int CountVertices(void);												// Anzahl der Punkte
	int CountFaces(void);													// Anzahl der Polygone
	void depthtree(int&,int&);												// Tiefe des Baumes links und rechts
//	void RenderTree(const VERTEX*);											// Baum komplett graphisch ausgeben (->BSPtraverseTreeAndRender)
	void RenderTree(const VERTEX*,const vidmem&);
};

int bsptree::CountVertices(void)
{
	return vertices->count_items();
}

int bsptree::CountFaces(void)
{
	return faces->count_items();
}

void bsptree::RenderTree(const VERTEX* QueryViewPort, const vidmem &VideoMemory)
{
	CamPos = *QueryViewPort;
	Render->SetViewPort(QueryViewPort);
	Render->SetActualVidMem(VideoMemory);
	traverse_render(bspnode);
}

void bsptree::OutTree(BSPNODE *tree, int &l, int &r)
{
	if (tree!=NULL)
	{
		if (tree->left) l++;
		if (tree->right) r++;
		OutTree(tree->left,l,r);
		OutTree(tree->right,l,r);
	}
}

void bsptree::depthtree(int &left, int &right)
{
	left = 0; right = 0;
	BSPNODE *bnode = bspnode;
	OutTree(bnode,left,right);
}

VERTEX bsptree::GetInterSection( tle<VERTEX> *begin, VERTEX n)
{
	float a=(begin->data*n);
	float subab = (float)(begin->nxt-begin);
	float t = 1/subab;
	return t*subab+begin->data;
}

bsptree::bsptree(tlist<tle<VERTEX>*>* verticesList, tlist<tle<TRIANGLE>*>* facesList)
{
	vertices = new tlist<VERTEX>;
	faces = new tlist<TRIANGLE>;
	plist = new polylist();
	bspnode = new BSPNODE;
	Render = new render;
	MakeInputFormate(verticesList,facesList);
	bspinit();
}

bsptree::~bsptree()
{
	destroybsptree(bspnode);
	delete vertices;
	delete faces;
	delete plist;
	delete Render;
}

void bsptree::bspinit(void) // initialize tree
{
	tlist<VERTEX> *test;
	tle<TRIANGLE> *begin = faces->getroot();
	while ( begin != NULL)
	{
		test = new tlist<VERTEX>;
// BUG; bei "echtem" Polygon solange lesen, bis keine Daten mehr vorhanden!
// und nicht annehmen, eingelesenes Poly sei Dreieck!
		test->push_back((*vertices)[begin->data.p1]);	// mainlist = polygonlist
		test->push_back((*vertices)[begin->data.p2]);
		test->push_back((*vertices)[begin->data.p3]);
		plist->AddToList(test);						// add pointlist to mainlist
		begin = begin->nxt;
	}
	createbsptree(plist,bspnode);		// bsptree complete
}

BSPNODE *bsptree::GetBspNode(void) const
{
	return bspnode;
}

// P4 = A = vor Ebenen;		B = P5 = hinter Ebene!
VERTEX *bsptree::GetPlaneIntersect(tle<VERTEX>* PX, const VERTEX &a, const VERTEX &b) const
{
	// konstante Differenz-Anteile der Determinanten:
	float a00 = PX->nxt->data.x-PX->data.x,	a10 = PX->nxt->nxt->data.x-PX->data.x,
		  a01 = PX->nxt->data.y-PX->data.y,	a11 = PX->nxt->nxt->data.y-PX->data.y,  
		  a02 = PX->nxt->data.z-PX->data.z, a12 = PX->nxt->nxt->data.z-PX->data.z;
	// konstante Produkt-Anteile der Determinanten:
	float a_00_11 = a00*a11,  a_10_02 = a10*a02,  a_01_12 = a01*a12,
		  a_11_02 = a11*a02,  a_12_00 = a12*a00,  a_10_01 = a10*a01,
	// variabler Anteil in Nennerdeterminante D
	      sub_x4_x5 = a.x-b.x,	sub_y4_y5 = a.y-b.y,  sub_z4_z5 = a.z-b.z,
	// variabler Anteil in Z�hlerdeterminante D3
		  sub_x4_x1 = a.x-PX->data.x,  sub_y4_y1 = a.y-PX->data.y,  sub_z4_z1 = a.z-PX->data.z;
	float D  = ((a_00_11*sub_z4_z5) + (a_10_02*sub_y4_y5) + (a_01_12*sub_x4_x5)) -
			   ((a_11_02*sub_x4_x5) + (a_12_00*sub_y4_y5) + (a_10_01*sub_z4_z5)),
	      D3 = ((a_00_11*sub_z4_z1) + (a_10_02*sub_y4_y1) + (a_01_12*sub_x4_x1)) -
			   ((a_11_02*sub_x4_x1) + (a_12_00*sub_y4_y1) + (a_10_01*sub_z4_z1));
	// freien Parameter t der Ebenengleichung bestimmen;
	float t3 = D3/D;
	// Einsetzen in Schnittpunktsgleichung:
	VERTEX *res = new VERTEX;
	res->x = (t3*(b.x-a.x))+a.x;
	res->y = (t3*(b.y-a.y))+a.y;
	res->z = (t3*(b.z-a.z))+a.z;
	// Schnittpunkt zur�ckliefern
	return res;
}

void bsptree::SplitPolygon(tlist<VERTEX> *splitplane, tlist<VERTEX> *plane, tlist<VERTEX> *planebefore, tlist<VERTEX> *planebehind)
{
	tle<VERTEX> *begin = plane->getroot();

	VERTEX v1 = splitplane->getroot()->data,
		   v2 = splitplane->getroot()->nxt->data,
		   v3 = splitplane->getroot()->nxt->nxt->data; // wenn auf ein Element nicht vorhanden-> splitplane defekt!
	VERTEX a_ = v3-v2;
	VERTEX b_ = v1-v2;
	VERTEX norm = a_%b_;
	while (begin!=NULL)
	{
			VERTEX a = begin->data,
				   b = ((begin->nxt!=NULL)?begin->nxt->data:plane->getroot()->data);
			bool a_before = (((norm*a)>0)?true:false),
				 b_before = (((norm*b)>0)?true:false);

			if (a_before & !b_before)
			{
				VERTEX *S = GetPlaneIntersect(splitplane->getroot(),a,b);
				planebefore->push_back(a);
				planebefore->push_back(*S);
				planebehind->push_back(*S);
			}
			else if (!a_before & b_before)
			{
				VERTEX *S = GetPlaneIntersect(splitplane->getroot(),a,b);
				planebehind->push_back(a);
				planebehind->push_back(*S);
				planebefore->push_back(*S);
			}
			else if (!a_before) // beide Punkte hinten-> letzten speichern
			{
				planebehind->push_back(a);
			}
			else if (a_before) // beide vorne -> letzten speichern
			{
				planebefore->push_back(a);
			}
			begin = begin->nxt;
	}
}

void bsptree::DetBeforeBehind(tlist<VERTEX> *splitplane, polylist *trilist, polylist *before, polylist *behind)
{
	node *begin = trilist->getroot();
	tlist<VERTEX> *planebefore, *planebehind;
	
	while (begin!=NULL)
	{
		planebefore = new tlist<VERTEX>;
		planebehind = new tlist<VERTEX>;
		//SplitPolygon(begin->data,planebefore,planebehind);
		//1.)_SplitPolygon(trilist->getroot(),begin->data,planebefore,planebehind);
		SplitPolygon(splitplane, begin->data,planebefore,planebehind);
		before->AddToList(planebefore);
		behind->AddToList(planebehind);
		begin = begin->nxt;
	}
}

void bsptree::destroybsptree(BSPNODE *bspnode)
{
  if (bspnode!=NULL)
  {
	  if (bspnode->left!=NULL)
		  destroybsptree(bspnode->left);
	  if (bspnode->right!=NULL) 
		  destroybsptree(bspnode->right);
	  delete bspnode;
  }
}

void bsptree::createbsptree(polylist *testlist, BSPNODE *bnode)
{
	if (testlist!=NULL)										// Abbruchbedingung
	{
		bnode->n = testlist->getroot()->data;
		polylist *listleft = new polylist();
		polylist *listright = new polylist();
		//DetBeforeBehind( testlist, listleft, listright );	// determine which part(s) of a plane are before or behind
		DetBeforeBehind(bnode->n, testlist, listleft, listright );	// determine which part(s) of a plane are before or behind
		// neuen Zweig an Hauptbaum anf�gen:
		if (listleft->getroot()!=NULL)						// linken Ast f�llen
		{
			bnode->left = new BSPNODE;
			bnode->left->left = NULL;
			bnode->left->right = NULL;
			bnode->left->n  = listleft->getroot()->data;	// erst-bestes LElem gew�hlt (optimierbar!)
			listleft->Pop();								// erstes Listenelement l�schen
		}
		if (listright->getroot()!=NULL)						// rechten Ast f�llen
		{
			bnode->right = new BSPNODE;
			bnode->right->left = NULL;
			bnode->right->right = NULL;
			bnode->right->n = listright->getroot()->data;
			listright->Pop();								// erstes Listenelement l�schen
		}
		// rekursiver Selbstaufruf:
		if (listleft->getroot() ) createbsptree(listleft,bnode->left );   // create left subtree
		if (listright->getroot()) createbsptree(listright,bnode->right ); // create right subtree
	}
}

// testen, ob Betrachter vor einer Fl�che steht
float bsptree::ClassifyPoint(tlist<VERTEX> *n)
{
	VERTEX cross = (n->getroot()->data%n->getroot()->nxt->data)*(-1);
	float D = CamPos*cross;						// compare dot-product
	return D;
}

void bsptree::traverse_render(const BSPNODE* Node)
{
   if (Node!=NULL)
   {
	   if (ClassifyPoint(Node->n) > 0.0f)
	   {
		   traverse_render(Node->right);
		   Render->SetVertexList(Node->n);
		   Render->RenderVertexList();
		   traverse_render(Node->left);
	   }
	   else if (ClassifyPoint(Node->n) < 0.0f)
	   {
		   traverse_render(Node->left);
		   Render->SetVertexList(Node->n);
		   Render->RenderVertexList();
		   traverse_render(Node->right);
	   }
	   else
	   {
			traverse_render(Node->left);
	   		traverse_render(Node->right);
	   }
   }
}
// alle Teillisten aus vertices und faces zusammenbringen
void bsptree::MakeInputFormate(tlist<tle<VERTEX>*> *verticesList, tlist<tle<TRIANGLE>*> *facesList)
{
	// "toggle: vertices"
	tle<tle<VERTEX>*> *vbegin = verticesList->getroot();
	while (vbegin!=NULL)						//.. h�nge begin->data an letztes Element von vertices
	{
		tle<VERTEX> *temp = new tle<VERTEX>;
		temp = vbegin->data;					// Anfang der anzuh�ngenden Liste

		while (temp!=NULL)						// zusammengeh�ngte Liste �bernehmen
		{
			vertices->push_back(temp->data);	// Daten �bernehmen (kopieren)
			temp = temp->nxt;
		}
		delete temp;
		vbegin = vbegin->nxt; 
	}
	// Die Indizes der Triangle-Definitionen m�ssen neu definiert werden, da Definitionen vieler verschiedener Objekte
	// in eine gemeinsame Liste gekettet werden
	// "toggle: faces"
	tle<tle<TRIANGLE>*> *tbegin = facesList->getroot();
	while (tbegin!=NULL)
	{
		tle<TRIANGLE> *temp = new tle<TRIANGLE>;
		temp = tbegin->data;			 // Anfang der anzuh�ngenden Liste
		// Lokale Position in Globale umrechnen, da alle Indixes in einer Liste sind
		int i = 0;						// Z�hler der Dreieckss�tze (Anzahl einzelner Listen)
		tle<tle<TRIANGLE>*> *test = facesList->getroot();  // Start holen
		tle<tle<VERTEX>*> *actvertList = verticesList->getroot();
		while (test!=tbegin)			// solange durchlaufen bis momentane Position getroffen
		{								// Bestimmen des Abstandes zum tats�chlichen Element in VertexList
			int j = 0;
			tle<VERTEX> *test2 = actvertList->data;
			while (test2!=NULL)
			{
				j++;
				test2 = test2->nxt;
			}
			i+=j;
			test = test->nxt;
			actvertList = actvertList->nxt;
		}
		while (temp!=NULL)				 // zusammengeh�ngte Liste �bernehmen
		{
			TRIANGLE WriteBuf = temp->data;
			WriteBuf.p1+=i;
			WriteBuf.p2+=i;
			WriteBuf.p3+=i;
			faces->push_back(WriteBuf);			 // Daten �bernehmen (kopieren)
			temp = temp->nxt;
		}
		delete temp;
		tbegin = tbegin->nxt;
	}
}

#endif