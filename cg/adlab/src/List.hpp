#ifndef ListDef
#define ListDef

#define NULL 0

template <class T>
struct tle
{
   T data;
   tle *nxt;
};

template <class T>
class tlist
{
   private:
     tle<T> *root;
	 tle<T> *last;					// Zeiger auf letztes Element
	 tle<T> *reversedlist;			// auf reversed root
	 void clear(tle<T> *test);
   public:
     tlist(void);
     ~tlist(void);
     void push(T pnt);
     void push_back(T pnt);
     void pop_front(void);
     void setroot(tle<T> *test);
	 void setlast(tle<T> *test);
     T first(void);
     T& operator[](int i)const;
     tle<T>* getroot(void);
	 tle<T>* getlast(void);
	 int count_items(void);
	 bool empty(void);
	 // Root von r�ckw�rtsausgegebener Liste 
	 tle<T>* getreversedroot(void); // root mu� bei jedem Durchlauf vollst�ndige durchlaufen werden!
	 void pushpost(int i,T pnt); // Einf�gen nach i.-tem Element
	 void poppost(int i);  // entfernt i.-tes Element
};

template <class T>
tlist<T>::tlist(void)		// erstellt leere Liste; setzt Gr��e auf 0
{
   root = NULL;
   last = NULL;
   reversedlist = NULL;
}

template <class T>
tlist<T>::~tlist(void)		// Destruktor: Liste freigeben
{
   clear(root);
   root = NULL;
   clear(reversedlist);
   reversedlist = NULL;
}

template <class T>
void tlist<T>::push(T pnt)		// f�gt direkt nach Anker neues Element ein
{
  tle<T> *h = new tle<T>;		// Hilfszeiger
  h->nxt = root;
  h->data = pnt;
  root = h;
}

template <class T>
void tlist<T>::push_back(T pnt)		// optimierte Methode mit direktem Anh�ngen ans Listenende
{
	tle<T> *h;
	h = new tle<T>;
	h->data = pnt;
    h->nxt = NULL;
	if (last!=NULL)				// bereits Elemente in Liste vorhanden
	{
		last->nxt = h;			// h an letztes Element anketten
	    last = h;
	}
	else root = last = h;		// noch kein Element in der Liste
}
template <class T>
void tlist<T>::pushpost(int i,T pnt)
{
	tle<T> *hmain = root;
	tle<T> *hm = hmain;
	for (int j=0;j<i;j++,hmain = hmain->nxt){}
	tle<T> *l = hmain->nxt;    // Listenschwanz
	tle<T> *h; h = new tle<T>; // Listenkopf
	h->data = pnt;
	h->nxt = l;
	hmain->nxt = h;
}

template <class T>
void tlist<T>::poppost(int i)
{
	if (i<1) return;
	tle<T> *h = root;
	int j= 1;
	while (h)
	{
		if (j==i)
		{
			tle<T> *l;
			l=((h->nxt)? h->nxt->nxt:NULL);
			tle<T> *temp = h->nxt;
			h->nxt = l;
			delete temp;
			break;
		}
		h = h->nxt;
		j++;
	}
}

template <class T>
void tlist<T>::pop_front(void)	// Entfernt 1.-es Element einer Liste
{
  tle<T> *h;
  h = root;
  root = h->nxt;
  delete h;
  if (root==NULL) last=NULL;
}

template <class T>
int tlist<T>::count_items(void)	// liefert Anzahl der Listenelemente
{
	tle<T> *node = root;
	for (int count = 0; node!=NULL; count++)
		node = node->nxt;
    return count;
}

template <class T>
bool tlist<T>::empty(void)
{
	return ((root==NULL)?true:false);
}

template <class T>
T tlist<T>::first(void)
{
  return root->data;
}

template <class T>
T& tlist<T>::operator[](int n)const{
  tle<T> *h = root;
  for(int i = 0; i < n; i++)
    h = h->nxt;
  return (T&)(h->data);
}

template <class T>
tle<T>* tlist<T>::getroot(void)
{
  return root;
}

template <class T>
tle<T>* tlist<T>::getlast(void)
{
	return last;
}

template <class T>
void tlist<T>::setroot(tle<T> *test)
{
  root = test;
}

template <class T>
void tlist<T>::setlast(tle<T> *test)
{
	last = test;
}

template <class T>
tle<T>* tlist<T>::getreversedroot(void)
{
	tle<T> *h = root;
	while (h != NULL)
	{
		tle<T> *k = new tle<T>;		// Hilfszeiger
		k->nxt = reversedlist;
		k->data = h->data;
		reversedlist = k;

		h = h->nxt;
	}
	return reversedlist;
}

template <class T>
void tlist<T>::clear(tle<T> *test)
{
  tle<T> *h;
  while (test != NULL)
  {
    h = test;
    test = test->nxt;
    delete h;
  }
}

#endif