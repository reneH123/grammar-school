#ifndef MouseDef
#define MouseDef

#include <windows.h>

// Input-Properties
#define MOUSE_SENSITY 0.001f

class mouse
{
	private:
		LPPOINT Pos;
	public:
		mouse(int x_ = MIDX, int y_ = MIDY);
		~mouse(void);
		LPPOINT GetPos(void) const;
		LPPOINT GetOldPos(void) const;
		void SetPos(LPPOINT *pos);
};

/*
	PIXEL *Pos;
	PIXEL GetPos(void) const;
	PIXEL GetOldPos(void) const;
*/
	
mouse::mouse(int x_, int y_)
{
	Pos = new POINT;
	Pos->x = x_;
	Pos->y = y_;
}

mouse::~mouse(void)
{
	delete Pos;
}

LPPOINT mouse::GetPos(void) const
{
//	GetCursorPos(Pos); BUG!!! muss aktiviert sein!!
	return Pos;
}
/*
	LPPOINT lpPoint = new POINT;
	if (GetCursorPos(lpPoint))
		Pos->x = lpPoint->x;
		Pos->y = lpPoint->y;
	delete lpPoint;
	return *Pos;
*/

LPPOINT mouse::GetOldPos(void) const
{
	return Pos;
}

/*
// from: Graphics Programming in C++; Mark Walmsley
class DIB;

class Sprite
{
public:
Sprite(DIB**);
virtual ~Sprite(void);
virtual void Move(void);
virtual void Animate(DIB* = NULL);
virtual void Update(void);		// // updates overall state of a sprite
virtual void Draw(HDC);
virtual void SetScale(int);
virtual POINT SetPosition(const POINT*);
virtual POINT SetVelocity(const POINT*);
virtual POINT SetAcceleration(const POINT*);
protected:
	POINT Position;
	POINT Velocity;
	POINT Acceleration;
	DIB* Images[MAX_IMAGES];
	DIB** Image;
	int Scale;
};

Sprite:Sprite(DIB** images)
{ 
	int i=0;
	while (Images[i++] = *images++);
	Image = Images;
	Position.x = Position.y = 0;
	Velocity.x = Velocity.y = 0;
	Acceleration.x = Acceleration.y = 0;
	Scale = 1;
}

POINT Sprite::SetPosition(const POINT* position)
{
	if (position) Position = *position;
	return Position;
}

POINT Sprite::SetVelocity(const POINT* velocity)
{
	if (velocity) Velocity = *velocity;
	return Velocity;
}

POINT Sprite::SetAcceleration(const POINT* acceleration)
{
	if (acceleration) Acceleration = *acceleration;
	return Acceleration;
}

void Sprite::Move(void)
{
	Position.x += Velocity.x;
	Position.y += Velocity.y;
	Velocity.x += Acceleration.x;
	Velocity.y += Acceleration.y;
}

void Sprite::SetScale(int)
{
	// permits app of a non-zero accel. by allowing
	// the sprite position to be measured in fractions of a pxl
}

void Sprite::Animate(DIB* image)
{
	if (image)
	{
		Image = Images;
		while (*Image != image)
			Image++;
	}
	else
	{
		Image++;
		if (!*Image) Image = Images;
	}
}

void Sprite::Update(void)
{
	Move();
	Animate();
}

void Sprite::Draw(HDC dc)
{
	POINT pixel;
	pixel.x = Position.x/Scale;
	pixel.y = Position.y/Scale;
	(*Image)->Blit(dc,pixel.x,pixel.y);
}
*/

#endif