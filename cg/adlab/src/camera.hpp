#ifndef CameraDef
#define CameraDef

#include <math.h>
#include "vertex.hpp"
#include "matrix.hpp"

class camera
{
private:
	VERTEX pos;					    // globale Koordinate im WKS
	Matrix Mat;					    // f�r Rotation/Skalierung/Translation
	//Rot_matrix rot;					// tempor�re Matrix f�r 3D-Operationen
public:
	camera(const VERTEX &c)		    // Konstruktor
	{
		pos.x = c.x;
		pos.y = c.y;
		pos.z = c.z;
	}
	~camera(void) {}		        // Destruktor
	void setcamera(const VERTEX &p)	// KameraPosition festlegen
	{
		pos = p;
	}
	void setcenterview(void)		// parallel zum Sichthorizont
	{
		pos.y = 0.0f;
	}
	VERTEX* getpos(void)		    // aktuelle KameraPosition
	{
		return &pos;
	}
	float getDistance(VERTEX &p)	// Abstand
	{
		return (p.z-pos.z);
	}
	void translateX(float dx)       // Kamera verschieben
	{
		pos.x+=dx;
	}
	void translateY(float dy)
	{
		pos.y+=dy;
	}
	void translateZ(float dz)
	{
		pos.z+=dz;
	}
	void rotateAlpha(float alpha)   // Kamera drehen
	{
        //M.fill_rot_matrixX(rot,alpha);
		//M.vec_mult_matrix(pos,rot);
		register float s, c;  // for reasons of speed 
		s=(float)sin(alpha); c=(float)cos(alpha);
		Mat(VERTEX(1,0,0),VERTEX(0,c,-s),VERTEX(0,s,c));
		pos = Mat*pos;
	}
	void rotateBeta(float beta)
	{
		register float s, c;
		s=(float)sin(beta); c=(float)cos(beta);
		Mat(VERTEX(c,0,s),VERTEX(0,1,0),VERTEX(-s,0,c));
		pos = Mat*pos;
	}
	void rotateGamma(float gamma)
	{
		register float s, c;
		s=(float)sin(gamma); c=(float)cos(gamma);
		Mat(VERTEX(c,-s,0),VERTEX(s,c,0),VERTEX(0,0,1));
		pos = Mat*pos;
	}
	void addscale(float s)									// Vergr��ern/Verkleinern
	{
		pos = pos*s;
	}
};

#endif