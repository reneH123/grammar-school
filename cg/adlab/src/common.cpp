#include "common.hpp"
#include <stdlib.h>
#include <string.h>
#include "list.hpp"

void swapPIXEL(PIXEL *p1, PIXEL *p2)
{
  PIXEL temp;
  temp = *p1;
  *p1 = *p2;
  *p2 = *&temp;
}

void GetResourceString(UINT uID, LPTSTR test)
{
	LoadString(Manager->GetInstance(),uID,test,MAX_LOADSTRING);
}

