#include "ddraw.hpp"

// Pixel auf den Bildschirm schreiben
void PutPix( PIXEL pixel, RGBCOLOR rgb )
{
	lpSurface[(4*pixel.x) + (lPitch*pixel.y) + 0] = rgb.r;
	lpSurface[(4*pixel.x) + (lPitch*pixel.y) + 1] = rgb.g;
	lpSurface[(4*pixel.x) + (lPitch*pixel.y) + 2] = rgb.b;
}

// Zeichnet ein Pixel in Frontbuffer
void putpixel( int x, int y, long c)
{
	if ( (x > 0) && (x < (long)dwWidth) && (y > 0) && (y < (long)dwHeight) )
	{
		// 4 ist f�r entsprechende Farbtiefe + Aufl�sung
		lpSurface[(4*x) + (lPitch*y) + 0] = c;
		lpSurface[(4*x) + (lPitch*y) + 1] = c;
		lpSurface[(4*x) + (lPitch*y) + 2] = c; // f�r c/2 ist BlauWert halbwertig
	}
}

// restoreAll stellt alle Surfaces wieder her
int restoreAll( void )
{
    int		result;

    result = lpDDSPrimary->Restore();
    if (result != DD_OK) 
	{
		return false;
	}
	return true;
}