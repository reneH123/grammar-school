#ifndef __COMMONDEF
#define __COMMONDEF

#include "punkt.hpp"

void swapINT(int &a, int &b);
void swapINT2(int *a, int *b);
void swapFLOAT(float &a, float &b);
void swapPIXEL(PIXEL &p1, PIXEL &p2);

#endif