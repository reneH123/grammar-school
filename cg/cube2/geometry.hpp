#ifndef __GEOMDEF
#define __GEOMDEF

#include "punkt.hpp"

void linehor(int x1, int x2, int y, long value);
void linetexturehor(int x1, int x2, int y, int index, int elem); // Zeiger auf Daten fehlt!
void zeichnelinie(PIXEL &p1, PIXEL &p2, long value);
void FillPoly(const PIXEL *p1_, const PIXEL *p2_, const PIXEL *p3_, long col);
void FillPolyTxt(const PIXEL *p1_, const PIXEL *p2_, const PIXEL *p3_, int elem);

#endif