#include <math.h>
#include "matrix.hpp"

Matrix::Matrix(){}			// Konstruktor
Matrix::~Matrix(){}			// Destruktor

void Matrix::rotatexaxis(VERTEX *c, float alpha)		// X-Achsen-Rotation
{
	VERTEX temp = *c;
	c->y = ((float)cos(alpha)*(temp.y)) + ((float)sin(alpha)*(temp.z));
	c->z = ((float)cos(alpha)*(temp.z)) - ((float)sin(alpha)*(temp.y));
}

void Matrix::rotateyaxis(VERTEX *c, float beta)			// Y-Achsen-Rotation
{
	VERTEX temp = *c;
	c->x = ((float)cos(beta)*temp.x) + ((float)sin(beta)*temp.z);
	c->z = ((float)cos(beta)*temp.z) - ((float)sin(beta)*temp.x);
}
  				
void Matrix::rotatezaxis(VERTEX *c, float gamma)		// Z-Achsen-Rotation
{
	VERTEX temp = *c;
	c->x = ((float)cos(gamma)*(temp.x)) +/*-*/ ((float)sin(gamma)*(temp.y));
	c->y = ((float)cos(gamma)*(temp.y)) -/*+*/ ((float)sin(gamma)*(temp.x));
}

void Matrix::translate(VERTEX *c, VERTEX *d )				// ‹bersetzen
{
	c->x += d->x; c->y += d->y; c->z += d->z;
}
 						
void Matrix::scale(VERTEX *c, float s)  					// Skalieren
{
	c->x *= s; c->y *= s; c->z *= s;
}

float Matrix::DotProduct(const VERTEX *v1, const VERTEX *v2)
{
	return (v1->x*v2->x) + (v1->y*v2->y) + (v1->z*v2->z);
}

void Matrix::VectorSubtract(const VERTEX *veca, const VERTEX *vecb, VERTEX *res)
{
	res->x = veca->x-vecb->x;
	res->y = veca->y-vecb->y;
	res->z = veca->z-vecb->z;
}

void Matrix::VectorAdd(const VERTEX *veca, const VERTEX *vecb, VERTEX *res)
{
	res->x = veca->x+vecb->x;
	res->y = veca->y+vecb->y;
	res->z = veca->z+vecb->z;
}

void Matrix::VectorMul(const VERTEX *veca, const VERTEX *vecb, VERTEX *res)
{
	res->x = veca->x*vecb->x;
	res->y = veca->y*vecb->y;
	res->z = veca->z*vecb->z;
}

void Matrix::VectorSMul(const VERTEX *veca, const float scalar, VERTEX *res)
{
	res->x = veca->x*scalar;
	res->y = veca->y*scalar;
	res->z = veca->z*scalar;
}	  

void Matrix::VectorDiv(const VERTEX *veca, const VERTEX *vecb, VERTEX *res)
{
	res->x = veca->x/vecb->x;	// Achtung: Absturzgefahr!
	res->y = veca->y/vecb->y;
	res->z = veca->z/vecb->z;
}

void Matrix::VectorSDiv(const VERTEX *veca, const float scalar, VERTEX *res)
{
	res->x = veca->x/scalar;	// Achtung: Absturzgefahr!
	res->y = veca->y/scalar;
	res->z = veca->z/scalar;
}

void Matrix::VectorCopy(const VERTEX *in, VERTEX *out)
{
	*out = *in;
}

void Matrix::CrossProduct(const VERTEX *veca, const VERTEX *vecb, VERTEX *res)
{
	VERTEX temp;

	temp.x = (veca->y*vecb->z) - (veca->z*vecb->y);
	temp.y = (veca->z*vecb->x) - (veca->x*vecb->z);
	temp.z = (veca->x*vecb->y) - (veca->y*vecb->x);
	*res = *&temp;
}
