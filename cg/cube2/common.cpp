#include "common.hpp"

void swapINT(int &a, int &b)
{
  int temp;

  temp = a;
  a = b;
  b = temp;
}

void swapINT2(int *a, int *b)
{
  int temp;
  temp = *a;
  *a = *b;
  *b = *&temp;
}

void swapFLOAT(float &a, float &b)
{
  float temp;

  temp = a;
  a = b;
  b = temp;
}

void swapPIXEL(PIXEL &p1, PIXEL &p2)
{
  PIXEL temp;

  temp = p1;
  p1 = p2;
  p2 = temp;
}