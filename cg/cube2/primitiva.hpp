#ifndef Primitivadef
#define Primitivadef

#include "punkt.hpp"
#include "List.hpp"
#include "matrix.hpp"


class Drawingobject
{
	private:
		VERTEXlist vlist;												// globale Liste f�r Punkte
// so sehen die Punkte f�r Betrachter aus:
		VERTEXlist vvlist;												// Punkte, wie sie wahrgenommen werden (Blickwinkel, ...) 
		PIXELlist pxllist;												// Bildpunkte f�r Ausgabeger�t

		TRIANGLElist tlist;												// globale Liste f�r Dreiecke		
		VERTEX center;													// globale Koordinate des Objektes im Weltkoordinatensystem
		Matrix Matrix_instance;											// f�r Rotation/Skalierung/Translation 
	public:
		Drawingobject(char *pszFileName, char *pszFileName2, const VERTEX *pos_);
		~Drawingobject(void);    										// Destruktor
		VERTEX getvertex(int p);										// 1 Punkt aus konstrukt
		VERTEX getcenter(void);											// Mitte des Objektes: global betrachtet
		TRIANGLE gettriangle(int p);									// 1 Dreieck aus dreiecke
		PIXEL getpixel( int p );										// holt das p.-te Pixel
// "neu":
		void convert3to2d(  const VERTEX &Bet );						// Umwandlung: 3D-Koordinaten in Ger�tekoordinaten
		void CopyVList( void );											// kopiert vvlist in vlist

		float visibletriangle(int p, const VERTEX *Bet);				// wenn (~ > 0) dann sichtbar
		int GetCntVertex(void);											// Anzahl der Elemente in Punktliste
		int GetCntTriangle(void);										// Anzahl der Elemente in Dreiecksliste
		void addtranslate(float dx, float dy, float dz);				// K�rper verschieben
		void addrotate(float alpha, float beta, float gamma);			// K�rper rotiert um eigene Achse
		void addscale(float s);											// K�rper vergr��ern/verkleinern
};

#endif