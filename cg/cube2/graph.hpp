#ifndef __GraphDef
#define __GraphDef

/* Globale Grafikeinstellungen: */
#define RESX			1024  //horizontale Aufl�sung
#define RESY			768   //vertikale Aufl�sung
#define MIDX			512   // = RESX /2        
#define MIDY			384	  // = RESY /2
#define DEPTHCOLOR		32    //Farbtiefe
#define MAXSIZEX		16384
#define MAXSIZEY		4096
#define MAXSIZEZ		8192

#endif