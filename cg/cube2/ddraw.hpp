#ifndef __DDRAWDEF
#define __DDRAWDEF

#include <ddraw.h>

// DirectDraw-Properties
extern LPDIRECTDRAW            lpDD;           // DirectDraw object
extern LPDIRECTDRAWSURFACE     lpDDSPrimary;   // DirectDraw primary surface
extern LPDIRECTDRAWSURFACE     lpDDSBack;      // DirectDraw back surface

extern unsigned char			*lpSurface;			// Zeiger auf Bilddaten
extern unsigned long			dwHeight, dwWidth;	// Height und Width der Surface
													// Anzahl Pixel pro Zeile und Zeilen pro Bild
extern long					   lPitch;				// Anzahl von Bytes pro Pixelzeile

#include "punkt.hpp"

void PutPix( PIXEL pixel, RGBCOLOR rgb );
void putpixel( int x, int y, long c);
int restoreAll( void );

#endif