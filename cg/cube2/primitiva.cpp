#include <math.h>
#include <windows.h>
#include "primitiva.hpp"
#include "graph.hpp"
#include <conio.h>

Drawingobject::Drawingobject(char *pszFileName, char *pszFileName2, const VERTEX *pos_)
{
	FILE *pfile;
	
	pfile = fopen(pszFileName, "r");
	if (!pfile)	{ exit(0); }
	
	VERTEX v;
	char s[256] = "";
	
    while (!feof(pfile))	// Koordinaten auslesen
	{
		fscanf( pfile, "%s\n", s );
		sscanf( s, "{%f,%f,%f}", &v.x, &v.y, &v.z );
		vlist.push_back(v);
		vvlist.push_back(v); // als Initialisation
	}
	fclose(pfile);
	
	pfile = fopen(pszFileName2, "r");   // erneut, damit an Anfangsposition der Datei gelangen
	if (!pfile)	{ exit(0); }			// Bad file name  
	TRIANGLE t;

	while (!feof(pfile))	// Dreiecksverbindungen auslesen
	{
		fscanf( pfile, "%s\n", s );
		sscanf( s, "{%d,%d,%d}", &t.p1, &t.p2, &t.p3 );
		tlist.push_back(t);
	}
	fclose(pfile);

	center = *pos_;
}

Drawingobject::~Drawingobject(void)
{
	if (!(vlist.empty()))  vlist.clear();
	if (!(tlist.empty()))  tlist.clear();
	if (!(vvlist.empty())) vvlist.clear();
}

VERTEX Drawingobject::getvertex(int p)
{
	return vlist[p];
}

PIXEL Drawingobject::getpixel( int p )
{
	return pxllist[p];
}

VERTEX Drawingobject::getcenter(void)
{
	VERTEX temp = {0,0,0};

	for (int i=0; i<vvlist.size(); i++) 
		Matrix_instance.VectorAdd(&temp,&vvlist[i],&temp);
	Matrix_instance.VectorSDiv(&temp,vvlist.size(),&center);

	return center;
}

TRIANGLE Drawingobject::gettriangle(int p)
{
	return tlist[p];
}

float Drawingobject::visibletriangle(int p, const VERTEX *Bet)
{
	TRIANGLE t = tlist[p];
	VERTEX  a = {(vvlist[t.p3].x-vvlist[t.p2].x), (vvlist[t.p3].y-vvlist[t.p2].y), (vvlist[t.p3].z-vvlist[t.p2].z)},
			b = {(vvlist[t.p1].x-vvlist[t.p2].x), (vvlist[t.p1].y-vvlist[t.p2].y), (vvlist[t.p1].z-vvlist[t.p2].z)},
			u;
   Matrix_instance.CrossProduct(&a,&b,&u);
   float D = (Matrix_instance.DotProduct(&u,&vvlist[t.p2]));
   u.z = -u.z;
   return (Matrix_instance.DotProduct(&u,Bet)+D);
}

void Drawingobject::CopyVList( void )
{
	for (int i=0; i<vlist.size(); i++)
	{
//		if (vvlist[i].z > 0) 
			vvlist[i] = vlist[i];
	}
}

void Drawingobject::convert3to2d( const VERTEX &Bet )
{
	int rx = RESX;
	int ry = RESY;
	int mx = MIDX;
	int my = MIDY;

	for (int i=0; i < vvlist.size(); i++)
	{
//		if (vvlist[i].z > 0)		// BUG: wenn ~ auftritt -> Fehler!!!
//		{
			// angenommen: 0.5 = Entfernung
			VERTEX v = vvlist[i];
					

			PIXEL p = { ( (RESX * 0.2f * (Bet.x+vvlist[i].x)) / (vvlist[i].z-Bet.z)) + MIDX,
				        ( (RESX * 0.2f * (Bet.y+vvlist[i].y)) / (vvlist[i].z-Bet.z)) + MIDY};		// Multipliziere( yclip mit Xres/Yres)
			pxllist.push_back(p);
//		}
	}
}

int Drawingobject::GetCntVertex(void)
{
	return vvlist.size();
}

int Drawingobject::GetCntTriangle(void)
{
	return tlist.size();
}

void Drawingobject::addtranslate(float dx, float dy, float dz)
{
	VERTEX vec = {dx,dy,dz};
	for (int i=0; i<vvlist.size(); i++) 
		Matrix_instance.translate(&vvlist[i],&vec);
}

void Drawingobject::addrotate(float alpha, float beta, float gamma)
{
	for (int i=0; i<vvlist.size(); i++) 
	{
		Matrix_instance.rotatexaxis(&vvlist[i],alpha);
		Matrix_instance.rotateyaxis(&vvlist[i],beta);
		Matrix_instance.rotatezaxis(&vvlist[i],gamma);
	}
}

void Drawingobject::addscale(float s)
{
	for (int i=0; i<vlist.size(); i++)		// Achtung es werden die physikalischen Koordinaten ver�ndert -> vlist[] !
		Matrix_instance.scale(&vlist[i],s);
}