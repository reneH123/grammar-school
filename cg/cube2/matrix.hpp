#ifndef Matrixdef
#define Matrixdef

#include "punkt.hpp"

//typedef float vec[3];	
//typedef vec MatrixType[3];

class Matrix
{
	public:
	  Matrix();	 											// Konstruktor
	  ~Matrix(); 											// Destruktor
	  void rotatexaxis(VERTEX *vec, float alpha); 			// X-Achsen-Rotation
	  void rotateyaxis(VERTEX *vec, float beta);  			// Y-Achsen-Rotation
	  void rotatezaxis(VERTEX *vec, float gamma); 			// Z-Achsen-Rotation
	  void translate(VERTEX *vec, VERTEX *d); 				// ‹bersetzen
	  void scale(VERTEX *vec, float s);  					// Skalieren
	  // Grund-Vektor-Operationen:
	  void VectorSubtract(const VERTEX *veca, const VERTEX *vecb, VERTEX *res);		// Subtrahiert vecb von veca
	  void VectorAdd(const VERTEX *veca, const VERTEX *vecb, VERTEX *res);			// Addiert beide Vektoren
	  void VectorMul(const VERTEX *veca, const VERTEX *vecb, VERTEX *res);			// Multipliziert beide Vektoren
	  void VectorSMul(const VERTEX *veca, const float scalar, VERTEX *res);
	  void VectorDiv(const VERTEX *veca, const VERTEX *vecb, VERTEX *res);			// Dividiert beide Vektoren
	  void VectorSDiv(const VERTEX *veca, const float scalar, VERTEX *res);
	  void VectorCopy(const VERTEX *in, VERTEX *out);								// Kopiert einen Vektor
	  void CrossProduct(const VERTEX *veca, const VERTEX *vecb, VERTEX *res);		// Kreuzprodukt o. Vektorprodukt
	  float DotProduct(const VERTEX *v1, const VERTEX *v2);							// Punktprodukt o. Skalarprodukt
};

#endif
