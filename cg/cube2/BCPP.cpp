// "index" bei linetexthor... eliminieren
// Implementation von "int INTRI(const PIXEL &p, const PIXELTRI &t)"
// BUG: startpos wird manchmal negativ in fillclippedtriangle
// Fl�cheninhaltberechnung versagt!
// appendPXL(const PIXEL &point, int &cnt) mit "PIXEL *array"-Unterst�tzung!
// misteri�ses starkes Verz�gern bei bestimmter Clipposition
#include <graphics.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <dos.h>
#include <math.h>
#include "graph.hpp"
#include "punkt.hpp"

// PCX-TOOL
#include <stdio.h>
#include <graphics.h>
#include <conio.h>
#include <stdlib.h>
#include <alloc.h>
#include <dos.h>

typedef struct {
  char manufacturer;
  char version;
  char encoding;
  char bits_per_pixel;
  short int xmin, ymin;
  short int xmax, ymax;
  short int hres;
  short int vres;
  char palette[48];
  char reserved;
  char colour_planes;
  short int bytes_per_line;
  short int palette_type;
  char filles[58];
} PCXHEAD;

unsigned char pcxpalette[768];
PCXHEAD header;
unsigned short pcxwidth, pcxheight, pcxbytes, pcxbits;
int linenum = 0;


int ReadPcxLine(FILE *fp, char * data) {
  int n = 0;
  do {
	 char c = fgetc(fp) & 0xff;
	 if ((c & 0xc0) == 0xc0) {
	  short i = c & 0x3f;
	  c = fgetc(fp);
	  while (i--)
	  {
		 *data++ = c;
		  n++;
	  }
	 }
	 else {
		*data++ = c;
		n++;
	 }
  } while (n < pcxwidth);
  return n;
}

void UnpackPcxFile(FILE * fp, char * data) {
  int i;
  for (i = 0; i < pcxheight; i++) {
	 ReadPcxLine(fp, data + i * pcxwidth);
  }
}


char * loadPCX(char * file, int * width, int * height) {
  FILE *fp;

  // �BERPR�FUNGEN: HEADER
  if ((fp = fopen(file, "rb")) == NULL)
	return 0;

  if (fread((char *) &header, 1, sizeof(PCXHEAD), fp) != sizeof(PCXHEAD))
	 return 0;

  if (header.manufacturer != 0x0a || header.version != 5)
	 return 0;

  if (header.bits_per_pixel == 1) pcxbits = header.colour_planes;
  else pcxbits = header.bits_per_pixel;

  if (pcxbits != 8) return 0;


  if (! fseek(fp, -769L, SEEK_END)) {
	if (fgetc(fp) != 0x0c || fread(pcxpalette, 1, 768, fp) != 768)
	  return 0;
  }
  else return 0;
  for (int i = 0; i < 768; i++) pcxpalette[i] >>= 2;
  // Palette setzen

// PR�FUNGEN ENDE

  fseek(fp, 128L, SEEK_SET);

  pcxwidth = (header.xmax - header.xmin) + 1;
  pcxheight = (header.ymax - header.ymin) + 1;
  pcxbytes = header.bytes_per_line;

  *width = pcxwidth;
  *height = pcxheight;
  char * data = (char *)malloc(pcxwidth * pcxheight);

  UnpackPcxFile(fp, data);

  fclose(fp);
  return data;
}

int height, width;							// global
char *lpcx = loadPCX("test.pcx",&width,&height);
//char *fillpcx= lpcx; // Inhalt abkopieren
// PCX-TOOL


#define plot_anz 8

VERTEX plot_org[plot_anz]; //orignalpunkte des zu zeichnenden Objektes
VERTEX plot_view[plot_anz]; //Punkte so wie der Betrachter sie sieht
PIXEL plot_scr[plot_anz]; //Punkte auf dem Bildschirm

#define tri_anz 12
triangle tri[tri_anz];  //Verbindungsinfo f�r die Punkte
float dist;

void swapINT(int *a, int *b)
{
  int temp;
  temp = *a;
  *a = *b;
  *b = *&temp;
}

void swapFLOAT(float &a, float &b)
{
  float temp;
  temp = a;
  a = b;
  b = temp;
}

void swapPIXEL(PIXEL *p1, PIXEL *p2)
{
  PIXEL temp;
  temp = *p1;
  *p1 = *p2;
  *p2 = *&temp;
}

#define RESX 640
#define RESY 480
#define midx 320
#define midy 240

void projiziere() //plot_view => plot_scr
{
  for( int i = 0; i < plot_anz; i++ )
  if( plot_view[i].z>0 )
  {
	 plot_scr[i].x = ( (RESX * dist * plot_view[i].x) / plot_view[i].z) + midx;
	 plot_scr[i].y = ( (RESX * dist * plot_view[i].y) / plot_view[i].z) + midy; //(multiply yclip by Xres/Yres).
  }
}

void rotatexaxis( float alpha )
{
  VERTEX temp;
  for (int i = 0; i < plot_anz; i++)
  {
	 temp = plot_view[i];
	 plot_view[i].y = ((float)cos(alpha)*temp.y) + ((float)sin(alpha)*temp.z);
	 plot_view[i].z = ((float)cos(alpha)*temp.z) - ((float)sin(alpha)*temp.y);
  }
}

void rotateyaxis( float beta )
{
  VERTEX temp;
  for ( int i = 0; i< plot_anz; i++)
  {
	 temp = plot_view[i];
	 plot_view[i].x = ((float)cos(beta)*temp.x) - ((float)sin(beta)*temp.z);
	 plot_view[i].z = ((float)sin(beta)*temp.x) + ((float)cos(beta)*temp.z);
  }
}

void rotatezaxis(float gamma)
{
  VERTEX temp;
  for (int i=0; i< plot_anz; i++)
  {
	 temp = plot_view[i];
	 plot_view[i].x = ((float)cos(gamma)*temp.x) + ((float)sin(gamma)*temp.y);
	 plot_view[i].y = ((float)cos(gamma)*temp.y) - ((float)sin(gamma)*temp.x);
  }
}

void translate(float dx, float dy, float dz)
{
  for (int i=0; i<8; i++)
  {
	 plot_view[i].x = plot_view[i].x + dx;
	 plot_view[i].y = plot_view[i].y + dy;
	 plot_view[i].z = plot_view[i].z + dz;
  }
}


void scale(float s)
{
  for (int i=0; i<8; i++)
  {
     plot_org[i].x = plot_org[i].x* s; // there are changing physical coordinates
	 plot_org[i].y = plot_org[i].y* s;//*= s;
     plot_org[i].z = plot_org[i].z* s;//*= s;
  }
}


float visible( int tri_i )
{
  int a = tri[tri_i].p1;
  int b = tri[tri_i].p2;
  int c = tri[tri_i].p3;
  VERTEX ab = { plot_view[b].x - plot_view[a].x, plot_view[b].y - plot_view[a].y, plot_view[b].z - plot_view[a].z};
  VERTEX ac = { plot_view[c].x - plot_view[a].x, plot_view[c].y - plot_view[a].y, plot_view[c].z - plot_view[a].z};

  // p3-p2 = ab , p1-p2 = ac
  VERTEX u = { ab.y*ac.z - ab.z*ac.y, ab.z*ac.x - ab.x*ac.z, ab.x*ac.y - ab.y*ac.x };

//  float res = ((u.x*Bet.x+u.y*Bet.y-u.z*Bet.z)+(u.x*pp2.x+u.y*pp2.y+u.z*pp2.z));
  float res = u.x*plot_view[a].x + u.y*plot_view[a].y + u.z*plot_view[a].z;
  return res;
}

// CLIPPING Teil2

void process_clip_minx( PIXEL &p1, const PIXEL &p2 )
{
  p1.y = -( float(p2.y-p1.y) / (p2.x-p1.x)*p1.x ) + p1.y;
  p1.x = 0;
}

void process_clip_miny( PIXEL &p1, const PIXEL &p2 )
{
  p1.x = -( float(p2.x-p1.x) / (p2.y-p1.y)*p1.y ) + p1.x;
  p1.y = 0;
}

void process_clip_maxx( PIXEL &p1, const PIXEL &p2 )
{
  p1.y = -( float(p2.y-p1.y) / (p2.x-p1.x)*(p1.x-RESX) ) + p1.y;
  p1.x = RESX;
}
void process_clip_maxy( PIXEL &p1, const PIXEL &p2 )
{
  p1.x = -( float(p2.x-p1.x) / (p2.y-p1.y)*(p1.y-RESY) ) + p1.x;
  p1.y = RESY;
}

unsigned clip_code( const PIXEL &p )
{
  unsigned res = 0;
  if( p.x<0 ) res |= 1;
  if( p.y<0 ) res |= 2;
  if( p.x>RESX ) res |= 4;
  if( p.y>RESY ) res |= 8;
  return res;
}


void draw_clipped(PIXEL *p1, PIXEL *p2, int &sign )
{
  unsigned
	 c1 = clip_code( *p1 ),
	 c2 = clip_code( *p2 );
  PIXEL _p1, _p2;
  _p1 = *p1;
  _p2 = *p2;
  if( (c1&c2)!=0 )								// zwischen beiden Punkten kein sichtbarer Ausschnitt
  {
	  sign++;										// wird ver�ndert -> beide nicht sichtbar
	  return;
  }
  if( (c1|c2)!=0 )
  {
	 if( (c1&1)!=0 ) process_clip_minx( _p1, _p2 );		// _p1 und _p2 manipuliert!
	 if( (c1&2)!=0 ) process_clip_miny( _p1, _p2 );
	 if( (c1&4)!=0 ) process_clip_maxx( _p1, _p2 );
	 if( (c1&8)!=0 ) process_clip_maxy( _p1, _p2 );

	 if( (c2&1)!=0 ) process_clip_minx( _p2, _p1 );
	 if( (c2&2)!=0 ) process_clip_miny( _p2, _p1 );
	 if( (c2&4)!=0 ) process_clip_maxx( _p2, _p1 );
	 if( (c2&8)!=0 ) process_clip_maxy( _p2, _p1 );
  }
  *p1 = _p1;
  *p2 = _p2;
  //  zeichnelinie(_p1.x,_p1.y,_p2.x,_p2.y,150);
}


// CLIPPING Teil2

void linetexturehor(int x1, int x2, int y, int index )
{
  VERTEX tp1 = plot_view[ tri[index].p1 ];
  VERTEX tp2 = plot_view[ tri[index].p2 ];
  VERTEX tp3 = plot_view[ tri[index].p3 ];

  VERTEX a = tp1;
  VERTEX p = { (tp2.x-tp1.x), (tp2.y-tp1.y), (tp2.z-tp1.z) };
  VERTEX q = { (tp3.x-tp1.x), (tp3.y-tp1.y), (tp3.z-tp1.z) };
  VERTEX b = { midx,midy,RESX*dist };

  if( x2 < x1 ) swapINT(&x2,&x1);

  for (int i=x1; (i<x2); i++)
  {
	 b.x  = i-midx;
	 b.y  = y-midy;

	 float D  = ((b.x*p.y*q.z)+(p.x*q.y*b.z)+(q.x*b.y*p.z))-((q.x*p.y*b.z)+(b.x*q.y*p.z)+(p.x*b.y*q.z));
	 float Du = ((b.x*a.y*q.z)+(a.x*q.y*b.z)+(q.x*b.y*a.z))-((q.x*a.y*b.z)+(b.x*q.y*a.z)+(a.x*b.y*q.z));
	 float Dv = ((b.x*p.y*a.z)+(p.x*a.y*b.z)+(a.x*b.y*p.z))-((a.x*p.y*b.z)+(b.x*a.y*p.z)+(p.x*b.y*a.z));
	 float drawx = 150.0*Du/D;
	 float drawy = 150.0*Dv/D;
	 if (drawx < 0) drawx = -drawx;
	 if (drawy < 0) drawy = -drawy;

	 int dr_x = int(drawx)%width,
		  dr_y = int(drawy)%height;

	 putpixel(i,y,lpcx[width*(dr_y)+(dr_x)] );
  }
}

void FillTriangle(const PIXEL &p1_, const PIXEL &p2_, const PIXEL &p3_, int index )
{
  float m1 = 0,	m2 = 0,	xl,xr;
  int ymax,ymin,drawxl,drawxr;
  PIXEL p[3];

  p[0] = p1_; p[1] = p2_; p[2] = p3_;
  for (int i=0; i<2; i++) 		// nach y-H�he sortieren
  {
	 if (p[0].y > p[1].y) swapPIXEL(&p[0],&p[1]);
	 if (p[1].y > p[2].y) swapPIXEL(&p[1],&p[2]);
  }
  ymin = p[0].y;                        // Start/ Endposition (0. Punkt)
  ymax = p[2].y;
  xl = p[0].x;
  xr = xl;
// event. BUG: p[0].x = p[1].x vernachl�ssigt!
  if ((p[0].y == p[1].y) && (p[0].x >= p[1].x))
  {
	 swapPIXEL(&p[0],&p[1]);
	 xl = p[0].x;
	 xr = p[1].x;
  }
  if (p[1].y-p[0].y != 0) m1 = (float)(p[1].x-p[0].x)/(p[1].y-p[0].y); // Steigung zum 1. erreichenden Punkt
  if (p[2].y-p[0].y != 0) m2 = (float)(p[2].x-p[0].x)/(p[2].y-p[0].y); // Steigung zum 2. erreichenden Punkt

  for (int y=ymin; y < p[1].y; y++)     // ersten Bereich f�llen
  {
		xl += m1;
		xr += m2;
		if (xl-(int)xl >= 0.5) drawxl = xl+1;
		else drawxl = xl;
		if (xr-(int)xr >= 0.5) drawxr = xr+1;
		else drawxr = xr;
		linetexturehor(drawxl,drawxr,y,index);
  }

  if (p[2].y-p[1].y != 0) m1 = float(p[2].x-p[1].x)/(p[2].y-p[1].y); // Steigung einer Seite neu berechnen

  if ((p[0].y == p[1].y) && (p[2].y > p[0].y)) // wenn abgeschnittene Seite
  {
	 xl = p[2].x;  // neuen Peripheriepunkt bestimmen
	 xr = xl;

	 for (int y=p[0].y; y < ymax; y++)  	        // �brigen Bereich f�llen
	 {
		if ((y>0)&&(y<RESX))
		{
		  xl -= m1;
		  xr -= m2;
		  if (xl-(int)xl >= 0.5) drawxl = xl+1;
		  else drawxl = xl;
		  if (xr-(int)xr >= 0.5) drawxr = xr+1;
		  else drawxr = xr;
		  linetexturehor(drawxl,drawxr,ymax-(y-p[0].y),index);
		 }
	 }
  }
  else
  {
	 for (; y < ymax; y++)  	        // �brigen Bereich f�llen
	 {
		if ((y>0)&&(y<RESX))
		{
		  xl += m1;
		  xr += m2;
		  if (xl-(int)xl >= 0.5) drawxl = xl+1;
		  else drawxl = xl;
		  if (xr-(int)xr >= 0.5) drawxr = xr+1;
		  else drawxr = xr;
		  linetexturehor(drawxl,drawxr,y,index);
		}
	 }
  }
}

void zeichnelinie(int x1, int y1, int x2, int y2, long value)
{
  int error,x,y,dx,dy,xincr,yincr;
  dx = x2-x1; dy = y2-y1;
  if (abs(dx) > abs(dy))
  {
	if (dx<0)
	{
		swapINT(&x2,&x1);
		swapINT(&y2,&y1);
		dx = x2-x1;
	}
	if (y2>y1) yincr = 1;
	else yincr = -1;
	error = -abs(dx)/2;
	for (x=x1,y=y1; x<=x2; x++)
	{
	// Gilt nur als Test f�rs Clippen
	/////////////////////////////////////////////////////////////
	  if ((x<0) && (x>640) && (y<0) && (y>480)) exit(1);
	/////////////////////////////////////////////////////////////
	  putpixel(x,y,value);
	  error += abs(dy);
	  if (error>=0)
	  {
		 y+=yincr;
		 error-=dx;
	  }
	}
  }
  else
  {
	 if (dy<0)
	 {
		swapINT(&x2,&x1);
		swapINT(&y2,&y1);
		dy = y2-y1;
	 }
	 if (x2>x1) xincr = 1;
	 else xincr = -1;
	 error = -abs(dy)/2;
	 for (y=y1,x=x1; y<=y2; y++)
	 {
	// Gilt nur als Test f�rs Clippen
	/////////////////////////////////////////////////////////////
		if ((x<0) && (x>640) && (y<0) && (y>480)) exit(1);
	/////////////////////////////////////////////////////////////
		putpixel(x,y,value);
		error+=abs(dx);
      if (error>=0)
      {
		  x+=xincr;
		  error-=dy;
		}
	 }
  }
}


// CLIPPING:
PIXEL pxl[10];										// Feld/Liste aller Clippunkte: Gr��e dynamisch!

// f�gt Element hinzu, pr�ft vorher, ob schon Gleiches vorhanden
void appendPXL(const PIXEL &point, int &cnt)
{
  for (int i=0; i<10; i++)
  {
	 if      ((pxl[i].x == point.x)&&(pxl[i].y == point.y)) return;
	 else if ((pxl[i].x ==       0)&&(pxl[i].y ==       0))
	 {
		pxl[i] = point;
		cnt++;
		return;
	 }
  }
}

// f�gt ein Element an den Anfang an
void preappend(const PIXEL &point, PIXEL *array, int &asize)
{
  for (int i=0; i<asize; i++)
  {
	 PIXEL temp = array[(int&)asize-1-i];
	 array[(int&)asize-i] = temp;
  }
  array[0] = point;
  asize++;
}

// bestimmt, ob Punkt p innerhalb des Bildschirms liegt (0;0;RESX;RESY)
int IN(const PIXEL &p)
{
  if ((p.x>=0)&&(p.x<=RESX)&&(p.y>=0)&&(p.y<=RESY)) return 1;
  else return -1;
}

// liefert Fl�cheninhalt eines Dreieckes(P1;P2;P3)
float getA(const PIXEL &p1, const PIXEL &p2, const PIXEL &p3)
{
  long res = (0.5*(p1.x*p2.y+p1.y*p3.x+p2.x*p3.y-p1.x*p3.y-p1.y*p2.x-p2.y*p3.x));
  if (res<0) return -res;
  else return res;
}

// bestimmt, ob ein bestimmter Punkt innerhalb eines Dreieckes liegt
/*** durch Vergleich von Fl�cheninhalten von AGes(t) und den entstehenden Teildreiecken mittels p ***/
int INTRI(const PIXEL &p, const PIXELTRI &t)
{
  long  AGes = getA(t.p1,t.p2,t.p3);
  long  A1   = getA(t.p1,t.p2,p),
		  A2   = getA(t.p1,t.p3,p),
		  A3   = getA(t.p2,t.p3,p);
  long  SAi  = A1+A2+A3;
  if (SAi != AGes) return -1;
  else return 1; 									// Fl�cheninhalte matchen
}

PIXEL findyminPXL(PIXEL *a, int elem) 		// suche MinimumY aus in "*a[0..elem]"
{
  PIXEL Max= a[0];
  for (int i=0; i<elem; i++)
  { if (a[i].y < Max.y)	Max = a[i]; }
  return Max;
}

PIXEL findxminPXL(PIXEL *a, int elem) 		// suche MinimumX aus *a
{
  PIXEL Max= a[0];
  for (int i=0; i<elem; i++)
  {
	 if ((a[i].x < Max.x) || ((a[i].x == Max.x)&&(a[i].y>Max.y))) Max = a[i];
  }
  return Max;
}

PIXEL findxmaxPXL(PIXEL *a, int elem) 		// suche MaximumX aus *a
{
  PIXEL Max= a[0];
  for (int i=0; i<elem; i++)
  {
	 if ((a[i].x > Max.x) || ((a[i].x == Max.x)&&(a[i].y>Max.y))) Max = a[i];
  }
  return Max;
}

// Verschiebt das Element "elem" aus "array" und h�ngt es in "distination" an
void transferelem(PIXEL &elem, PIXEL *array, int &asize, PIXEL *distination, int &dissize)
{
	for (int i=0; i<asize; i++)
	{
	  if ((array[i].x==elem.x)&&(array[i].y==elem.y))
	  {
		 distination[(int&)dissize] = elem;	// Anh�ngen an letztes Element
		 dissize++;
		 // Verschiebe nun alle folgenden Elemente von array um 1 Element nach links
		 for (int j=i; j<asize/*-1*/; j++)
			array[j] = array[j+1];
		 asize--;
// BUG: (dissize+1) kann �berlauf verursachen! oder distination[dissize]
		 return;
	  }
	}
}

// entfernt das Element "elem" aus "array"
void eraseelem(PIXEL &elem, PIXEL *array, int &asize)
{
	for (int i=0; i<asize; i++)
	{
		if ((array[i].x==elem.x)&&(array[i].y==elem.y))
		{
			for (int j=i; j<asize; j++)
				array[j] = array[j+1];
			asize--;
			return;
		}
	}
}

// geclipptes Dreieck f�llen
void FillClippedTriangle(const PIXEL &p1_, const PIXEL &p2_, const PIXEL &p3_, int index)
{
  PIXEL p1 = p1_, p2 = p2_, p3 = p3_;

  if ((IN(p1)>0)&&(IN(p2)>0)&&(IN(p3)>0))	// wenn vollst�ndig sichtbar
  { FillTriangle(p1,p2,p3,index); return; }
  // Dreieck clippen werden:
  // =======================
  // Init:
  /**** pxl[i] mu� vorher noch "genullt" werden (OK)!****/
  int INTERSECTS = 0, 							// Anzahl der Schnittpunkte
		testsign   = 0;							// Testbyte f�r geclippte Geradensegmente
  // 0. alle Schnittpunkte mit Geraden + freiliegende Punkte des Dreieckes ermitteln
  PIXEL pp1 = p1, pp2 = p2, pp3 = p3; 		// Hilfsvariablen f�r Schnittpunktberechnung

  draw_clipped( &pp1, &pp2, testsign);
  if (testsign==0) { appendPXL(pp1, INTERSECTS); appendPXL(pp2, INTERSECTS); }
  pp1 = p1; pp3 = p3; testsign = 0;       // neu laden
  draw_clipped( &pp1, &pp3, testsign );
  if (testsign==0) { appendPXL(pp1, INTERSECTS); appendPXL(pp3, INTERSECTS); }
  pp2 = p2; pp3 = p3; testsign = 0;       // neu laden
  draw_clipped( &pp2, &pp3, testsign );
  if (testsign==0) { appendPXL(pp2, INTERSECTS); appendPXL(pp3, INTERSECTS); }
  // 1. Pr�fen: welche Eckpunkte des Clippfensters im Dreieck(p1,p2,p3) liegen?
	 // vorerst mit Fehlern in Fl�chenberechnungsroutine!:
  PIXELTRI test;	//!!!Achtung: test wird kurzweilig in INTRI �berschrieben
  PIXEL testpxl;
  test.p1 =p1; test.p2 =p2; test.p3 =p3;

  testpxl.x = 0; testpxl.y = 0;
  if (INTRI(testpxl, test)>0) appendPXL(testpxl, INTERSECTS); // li oben
  testpxl.x = RESX; testpxl.y = 0;
  if (INTRI(testpxl, test)>0) appendPXL(testpxl, INTERSECTS); // re oben
  testpxl.x = 0; testpxl.y = RESY;
  if (INTRI(testpxl, test)>0) appendPXL(testpxl, INTERSECTS); // li unten
  testpxl.x = RESX; testpxl.y = RESY;
  if (INTRI(testpxl, test)>0) appendPXL(testpxl, INTERSECTS); // re unten

  // 2. Anfangspunkt suchen
  PIXEL startpos = findyminPXL(pxl,INTERSECTS); // Punkt mit h�hstem Y-Wert = Ausgangspunkt
  eraseelem(startpos,pxl,INTERSECTS);
  // 3. Triangulation: 			            // Ausgangspunkt liegt im 1/2 Quadranten
  PIXEL  lefts[5];								// �u�ersterst links gelegene Punkte
  PIXEL rights[5];							   // �u�ersterst links gelegene Punkte
  for (int lv=0; lv<5; lv++) {  lefts[lv].x=0; lefts[lv].y=0; rights[lv].x=0; rights[lv].y=0; } // Init:
//BUG: dynamische Gr��e f�r lefts und rights! (Liste)
  int leftssize  = 0, rightssize = 0;
  PIXEL left  = findxminPXL(pxl,INTERSECTS);  // linke Begrenzung finden
  PIXEL right = findxmaxPXL(pxl,INTERSECTS);	 // rechte Begrenzung finden
  for (int i=0; i<INTERSECTS; i++)			// Heraussuchen der "Ausnahmen"
  {
	 if ((pxl[i].x<=right.x)&&(pxl[i].y<right.y)&&(pxl[i].x>=startpos.x)&&(pxl[i].y>=startpos.y)
	 &&(pxl[i].x>left.x))
		transferelem(pxl[i],pxl,INTERSECTS,rights,rightssize);
	 if ((pxl[i].x>= left.x)&&(pxl[i].y< left.y)&&(pxl[i].x<=startpos.x)&&(pxl[i].y>=startpos.y)
	 &&(pxl[i].x<right.x))
		transferelem(pxl[i],pxl,INTERSECTS,lefts,leftssize);
  }
  for (int ls=0; ls<leftssize; ls++)		// die Elemente links sortieren
  {
	 for (ls=0; ls<leftssize-1; ls++)
//OK-BUG: mu� vom gr��tem zum kleinsten!
	 { if (lefts[ls].x<lefts[ls+1].x) swapPIXEL(&lefts[ls],&lefts[ls+1]); }
  }
  for (int lr=0; lr<rightssize; lr++)		// die Elemente rechts sortieren
  {
	 for (lr=0; lr<rightssize-1;lr++)
	 { if (rights[lr].x>rights[lr+1].x) swapPIXEL(&rights[lr],&rights[lr+1]); }
  }
  for (int l=0; l<INTERSECTS; l++)			// restliche Elemente zwischen left und right nach X-MinimumSortieren
  {
	 int min = l;
	 for (int j=l; j<INTERSECTS;j++)
	 { if (pxl[min].x > pxl[j].x) min = j; }
	 if (min != l) swapPIXEL(&pxl[l],&pxl[min]);
  }
  // 4. F�LLEN:									// "startpos" = Bezugspunkt
  if (leftssize>0)								// Grenzelemente nur bei bereits vorhandenen anheften
  {
	  for (int c=0; c<leftssize; c++)
		 preappend(lefts[leftssize-1-c], pxl, INTERSECTS); // mu� vorn angeheftet werden
//BUG: mu� r�ckw�rts am Anfang angeheftet werden!: lefts[c]
	  //ACHTUNG: nicht vergessen, da� lefts ein Elem weniger besitzt und leftssize--;
  }
  if (rightssize>0)                       // Grenzelemente nur anheften, wenn bereits Elemente vorhanden!
  {
	  for (int c=0; c<rightssize; c++)		// rechts-�berstehende Dreiecke f�llen
		 pxl[INTERSECTS+c] = rights[c];
	  INTERSECTS++;
//BUG: nicht vergessen, da� lefts ein Elem weniger besitzt und leftssize--;
  }
  for (int c=1; c<INTERSECTS; c++)			// alle Dreiecke f�llen
	 FillTriangle(startpos, pxl[c-1],pxl[c],index);
//BUG: "Index"-parameter �berfl�ssig!
}

void DrawFrame()
{
  PIXEL p1_, p2_, p3_;

  for (int i=0; i<tri_anz; i++)
  {
	  if (visible(i) > 0) continue;

		 for (int j=0; j<10; j++)			// Init: "Nullen"
		 { pxl[j].x = 0; pxl[j].y = 0; }

		 FillClippedTriangle( plot_scr[ tri[i].p1 ], plot_scr[ tri[i].p2 ], plot_scr[ tri[i].p3 ], i );
  }
}

VERTEX dreh;
VERTEX move;


void doInit()
{
  int gdriver = DETECT, gmode, errorcode;

  initgraph(&gdriver, &gmode, "\\TCPP\\BGI\\");
  errorcode = graphresult();
  if (errorcode != grOk)
  {
	 printf("Grafikfehler: %s\n",grapherrormsg(errorcode));
	 printf("Dr�cken Sie eine beliebige Taste!");
	 getch();
	 exit(1);
  }

  dist = 0.5;

  plot_org[0].x = -2;	plot_org[0].y = -2;	plot_org[0].z = -2;
  plot_org[1].x = -2;	plot_org[1].y = +2;	plot_org[1].z = -2;
  plot_org[2].x = +2;	plot_org[2].y = +2;	plot_org[2].z = -2;
  plot_org[3].x = +2;	plot_org[3].y = -2;	plot_org[3].z = -2;
  plot_org[4].x = -2;	plot_org[4].y = -2;	plot_org[4].z = +2;
  plot_org[5].x = -2;	plot_org[5].y = +2;	plot_org[5].z = +2;
  plot_org[6].x = +2;	plot_org[6].y = +2;	plot_org[6].z = +2;
  plot_org[7].x = +2;	plot_org[7].y = -2;	plot_org[7].z = +2;
  tri[ 0].p1 = 5; tri[ 0].p2 = 4; tri[ 0].p3 = 6;
  tri[ 1].p1 = 4; tri[ 1].p2 = 7; tri[ 1].p3 = 6;
  tri[ 2].p1 = 1; tri[ 2].p2 = 0; tri[ 2].p3 = 5;
  tri[ 3].p1 = 0; tri[ 3].p2 = 4; tri[ 3].p3 = 5;
  tri[ 4].p1 = 0; tri[ 4].p2 = 3; tri[ 4].p3 = 4;
  tri[ 5].p1 = 3; tri[ 5].p2 = 7; tri[ 5].p3 = 4;
  tri[ 6].p1 = 2; tri[ 6].p2 = 1; tri[ 6].p3 = 5;
  tri[ 7].p1 = 5; tri[ 7].p2 = 6; tri[ 7].p3 = 2;
  tri[ 8].p1 = 6; tri[ 8].p2 = 7; tri[ 8].p3 = 3;
  tri[ 9].p1 = 3; tri[ 9].p2 = 2; tri[ 9].p3 = 6;
  tri[10].p1 = 3; tri[10].p2 = 0; tri[10].p3 = 2; // 230
  tri[11].p1 = 2; tri[11].p2 = 0; tri[11].p3 = 1;

  dreh.x = dreh.y = dreh.z = move.x = move.y = 0;
  move.z = 10;
}

void plot_copy()
{
  for( int i=0; i<plot_anz; i++ )
  {
	 plot_view[i].x = plot_org[i].x;
	 plot_view[i].y = plot_org[i].y;
	 plot_view[i].z = plot_org[i].z;
  }
}

void main(void)
{
	int ch;

	doInit();
	while (1)
	{
		ch = getch();
		cleardevice();

	  switch (ch)
			{
			  case 103: {			         // g
					free(lpcx);
					closegraph();
					exit(1);
					}    		  break;
			  case 27: free(lpcx); exit(1);	      break;//ESC
			  case 49: dreh.y += 0.17;   				break; //1
			  case 50: dreh.y -= 0.17;   				break; //2
			  case 52: dreh.x += 0.17;   				break; //4
			  case 53: dreh.x -= 0.17;   				break; //5
			  case 55: dreh.z += 0.17;   				break; //7
			  case 56: dreh.z -= 0.17;   				break; //8
			  case 121:move.z -= 0.5; break;	//y-nah
			  case 120:move.z += 0.5; break; //x-fern
			  case 115:move.x += 0.5; break; //CLeft:s
			  case 102:move.x -= 0.5; break; //CRight:f
			  case 101:move.y += 0.5; break; //CUp:e
			  case  99:move.y -= 0.5; break; //CDown:c


			  case 51: scale(0.9);	break; //3
			  case 54: scale(1.1);	break; //6

			  case 57: { //9 --> hier Breakpoint zum Debuggen
           ;
			  } break;
			}

		plot_copy();
		rotatexaxis( dreh.x );
		rotateyaxis( dreh.y );
		rotatezaxis( dreh.z );
		translate( move.x, move.y, move.z );
		projiziere();

		DrawFrame();
	}
}
