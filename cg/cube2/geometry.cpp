#include "geometry.hpp"
#include "ddraw.hpp"
#include "common.hpp"


// zeichnet eine horizontale Linie
void linehor(int x1, int x2, int y, long value)
{
	if (x2-x1 < 0) swapINT(x2,x1);
	for (int x=x1; x<=x2; x++)
		putpixel(x,y,value);
}

void linetexturehor(int x1, int x2, int y, int index, int elem)
{
/*  VERTEX tp1 = plot_view[ tri[index].p1 ];
  VERTEX tp2 = plot_view[ tri[index].p2 ];
  VERTEX tp3 = plot_view[ tri[index].p3 ];

  VERTEX a = tp1;
  VERTEX p = { (tp2.x-tp1.x), (tp2.y-tp1.y), (tp2.z-tp1.z) };
  VERTEX q = { (tp3.x-tp1.x), (tp3.y-tp1.y), (tp3.z-tp1.z) };
  VERTEX b = { MIDX,MIDY,RESX*dist };

  if( x2 < x1 ) swapINT(x2,x1);
  b.y  = y-MIDY;

  float pxqybz = (p.x*q.y*b.z);
  float axqybz = (a.x*q.y*b.z);
  float pxaybz = (p.x*a.y*b.z);
  float qxbypz = (q.x*b.y*p.z);
  float qxbyaz = (q.x*b.y*a.z);
  float axbypz = (a.x*b.y*p.z);
  float qxpybz = (q.x*p.y*b.z);
  float qxaybz = (q.x*a.y*b.z);
  float axpybz = (a.x*p.y*b.z);
  float pxbyqz = (p.x*b.y*q.z);
  float axbyqz = (a.x*b.y*q.z);
  float pxbyaz = (p.x*b.y*a.z);

  for (int i=x1; (i<x2); i++)
  {
	 b.x  = (float)i-MIDX;
	 float D  = ((b.x*p.y*q.z)+pxqybz+qxbypz)-(qxpybz+(b.x*q.y*p.z)+pxbyqz);
	 float Du = ((b.x*a.y*q.z)+axqybz+qxbyaz)-(qxaybz+(b.x*q.y*a.z)+axbyqz);
	 float Dv = ((b.x*p.y*a.z)+pxaybz+axbypz)-(axpybz+(b.x*a.y*p.z)+pxbyaz);


	 float drawx = (float)(150.0*Du/D);
	 float drawy = (float)(150.0*Dv/D);
	 if (drawx < 0) drawx = -drawx;
	 if (drawy < 0) drawy = -drawy;

	 int dr_x = int(drawx)%width,
		 dr_y = int(drawy)%height;

	 putpixel(i,y,lpcx[width*(dr_y)+(dr_x)] );
  }*/
}

/*
// zeichnet eine beliebige Linie; nach BRESENHAM-Algorithmus
void zeichnelinie(PIXEL &p1, PIXEL &p2, long value)
{
  int error,x,y,dx,dy,xincr,yincr;
  dx = p2.x-p1.x; dy = p2.y-p1.y;
  if (abs(dx) > abs(dy))
  {
	if (dx<0)
	{
	   swapPIXEL(p1,p2);
	   dx = p2.x-p1.x;
	}
	if (p2.y>p1.y) yincr = 1;
	else yincr = -1;
	error = -abs(dx)/2;
	for (x=p1.x,y=p1.y; x<=p2.x; x++)
	{
	  putpixel(x,y,value);
	  error += abs(dy);
	  if (error>=0)
	  {
	    y+=yincr;
	    error-=dx;
	  }
	}
  }
  else
  {
    if (dy<0)
    {
      swapPIXEL(p2,p1);
      dy = p2.y-p1.y;
    }
    if (p2.x>p1.x) xincr = 1;
    else xincr = -1;
    error = -abs(dy)/2;
    for (y=p1.y,x=p1.x; y<=p2.y; y++)  
    {
      putpixel(x,y,value);
      error+=abs(dx);
      if (error>=0)
      {
		  x+=xincr;
		  error-=dy;
      }
    }
  }
  */

void zeichnelinie(PIXEL &p1, PIXEL &p2, long value)
{
  int error,x,y,dx,dy,xincr,yincr;
  int x1 = p1.x, 
	  y1 = p1.y, 
	  x2 = p2.x,
	  y2 = p2.y;
  dx = x2-x1; dy = y2-y1;
  if (abs(dx) > abs(dy))
  {
	if (dx<0)
	{
	   swapINT2(&x2,&x1);
	   swapINT2(&y2,&y1);
	   dx = x2-x1;
	}
	if (y2>y1) yincr = 1;
	else yincr = -1;
	error = -abs(dx)/2;
	for (x=x1,y=y1; x<=x2; x++)
	{
	  putpixel(x,y,value);
	  error += abs(dy);
	  if (error>=0)
	  {
	    y+=yincr;
	    error-=dx;
	  }
	}
  }
  else
  {
    if (dy<0)
    {
      swapINT2(&x2,&x1);
      swapINT2(&y2,&y1);
      dy = y2-y1;
    }
    if (x2>x1) xincr = 1;
    else xincr = -1;
    error = -abs(dy)/2;
    for (y=y1,x=x1; y<=y2; y++)  
    {
      putpixel(x,y,value);
      error+=abs(dx);
      if (error>=0)
      {
		  x+=xincr;
		  error-=dy;
      }
    }
  }
}

void FillPoly(const PIXEL *p1_, const PIXEL *p2_, const PIXEL *p3_, long col)
{
	float m1 = 0,	m2 = 0,	xl,xr;
	int ymax,ymin,drawxl,drawxr;
	PIXEL p[3];

	p[0] = *p1_; p[1] = *p2_; p[2] = *p3_;

	for (int i=0; i<2; i++) 		// nach y-H�he sortieren
	{
	    if (p[0].y > p[1].y) swapPIXEL(p[0],p[1]);
		if (p[1].y > p[2].y) swapPIXEL(p[1],p[2]);
	}

	ymin = p[0].y;                        // Start/ Endposition (0. Punkt)
	ymax = p[2].y;

	xl = (float)p[0].x;
	xr = xl;

	// wenn (~) && (p[0].x = p[1].x) dann kein Dreieck, sondern Linie!
	if ((p[0].y == p[1].y) && (p[0].x >/*=*/ p[1].x))
	{
		swapPIXEL(p[0],p[1]);
		xl = (float)p[0].x;
		xr = (float)p[1].x;
	}

	if (p[1].y-p[0].y != 0) m1 = (float)(p[1].x-p[0].x)/(p[1].y-p[0].y); // Steigung zum 1. erreichenden Punkt
	if (p[2].y-p[0].y != 0) m2 = (float)(p[2].x-p[0].x)/(p[2].y-p[0].y); // Steigung zum 2. erreichenden Punkt

	for (int y=ymin; y < p[1].y; y++)     // ersten Bereich f�llen
	{
	    xl += m1;
	    xr += m2;
	    if (xl-(int)xl >= 0.5) drawxl = (int)xl+1;
	    else drawxl = (int)xl;
	    if (xr-(int)xr >= 0.5) drawxr = (int)xr+1;
	    else drawxr = (int)xr;
		linehor(drawxl,drawxr,y,col);	// mit Linie f�llen
	}

	if (p[2].y-p[1].y != 0) m1 = float(p[2].x-p[1].x)/(p[2].y-p[1].y); // Steigung einer Seite neu berechnen

	if ((p[0].y == p[1].y) && (p[2].y > p[0].y)) // wenn abgeschnittene Seite
	{
		xl = (float)p[2].x;  // neuen Peripheriepunkt bestimmen
		xr = xl;

		for (int y=p[0].y; y < ymax; y++)  	        // �brigen Bereich f�llen
		  {
			xl -= m1;
			xr -= m2;
			if (xl-(int)xl >= 0.5) drawxl = (int)xl+1;
			else drawxl = (int)xl;
			if (xr-(int)xr >= 0.5) drawxr = (int)xr+1;
			else drawxr = (int)xr;
			linehor(drawxl,drawxr,ymax-(y-p[0].y),col);       // mit Linie f�llen
		}
	}
	else
	{
		for ( ; y < ymax; y++)  	        // �brigen Bereich f�llen
		{
			xl += m1;
			xr += m2;
			if (xl-(int)xl >= 0.5) drawxl = (int)xl+1;
			else drawxl = (int)xl;
			if (xr-(int)xr >= 0.5) drawxr = (int)xr+1;
			else drawxr = (int)xr;
			linehor(drawxl,drawxr,y,col);       // mit Linie f�llen
		}
	}
}

// elem nur als Parameter�bergabe f�r Line~!!!
void FillPolyTxt(const PIXEL *p1_, const PIXEL *p2_, const PIXEL *p3_, int elem)
{
	float m1 = 0,	m2 = 0,	xl,xr;
	int ymax,ymin,drawxl,drawxr;
	PIXEL p[3];

	p[0] = *p1_; p[1] = *p2_; p[2] = *p3_;

	for (int i=0; i<2; i++) 		// nach y-H�he sortieren
	{
	    if (p[0].y > p[1].y) swapPIXEL(p[0],p[1]);
		if (p[1].y > p[2].y) swapPIXEL(p[1],p[2]);
	}

	ymin = p[0].y;                        // Start/ Endposition (0. Punkt)
	ymax = p[2].y;

	xl = (float)p[0].x;
	xr = xl;

	// wenn (~) && (p[0].x = p[1].x) dann kein Dreieck, sondern Linie!
	if ((p[0].y == p[1].y) && (p[0].x >/*=*/ p[1].x))
	{
		swapPIXEL(p[0],p[1]);
		xl = (float)p[0].x;
		xr = (float)p[1].x;
	}

	if (p[1].y-p[0].y != 0) m1 = (float)(p[1].x-p[0].x)/(p[1].y-p[0].y); // Steigung zum 1. erreichenden Punkt
	if (p[2].y-p[0].y != 0) m2 = (float)(p[2].x-p[0].x)/(p[2].y-p[0].y); // Steigung zum 2. erreichenden Punkt

	for (int y=ymin; y < p[1].y; y++)     // ersten Bereich f�llen
	{
	    xl += m1;
	    xr += m2;
	    if (xl-(int)xl >= 0.5) drawxl = (int)xl+1;
	    else drawxl = (int)xl;
	    if (xr-(int)xr >= 0.5) drawxr = (int)xr+1;
	    else drawxr = (int)xr;
		linetexturehor(drawxl,drawxr,y,1,elem);
	}

	if (p[2].y-p[1].y != 0) m1 = float(p[2].x-p[1].x)/(p[2].y-p[1].y); // Steigung einer Seite neu berechnen

	if ((p[0].y == p[1].y) && (p[2].y > p[0].y)) // wenn abgeschnittene Seite
	{
		xl = (float)p[2].x;  // neuen Peripheriepunkt bestimmen
		xr = xl;

		for (int y=p[0].y; y < ymax; y++)  	        // �brigen Bereich f�llen
		  {
			xl -= m1;
			xr -= m2;
			if (xl-(int)xl >= 0.5) drawxl = (int)xl+1;
			else drawxl = (int)xl;
			if (xr-(int)xr >= 0.5) drawxr = (int)xr+1;
			else drawxr = (int)xr;
			linetexturehor(drawxl,drawxr,ymax-(y-p[0].y),1,elem);
		}
	}
	else
	{
		for ( ; y < ymax; y++)  	        // �brigen Bereich f�llen
		{
			xl += m1;
			xr += m2;
			if (xl-(int)xl >= 0.5) drawxl = (int)xl+1;
			else drawxl = (int)xl;
			if (xr-(int)xr >= 0.5) drawxr = (int)xr+1;
			else drawxr = (int)xr;
			linetexturehor(drawxl,drawxr,y,1,elem);
		}
	}
}