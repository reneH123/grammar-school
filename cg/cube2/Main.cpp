// TODO:
// FEHLER: Clippen der Textur (->Triangulation!?)
// FEHLER: Routine zur Bestimmung, ob ein Punkt innerhalb eines Dreiecks liegt
// FEHLER: Absturz bei 1. (.z<0)  2. Linien au�erhalb des sichtbaren Bereichs 3. Textur clippen
// FEHLT: Koordinatenursprung(0/0/0) mu� im Objekt sein!
// ALSO TODO:
// wie kann ich �berall Matrizenoperationen nutzen: (primitiva.cpp->�bersichtlichkeit bewahren?)
// ist es "sinnvoll" eine Klasse Weltkoord anzulegen (Aufwand?)?
// wie kann man Text im Grafikmodus ausgeben?	--> �ber Bilddateien (Fonts)
// wie kann man Farbpalette des PCX-Bildes einstellen?	--> Umwandlung von 8Bit-Farbe in TrueColor
// Ungekl�rt:
// wie mu� Clipp-Routine umgestaltet werden, damit nicht nur (0/0) und (RESX/RESY) Eckpunkte des Fensters sind?
// wie funktionieren C++-Exceptions (Datei fehlt(fehlerhaft), Division durch Null, ....)?
// Listenverwaltung + einige grundlegende Funktionen?

#include "resource.h"
#include <windows.h>
#include <mmsystem.h>       //The Timerinterface
#include "graph.hpp"
#include "geometry.hpp"
#include "clip.hpp"

// DirectDraw-Properties
#include "ddraw.hpp"

LPDIRECTDRAW            lpDD;           // DirectDraw object
LPDIRECTDRAWSURFACE     lpDDSPrimary;   // DirectDraw primary surface
LPDIRECTDRAWSURFACE     lpDDSBack;      // DirectDraw back surface
unsigned char			*lpSurface; // Zeiger auf Bilddaten
unsigned long			dwHeight, dwWidth;	// Height & Width of the Surface
long					lPitch; // Anzahl von Bytes pro Pixelzeile

// Win32-Properties
HFONT					hfont;							//handle f�r Font auf Bildschirm
BOOL                    bActive;						// is application active?
BOOL					bFlip;
HACCEL					hAccelTable;					// Handle zum Accelerator
HWND					hMainWnd;						// Handle zum Hauptfenster der Anwendung
HINSTANCE hInst;										// aktuelle Instanz


//Statistik
int framerate;
int framecount;
int framecount0;
DWORD frametime;
DWORD frametime0;


// Camera-Properties
#include "camera.hpp"
VERTEX campos = {0.0f,0.0f,20.0f};
Camera Camera_instance(&campos);

#include "Main.hpp"
WORLDITEMlist worlditemlist;


void initworldlist(void)
{
	WORLDITEM w;		// Puffer
	// 1. Objekt	
	w.pos.x = -2;	w.pos.y = 0;	w.pos.z = -1;
	w.FileNameVertexDefine = "cube2_vertex.txt";
	w.FileNameTrianglesDefine = "cube_tri.txt";
	w.FileNameTexturDefine = "snow.pcx";
	w.DO = new Drawingobject(w.FileNameVertexDefine,w.FileNameTrianglesDefine,&w.pos);
	w.pcxtextur = new PCXClass(w.FileNameTexturDefine);
	worlditemlist.push_back(w);
	// 2. Objekt
	w.pos.x = 5;	w.pos.y = 7;	w.pos.z = 5;
	w.FileNameVertexDefine = "prisma_vertex.txt";
	w.FileNameTrianglesDefine = "prisma2_tri.txt";
	w.FileNameTexturDefine = "snow.pcx";
	w.DO = new Drawingobject(w.FileNameVertexDefine,w.FileNameTrianglesDefine,&w.pos);
	w.pcxtextur = new PCXClass(w.FileNameTexturDefine);
	worlditemlist.push_back(w);
	// 3. Objekt
	w.pos.x = -2;	w.pos.y = -3;	w.pos.z = -7;
	w.FileNameVertexDefine = "pyramide_vertex.txt";
	w.FileNameTrianglesDefine = "pyramide_tri.txt";
	w.FileNameTexturDefine = "snow.pcx";
	w.DO = new Drawingobject(w.FileNameVertexDefine,w.FileNameTrianglesDefine,&w.pos);
	w.pcxtextur = new PCXClass(w.FileNameTexturDefine);
	worlditemlist.push_back(w);
}

int DrawFrame ()
{
	HRESULT				ddrval;
	DDSURFACEDESC       ddsd;
	DDBLTFX				ddbltfx;

	// Bildschirm l�schen
	ZeroMemory (&ddbltfx, sizeof(ddbltfx));
	ddbltfx.dwSize = sizeof (ddbltfx);
	ddbltfx.dwFillColor = RGB( 0, 0, 0 );
    ddrval = lpDDSBack->Blt ( NULL, NULL, NULL, DDBLT_COLORFILL | DDBLT_WAIT, &ddbltfx );

	// Zugriff auf Videospeicher
	ZeroMemory (&ddsd, sizeof(ddsd));
	ddsd.dwSize = sizeof (ddsd);
	while ((ddrval = lpDDSBack->Lock( NULL, &ddsd, 0, NULL )) == DDERR_WASSTILLDRAWING);

	if (ddrval != DD_OK) 
	{
		return ddrval;
	}

	// Zeiger auf Bilddaten merken
	lpSurface = (unsigned char*)ddsd.lpSurface;

	// FPS:
	HDC hdc;		// Handle f�r Surface

	if( lpDDSPrimary->GetDC(&hdc) != DD_OK )
	{
		MessageBox(NULL,"Error meanwhile getting hdc occured!","Error",MB_ICONERROR);
		exit(0);			// bei Fehler sofort Programm beenden
	}

	char string[128];
    int len;
    len = wsprintf( string, "FPS %02d", framerate );	// String erzeugen

	SetBkMode( hdc, TRANSPARENT );					// Durchsichtig
	SelectObject( hdc, hfont );
	SetTextColor( hdc, RGB(255, 255, 255) );		// Text wei�
	TextOut( hdc, 0, RESY-20, string, len );
	lpDDSPrimary->ReleaseDC( hdc );

	//Statistik auffrischen:
    framecount++;
    frametime = timeGetTime();
    if( ( frametime-frametime0 )>1000 ) 
	{
		framerate = ( framecount-framecount0 )*1000 / ( frametime-frametime0 );
		frametime0 = frametime;
		framecount0 = framecount;
	}

	for (int i=0; i<1/*worlditemlist.size()*/; i++) // bei (i=1) und (i=2) Fehler! -> Zentriertes Objekt!
	{
		worlditemlist[i].DO->CopyVList();
		worlditemlist[i].DO->addrotate(0.5f, 0.004f, 0.003f);
		worlditemlist[i].DO->addtranslate(0.0f,0.0f,0.0f);
		worlditemlist[i].DO->convert3to2d( Camera_instance.getpos() );

//		VERTEX cam = Camera_instance.getpos();	// Position des Betrachters
		for (int j=0; j<worlditemlist[i].DO->GetCntTriangle(); j++)
		{
			TRIANGLE shifttriangle = worlditemlist[i].DO->gettriangle(j);

			PIXEL p1 = worlditemlist[i].DO->getpixel(shifttriangle.p1),
				  p2 = worlditemlist[i].DO->getpixel(shifttriangle.p2),
				  p3 = worlditemlist[i].DO->getpixel(shifttriangle.p3);

			if (POINTMODACTIVE > 0)
			{
				putpixel(p1.x,p1.y,210);
				putpixel(p2.x,p2.y,210);
				putpixel(p3.x,p3.y,210);
			}
			if (WIREFRAMEACTIVE > 0)
			{
				int testsign = 0;
				draw_clipped( &p1, &p2, testsign );
				if (testsign == 0) zeichnelinie(p1,p2,210);
				testsign = 0;
				draw_clipped( &p2, &p3, testsign  );
				if (testsign == 0) zeichnelinie(p2,p3,210);				
				testsign = 0;
				draw_clipped( &p3, &p1, testsign  );
				if (testsign == 0) zeichnelinie(p3,p1,210);
			}
		}
		//worlditemlist[i].DO->addrotate(0.7f, 0.0f, 0.0f);
	}

	/*

	// alle Objekte ausgeben: BEGINN
	for (int i=0; i<worlditemlist.size(); i++)
	{
//		VERTEX cam = Camera_instance.getpos();	// Position des Betrachters
		for (int j=0; j<worlditemlist[i].DO->GetCntTriangle(); j++)
		{
			worlditemlist[i].DO->CopyVList();
			worlditemlist[i].DO->addrotate(0.005f, 0.004f, 0.003f);
			worlditemlist[i].DO->addtranslate(0.0f,0.0f,0.0f);
			worlditemlist[i].DO->convert3to2d();
							
			TRIANGLE shifttriangle = worlditemlist[i].DO->gettriangle(j);

			PIXEL p1 = worlditemlist[i].DO->getpixel(shifttriangle.p1),
				  p2 = worlditemlist[i].DO->getpixel(shifttriangle.p2),
				  p3 = worlditemlist[i].DO->getpixel(shifttriangle.p3);
			      //cp = worlditemlist[i].DO->convert3dto2d(&globpos, &cam);	// Zentraler Punkt

			if (POINTMODACTIVE > 0)		// viele Punkte werden mehrfach �bermalt -> sinnlos!
			{
				putpixel(p1.x,p1.y,210);
				putpixel(p2.x,p2.y,210);
				putpixel(p3.x,p3.y,210);
			}
//			if (CENTERMODACTIVE > 0)
//			{
//				putpixel(cp.x,cp.y,160);	
//			}

			if ( worlditemlist[i].DO->visibletriangle(j,&Camera_instance.getpos()) > 0) 
				continue;

			if (WIREFRAMEACTIVE > 0)
			{
				int testsign = 0;
				draw_clipped( &p1, &p2, testsign );
				if (testsign != 0) zeichnelinie(p1,p2,210);
				testsign = 0;
				draw_clipped( &p2, &p3, testsign  );
				if (testsign != 0) zeichnelinie(p2,p3,210);				
				testsign = 0;
				draw_clipped( &p3, &p1, testsign  );
				if (testsign != 0) zeichnelinie(p3,p1,210);				
			}
			
			if (COLFILLEDMODACTIVE > 0)    
			{ 
					FillPoly(&p1,&p2,&p3,210);  
			}
			if (TEXTUREFILLEDMODCTIVE > 0) 
			{
				if (worlditemlist[i].pcxtextur->loadPCX()!=NULL)
					FillPolyTxt(&p1,&p2,&p3,i); 
			}
		}
		//worlditemlist[i].DO->addrotate(0.005f, 0.004f, 0.003f);
	}
*/

	// Zugriff auf Videospeicher wieder freigeben
	ddrval = lpDDSBack->Unlock( lpSurface );
	if (ddrval != DD_OK) 
	{
		return false;
	}

	// Backbuffer mit Frontbuffer vertauschen
	while (1) 
	{
		ddrval = lpDDSPrimary->Flip( NULL, 0 );
		if (ddrval == DD_OK)
			break;

		if (ddrval == DDERR_SURFACELOST) 
		{
			if (!restoreAll())
				return false;
		}
		if (ddrval != DDERR_WASSTILLDRAWING) 
		{
			if (ddrval != DD_OK) 
			{
				return false;
			}
		}
	}
	return true;  // All done
}

// Alle angeforderten Speicherbereiche und Objekte wieder freigeben
void finiObjects( void )
{
	if( lpDD != NULL ) {
		if( lpDDSPrimary != NULL ) {
			// gibt die Oberfl�che frei
			lpDDSPrimary->Release();
			lpDDSPrimary = NULL;
		}
		// gibt das IDirectDraw object frei
		lpDD->Release();
		lpDD = NULL;
	}
	// Weltitem-Liste wieder freigeben: samt Inhalt
	for (int i=0; i<worlditemlist.size(); i++)
	{
		worlditemlist[i].DO->~Drawingobject();
		worlditemlist[i].pcxtextur->~PCXClass();
	}
	if (!worlditemlist.empty())	worlditemlist.clear();
	Camera_instance.~Camera();		// Betrachter wieder freigeben
}

/* Fensterbehandlungsroutine f�r das Hauptfenster. Hier werden die von 
  Windows gesendeten Nachrichten ausgewertet. Wichtige Nachrichten sind z.B. */
long FAR PASCAL WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch( message ) 
	{
		case WM_ACTIVATEAPP:
			bActive = wParam;
			// Check/restore the primary surface
			if( lpDDSPrimary )
				if( lpDDSPrimary->IsLost() )
					lpDDSPrimary->Restore();
			// Check/restore the back buffer
			if( lpDDSBack )
				if( lpDDSBack->IsLost() )
					lpDDSBack->Restore();
		break;
		case WM_CREATE:
			ShowCursor ( FALSE ); 
		break;
		case WM_COMMAND:	// Positionierungen des Betrachters in Welt
			switch(LOWORD(wParam))
			{
				case IDM_QUIT:
					PostMessage(hWnd, WM_CLOSE, 0, 0);
				break;
				case IDM_ABOUT:
					// kleines Testfenster
				break;
				case IDM_LEFT:
					Camera_instance.addrotate(0.0f, 0.017f,0.0f);
				break;
				case IDM_RIGHT:
					Camera_instance.addrotate(0.0f,-0.017f,0.0f);
				break;
				case IDM_FORWARD:
					Camera_instance.addtranslate(0.0f,0.0f,-0.5f);
				break;
				case IDM_BACKWARD:
					Camera_instance.addtranslate(0.0f,0.0f,0.5f);
				break;
				case IDM_UP:
					Camera_instance.addtranslate(0.0f,-0.5f,0.0f);
				break;
				case IDM_DOWN:
					Camera_instance.addtranslate(0.0f,0.5f,0.0f);
				break;
				case IDM_STEPLEFT:
					Camera_instance.addtranslate(0.5f,0.0f,0.0f);
				break;
				case IDM_STEPRIGHT:
					Camera_instance.addtranslate(-0.5f,0.0f,0.0f);
				break;
				case IDM_LOOKUP:
					Camera_instance.addrotate(-0.017f,0.0f,0.0f);
				break;
				case IDM_LOOKDOWN:
					Camera_instance.addrotate(0.017f,0.0f,0.0f);
				break;
				case IDM_CENTERVIEW:
					Camera_instance.setcenterview();
				break;
				case IDM_ZOOMIN:
					Camera_instance.addscale(0.9f);
				break;
				case IDM_ZOOMOUT:
					Camera_instance.addscale(1.1f);
				break;
				case IDM_FASTLEFT:
					Camera_instance.addrotate(0.0f,0.034f,0.0f);
				break;
				case IDM_FASTRIGHT:
					Camera_instance.addrotate(0.0f,-0.034f,0.0f);
				break;
				case IDM_FASTFORWARD:
					Camera_instance.addtranslate(0.0f,0.0f,-1.0f);
				break;
				case IDM_FASTBACKWARD:
					Camera_instance.addtranslate(0.0f,0.0f,1.0f);;
				break;
				case IDM_FASTSTEPLEFT:
					Camera_instance.addtranslate(-1.0f,0.0f,0.0f);
				break;
				case IDM_FASTSTEPRIGHT:
					Camera_instance.addtranslate(1.0f,0.0f,0.0f);
				break;
				case IDM_STEPLEFTALT:// gleich mit FASTSTEPLEFT
					Camera_instance.addtranslate(-0.5f,0.0f,0.0f);
				break;
				case IDM_STEPRIGHTALT: // gleich mit FASTSTEPRIGHT
					Camera_instance.addtranslate(0.5f,0.0f,0.0f);
				break;

				// Tests auf verschiedene Modelle:
				case IDM_POINTMOD:
					POINTMODACTIVE = -POINTMODACTIVE;
				break;
				case IDM_WIREFRAME:
					WIREFRAMEACTIVE = -WIREFRAMEACTIVE;	
				break;
				case IDM_COLFILLEDMOD:
					COLFILLEDMODACTIVE	= -COLFILLEDMODACTIVE;
				break;
				case IDM_TEXTUREFILLEDMOD:
					TEXTUREFILLEDMODCTIVE = -TEXTUREFILLEDMODCTIVE;
				break;
				case IDM_CENTERMOD:
					CENTERMODACTIVE = -CENTERMODACTIVE;
				break;
			}
				case WM_KEYDOWN:
					switch( wParam ) 
					{
						case VK_ESCAPE:
						case VK_F12:
							PostMessage(hWnd, WM_CLOSE, 0, 0);
						break;
					}
				break;
				case WM_DESTROY:
					ShowCursor ( TRUE ); 
					finiObjects();
					PostQuitMessage( 0 );
				break;
			}

		return DefWindowProc(hWnd, message, wParam, lParam);
} 

// erzeugt Fenster + initialisiert Daten
static BOOL doInit( HINSTANCE hInstance, int nCmdShow )
{
	WNDCLASS            wc;
    DDSURFACEDESC       ddsd;
	DDSCAPS				ddscaps;
    HRESULT             ddrval;

	// Registrieren des Hauptfensters
    wc.style = 0;
    wc.lpfnWndProc = WindowProc;	
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;		
    wc.hInstance = hInstance;
    wc.hIcon = NULL;
    wc.hCursor = LoadCursor( NULL, IDC_ARROW );
    wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = "VerhaltenClassName";//"UndeathClassName";
    RegisterClass( &wc );
    hInst = hInstance; // Instanzzugriffsnummer in globale Variable speichern
	// Hauptfenster erzeugen (Parameter mal bitte in der Hilfe nachschauen)
    hMainWnd = CreateWindow( "VerhaltenClassName", "Verhalten in R�",
		WS_POPUP | WS_SYSMENU | WS_OVERLAPPED, CW_USEDEFAULT, CW_USEDEFAULT, 
		RESX, RESY, NULL, NULL, hInstance, NULL );
	// Probe, ob Fenster tats�chlich erstellt wurde
	if (!hMainWnd) { return FALSE; }

	// Fenster nun auch auf dem Desktop anzeigen
	ShowWindow(hMainWnd, nCmdShow);
	UpdateWindow(hMainWnd);

	// DirectDraw (version 1.0) interface holen und in "lpDD" abspeichern
	ddrval = DirectDrawCreate( NULL, &lpDD, NULL );
	if( ddrval != DD_OK ) 
	{
		goto error0;
	};

	// Bildmodus einstellen (hier: Vollbild - Achtung: Dadurch kein Debugging m�glich)
	ddrval = lpDD->SetCooperativeLevel(hMainWnd, DDSCL_EXCLUSIVE | DDSCL_FULLSCREEN );

	if( ddrval != DD_OK ) 
	{
		goto error0;
	};

	ddrval = lpDD->SetDisplayMode(RESX, RESY, DEPTHCOLOR);
	if( ddrval != DD_OK ) 
	{
		goto error0;
	};

	ZeroMemory(&ddsd, sizeof(ddsd));
	ddsd.dwSize         = sizeof(ddsd);
	ddsd.dwFlags        = DDSD_CAPS | DDSD_BACKBUFFERCOUNT;
	ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE | DDSCAPS_FLIP | DDSCAPS_COMPLEX;
	ddsd.dwBackBufferCount = 1;
	ddrval = lpDD->CreateSurface(&ddsd, &lpDDSPrimary, NULL);
	ddrval = lpDD->SetDisplayMode(RESX, RESY, DEPTHCOLOR);
	if( ddrval != DD_OK )
	{
		goto error0;
	};

	// Der Backbuffer wurde bereits im vorigen Aufruf mit erzeugt (=> BackBufferCount = 1)
	// Jetzt wird nur noch des Interface darauf vom DirectX geholt und in 
	// "lpDDSBack" gespeichert.
	
	ddscaps.dwCaps = DDSCAPS_BACKBUFFER;
	ddrval = lpDDSPrimary->GetAttachedSurface (&ddscaps, &lpDDSBack); // &lpDDSBack kein Zugriff!?
	if( ddrval != DD_OK )
	{
		goto error0;
	};

	// und noch ein paar Infos zum Bildspeicher (Breite, H�he und Byte pro Pixelzeile)
	ZeroMemory(&ddsd, sizeof(ddsd));
	ddsd.dwSize = sizeof(ddsd);
	ddrval = lpDDSPrimary->GetSurfaceDesc( &ddsd );
	if( ddrval != DD_OK ) 
	{
		goto error0;
	};
	dwHeight = ddsd.dwHeight;
	dwWidth = ddsd.dwWidth;
	lPitch = ddsd.lPitch;

	return TRUE;

error0:
	MessageBox( hMainWnd, "failure occured!", 0, MB_OK );
	finiObjects();
	DestroyWindow( hMainWnd );
	return FALSE;
}


// WINMAIN wird nach dem Laden der *.EXE von Windows aufgerufen.
int PASCAL WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,
			LPSTR lpCmdLine, int nCmdShow)
{
    MSG         msg;

	initworldlist();  // Initialisiere Weltitems

	// Initialisiere die Anwendung
    if( !doInit( hInstance, nCmdShow )) 
	{
		return FALSE;
    }

	// Laden des Accelerators aus den Resourcen
	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDR_ACCELERATORMAIN);

	while (1)
	{
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) {	break;	}
			if (!hMainWnd || !TranslateAccelerator(hMainWnd, hAccelTable, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}			
		}
		else
		{
			if (bActive)
			{
				if (!DrawFrame()) {
					finiObjects(); }
				else;
			}
			else
				WaitMessage ();
		}
	}
	return msg.wParam;
}