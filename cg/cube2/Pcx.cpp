#include "Pcx.hpp"
#include <stdlib.h>

int PCXClass::ReadPcxLine(FILE *fp, char * data) 
{
  int n = 0;
  do 
  {
	char c = fgetc(fp) & 0xff;
	if ((c & 0xc0) == 0xc0) 
	{
		short i = c & 0x3f;                           
		c = fgetc(fp);
		while (i--) 
		{
		   *data++ = c;
			n++;
		}
    }
    else 
	{
      *data++ = c;
      n++;
    }
  } while (n < pcxwidth);
  return n;
}

void PCXClass::UnpackPcxFile(FILE * fp, char * data) 
{
  for (int i = 0; i < pcxheight; i++) 
    ReadPcxLine(fp, data + i * pcxwidth);
}

PCXClass::PCXClass(char *pszFileName)
{
  pcxheight = 0;
  pcxwidth = 0;
  file = pszFileName;	
  errormsg = "";
  data=NULL;
  if ((fp = fopen(file, "rb")) == NULL)
   errormsg = "Fehler beim Öffnen der Datei!";

  if (errormsg == "")
  {
	if (fread((char *) &header, 1, sizeof(PCXHEAD), fp) != sizeof(PCXHEAD))
		errormsg = "Falsche PCX-Headergröße!";
  }

  if (header.manufacturer != 0x0a || header.version != 5)
    errormsg = "Nichtunterstützte PCX-Version!";

  if (header.bits_per_pixel == 1) pcxbits = header.colour_planes;
  else pcxbits = header.bits_per_pixel;

  if (pcxbits != 8) errormsg = "Fehler in Anzahl der Bits pro Pixel (bpp)";

/*  zZ Sinnlos:
  if (errormsg ==)	
  {
		if (! fseek(fp, -769L, SEEK_END)) 
		{
			if (fgetc(fp) != 0x0c || fread(pcxpalette, 1, 768, fp) != 768)
				return 0;
		}
		else return 0;
  }
  for (int i = 0; i < 768; i++) 
	pcxpalette[i] >>= 2;*/
  
  if (errormsg == "") 
  {
	  fseek(fp, 128L, SEEK_SET);
	  pcxwidth = (header.xmax - header.xmin) + 1;
	  pcxheight = (header.ymax - header.ymin) + 1;
	  pcxbytes = header.bytes_per_line;
	  (char *)data =  (char *)malloc(pcxwidth * pcxheight);
	  UnpackPcxFile(fp, data);
	  fclose(fp);
  }

}

PCXClass::~PCXClass(void)
{
	// andere Daten wieder freigeben...
}

char *PCXClass::loadPCX(void) 
{
  return (char *)data;
}

void PCXClass::copydata(char **ptr)
{
	(char *)*ptr = (char *)data;
}

char *PCXClass::geterrormsg(void)
{
	return errormsg;
}

void PCXClass::getheader(PCXHEAD *header_)
{
	*header_ = header;
}

unsigned short PCXClass::getwidth(void)
{
	return pcxwidth;
}

unsigned short PCXClass::getheight(void)
{
	return pcxheight;
}

unsigned short PCXClass::getbytes(void)
{
	return pcxbytes;
}

unsigned short PCXClass::getbits(void)
{
	return pcxbits;
}