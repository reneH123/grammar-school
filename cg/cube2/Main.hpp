#ifndef Maindef
#define Maindef

#include "primitiva.hpp"
#include "punkt.hpp"
#include "Pcx.hpp"

typedef struct tagWORLDITEM{	// Weltenelement
	VERTEX pos;
	char *FileNameVertexDefine;
	char *FileNameTrianglesDefine;
	char *FileNameTexturDefine;
	Drawingobject *DO;
	PCXClass *pcxtextur;
} WORLDITEM;


#include <vector>
using namespace std;

typedef vector<WORLDITEM> WORLDITEMlist;

// Darstellen der Modelle als: (">0" bedeutet aktiviert)
extern int POINTMODACTIVE			=  1;		// Punktmodell
extern int WIREFRAMEACTIVE			= -1;		// Drahtgittermodell
extern int COLFILLEDMODACTIVE		= -1;		// gefüllte Teilflächen
extern int TEXTUREFILLEDMODCTIVE	= -1;		// gefüllte Texturflächen
extern int CENTERMODACTIVE			= -1;		// Mittelpunkt des Konstrukts

#endif