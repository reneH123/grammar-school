#include "clip.hpp"
#include "graph.hpp"

void process_clip_minx( PIXEL &p1, const PIXEL &p2 )
{
  p1.y = (int)(-( float(p2.y-p1.y) / (p2.x-p1.x)*p1.x ) + p1.y);
  p1.x = 0;
}

void process_clip_miny( PIXEL &p1, const PIXEL &p2 )
{
  p1.x = (int)(-( float(p2.x-p1.x) / (p2.y-p1.y)*p1.y ) + p1.x);
  p1.y = 0;
}

void process_clip_maxx( PIXEL &p1, const PIXEL &p2 )
{
  p1.y = (int)(-( float(p2.y-p1.y) / (p2.x-p1.x)*(p1.x-RESX) ) + p1.y);
  p1.x = RESX;
}
void process_clip_maxy( PIXEL &p1, const PIXEL &p2 )
{
  p1.x = (int)(-( float(p2.x-p1.x) / (p2.y-p1.y)*(p1.y-RESY) ) + p1.x);
  p1.y = RESY;
}

unsigned clip_code( const PIXEL &p )
{
  unsigned res = 0;
  if( p.x<0 ) res |= 1;
  if( p.y<0 ) res |= 2;
  if( p.x>RESX ) res |= 4;
  if( p.y>RESY ) res |= 8;
  return res;
}

void draw_clipped(PIXEL *p1, PIXEL *p2, int &sign )
{
  unsigned
	 c1 = clip_code( *p1 ),
	 c2 = clip_code( *p2 );
  PIXEL _p1 = *p1, _p2 = *p2;
  if( (c1&c2)!=0 )										// zwischen beiden Punkten kein sichtbarer Ausschnitt
  {
	  sign++;											// beide nicht sichtbar
	  return;
  }
  if( (c1|c2)!=0 )
  {
	 if( (c1&1)!=0 ) process_clip_minx( _p1, _p2 );		
	 if( (c1&2)!=0 ) process_clip_miny( _p1, _p2 );
	 if( (c1&4)!=0 ) process_clip_maxx( _p1, _p2 );
	 if( (c1&8)!=0 ) process_clip_maxy( _p1, _p2 );

	 if( (c2&1)!=0 ) process_clip_minx( _p2, _p1 );
	 if( (c2&2)!=0 ) process_clip_miny( _p2, _p1 );
	 if( (c2&4)!=0 ) process_clip_maxx( _p2, _p1 );
	 if( (c2&8)!=0 ) process_clip_maxy( _p2, _p1 );
  }
  *p1 = _p1; *p2 = _p2;									// p1 und p2 nun ver�ndert
}