//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDR_ACCELERATORMAIN             101
#define IDM_CUBELEFT                    40001
#define IDM_LEFT                        40001
#define IDM_CUBERIGHT                   40002
#define IDM_RIGHT                       40002
#define IDM_CUBEUP                      40003
#define IDM_FORWARD                     40003
#define IDM_CUBEDOWN                    40004
#define IDM_BACKWARD                    40004
#define IDM_QUIT                        40005
#define IDM_CUBELR                      40006
#define IDM_CUBEDU                      40007
#define IDM_CUBEFAR                     40008
#define IDM_ZOOMOUT                     40008
#define IDM_CUBENEAR                    40009
#define IDM_ZOOMIN                      40009
#define IDM_CUBENF                      40010
#define IDM_ROTXAUF                     40011
#define IDM_LOOKUP                      40011
#define IDM_ROTXAB                      40012
#define IDM_LOOKDOWN                    40012
#define IDM_ROTYAUF                     40013
#define IDM_ROTYAB                      40014
#define IDM_ROTZAUF                     40015
#define IDM_ROTZAB                      40016
#define IDM_CUBEZOOMIN                  40017
#define IDM_CUBEZOOMOUT                 40018
#define IDM_UP                          40019
#define IDM_DOWN                        40020
#define IDM_STEPLEFT                    40021
#define IDM_STEPRIGHT                   40022
#define IDM_FASTFORWARD                 40023
#define IDM_FASTBACKWARD                40024
#define IDM_FASTLEFT                    40025
#define IDM_FASTRIGHT                   40026
#define IDM_FASTSTEPLEFT                40027
#define IDM_FASTSTEPRIGHT               40028
#define IDM_STEPLEFTALT                 40029
#define IDM_STEPRIGHTALT                40030
#define IDM_CENTERVIEW                  40031
#define IDM_ABOUT                       40032
#define IDM_WIREFRAME                   40033
#define IDM_POINTMOD                    40034
#define IDM_COLFILLEDMOD                40035
#define IDM_TEXTUREFILLEDMOD            40036
#define IDM_CENTERMOD                   40037

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40038
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
