#include "camera.hpp"

Camera::Camera(VERTEX *c)
{
	pos.x = c->x;
	pos.y = c->y;
	pos.z = c->z;
}

Camera::~Camera(void){}

void Camera::setcamera(const VERTEX *p)
{
	pos = *p;
}

void Camera::setcenterview(void)
{
	pos.y = 0.0f;
}

VERTEX Camera::getpos(void)
{
	return pos;
}

void Camera::addtranslate(float dx, float dy, float dz)
{
	VERTEX d = {dx,dy,dz};
	Matrix_instance.translate(&pos,&d);
}

void Camera::addrotate(float alpha, float beta, float gamma)
{
	Matrix_instance.rotatexaxis(&pos,alpha);
	Matrix_instance.rotateyaxis(&pos,beta);
	Matrix_instance.rotatezaxis(&pos,gamma);
}

void Camera::addscale(float s)
{
	Matrix_instance.scale(&pos,s);
}

float Camera::getDistance(VERTEX *p)
{
	return (p->z-pos.z);
}