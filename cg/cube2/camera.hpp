#ifndef Cameradef
#define Cameradef

#include "punkt.hpp"
#include "matrix.hpp"

class Camera
{
private:
	VERTEX pos;												//	globale Koordinate im WKS
	Matrix Matrix_instance;									// f�r Rotation/Skalierung/Translation
public:
	Camera(VERTEX *c);										//	Konstruktor
	~Camera(void);											//	Destruktor
	void setcamera(const VERTEX *p);						//	KameraPosition festlegen
	void setcenterview(void);								//  parallel zum Sichthorizont
	VERTEX getpos(void);									//	aktuelle KameraPosition
	float getDistance(VERTEX *p);							// Abstand
	void addtranslate(float dx, float dy, float dz);		// Kamera verschieben
	void addrotate(float alpha, float beta, float gamma);	// Kamera drehen
	void addscale(float s);									// Vergr��ern/Verkleinern
};

#endif