#ifndef __PCXDEF
#define __PCXDEF

#include <stdio.h>

typedef struct {
  char manufacturer;
  char version;
  char encoding;
  char bits_per_pixel;
  short int xmin, ymin;
  short int xmax, ymax;
  short int hres;
  short int vres;
  char palette[48];
  char reserved;
  char colour_planes;
  short int bytes_per_line;
  short int palette_type;
  char filles[58];
} PCXHEAD;

class PCXClass
{
	private:
		//unsigned char pcxpalette[768]; // noch keine Funktion übernehmend
		PCXHEAD header;				  // Headerinformationen	
		unsigned short pcxwidth, pcxheight, pcxbytes, pcxbits;	// wichtige Bildattribute
		FILE *fp;						// Zeiger auf interne Dateivariable
		char *file;						// Dateiname
		char *data;						// Zeiger auf PCX-Daten
		char *errormsg;					// Fehlermeldung

		void UnpackPcxFile(FILE * fp, char *data);		// gesamten Bildinhalt in data speichern
		int ReadPcxLine(FILE *fp, char *data);			// eine vollständige Zeile in data speichern
	public:
		PCXClass(char *pszFileName);  // Konstruktor
		~PCXClass(void);    		  // Destruktor

		char *loadPCX(void);
		void copydata(char **ptr);	  // gleiche wie loadPCX, nur als Variablenparameter		

		char *geterrormsg(void);	// zeigt Fehlernachricht während des Ladens an
		void getheader(PCXHEAD *header_);
		unsigned short getwidth(void);
		unsigned short getheight(void);
		unsigned short getbytes(void);
		unsigned short getbits(void);
};


#endif