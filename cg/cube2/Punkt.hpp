#ifndef __Punktdef
#define __Punktdef

typedef struct tagVERTEX{   // Punkt im 3D-Raum
	float x,y,z;
} VERTEX;

typedef struct tagPIXEL{	// Bildschirmkoordinatenangabe
	int x,y;
} PIXEL;

typedef struct tagTRIANGLE  // Struktur zur Verwaltung der Linien zwischen Punkten
{
	int p1,p2,p3;
} TRIANGLE;

typedef	struct tagRGBCOLOR {
	unsigned char	r,g,b;
} RGBCOLOR;

#endif