#ifndef Listdef
#define Listdef

#include <vector>
#include "punkt.hpp"

using namespace std;
typedef vector<VERTEX> VERTEXlist;
typedef	vector<TRIANGLE> TRIANGLElist;
typedef vector<PIXEL> PIXELlist;
typedef vector<int> intlist;

#endif