#ifndef MACROS
#define MACROS
// Konstanten:
#define NULL 0
#define ERRCODE 0xFFFFFFFF
#define USEDIRECTDRAW
// graphicmode-properties:
#define x320 320
#define y240 240
#define x400 400
#define y300 300
#define x512 512
#define y384 384
#define x640 640
#define y480 480
#define x800 800
#define y600 600
#define x1024 1024
#define y768 768
#define x1152 1152
#define y864 864
#define x1280 1280
#define y960 960
#define vidres 8            // possible video modes

#define BPP8 8
#define BPP16 16
#define BPP24 24
#define BPP32 32
// actual settings:
#define BPP BPP32			// pc  1024x768x32; laptop 640x480x32
#define RESX x640//x320//
#define RESY y480//y240//
#define MIDX RESX/2
#define MIDY RESY/2
#define INF 1e20            // infinite large number
// Macros:
#define pred(d,max) (((d==max)?0:d+1))	// Nachfolger
#define succ(d,max) (((d==0)?max:d-1))	// Vorg�nger
#define Sign(x)		((x<0)?0:1)			// Vorzeichen
#define Abs(x)		((x<0)?-x:x)		// Absolutbetrag einer Zahl
#define SQRT(a)		(sqrt(a))			// Wurzel
#define SQR(a)		(a*a)				// Quadrat
#define swapVal(a, b)\
		(tmp=a),\
		(a=b),\
		(b=tmp)
#define ZeroVector  (0,0,0)
#define INFVector	(INF,INF,INF)
#define FRUSTUMPLANES  4

#endif
