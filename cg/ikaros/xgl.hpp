#ifndef XGRAPHICLIBRARY
#define XGRAPHICLIBRARY

#include "glob.hpp"
#include "xcol.hpp"
#include "math2d.hpp"										 // pixel definitions
#include "DEFS"

class xglx
{
protected:
	struct vidmem{
		unsigned char *lpSurface;							 // pointer to video informations
		unsigned long height, width;						 // height and width of surface
		long pitch;											 // bytes in a pixel colum
		int bpp;
	};
	vidmem VideoMemory;										 // structure to manage Video-Memory and belonging surfaces	
	bool fpsactive;											 // managing "FramePerSecound"-menu
	bool windowed;											 // windowed mode or fullscreen mode ?
	RECT g_rcViewport;										 // position and size to blt from
	RECT g_rcScreen;										 // screen position for blt
	HDC hDC;												 // handle to device context
	HBRUSH hBrush;											 // handle to Brush -> Background (GDI)
	HPEN   hPen;											 // handle to Pen -> Text(GDI)
	void settingfailed(const char* errmsg)					 // message box with errmsg
	{
		MessageBox(NULL, errmsg, 0, MB_OK);
		finiObjects();
	}
	virtual void finiObjects(void){};						 // finite objects
public:
	xglx(){};												 // virtual constructor
	~xglx(){};												 // virtual destructor
	inline const void done(void){finiObjects();}			 // call intern: finiobjects
	inline const void checkfps(void){fpsactive=!fpsactive;}	 // swap bool(fpsACTIVITY)
	inline const bool checkedfps(void){return fpsactive;}	 // get wheter fps is checked in some menu
	inline const vidmem* GetVideoMemory(void){return &VideoMemory;}	// pointer to information of current registration
	inline const void ChangeMode(void){windowed = !windowed;}// Changes: (Fullscreen -> WindMode) or (W->FS)
	inline const bool WindowedMode(void){return windowed;}	 // is Windowed Mode set ?
	void init(const int rx_=0,const int ry_=0,const int c_=8,const bool wm_=true, const bool fpsact_=true) // set graphical properties at first
	{
		VideoMemory.width = rx_;
		VideoMemory.height= ry_;
		VideoMemory.bpp = c_;
		windowed = wm_;
		fpsactive = fpsact_;
	}
	void PutPix(const pixel& pxl,rgb& RGB)			         // PutPix: puts pixels with RGB-values
	{
		VideoMemory.lpSurface[(4*pxl.x) + (VideoMemory.pitch*pxl.y) + 0] = RGB.r;
		VideoMemory.lpSurface[(4*pxl.x) + (VideoMemory.pitch*pxl.y) + 1] = RGB.g;
		VideoMemory.lpSurface[(4*pxl.x) + (VideoMemory.pitch*pxl.y) + 2] = RGB.b;
	}
	void putpixel(int x,int y,long c)						 // simple putpixel
	{
//ACHTUNG: Bereichs�berpr�fung langsam: sicher ist sicher!
		if ((x>0)&&(x<(long)VideoMemory.width)&&(y>0)&&(y<(long)VideoMemory.height))
		{
	/*		// 4 -> (VideoMemory.bpp/8) ist f�r entsprechende Farbtiefe + Aufl�sung
			// c/2 halber Blau-Wert
			VideoMemory.lpSurface[(4*x) + ((int)VideoMemory.pitch*y) + 0] = (unsigned char)c;
			VideoMemory.lpSurface[(4*x) + ((int)VideoMemory.pitch*y) + 1] = (unsigned char)c;
			VideoMemory.lpSurface[(4*x) + ((int)VideoMemory.pitch*y) + 2] = (unsigned char)c; */
		}
	}
	void printmessage(int x,int y,char* string)				 // print message to screen
	{
		TextOut(hDC,x,y-20,string,strlen(string));
	}
	void ChangeCoop(HWND window, RECT g_rc)					 // Change cooperative level
	{
		releaseallobjects(window);							 // Release all objects that need to be re-created for the new device
		if (windowed)										 // In case we're coming from a fullscreen mode, restore the window size
		{
	        SetWindowPos(window,HWND_NOTOPMOST,g_rc.left,g_rc.top,(g_rc.right - g_rc.left),(g_rc.bottom - g_rc.top),SWP_SHOWWINDOW);
			setwindowedmode(window);
		}
		if (!windowed) setfullscreen(window);				 // Re-create the surfaces
	}
	void SetGDIProperties(int brushtype, int pentype)		 // set properties to draw in GDI
	{
		SetBkColor(hDC,RGB(0,0,255));
		SetTextColor(hDC,RGB(255,255,0));
		hBrush = (HBRUSH)SelectObject(hDC,GetStockObject(brushtype));
		hPen = (HPEN)SelectObject(hDC,GetStockObject(pentype));
		SelectObject(hDC,hPen);
		SelectObject(hDC,hBrush);
	}
	void MoveSurfAbs(const pixel &p)						 // move screen area to absolute value
	{
		g_rcScreen.left=p.x;
		g_rcScreen.right=p.x;
		g_rcScreen.top=p.y;
		g_rcScreen.bottom=p.y;
	}
	void MoveSurfRel(const pixel &p)						 // move screen area relativly by delta p
	{
		g_rcScreen.left+=p.x;
		g_rcScreen.right+=p.x;
		g_rcScreen.top+=p.y;
		g_rcScreen.bottom+=p.y;
	}
	virtual void refreshclient(void){};						 // refresh client area to draw
	virtual RECT* GetScreen(void){return &g_rcScreen;} 		 // get screen coordinates
	virtual RECT* GetView(void){return &g_rcViewport;}		 // get viewport coordinates
	virtual bool blt(void){return false;}					 // Blitting
	virtual bool flip(void){return false;}				     // PageFlipping
	virtual void detect(char* &platform,char* &version){};	 // Version der Graphikbibliothek
	virtual bool setfullscreen(HWND window){return false;}   // set fullscreen
	virtual bool setwindowedmode(HWND window){return false;} // set windowed mode -> GDI-influences
	virtual void restoreSurfaces(HWND window){};             // restore lost Surfaces
	virtual void releaseallobjects(HWND window){};			 // Release all DDraw objects we use
};


#ifdef DIRECTDRAW

#include <ddraw.h>
#include <windows.h>

class xddraw: public xglx
{
private:
	LPDIRECTDRAW            lpDD;               // DirectDraw object
	LPDIRECTDRAWSURFACE     lpDDSPrimary;       // DirectDraw primary surface
	LPDIRECTDRAWSURFACE     lpDDSBack;          // DirectDraw back surface
	typedef HRESULT(WINAPI * DIRECTDRAWCREATE) (GUID *, LPDIRECTDRAW *, IUnknown *);
	void GetDXVersion(LPDWORD pdwDXVersion, LPDWORD pdwDXPlatform)
	{
		HRESULT                 hr;
		HINSTANCE               DDHinst = 0;
		HINSTANCE               DIHinst = 0;
		LPDIRECTDRAW            pDDraw = 0;
		LPDIRECTDRAW2           pDDraw2 = 0;
		DIRECTDRAWCREATE        DirectDrawCreate = 0;
		OSVERSIONINFO           osVer;
		LPDIRECTDRAWSURFACE     pSurf = 0;
		LPDIRECTDRAWSURFACE3    pSurf3 = 0;
	//	LPDIRECTDRAWSURFACE4    pSurf4 = 0;
	    // First get the windows platform
	    osVer.dwOSVersionInfoSize = sizeof(osVer);
	    if (!GetVersionEx(&osVer))
	    {
			*pdwDXVersion = 0;
			*pdwDXPlatform = 0;
			return;
		}
		if (osVer.dwPlatformId == VER_PLATFORM_WIN32_NT)
		{
			*pdwDXPlatform = VER_PLATFORM_WIN32_NT;
			// NT is easy... NT 4.0 is DX2, 4.0 SP3 is DX3, 5.0 is DX5
			// and no DX on earlier versions.
			if (osVer.dwMajorVersion < 4)
			{
	            *pdwDXPlatform = 0; // No DX on NT3.51 or earlier
				return;
			}
			if (osVer.dwMajorVersion == 4)
			{
				// NT4 up to SP2 is DX2, and SP3 onwards is DX3, so we are at least DX2
				*pdwDXVersion = 0x200;
				// We're not supposed to be able to tell which SP we're on, so check for dinput
				// It must be NT4, DX2
				*pdwDXVersion = 0x300;  // DX3 on NT4 SP3 or higher
				return;
			}
			// Else it's NT5 or higher, and it's DX5a or higher:
			// Drop through to Win9x tests for a test of DDraw (DX6 or higher)
		}
		else
		{
	        // Not NT... must be Win9x
			*pdwDXPlatform = VER_PLATFORM_WIN32_WINDOWS;
		}
		// Now we know we are in Windows 9x (or maybe 3.1), so anything's possible.
		// First see if DDRAW.DLL even exists.
		DDHinst = LoadLibrary("DDRAW.DLL");
		if (DDHinst == 0)
		{
	        *pdwDXVersion = 0;
			*pdwDXPlatform = 0;
			FreeLibrary(DDHinst);
			return;
		}
		//  See if we can create the DirectDraw object.
		DirectDrawCreate = (DIRECTDRAWCREATE)GetProcAddress(DDHinst, "DirectDrawCreate");
		if (DirectDrawCreate == 0)
		{
	        *pdwDXVersion = 0;
			*pdwDXPlatform = 0;
			FreeLibrary(DDHinst);
			OutputDebugString("Couldn't LoadLibrary DDraw");
			return;
		}
		hr = DirectDrawCreate(NULL, &pDDraw, NULL);
		if (FAILED(hr))
		{
	        *pdwDXVersion = 0;
			*pdwDXPlatform = 0;
			FreeLibrary(DDHinst);
			OutputDebugString("Couldn't create DDraw");
			return;
		}
		//  So DirectDraw exists.  We are at least DX1.
		*pdwDXVersion = 0x100;
		//  Let's see if IID_IDirectDraw2 exists.
		hr = pDDraw->QueryInterface(IID_IDirectDraw2,(LPVOID*)&pDDraw2);
		if (FAILED(hr))
		{
	        // No IDirectDraw2 exists... must be DX1
			pDDraw->Release();
			FreeLibrary(DDHinst);
			OutputDebugString("Couldn't QI DDraw2");
			return;
		}
		// IDirectDraw2 exists. We must be at least DX2
		pDDraw2->Release();
		*pdwDXVersion = 0x200;
		// DirectInputCreate exists. That's enough to tell us that we are at least DX3
		*pdwDXVersion = 0x300;
		// Checks for 3a vs 3b?
		// We can tell if DX5 is present by checking for the existence of IDirectDrawSurface3.
		// First we need a surface to QI off of.
		DDSURFACEDESC desc;

		ZeroMemory(&desc, sizeof(desc));
		desc.dwSize = sizeof(desc);
		desc.dwFlags = DDSD_CAPS;
		desc.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;

	    hr = pDDraw->SetCooperativeLevel(NULL, DDSCL_NORMAL);
	    if (FAILED(hr))
	    {
			// Failure. This means DDraw isn't properly installed.
			pDDraw->Release();
			FreeLibrary(DDHinst);
			*pdwDXVersion = 0;
			OutputDebugString("Couldn't Set coop level");
			return;
		}
		hr = pDDraw->CreateSurface(&desc, &pSurf, NULL);
		if (FAILED(hr))
		{
	        // Failure. This means DDraw isn't properly installed.
			pDDraw->Release();
			FreeLibrary(DDHinst);
			*pdwDXVersion = 0;
			OutputDebugString("Couldn't CreateSurface");
			return;
		}
		// Try for the IDirectDrawSurface3 interface. If it works, we're on DX5 at least
		if (FAILED(pSurf->QueryInterface(IID_IDirectDrawSurface3, (LPVOID*)&pSurf3)))
		{
	        pDDraw->Release();
			FreeLibrary(DDHinst);
			return;
		}
//     // QI for IDirectDrawSurface3 succeeded. We must be at least DX5
//    *pdwDXVersion = 0x500;
    // Try for the IDirectDrawSurface4 interface. If it works, we're on DX6 at least
//    if (FAILED(pSurf->QueryInterface(IID_IDirectDrawSurface4, (LPVOID *) & pSurf4)))
//    {
//        pDDraw->Release();
//        FreeLibrary(DDHinst);
//        return;
//    } 
	    // QI for IDirectDrawSurface4 succeeded. We must be at least DX6
	    *pdwDXVersion = 0x600;
	    pSurf->Release();
	    pDDraw->Release();
	    FreeLibrary(DDHinst);
	    return;
	}
protected:
	virtual void finiObjects(void)							 // finite objects
	{
		if (lpDD != NULL) 
		{
			if(lpDDSPrimary != NULL)						 // release surfaces
			{
				lpDDSPrimary->Release();
				lpDDSPrimary = NULL;
			}
			lpDD->Release();								 // release "IDirectDraw"-object
			lpDD = NULL;
		}
	}

public:
	friend HRESULT CALLBACK ModeCallback(LPDDSURFACEDESC pdds, LPVOID lParam)
	{
		AppInfo->resolutions.push_back(restype(pdds->dwWidth,pdds->dwHeight,pdds->ddpfPixelFormat.dwRGBBitCount));
		return true;
	}
	xddraw(HWND window, bool windowed = true)				 // constructor
	{
		HRESULT             ddrval;
		LPDIRECTDRAW		pDD;							 // DD1 interface, used to get actual interface
		
		ddrval = DirectDrawCreate(NULL,&pDD,NULL);			 // DirectDraw (version 1.0) interface holen und in "pDD" abspeichern
		if( ddrval != DD_OK ) 
		{
			settingfailed("Error: Could not fetch lpDD!");
			exit(0);
		}
		ddrval = pDD->QueryInterface(IID_IDirectDraw,(LPVOID*)&lpDD);	// Fetch DirectDraw4 interface
		if( ddrval != DD_OK ) 
		{
			settingfailed("Error: QueryInterface FAILED!");
			exit(0);
		}
		lpDD->EnumDisplayModes(0,NULL,NULL,ModeCallback);	 // enumerates all possible modes
		
		restype res = AppInfo->resolutions.getlast()->data;  // highest available settings to graphic hardware supported
		init(res.x,res.y,res.bpp,windowed);					 // set screen properties

		char* platform;
		char* version;
		detect(platform,version);

		if (!windowed)
		{
			if (!setfullscreen(window))
			{
				settingfailed("Error: SetFullScreen irregular!");
				exit(0);
			}
		}
		else
		{
			if (!setwindowedmode(window))
			{
				settingfailed("Error: SetWindowedMode irregular!");
				exit(0);
			}
			// ... and now set lowest resolution
			res = AppInfo->resolutions.getroot()->data;
			AppInfo->width  = VideoMemory.width  = res.x;
			AppInfo->height = VideoMemory.height = res.y;
			AppInfo->bpp    = res.bpp; 
			if (!MoveWindow(window,0,0,AppInfo->width,AppInfo->height,TRUE))
			{
				settingfailed("Error: SetWindowedMode: Couldn't set RESXxRESY!");
			}
		}
	}
	~xddraw(void){done();}									 // destructor
	virtual bool blt(void)									 // Blitting
	{
		HRESULT				ddrval;
		DDSURFACEDESC       ddsd;
		DDBLTFX				ddbltfx;
		ZeroMemory (&ddbltfx, sizeof(ddbltfx));	// Bildschirm l�schen
		ddbltfx.dwSize = sizeof (ddbltfx);
		ddbltfx.dwFillColor = RGB( 0, 0, 0 );
		ddrval = lpDDSBack->Blt( NULL, NULL, NULL, DDBLT_COLORFILL | DDBLT_WAIT, &ddbltfx );
		// Zugriff auf Videospeicher
		ZeroMemory (&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof (ddsd);
		while ((ddrval = lpDDSBack->Lock( NULL, &ddsd, 0, NULL )) == DDERR_WASSTILLDRAWING);
		if (ddrval != DD_OK) return false;
		// Zeiger auf Bilddaten merken
		VideoMemory.lpSurface = (unsigned char*)ddsd.lpSurface;
		memset(VideoMemory.lpSurface, 0x00, ddsd.lPitch * ddsd.dwHeight);	// Frame l�schen (Farbe =0)
		VideoMemory.pitch = (long)ddsd.lPitch; // aktuelle Breite [Bytes]
		return true;  // All done
	}
	virtual bool flip(void)									 // Pageflipping
	{
		HRESULT				ddrval;
		ddrval = lpDDSBack->Unlock(VideoMemory.lpSurface); // Zugriff auf Videospeicher wieder freigeben
		if (ddrval != DD_OK) { return false; }
		while (1)				// Backbuffer mit Frontbuffer vertauschen
		{
			if (WindowedMode())								 // If we are in windowed mode, perform a blt.
		    {
				ddrval = lpDDSPrimary->Blt(GetScreen(),lpDDSBack,GetView(),DDBLT_WAIT,NULL);
			}
			else ddrval = lpDDSPrimary->Flip( NULL, 0 );
			if (ddrval == DD_OK) break;
			if (ddrval == DDERR_SURFACELOST) 
			{ 
				if (!lpDDSPrimary->Restore()) return false; 
			}
			if (ddrval != DDERR_WASSTILLDRAWING) { if (ddrval != DD_OK) { return false; } }
		}
		return true;  // All done
	}
	virtual void refreshclient(void)						 // redraw client area if usefull
	{
		HRESULT ddrval;
		while (true)
		{
			ddrval = lpDDSPrimary->Blt(&g_rcScreen,lpDDSBack,&g_rcViewport,DDBLT_WAIT,NULL);	// If we are in windowed mode, perform a blt.
			if (ddrval == DD_OK) break;
			if (ddrval == DDERR_SURFACELOST)
			{
				ddrval = lpDDSPrimary->Restore();
				if (ddrval != DD_OK ) break;
			}
			if (ddrval != DDERR_WASSTILLDRAWING) break;
		}
	}
	virtual void detect(char* &platform, char* &version)		 // get current DirectX version and platform
	{
		DWORD   dwDXVersion;
		DWORD   dwDXPlatform;

		GetDXVersion(&dwDXVersion, &dwDXPlatform);
		switch (dwDXPlatform)
		{
	        case VER_PLATFORM_WIN32_WINDOWS:
				platform = "Windows 9X";
			break;
			case VER_PLATFORM_WIN32_NT:
				platform = "Windows NT";
            break;
			default:
				platform = "Operating System Error!";
		}
		switch (dwDXVersion)
		{
	        case 0x000:
				version = "No DirectX installed";
            break;
			case 0x100:
				version = "DirectX 1";
            break;
			case 0x200:
				version = "DirectX 2";
            break;
			case 0x300:
				version = "DirectX 3";
            break;
			case 0x400:	
				version = "DirectX 4";
			break;
			case 0x500:
				version = "DirectX 5";
            break;
			case 0x600:
				version = "DirectX 6";
            break;
			default:
				version = "Unknown version of DirectX installed.";
		}
	}
	virtual bool setfullscreen(HWND window)					 // set fullscreen
	{
		DDSURFACEDESC       ddsd;
		DDSCAPS				ddscaps;
		HRESULT             ddrval;
		// Bildmodus einstellen (hier: Vollbild - Achtung: Dadurch kein Debugging m�glich)
		ddrval = lpDD->SetCooperativeLevel(window, DDSCL_FULLSCREEN | DDSCL_EXCLUSIVE );
		if( ddrval != DD_OK ) 
		{
			settingfailed("Error: SetCooperativeLevel !");
			return false; 
		};
		AppInfo->MatchRes((int&)VideoMemory.width,(int&)VideoMemory.height);
		ddrval = lpDD->SetDisplayMode(VideoMemory.width,VideoMemory.height,VideoMemory.bpp);
		if( ddrval != DD_OK ) 
		{ 
			settingfailed("Error: SetDisplayMode !");
			return false; 
		};
		// Get the dimensions of the viewport and screen bounds
    	// Store the rectangle which contains the renderer
    	SetRect(&g_rcViewport, 0, 0, AppInfo->width, AppInfo->height);
    	memcpy(&g_rcScreen, &g_rcViewport, sizeof(RECT));
		// Create the primary surface with 1 back buffer
		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize         = sizeof(ddsd);
		ddsd.dwFlags        = DDSD_CAPS | DDSD_BACKBUFFERCOUNT;
		ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE | DDSCAPS_FLIP | DDSCAPS_COMPLEX;	
		ddsd.dwBackBufferCount = 1;
		ddrval = lpDD->CreateSurface(&ddsd, &lpDDSPrimary, NULL);
		if( ddrval != DD_OK )
		{	
			settingfailed("Error: CreateSurface FAILED!");
			return false; 
		};
		// Der Backbuffer wurde bereits im vorigen Aufruf mit erzeugt (=> BackBufferCount = 1)
		// Jetzt wird nur noch des Interface darauf vom DirectX geholt und in 
		// "lpDDSBack" gespeichert.
		ddscaps.dwCaps = DDSCAPS_BACKBUFFER;
		ddrval = lpDDSPrimary->GetAttachedSurface (&ddscaps, &lpDDSBack);
		if( ddrval != DD_OK ) 
		{ 
			settingfailed("Error: GetAttachedSurface !"); 
			return false; 
		};
		return true;
	}
	virtual bool setwindowedmode(HWND window)				 // set windowed mode
	{
		HRESULT             ddrval;
		DDSURFACEDESC       ddsd;
		LPDIRECTDRAWCLIPPER pClipper;
		// Get normal windowed mode
		ddrval = lpDD->SetCooperativeLevel(window, DDSCL_NORMAL);
		if( ddrval != DD_OK )
		{
			settingfailed("Error: SetCooperativeLevel !");
			return false;
		}
		if (!MoveWindow(window,0,0,AppInfo->width,AppInfo->height,TRUE))
		{
			settingfailed("Error: SetWindowedMode: Couldn't set RESXxRESY!");
			return false;
		}
		AppInfo->width = VideoMemory.width;
		AppInfo->height= VideoMemory.height;
		// Get the dimensions of the viewport and screen bounds
		GetClientRect(window, &g_rcViewport);
		GetClientRect(window, &g_rcScreen);
		ClientToScreen(window, (POINT*)&g_rcScreen.left);
		ClientToScreen(window, (POINT*)&g_rcScreen.right);
		// Create the primary surface
		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.dwFlags = DDSD_CAPS;
		ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;
		ddrval = lpDD->CreateSurface(&ddsd, &lpDDSPrimary, NULL);
		if( ddrval != DD_OK )
		{
			settingfailed("Error: SetCooperativeLevel !");
			return false;
		}
		// Create a clipper object since this is for a Windowed render
		ddrval = lpDD->CreateClipper(0, &pClipper, NULL);
		if( ddrval != DD_OK )
		{
			settingfailed("CreateClipper FAILED");
			return false;
		}
		// Associate the clipper with the window
		pClipper->SetHWnd(0, window);
		lpDDSPrimary->SetClipper(pClipper);
		pClipper->Release();
		pClipper = NULL;
		// Get the backbuffer. For fullscreen mode, the backbuffer was created
		// along with the primary, but windowed mode still needs to create one.
		ddsd.dwFlags        = DDSD_WIDTH | DDSD_HEIGHT | DDSD_CAPS;
		ddsd.dwWidth        = AppInfo->width;
		ddsd.dwHeight       = AppInfo->height;
		ddsd.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN;
		ddrval = lpDD->CreateSurface(&ddsd, &lpDDSBack, NULL);
		if( ddrval != DD_OK )
		{
			settingfailed("CreateSurface2 FAILED");
			return false;
		}
		VideoMemory.height = ddsd.dwHeight;
		VideoMemory.width = ddsd.dwWidth;
		VideoMemory.pitch = ddsd.lPitch;
		return true;
	}
	virtual void restoreSurfaces(HWND window)				 // restore surfaces
	{
		if (lpDDSPrimary)									 // Check/restore the primary surface
			if( lpDDSPrimary->IsLost() )
				lpDDSPrimary->Restore();
		if (lpDDSBack)										 // Check/restore the back buffer
			if( lpDDSBack->IsLost() )
				lpDDSBack->Restore();
	}
	virtual void releaseallobjects(HWND window)				 // release front and backbuffer - objects
	{
		if (lpDD != NULL)
		{
			lpDD->SetCooperativeLevel(window,DDSCL_NORMAL);
			if (lpDDSBack != NULL)
			{
	            lpDDSBack->Release();
				lpDDSBack = NULL;
			}
			if (lpDDSPrimary != NULL)
			{
	            lpDDSPrimary->Release();
		        lpDDSPrimary = NULL;
	        }
	    }
	}
};
extern xddraw* DDraw;
#endif


#ifdef OPENGL
#include <gl/gl.h>

class xgl: public xglx
{
private:
	HGLRC hRC;												 // OpenGL handle: graphical surface
	HWND hWnd;												 // handle to actual window
protected:
	virtual void finiObjects(void)							 // finite objects
	{
		wglMakeCurrent(NULL,NULL);
		wglDeleteContext(hRC);
		ReleaseDC(hWnd,hDC);
	}
public:
	xgl(HWND window, bool windowed = true)					 // constructor
	{
		hWnd = window;
		init();
	}
	~xgl(void){done();}										 // destructor
	virtual bool setfullscreen(HWND window)
	{
		if (window!=NULL) { hWnd = window; }
		return false;
	}
	virtual bool setwindowedmode(HWND window)
	{
		if (window!=NULL) {hWnd = window;}
		//build pixelformat - nessessary for opengl in window
		PIXELFORMATDESCRIPTOR pfd;
		int iFormat;
		// get the device context (DC)
		hDC = GetDC(hWnd);
		// set the pixel format for the DC
		ZeroMemory(&pfd, sizeof(pfd));
		pfd.nSize = sizeof(pfd);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 24;
		pfd.cDepthBits = 32;
		pfd.iLayerType = PFD_MAIN_PLANE;
		iFormat = ChoosePixelFormat(hDC,&pfd);
		if (!iFormat) { return 0; }
		SetPixelFormat(hDC,iFormat,&pfd);
		// create and enable the render context (RC)
		hRC = wglCreateContext(hDC);
		wglMakeCurrent(hDC,hRC);
		return true;
	}
	virtual void detect(char* &platform,char* &version){};	 // detect GL version/platform
	virtual bool blt(void)					 // Blitting
	{
		float theta=1.0;
		glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
		glClear( GL_COLOR_BUFFER_BIT );
		glPushMatrix();
		glRotatef( 50*theta, 0.0f, 0.0f, 1.0f );
		glBegin( GL_TRIANGLES );
		glColor3f( 1.0f, 0.0f, 0.0f ); glVertex2f( 0.0f, 1.0f );
		glColor3f( 0.0f, 1.0f, 0.0f ); glVertex2f( 0.87f, -0.5f );
		glColor3f( 0.0f, 0.0f, 1.0f ); glVertex2f( -0.87f, -0.5f );
		glEnd();
		glRotatef( -20*theta, 0.0f, 0.0f, 1.0f );
		glBegin( GL_QUADS);
		glColor3f(1.0f, 0.0f, 1.0f );
		glVertex3f(0.0f,100.0f,0.0f);
		glColor3f( 0.0f, 1.0f, 0.0f );
		glVertex3f(100.0f,100.0f,0.0f);
		glColor3f( 0.0f, 0.0f, 0.0f );
		glVertex3f(100.0f,0.0f,0.0f);
		glColor3f( 0.0f, 0.0f, 1.0f );
		glVertex3f(0.0f,0.0f,-1.0f);
		glEnd();
		glPopMatrix();
		return true;
	}
	virtual bool flip(void){return false;}					 // Pageflipping
	virtual void restoreSurfaces(HWND window){};			 // restore graphical surfaces
	virtual void releaseallobjects(HWND window){};			 // release front and back-buffer
	virtual void refreshclient(void){};						 // refresh client area
};
extern xgl* OpenGL;
#endif

#endif