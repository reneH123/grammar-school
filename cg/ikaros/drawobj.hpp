#ifndef DRAWINGOBJECT
#define DRAWINGOBJECT

#include "math2d.hpp"
#include "math3d.hpp"
#include "list.hpp"
#include "putp.hpp"

// Auflistung aller definierter Klassen
class drawingobject;
class drawingobject2d;
class drawingobject3d;
class point2d;
class point3d;
class line3d;
class plain;
class lineoids;
class linehor;
class linever;
class line;
class rectangle;
class polygon2d;
class polygon3d;
class polyeder;
class model;


// weitere geometrische Objekte: Kreis, Ellipse; Kugel, Perle
class drawingobject 
{
protected:
	int color;
	virtual void Clip(void){};						// clip itself
	virtual bool Visible(void){return false;}		// is object visible?
	virtual unsigned code(void){return 0;}			// in which section is object?
public:
	drawingobject(int c=0)							// constructor
	{
		color = c;
	}						
	~drawingobject(void){};							// destructor
	virtual void Show(void){}; 						// show object
	void Hide(void)									// hide object
	{
		int c = color;
		color = 0;
		Show();
		color = c;
	}
	virtual drawingobject* copy(void)				// pointer to actual object
	{
		return this;								// Vielleicht: ? return (point2d*)new(p,color);
	}
	virtual void Move(void){};						// move object
	virtual void Rotate(void){};					// rotate object
	virtual void Scale(void){};						// scale object
};

typedef tlist<drawingobject*> drawingobjects;		// list of drawingobjects

class drawingobject2d: public drawingobject			// base class
{
protected:
	pixel p;
	clipwindow_tag cwindow;							// clipwindow-area
	virtual unsigned code(const pixel& p)					// ClipCode f�r Punktposition nach Cohen
	{
		unsigned res = 0;
		if (p.x<0) res |= 1;
		if (p.y<0) res |= 2;
		if (p.x>cwindow.resx) res |= 4;
		if (p.y>cwindow.resy) res |= 8;
		return res;
	}
	void minx(pixel p1,const pixel& p2)				// MinX clipping ( plane : X = 0    )
	{
		p1.y = (int)(-( float(p2.y-p1.y) / (p2.x-p1.x)*p1.x ) + p1.y);
		p1.x = 0;
	}
	void miny(pixel& p1,const pixel& p2 )			// MinY clipping ( plane : Y = 0    )
	{
		p1.x = (int)(-( float(p2.x-p1.x) / (p2.y-p1.y)*p1.y ) + p1.x);
		p1.y = 0;
	}
	void maxx(pixel& p1,const pixel& p2)			// MaxX clipping ( plane : X = ResX )
	{
		p1.y = (int)(-( float(p2.y-p1.y) / (p2.x-p1.x)*(p1.x-cwindow.resx) ) + p1.y);
		p1.x = cwindow.resx;
	}
	void maxy(pixel& p1,const pixel& p2)			// MaxY clipping ( plane : Y = ResY )
	{
		p1.x = (int)(-( float(p2.x-p1.x) / (p2.y-p1.y)*(p1.y-cwindow.resy) ) + p1.x);
		p1.y = cwindow.resy;
	}
	virtual bool Visible(void)						// is object visible?
	{
		return (Visible(p));
	}
	virtual bool Visible(pixel& pxl)				// is object visible?
	{
		return ((pxl.x>0)&(pxl.x<cwindow.resx)&(pxl.y>0)&(pxl.y<cwindow.resy)?true:false);
	}
public:
	drawingobject2d(const clipwindow_tag& cw,const pixel& P,const int icol=0)// constructor
	{
		cwindow = cw;
		color = icol;
		p = P;
	}
	drawingobject2d(void)							// parameter less constructor
	{
		cwindow.resx = AppInfo->width;//0;
		cwindow.resy = AppInfo->height;//0;
		drawingobject();
		p = pixel(0,0);
	}
    ~drawingobject2d(void) {};						// destructor
    void Move(const pixel& divp)					// move object
	{
		p+=divp;
	}
	virtual void Rotate(void)						// rotate object
	{
		// noch zu implementieren!: 2D-Rotation.
	}
	virtual void Scale(const float s)				// scale object
	{
		p*=s;
	}
	virtual pixel get(void)							// get object
	{
		return p;
	}
};

class drawingobject3d: public drawingobject  		// base class
{
protected:
	frustum Frustum;  // general frustum
	proj P;
	vertex v;
	virtual bool Visible(void)						// is vertex in frustum
	{
		return (Visible(v)); 
	}
	virtual bool Visible(vertex& vrtx)
	{
		return Frustum.inside(vrtx);
	}
/*	virtual unsigned code(const vertex& p) // �berfl�ssig -> frustum
	{
		// in 9 sections
		return 0;
	} */
public:
	drawingobject3d(const vertex& V=ZeroVector,const int icol=0)// constructor
	{
		Frustum.setup(1);  // initializes all planes in frustum
		P(vertex(ZeroVector));
		color = icol;
		v = V;
	}
	~drawingobject3d(void){};						// destructor
	virtual void Move(vertex& divv)					// move object
	{
		v+=divv;
	}
	virtual void Rotate(const float alpha, const float beta, const float gamma)						// rotate object
	{
		// X-axis & Y-axis & Z-axis rotation
		matrix rotX = matrix(vertex(1.0f,0.0f,0.0f),vertex(0.0f,cos(alpha),sin(alpha)),vertex(0.0f,-sin(alpha),cos(alpha))),
			   rotY = matrix(vertex(cos(beta),0.0f,-sin(beta)),vertex(0.0f,1.0f,0.0f),vertex(sin(beta),0.0f,cos(beta))),
			   rotZ = matrix(vertex(cos(gamma),sin(gamma),0.0f),vertex(-sin(gamma),cos(gamma),0.0f),vertex(0.0f,0.0f,1.0f)); 
		matrix Rot = rotX*rotY*rotZ;
		v=Rot*v;
	}
	virtual void Scale(const float s)
	{
		v*=s;
	}
	virtual vertex get(void)						// get object
	{
		return v;
	}
};

class point2d: public drawingobject2d				// 3D-Point in coordinate system
{
protected:
	virtual bool Visible(void)						// is object visible?
	{
		return (Visible(p));
	}
	virtual bool Visible(pixel& pxl)				// is object visible?
	{
		return (drawingobject2d::Visible(pxl)?true:false);
	}
public:
	point2d(const pixel& P,const int newcol)		// constructor
	{
		p = P;
		color = newcol;
	}
	point2d(void)									// constructor
	{
		p = pixel(0,0);
		color = 0;
	}
	~point2d(void){};								// destructor
	virtual void Show(void)							// putpixel (= set point)
	{
		if (Visible())
		{
			putpixel(p.x,p.y,color);
		}
	}
};

class lineoids: public point2d		// alle Linien�hnliche-Oberklasse
{
protected:
	pixel q;						// second point of a line (-definition)
	virtual void Clip(void)			// clip itself (Cohen&Sutherland)
	{
		pixel p1 = p,
			  p2 = q;
		unsigned c1 = code(p1),
				 c2 = code(p2);
		pixel _p1 = p1, _p2 = p2;
		if( (c1&c2)!=0 ) return;								// zwischen beiden Punkten kein sichtbarer Ausschnitt
		if( (c1|c2)!=0 )
		{
			if ((c1&1)!=0) minx(_p1,_p2);
			if ((c1&2)!=0) miny(_p1,_p2);
			if ((c1&4)!=0) maxx(_p1,_p2);
			if ((c1&8)!=0) maxy(_p1,_p2);
		    if ((c2&1)!=0) minx(_p2,_p1);
			if ((c2&2)!=0) miny(_p2,_p1);
			if ((c2&4)!=0) maxx(_p2,_p1);
			if ((c2&8)!=0) maxy(_p2,_p1);
		}
		p = _p1;
		q = _p2;
	}
public:
	lineoids(const pixel& A,const pixel& B,const int f)	// constructor
	{
		p = A;
		q = B;
		color = f;
	}
	lineoids(void)					// parameterless constructor
	{
		color = 0;
	}
	~lineoids(void){};				// destructor
	virtual void operator()(const pixel& a,const pixel& b)	// set new coords of points
	{
		p = a;
		q = b;
	}
	virtual void Show(void){};		    // virtual definition for showing a line in derived classes
};

class linehor: public lineoids		// horizontale line
{
public:
	linehor(const pixel& A,const int& BX,const int f)
	{
		p = A;
		q = pixel(BX,p.y);
		color = f;
	}
	void operator()(const pixel& A,const int& BX,const int f)	// "set"-operator
	{
		p = A;
		q = pixel(BX,p.y);
		color = f;
	}
	linehor(void)
	{
		lineoids();
	}
	~linehor(void){};			// destructor
	void Show(void)				// draw horizontale line
	{
		bool a = Visible(), b = Visible(q);
		if (!(a|b)) return; // both points are out
		if (!(a&b)) Clip(); // one is out one is in -> clip
		// no other possibility -> draw it now!:
		if ((q.x-p.x)<0) q.swap2(&p);
		int x=p.x;
		for (;x<=q.x;x++,putpixel(x,p.y,color)){}
	}
};

class linever: public lineoids		// verticale line
{
public:
	linever(const pixel& A,const int& BY,const int f)	// constructor
	{
		p = A;
		q = pixel(p.x,BY);
		color = f;
	}
	void operator()(const pixel& A,const int& BY,const int f)	// "set"-operator
	{
		p = A;
		q = pixel(p.x,BY);
		color = f;
	}
	linever(void)
	{
		lineoids();
	}
	~linever(void){};			// destructor
	void Show(void)				// draw verticale line
	{
		bool a = Visible(), b = Visible(q);
		if (!(a|b)) return; // both points are out
		if (!(a&b)) Clip(); // one is out one is in -> clip
		// draw it:
		if ((q.y-p.y) < 0) q.swap2(&p);
		int y=p.y;
		for (;y<=q.y;y++,putpixel(p.x,y,color))	{}
	}
};

class line: public lineoids							// fundamental for wirefrime graphics
{
public:
	line(const pixel& A,const pixel& B,const int f)	// constructor
	{
		p = A;
		q = B;
		color = f;
	}
	line(void)							// constructor without parameters
	{
		lineoids();
	}
	~line(void){};						// destructor
	void Show(void)						// drawing line (Bresenham)
	{
		bool a = Visible(), b = Visible(q);
		if (!(a|b)) return; // both points are out
		if (!(a&b)) Clip(); // one is out one is in -> clip
		// draw line
		int error,x,y,dx,dy,xincr,yincr;
		dx = q.x-p.x; dy = q.y-p.y;
		if (abs(dx) > abs(dy))
		{
			if (dx<0)
			{
				p.swap2(&q);
				dx = q.x-p.x;
			}
			if (q.y>p.y) yincr = 1;
			else yincr = -1;
			error = -abs(dx)/2;
			for (x=p.x,y=p.y; x<=q.x; x++)
			{
				putpixel(x,y,color);
				error += abs(dy);
				if (error>=0)
				{
					y+=yincr;
					error-=dx;
				}
			}
		}
		else
		{
			if (dy<0)
			{
				q.swap2(&p);
				dy = q.y-p.y;
			}
			if (q.x>p.x) xincr = 1;
			else xincr = -1;
			error = -abs(dy)/2;
			for (y=p.y,x=p.x; y<=q.y; y++)  
			{
				putpixel(x,y,color);
				error+=abs(dx);
				if (error>=0)
				{
					x+=xincr;
					error-=dy;
				}
			}
		} 
	}
};

class rectangle: public point2d
{
private:
	pixel q;
public:
	rectangle(const pixel& A,const pixel& B,const int f)	// constructor
	{
		p = A;
		q = B;
		color = f;
	}
	rectangle(void)					// parameterless constructor
	{
		color = 0;
	}
	~rectangle(void) {}				// destructor
    virtual void Show(void) 
	{
		linehor lhor(p,q.x,color);
		lhor.Show();
		lhor(pixel(p.x,q.y),q.x,color);
		lhor.Show();
		linever lver(p,q.y,color);
		lver.Show();
		lver(pixel(q.x,p.y),q.y,color);
		lver.Show();
	}
};

class polygon2d: public point2d
{
protected:
	pixels polygon;				// pointlist = polygon
	virtual bool Visible(void)	// is polygon visible: check all points are inside?
	{
		pixelPTR root = polygon.getroot();
		for (;root;root=root->nxt)
		{
			if (!point2d::Visible(root->data)) return false;
		}
		return true;
	}
	virtual void Clip(void)		// clip polygon (Sutherland&Hodgeman)
	{
		pixels* res = new pixels;	// Zwischenresultatspolygon
		res->setroot(polygon.getroot());
		pixelPTR begin; 
		// 1. MaxX clipping ( plane : X = ResX )
		begin = res->getroot();
		polygon.setroot(begin);
		res->setroot(NULL);
		res->setlast(NULL);
		while (begin)						
		{
			pixel p1 = begin->data,								// Punkte f�r Linie laden
				  p2 = ((begin->nxt)?begin->nxt->data:polygon.getroot()->data); 
			unsigned c1 = code(p1),						// deren Statute
					 c2 = code(p2);				
			if (((c1&4)!=4)&((c2&4)!=4)) res->push_back(p2);	// IN ->IN;  save the last one.
			else if	(((c1&4)!=4)&((c2&4)==4))					// IN ->OUT; save the intersect point
			{
				pixel newp1 = p1;
				maxx(newp1,p2);
				res->push_back(newp1);
			}
			else if (((c1&4)==4)&((c2&4)!=4))					// OUT->IN;  save the intersect point + and the vertex that is in
			{
				pixel newp1 = p1;
				maxx(newp1,p2);
				res->push_back(newp1);
				res->push_back(p2);
			}
			begin = begin->nxt;
		}
		// 2. MinY clipping ( plane : Y = 0    )
		begin = res->getroot();
		polygon.setroot(begin);
		res->setroot(NULL);
		res->setlast(NULL);
		while (begin)						
		{
			pixel p1 = begin->data,								// Punkte f�r Linie laden
				  p2 = ((begin->nxt)?begin->nxt->data:polygon.getroot()->data); 
			unsigned c1 = code(p1),						// deren Statute
					 c2 = code(p2);				
			if (((c1&2)!=2)&((c2&2)!=2)) res->push_back(p2);	// IN ->IN;  save the last one.
			else if	(((c1&2)!=2)&((c2&2)==2))					// IN ->OUT; save the intersect point
			{
				pixel newp1 = p1;
				miny(newp1,p2);
				res->push_back(newp1);
			}	
			else if (((c1&2)==2)&((c2&2)!=2))					// OUT->IN;  save the intersect point + and the vertex that is in
			{
				pixel newp1 = p1;
				miny(newp1,p2);
				res->push_back(newp1);
				res->push_back(p2);
			}
			begin = begin->nxt;
		}
		// 3. MinX clipping ( plane : X = 0    )
		begin = res->getroot();
		polygon.setroot(begin);
		res->setroot(NULL);
		res->setlast(NULL);
		while (begin)						
		{
			pixel p1 = begin->data,								// Punkte f�r Linie laden
		 		  p2 = ((begin->nxt)?begin->nxt->data:polygon.getroot()->data); 
			unsigned c1 = code(p1),						// deren Statute
					 c2 = code(p2);				
			if (((c1&1)!=1)&((c2&1)!=1)) res->push_back(p2);	// IN ->IN;  save the last one.
			else if	(((c1&1)!=1)&((c2&1)==1))					// IN ->OUT; save the intersect point
			{
				pixel newp1 = p1;
				minx(newp1,p2);
				res->push_back(newp1);
			}
			else if (((c1&1)==1)&((c2&1)!=1))					// OUT->IN;  save the intersect point + and the vertex that is in
			{
				pixel newp1 = p1;
				minx(newp1,p2);
				res->push_back(newp1);
				res->push_back(p2);
			}
			begin = begin->nxt;
		}
		// 4. MaxY clipping ( plane : Y = ResY )
		begin = res->getroot();
		polygon.setroot(begin);
		res->setroot(NULL);
		res->setlast(NULL);
		while (begin)						
		{
			pixel p1 = begin->data,								// Punkte f�r Linie laden
				  p2 = ((begin->nxt)?begin->nxt->data:polygon.getroot()->data); 
			unsigned c1 = code(p1),						// deren Statute
					 c2 = code(p2);				
			if (((c1&8)!=8)&((c2&8)!=8)) res->push_back(p2);	// IN ->IN;  save the last one.
			else if	(((c1&8)!=8)&((c2&8)==8))					// IN ->OUT; save the intersect point
			{
				pixel newp1 = p1;
				maxy(newp1,p2);
				res->push_back(newp1);
			}
			else if (((c1&8)==8)&((c2&8)!=8))					// OUT->IN;  save the intersect point + and the vertex that is in
			{
				pixel newp1 = p1;
				maxy(newp1,p2);
				res->push_back(newp1);
				res->push_back(p2);
			}
			begin = begin->nxt;
		}
		polygon.setroot(res->getroot()); // liefere geclipptes Polygon zur�ck
	}
public:
	polygon2d(pixelPTR P,const int f,bool copylist=false)	// constructor
	{
		if (copylist) // have all element been copied? (for internal save: real data instead of pointer -> slow)
		{
			// alle Elemente der Liste abkopieren -> polygon
			polygon.setroot(NULL);	// initialisiere "polygon"
			for (;P;P=P->nxt)
			{
				polygon.push_back(P->data);
			}
		}
		else polygon.setroot(P);
		color = f;
	}
	polygon2d(void)							// parameter less constructor
	{
		polygon.setroot(NULL);
		color = 0;
	}
	~polygon2d(void){};						// destructor
	virtual void Show(void)					// show 2d polygon
	{
		if (!Visible()) Clip();				// means: (1) polygon has to be clipped or (2) polygon is fully outside
// 2Optimize: if 2 -> return;
		linehor lhor;						// Aufruf des erweiterten Konstruktors: color=0
		pixelPTR beginpxl = polygon.getroot();
		pixel ymax = beginpxl->data, ymin = ymax;
		tlist<tlist<int>*> pixeledgelist;	// Liste der Polygonkanten: Werte f�r je ein X-Wert
		tlist<int> *newpxl;					// nur tempor�r
		int yminindex = 0;
		// Bestimme max Y / min Y + Erstelle Kantenliste
		int i = 0;							// tempor�re Variable nur durch zum Z�hlen der momentane Position in Liste
		while (beginpxl!=NULL)
		{
			if ((*beginpxl).data.y > ymax.y) ymax = (*beginpxl).data;					 // Y Max
			if ((*beginpxl).data.y < ymin.y) { yminindex = i; ymin = (*beginpxl).data; } // Y Min
			pixel point1, point2;
			if ((*beginpxl).nxt!=NULL) { point1 = (*beginpxl).data;	point2 = (*beginpxl).nxt->data; }
			else { point1 = (*beginpxl).data;	point2 = polygon.getroot()->data; }				 // Linie zwischen letztem und erstem Punkt
			// Start: Berechnen aller X-Werte einer Kante
			int divY = point2.y-point1.y, divX = point2.x-point1.x;
			float m = (float)(divY)/(divX); // Achtung div 0 m�glich! (nicht wirklich ;) )
			m = ((m>0.0f)?m:-m);
	        divY = ((divY<0)?-divY:divY);
			newpxl = new tlist<int>;
			for (int y=0; y<(divY+1); y++)
			{
				int newpixelpos = (divX>0)?(point1.x+(int)(y/m)):(point1.x-(int)(y/m));
				(*newpxl).push_back(newpixelpos);
			}
			pixeledgelist.push_back(newpxl);
			// Ende der Kanten-X-Wert-Berechnungen
			beginpxl = beginpxl->nxt;
			i++;
		}
		int sizeoflist = i-1;				// wie lang ist die zu durchlaufende Liste
		// Kanten mit gleichen X-Werten verbinden
		int acty = ymin.y;					// Z�hler f�r HorLine
		int pindexleft  = pred(yminindex,sizeoflist),
			pindexright = succ(yminindex,sizeoflist);
		// Kantenlisten laden
		tle<int>* leftedge = (*pixeledgelist[succ(pindexleft,sizeoflist)]).getroot(); // Zeiger ein Element vorher pos.
		tle<int>* rightedge = (*pixeledgelist[pindexright]).getreversedroot();        // Daten der Liste von hinten gebraucht
		// jede Zeile hintereinanderweg zeichnen
		while (acty < ymax.y)
		{
			// y-Wert vom linken Punkt
			pixelPTR test = polygon.getroot();
			int j = 0;
			for (j=0; j<pindexleft; j++, test = test->nxt) {}
			int leftpointy =  test->data.y;	// Y-Wert der Rand-Eck-punkte
			// y-Wert vom rechten Punkt
			test = polygon.getroot();
			for (j=0; j<pindexright; j++, test = test->nxt) {}
			int rightpointy = test->data.y;
			// y-Werte der Zwischenbereiche ermitteln
			int partialy = ((leftpointy<rightpointy)?leftpointy:rightpointy); // 1.Abschnitt
			int furthery = ((partialy==leftpointy)?rightpointy:leftpointy);   // 2.Abschnitt
			// Verbinden der X-Werte der beiden Kantenlisten
			while (acty < (partialy+1))		// Achtung: Zugriff auf Null m�glich!
			{
				int x1 = leftedge->data, x2 = rightedge->data;
				lhor(pixel(x1,acty),x2,color);						// lade zu setztende Punktkoordinate
				lhor.Show();									// zeige diese an
				leftedge = leftedge->nxt;
				rightedge = rightedge->nxt;
				acty++;
			}
			// Indizes aktualisieren
			if (partialy==rightpointy)		// wenn rechte Seite die Teilgrenze darstellt
			{
				pindexright = succ(pindexright,sizeoflist);
				rightedge = (*pixeledgelist[pindexright]).getreversedroot(); // neue Kante laden // -1 ?
			}
			else							// wenn linke Seite die Teilgrenze darstellt
			{
				pindexleft = pred(pindexleft,sizeoflist);
				leftedge = (*pixeledgelist[succ(pindexleft,sizeoflist)]).getroot();
			}
		}
		delete newpxl;
	}
};

class point3d: public drawingobject3d				// 3D-Point in coordinate system
{
public:
	point3d(vertex Cam=ZeroVector,const vertex& V=ZeroVector,const int newcol=0)		// constructor
	{
		P(Cam);					// set up view of camera
		v = V;
		color = newcol;
	}
	~point3d(void){};						// destructor
	virtual void Show(void)					// make 3D-point visible
	{
		pixel p = P(v);
		putpixel(p.x,p.y,color);
	}
};

class line3d: public point3d				// line in 3D: "Gerade"
{
public:
	line3d(linetype lt)					// constructor
	{
		v = lt.A;
		a = lt.B;
	}
	~line3d(void){};						// destructor
	line3d& operator() (const linetype lt)  // set new values
	{
		v = lt.A;
		a = lt.B;
		return *this;
	}
	inline float distance(const vertex p)	// distance between line and point
	{
		float A=a.y/a.x, B=-1, C=v.y+(A*v.x);
		return Abs((float)(A*p.x)+(B*p.y)+C)/(SQRT(SQR(A)+SQR(B)));
	}
	virtual void Show(vertex& Camera)
	{
		proj P(Camera);
		line l2d(P(v),P(a),color);		// Problem auf 2d reduzieren
		l2d.Show();
	}
	// Ortsvektor = v aus Klasse (nach 2 Stufen weiter h�her in Hierarchie)
public:
	vertex a;						// Richtungsvektor
};


// BetrachterPosition mu� verrechnet sein!
class polygon3d: public point3d  // Vewaltet intern polygon2d (nach Clip2D)
{
protected:
	tle<vertex>* polygon;
	// Textur kommt neu hinzu!
	virtual bool Visible(vertex& Cam)	// BackFace-Culling
	{
		vertex A(polygon->data),
			   P0(polygon->nxt->data),
			   B(polygon->nxt->nxt->data);
		vertex a = A-P0, b =B-P0;
		float c = (a%b)*Cam;			// *(-1) hier ignoriert
		if (c < 0) return true;			// daf�r hier ber�cksichtigt
		else return false;
	}
	virtual void Clip(void)		// clip polygon (...??)
	{
		return;
	}
/*
	vertex XYInterSect(const vertex* a,const vertex* b)	// Schnittpunkt: Ebene (z=0) + Gerade zw. 2 Punkten
	{
		float t = -(a->z)/(a->z-b->z);
		vertex S;
		S.x = a->x+(t*(a->x-b->x));
		S.y = a->y+(t*(a->y-b->y));
		S.z = 0;
		return S;
	}
	// Clippen bez�glich XY-Ebene im Betrachter
	void ClipNearPlane(void) // -> sp�ter Volumenclippen vornehmen
	{
		vertex intersect; // Schnittpunkt mit XY-Ebene
		vertices* res = new vertices;
		tle<vertex>* begin = polygon.getroot();
		while (begin)
		{
			vertex a = begin->data - ViewPort,
				   b = ((begin->nxt)?begin->nxt->data:polygon.getroot()->data) - ViewPort;
			bool a_before = ((a.z>0)?true:false),
				 b_before = ((b.z>0)?true:false);
			if (a_before&!b_before)
			{
				intersect = XYInterSect(&a,&b);
				res->push_back(a);
				res->push_back(intersect);
			}
			else if (!a_before&b_before)
			{
				intersect = XYInterSect(&b,&a);
				res->push_back(intersect);
			}
			else if ( a_before )
			{
				res->push_back(a);
			}
			begin = begin->nxt;
		}
		polygon = *res;
	} */
public:
	polygon3d(tle<vertex>* P, const int f)  // constructor
	{
		polygon = P;
		color = f;
	}
	polygon3d(void)						// parameterless constructor
	{
		color = 0;
	}
	~polygon3d(void){};					// destructor
	virtual void Show(vertex& Camera)
	{
		if (!Visible(Camera)) Clip();		// means: (1) polygon has to be clipped or (2) polygon is totally outside
		// 2Optimize: if 2 -> return;
		// Trafo, Projektion, ...
		// Rastern: Fill polygon in 3D! -> (use 2D polygon!)
	}
};

// vieleckiger Raumk�rper
class polyeder: public polygon3d
{
	// boundary boxes
private:
public:
	polyeder(void){}						// constructor
	~polyeder(void){}						// destructor

};

// aus verschiedenen zusammengesetztes Raumk�rpern bestehendes Modell
class model: public polyeder
{
	// boundary boxes inheritage
private:
public:
	model(void){}							// constructor
	~model(void){}							// destructor
};

#endif