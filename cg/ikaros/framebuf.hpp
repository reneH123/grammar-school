#ifndef FRAMEBUFFER
#define FRAMEBUFFER

#include "putp.hpp"
#include "scene.hpp"

void DrawFrame(void)			// Draw all to be drawn
{
	Scene->Render();			// render scene
	// write position of camera on screen
	char string[80];
	char string2[80];
	strcpy( string, "Camera:(" );
	gcvt(Scene->CameraGet().x,2,string2);
	strcat( string, string2);
	strcat( string, "/");
	gcvt(Scene->CameraGet().y,2,string2);
	strcat( string, string2);
	strcat( string, "/");
	gcvt(Scene->CameraGet().z,2,string2);
	strcat( string, string2);
	strcat( string, ")" );
	printmessage(10,AppInfo->height-30,string);
	int dec, sign;
	strcpy( string, fcvt(AppInfo->width,0,&dec,&sign));
	strcat( string, "x");
	strcat( string, fcvt(AppInfo->height,0,&dec,&sign));
	strcat( string, "x");
	strcat( string, fcvt(AppInfo->bpp,0,&dec,&sign));
	printmessage(AppInfo->width-100,AppInfo->height-30,string);
	// end of transmission
}

#endif