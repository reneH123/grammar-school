#ifndef PUTPIXEL
#define PUTPIXEL

#include "xgl.hpp"

#ifdef DIRECTDRAW
	void putpixel(int x, int y, long c)
	{
		DDraw->putpixel(x,y,c);
	}
    void printmessage(int x,int y,char* string)
	{
		DDraw->printmessage(x,y,string);
	}
#endif
#ifdef OPENGL
	void putpixel(int x, int y, long c)
	{
		OpenGL->putpixel(x,y,c);
	}
	void printmessage(int x,int y,char* string)
	{
		OpenGL->printmessage(x,y,string);
	}
#endif

/*
extern trafo T;					// trafo-object
extern proj P;					// projektions-objekt

void Fill_Trafo_Matrix(vertex& _cam)			// push camera-coords and generate modl-view-matrix
{
	vertex to=(-_cam.abs()),
		   up(0,0,1),
		   right = (to%up).abs(),
		   down = to%right;
	T(matrix(right,down,to),-_cam);
	-_cam; // Zur�cksetzen, da un�res Minus fehlerhaft!
}
*/

#endif