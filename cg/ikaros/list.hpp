#ifndef LIST
#define LIST

#include "macros.hpp"

template <class T>
struct tle
{
   T data;
   tle *nxt;
};

template <class T>
class tlist
{
   protected:
     tle<T>* root;
	 tle<T>* last;					// pointer to last element
	 tle<T>* reversedlist;			// ~ to reversed root
	 virtual void clear(tle<T>* test);	// clear whole list
   public:
	 tlist(void)				// create new list; size = 0
	 {
	   root = NULL;
	   last = NULL;
	   reversedlist = NULL;
	 }
	 ~tlist(void)				// destructor: pop whole list
	 {
		clear(root);
		root = NULL;
		clear(reversedlist);
		reversedlist = NULL;
	 }
	 template <class T>
	 void push(T pnt)			// append new element behind root
	 {
		tle<T> *h = new tle<T>;		// temp pointer
		h->nxt = root;
		h->data = pnt;
		root = h;
	 }
/*	 template <class T>
	 T& operator[](int n)const	// get individual data
	 {
		tle<T> *h = root;
		for(int i = 0; i < n; i++)
			h = h->nxt;
		return (T&)(h->data);
	 }*/
	 bool empty(void)			// is list empty?
	 {
		return ((root==NULL)?true:false);
	 }
	 // backshifted elements of the root:
/*	 template <class T>
	 tle<T>* getreversedroot(void)	// root has to be traversed every time!
	 {
		tle<T> *h = root;
		while (h != NULL)
		{
			tle<T> *k = new tle<T>;		// temp pointer
			k->nxt = reversedlist;
			k->data = h->data;
			reversedlist = k;
			h = h->nxt;
		}
		return reversedlist;
	}*/
	template <class T>
	void pushpost(int i,T pnt)		// append after i. element
	{
		tle<T> *hmain = root;
		tle<T> *hm = hmain;
		for (int j=0;j<i;j++,hmain = hmain->nxt){}
		tle<T> *l = hmain->nxt;			// tail of list
		tle<T> *h; h = new tle<T>;		// head of list
		h->data = pnt;
		h->nxt = l;
		hmain->nxt = h;
	}
	// implementation later:
	virtual void pop_front(void);
	virtual void poppost(int i);
	tle<T>* getroot(void);
	tle<T>* getlast(void);
	void setroot(const tle<T>* test);
	void setlast(tle<T> *test);
	int count_items(void);
	T first(void);
	void push_back(T pnt);

	T& operator[](int i)const;
	tle<T>* getreversedroot(void);	// root has to be traversed every time!
};

template <class T>
void tlist<T>::clear(tle<T>* test)	// clear whole list
{
	 tle<T>* h;
	 while (test)
	 {
	    h = test;
	    test = test->nxt;
		delete h;
	}
}	 
template <class T>
void tlist<T>::pop_front(void)	// delete first element
{
	tle<T> *h;
	h = root;
	root = h->nxt;
	delete h;
	if (root==NULL) last=NULL;
}
template <class T>
tle<T>* tlist<T>::getroot(void)			// get root (ptr)
{
	return root;
}
template <class T>
tle<T>* tlist<T>::getlast(void)		// get last	(ptr)
{
	return last;
}
template <class T>
T& tlist<T>::operator[](int n)const
{
  tle<T> *h = root;
  for(int i = 0; i < n; i++)
    h = h->nxt;
  return (T&)(h->data);
}
template <class T>
tle<T>* tlist<T>::getreversedroot(void)	// root has to be traversed every time!
 {
	tle<T> *h = root;
	while (h != NULL)
	{
		tle<T> *k = new tle<T>;		// temp pointer
		k->nxt = reversedlist;
		k->data = h->data;
		reversedlist = k;
		h = h->nxt;
	}
	return reversedlist;
}
template <class T>
void tlist<T>::setroot(const tle<T>* test)	// set new root with other data
{
	root = (tle<T>*)test;
}
template <class T>
void tlist<T>::setlast(tle<T> *test)	// set at the end: last pointer of data
{
	last = test;
}
template <class T>
void tlist<T>::push_back(T pnt)		// optimized methode: append to pointer of last element
 {
	tle<T> *h;
	h = new tle<T>;
	h->data = pnt;
    h->nxt = NULL;
	if (last!=NULL)				// element already in list
	{
		last->nxt = h;			// h append to last element
		last = h;
	}
	else root = last = h;		// no element in list
}
template <class T>
void tlist<T>::poppost(int i)		// pop i. element
{
	if (i<1) return;
	tle<T> *h = root;
	int j= 1;
	while (h)
	{
		if (j==i)
		{
			tle<T> *l;
			l=((h->nxt)? h->nxt->nxt:NULL);
			tle<T> *temp = h->nxt;
			h->nxt = l;
			delete temp;
			break;
		}
		h = h->nxt;
		j++;
	}
}
template <class T>
int tlist<T>::count_items(void)		// counts elements in list
{
	tle<T> *node = root;
	for (int count = 0; node!=NULL; count++)
		node = node->nxt;
    return count;
}
template <class T>
T tlist<T>::first(void)				// get first data
{
	return root->data;
}

#endif