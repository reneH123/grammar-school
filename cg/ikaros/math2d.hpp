#ifndef MATHEMATICS_2D
#define MATHEMATICS_2D

#include <math.h>

typedef struct clipwindow_tag
{
	int resx;
	int resy;
}clipwindow;

#define LengthPlanar(v)\
       (sqrt(((a).x*(a).x)+((a).y*(a).y)))

class pixel
{   
public:
	pixel(int x_=0,int y_=0)					// constructor
	{
		x=x_;
		y=y_;
	}
	inline pixel& operator() (const int a,const int b)		// interior manipulation
	{
		return pixel(x=a,y=b);
	}
	inline pixel& operator+=(const pixel& b)	// componentwise manipulation
	{
		return pixel(x+b.x,y+b.y);
	}
	inline pixel& operator-=(const pixel& b)				
	{
		return pixel(x-b.x,y-b.y);
	}
	inline pixel& operator*=(int b)
	{
		return pixel(x*b,y*b);
	}
	inline pixel& operator/=(int b)
	{
		return pixel(x/b,y/b);
	}
	inline pixel& norm(void)					// scale to norm scalar
	{
		return pixel(x=1,y=1);
	}
	inline float abs(const pixel& a)			// absolute value of length
	{
		return (float)LengthPlanar(a);
	}
	void swap2(pixel* a)						// swap new value with intern
	{
		pixel temp(x,y), 
			  *b = new pixel((*a).x,(*a).y);
		*a = temp;
		x = (*b).x;
		y = (*b).y;
		delete b;
	}
	int x,y;									// components
};

typedef tlist<pixel> pixels;
typedef tle<pixel>* pixelPTR;

#endif