#ifndef FACILITY
#define FACILITY

#include "bsp.hpp"

// Auflistung aller definierter Klassen
class facility;

typedef tlist<facility*> facilities;		// list of drawingobjects

// Oberklasse f�r alle nachfolgenden Objekte, die "Gegenstand" sind: T�ren, Portale, Br�cken,...
class facility
{   
public:
	facility(void){}			// constructor
	~facility(void){}			// destructor
private:
};

#endif