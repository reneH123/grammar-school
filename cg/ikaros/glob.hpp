#ifndef GLOBALS
#define GLOBALS

#include "list.hpp"
#include <windows.h>

struct restype
{
	restype(const int x_=0,const int y_=0,const int bpp_=0)		// standart constructor
	{
		x = x_;
		y = y_;
		bpp = bpp_;
	};
	int x;		// X resolution
	int y;		// Y resolution
	int bpp;
};

class appdesc{			// description of application
private:
	int actualmode;				// intern variable for counting; no other usage
public:
	appdesc(int bitsperpixel,int resx,int resy,const char* apptitle)
	{
		bpp = bitsperpixel;
		width = resx;
		height = resy;
		title = (char*) apptitle;
		// push all supported modes into resolution-list; later pop them wheter doesn't fit
		actualmode = 0;
	}
	int bpp;
	int width,height;
	tlist<restype> resolutions;			// list of possible resolutions
	char* title;
	void MatchRes(void)					// implicite resolution matching
	{
		MatchRes(width,height);
	}
	void MatchRes(int& x,int& y)		// match resolution to best vid-res pattern
	{
		tle<restype>* root = resolutions.getroot();
		if (root)
		{
			restype newx = root->data;
			int a,b;
			for (;root->nxt;root=root->nxt)
			{
				a=Abs((root->data.x-x));
				b=Abs((root->nxt->data.x-x));
				newx = (a<b)?newx = root->data:newx = root->nxt->data;
				if (a<b) break;
			}
			x=newx.x;
			y=newx.y;
		}
	}
	void GetHigherRes(void)				// determine which is next higher res
	{
		if ((actualmode+1) < resolutions.count_items()) 
		{
			actualmode++;
			restype r = resolutions[actualmode];
			width  = r.x;
			height = r.y;
			bpp = r.bpp;
		}
	}
	void GetLowerRes(void)				// determine which is next lower res
	{
		if (actualmode > 0)
		{
			actualmode--;
			restype r = resolutions[actualmode];
			width  = r.x;
			height = r.y;
			bpp = r.bpp;
		}
	}
};
extern appdesc* AppInfo;

class manager{			// instance managing
public:
	manager() {}							// constructor
	~manager() {}							// destructor
	void SetInstance(HINSTANCE instance)	// set current instance
	{
		Instance = instance;
	}
	HINSTANCE GetInstance(void) const		// get current instance
	{
		return Instance;
	}
private:
	HINSTANCE Instance;
};
extern manager* Manager;

#endif