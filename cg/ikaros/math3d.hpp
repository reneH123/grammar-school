#ifndef MATHEMATICS_3D
#define MATHEMATICS_3D

#include <math.h>
#include "math2d.hpp"

#define Dot_product(a, b)\
	   ((a).x * (b).x + (a).y * (b).y + (a).z * (b).z)
#define Length(a, b)\
       (sqrt((((a).x-(b).x)*((a).x-(b).x))+(((a).y-(b).y)*((a).y-(b).y))+(((a).z-(b).z)*((a).z-(b).z))))
#define det2x2(a11, a21, a12, a22)\
		((a11*a22)-(a12*a21))

class vertex
{   
public:
	vertex(float x_ = 0, float y_ = 0, float z_ = 0)		// constructor
	{
		x = x_;
		y = y_;
		z = z_; 
	}
	vertex& operator() (const float a,const float b,const float c)	// interior manipulation
	{
		x=a;
		y=b;
		z=c;
		return *this;
	}
	vertex& operator+=(const vertex& b)				// componentwise manipulation
	{
		x+=b.x;
		y+=b.y;
		z+=b.z;
		return *this;
	}
	vertex& operator-=(const vertex& b)				
	{
		x-=b.x;
		y-=b.y;
		z-=b.z;
		return *this;
	}
	vertex& operator*=(float b)
	{
		x*=b;
		y*=b;
		z*=b;
		return *this;
	}
	vertex operator/=(float b)
	{
		x/=b;
		y/=b;
		z/=b;
		return *this;
	}
	vertex operator%(const vertex& b)			// cross product
	{ 
		return vertex(y*b.z-z*b.y,z*b.x-x*b.z,x*b.y-y*b.x);
	}
	float operator*(const vertex& b)			// scalar product
	{
		return Dot_product(*this,b);
	}
	inline const vertex operator+(const vertex& b)	// sum
	{
		return vertex(x+b.x,y+b.y,z+b.z);
	}	
	inline const vertex operator-(const vertex& b)  // difference
	{
		return vertex(x-b.x,y-b.y,z-b.z);
	}
	vertex operator-(void)						// unary minus
	{
		x*=-1;
		y*=-1;
		z*=-1;
		return *this;
	}
	inline const vertex operator*(float b)		// scalewise multiplyings
	{
		return vertex(b*x,b*y,b*z);
	}
	inline const vertex operator/(float b)		// scalewise dividings
	{
		float inv = 1/b;						// more speed
		return vertex(x*inv,y*inv,z*inv);
	}
	inline int operator==(const vertex& b)		// equality
	{
		return (((x==b.x)&(y==b.y)&(z==b.z))?true:false);
	}
	inline const vertex identity(void)			// scale to norm scalar: 1
	{
		return vertex(1,1,1);
	}
	inline float abs(void)						// absolute value of length
	{
		return Length(vertex(),*this);
	}
	float x,y,z;								// components
};

typedef tlist<vertex> vertices;

class matrix
{
public:
	matrix()												// constructor1: init with zeros
	{
		x(0,0,0);
		y(0,0,0); 
		z(0,0,0);
	}
	matrix(const vertex& a, const vertex& b, const vertex& c)	// constructor2: filling rows
	{
		x=a;
		y=b;
		z=c;
	}
	// data manipulation:
	inline const matrix diagonal(float a, float b, float c)	// bidirectionale matrix	
	{
		return matrix(x=vertex(a,0,0),y=vertex(0,b,0),z=vertex(0,0,c));
	}
	inline const matrix identity(void)						// identity matrix
	{
		return matrix(x=vertex(1,0,0),y=vertex(0,1,0),z=vertex(0,0,1));
	}
	inline const matrix zero(void)							// zero all components
	{
		return matrix(x=vertex(0,0,0),y=vertex(0,0,0),z=vertex(0,0,0));
	}
	inline matrix& transpose(void)							// transposed matrix
	{
		float tmp;
		swapVal(y.x,x.y);
		swapVal(z.x,x.z);
		swapVal(z.y,y.z);
		return *this;
	}
	inline matrix& inverse(void)							// inverted matrix
	{
		vertex b_i1,b_i2,b_i3;			// vertices (vectors) for final result
		matrix M(*this);				// load current matrix
		float D = M.det();				// common dividing determinante
		// b_i1
		M(vertex(1,0,0),(*this).y,(*this).z);		
		b_i1.x = M.det();
		M((*this).x,vertex(1,0,0),(*this).z);
		b_i1.y = M.det();
		M((*this).x,(*this).y,vertex(1,0,0));
		b_i1.z = M.det();
		b_i1/=D;
		// b_i2
		M(vertex(0,1,0),(*this).y,(*this).z);
		b_i2.x = M.det();
		M((*this).x,vertex(0,1,0),(*this).z);
		b_i2.y = M.det();
		M((*this).x,(*this).y,vertex(0,1,0));
		b_i2.z = M.det();			
		b_i2/=D;
		// b_i3
		M(vertex(0,0,1),(*this).y,(*this).z);
		b_i3.x = M.det();
		M((*this).x,vertex(0,0,1),(*this).z);
		b_i3.y = M.det();
		M((*this).x,(*this).y,vertex(0,0,1));
		b_i3.z = M.det();
		b_i3/=D;
		x=b_i1;
		y=b_i2;
		z=b_i3;
		return *this;
	}
	matrix& operator() (const vertex& a,const vertex& b,const vertex& c)	// set new rows
	{
		x=a;
		y=b;
		z=c;
		return *this;
	}
	// acces to components:
	inline vertex& operator[] (int i)							// row i in vector
	{
		return ((i==0)?x:((i==1)?y:z));
	}
	inline vertex operator[] (int i) const
	{
		return ((i==0)?x:((i==1)?y:z));
	}
	vertex column(int j) // column i in vector: 0..2
	{
		if (j==0) return vertex(x.x,y.x,z.x);
		else if (j==1) return vertex(x.y,y.y,z.y);
		else return vertex(x.z,y.z,z.z);
	}
	// componentwise manipulation:
	matrix& operator+=(const matrix& B)
	{
		x+=B.x;
		y+=B.y;
		z+=B.z;
		return *this;
	}
	matrix& operator-=(const matrix& B)
	{
		x-=B.x;
		y-=B.y;
		z-=B.z;
		return *this;
	}
	matrix& operator*=(float d)
	{
		x*=d;
		y*=d;
		z*=d;
		return *this;
	}
	matrix& operator*=(const matrix& B)		// matrices multiplyer: functor
	{
		matrix M(*this);
		x=M*B[0];
		y=M*B[1];
		z=M*B[2];
		return *this;
	}
	matrix& operator/=(float d)
	{
		x/=d;
		y/=d;
		z/=d;
		return *this;
	}
	matrix operator-(void)					// unary minus
	{
		x*=-1;
		y*=-1;
		z*=-1;
		return *this;
	}
	inline const matrix operator+(const matrix& B)		// sum
	{
		return matrix(x+B.x,y+B.y,z+B.z);
	}
	inline const matrix operator-(const matrix& B)		// difference
	{
		return matrix(x-B.x,y-B.y,z-B.z);
	}
	inline const matrix operator*(float d)				// scalewise multiplyings
	{
		return matrix(x*d,y*d,z*d);
	}
	inline const matrix operator/(float d)				// scalewise dividings
	{
		return matrix(x/d,y/d,z/d);
	}
	// multiplying matrices:
	inline vertex operator*(const vertex& b)			// matrix and vector
	{
		return vertex(column(0)*b,column(1)*b,column(2)*b);
	}
	inline matrix operator*(const matrix& B)			// product of two matrices
	{
		return matrix((*this)*B[0],(*this)*B[1],(*this)*B[2]);
	}
	inline float det(void)										// Cramer: determinante
	{
		return (x.x*y.y*z.z)+(x.z*y.x*z.y)+(x.y*y.z*z.x)-(x.x*y.z*z.y)-(x.y*y.x*z.z)-(x.z*y.y*z.x);
	}
	inline float trace(void)									// sum: M(..): a11+a22+a33
	{
		return x.x+y.y+z.z;
	}
	inline int operator==(const matrix& B)				// equality
	{
		return ((x==B.x)&(y==B.y)&(z==B.z)?true:false);
	}
	vertex x,y,z;												// separate vectorrows
};


typedef struct linetype_tag
{
	linetype_tag(const vertex& a_, const vertex& b_)	// parameters in constructor
	{
		A = a_;
		B = b_;
	}
	linetype_tag() {};					// parameterless constructor
	vertex A; // start point
	vertex B; // end point
} linetype;

// plane in 3D
class plane
{
public:
	plane(vertex P0_=ZeroVector, vertex A_=ZeroVector, vertex B_=ZeroVector)	// constructor
	{
		p0 = P0_;
		a = A_;
		b = B_;
	}
	plane& operator() (const vertex P0_, const vertex A_, const vertex B_)
	{
		p0 = P0_;
		a = A_;
		b = B_;
		return *this;
	}
	vertex operator%(linetype& a1)	// intersection: plane & ray(line)
	{
		matrix M(a,b,-a1.A);			// Nennerdeterminante
		-a1.A;							// Vorzeichen wieder neutralisieren
		float D = M.det();
		if (D!=0)						// GLS l�sbar?
		{
			M[2] = a1.B-p0;
			float t = M.det();			// GLS nach t umgestellt:
			t/=D;						// Dt/D = t
			vertex res = a1.A*t;		// Einsetzen in eine der beider Gleichungen: Gerade
			res+=a1.B;
			return res;					// Schnittpunkt
		}
		else return INFVector;			// unl�sbar: parallel/identisch!!!!
	}
	linetype operator%(plane& p)		// intersection: plane & plane
	{
		matrix M(a,b,-p.a);
		-p.a;							// Vorzeichen wieder neutralisieren
		float D = M.det();
		linetype ret;
		if (D!=0)						// GLS l�sbar?
		{
			M(a,b,p.b);
			M.transpose();
			float Ainv = M.det();
			Ainv/=D;
			vertex vd = p.p0-p0;
			float E = (det2x2(a.x*b.y, vd.z, a.y*b.x, vd.z) +			// 2x2 Determinanten
					   det2x2(a.z*b.x, vd.y, a.x*b.z, vd.y) +
					   det2x2(a.y*b.z, vd.x, a.z*b.y, vd.x))/D;
			ret.A = p.p0 + (p.a*E);
			ret.B = p.b + (p.a*Ainv);
		}
		else							// unl�sbar: parallel/identisch!!!!
		{
			ret.A = INFVector;
			ret.B = INFVector;
		}
		return ret;
	}
	float operator*(vertex& v)			// if < 0 before else behind plane
	{
		return (normal*v);
	}
	vertex norm(void)		// Normalenvektor auf Ebene
	{
		return normal = vertex(a-p0)%vertex(b-p0);
	}
	float distance(const vertex& c)		// Abstand zu einem bestimmten Punkt
	{
		dist = normal*c;			// scalar product
		return dist;
	}
	// start-point = v (2 Hierarchien weiter oben)-> bereits vererbt
	vertex p0,a,b;	// vectors that define plane
	vertex normal;	    // normale vector on plain
	float dist;	    // distance of one point in 3d
};

// sichtbarer Blickkegel
typedef struct{
	void setup(float project_scale)
	{
		float angle_horizontal =  atan2(AppInfo->width/2,project_scale)-0.0001;
		float angle_vertical   =  atan2(AppInfo->height/2,project_scale)-0.0001;
		float sh               =  sin(angle_horizontal);
		float sv               =  sin(angle_vertical);
		float ch               =  cos(angle_horizontal);
		float cv               =  cos(angle_vertical);  
		// left  
		sides[0].normal.x=ch;
		sides[0].normal.y=0;
		sides[0].normal.z=sh;
		sides[0].dist    = 0;  
		// right
		sides[1].normal.x=-ch;
		sides[1].normal.y=0;
		sides[1].normal.z=sh;
		sides[1].dist    = 0;  
		// top
		sides[2].normal.x=0;
		sides[2].normal.y=cv;
		sides[2].normal.z=sv;
		sides[2].dist    = 0;
		// bottom
		sides[3].normal.x=0;
		sides[3].normal.y=-cv;  
		sides[3].normal.z=sv;  
		sides[3].dist    = 0;
		// z-near clipping plane  
		znear.normal.x=0;
		znear.normal.y=0;
		znear.normal.z=1;
		znear.dist    = -10;  // test value
	}
	bool inside(vertex& v)		// checks wheter vertex is in frustum or outside
	{
		int i=0;
		for (;i<FRUSTUMPLANES;i++)
		{
			if ((sides[i]*v)>0)
			{
				return false;
			}
		}
		return true;
	}
	plane sides[FRUSTUMPLANES]; // represents the 4 sides of frustum
	plane znear;    // the z-near plane
}frustum;


class trafo		// geometric transformations p' = A*p+c;
{
public:
	trafo(matrix& rot, vertex& trans)
	{
		A=rot;
		c=trans;
	}
	trafo operator*(trafo& T1) // successive tranformation T2*T1
	{
		return trafo(A*T1.A, vertex(A*T1.c+c));
	}
	vertex operator()(vertex& p) // transform one point p -> p'
	{
		return A*p+c;
	}
	void operator()(matrix& rot, vertex& trans)	// set new values
	{
		A = rot;
		c = trans;
	}
	trafo inverse(void) // back transformation p' -> p
	{
		matrix Ainv = A.inverse();
		return trafo(Ainv, -(Ainv*c));
	}
	matrix A; // rotating and scaling matrix
	vertex c; // translating vector
};


// !!!ausschlie�lich f�r Bildschirm!!!
class proj		// central viewing projection
{
public:
	proj(vertex& vp)				// constructor
	{
		Pcam = vp;
	}
	proj(){};						// parameterless constructor
	pixel operator()(vertex& p3d)	// get projection of 3d point
	{
		float zrecip = 1/(p3d.z-Pcam.z);
		return pixel (phys = (((AppInfo->width*(Pcam.x+p3d.x))*zrecip)+(AppInfo->width/2), ((AppInfo->width*(Pcam.y+p3d.y))*zrecip)+(AppInfo->height/2)));
	}
private:
	vertex Pcam;
	pixel phys;
};

#endif