#ifndef SCENE
#define SCENE

#include "drawobj.hpp"
#include "facility.hpp"

#include "loadw.hpp" // loading from HDD


class scene
{   
public:
	scene(void)		// constructor
	{
pixels plist;
  plist.push_back(pixel(100,100));
  plist.push_back(pixel( 80,120));
  plist.push_back(pixel(250,250));
polygon2d* p2d;
p2d = new polygon2d(plist.getroot(),155,true);
DOs.push_back(p2d);
//delete plist;

	line* l1;
	l1 = new line(pixel(2,50),pixel(150, 50),255);
	line* l2;
	l2 = new line(pixel(50,10),pixel( 50,110),156);
	line* l3;
	l3 = new line(pixel(50,10),pixel(150,110),155);
	linehor* l4;
	l4 = new linehor(pixel(10,20),150,0x70);
	linever* l5;
	l5 = new linever(pixel(100,20),70,155);
	line3d* l6;
	l6 = new line3d(linetype(vertex(10,10,20),vertex(-2,3,5)));

DOs.push_back(l1);
DOs.push_back(l2);
DOs.push_back(l3);
DOs.push_back(l4);
DOs.push_back(l5);
DOs.push_back(l6);
	}			
	~scene(void){}			// destructor
	void CameraSetup(vertex& cam)
	{
		Camera = cam;
	}
	const vertex& CameraGet(void)
	{
		return Camera;
	}
	void CameraAddTrans(const vertex& c)
	{
		Camera+=c;
	}
	void CameraAddRotAlpha(const float alpha)  // rotate around x-axis
	{
		matrix rotX = matrix(vertex(1.0f,       0.0f,      0.0f),
							 vertex(0.0f, cos(alpha),sin(alpha)),
							 vertex(0.0f,-sin(alpha),cos(alpha)));
		Camera=rotX*Camera;
	}
	void CameraAddRotBeta(const float beta)		// rotate around y-axis
	{
		matrix rotY = matrix(vertex(cos(beta),0.0f,-sin(beta)),
							 vertex(    0.0f ,1.0f,      0.0f),
							 vertex(sin(beta),0.0f,cos(beta)));
		Camera=rotY*Camera;
	}
	void CameraAddRotGamma(const float gamma)	// rotate around z-axis
	{
		matrix rotZ = matrix(vertex( cos(gamma),sin(gamma),0.0f),
							 vertex(-sin(gamma),cos(gamma),0.0f),
							 vertex(       0.0f,      0.0f,1.0f));
		Camera=rotZ*Camera;
	}
	inline const void operator<<(drawingobject* dobj)
	{
		DOs.push_back(dobj);
	}
	inline const void operator<<(facility* fac)
	{
		FACs.push_back(fac);
	}
	void Render(void)				// renders whole scene
	{
		tle<drawingobject*>* root = DOs.getroot();
		for (;root;root=root->nxt)
		{
			root->data->Show();
		}
		// ... FACs.
	}
private:
	vertex Camera;			// global camera
	drawingobjects DOs;     // global objects: (1) monsters
	facilities FACs;// global facilities: (2) arenas, backgrounds
};
extern scene* Scene;

#endif