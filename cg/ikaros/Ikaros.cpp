#include "ikaros.hpp"
/*
Rene Haberland, 22nd December 2000, MSVCPP6 project (import by dsw)
  DirectDraw and first elements of Direct3D

Graphics:
  -- 
BUG: switch between fullscreen and window
BUG: when switching into higher resolution (any configuration is buggy)
     when low res, then everything is just perfect (vram issue?!)

3d stuff:
  -- 
clipping frustum in screen needs to be re-calculated when changing graphics mode

*/

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow)
{
	Manager = new (manager);
	Manager->SetInstance(hInstance);
	window::Register();
	window Window;		    // initialize application
	if (!Window.Create("Ikaros"))
	{
		MessageBox(NULL,"Err: create window",0,MB_OK);
		return ERRCODE;
	}
	Window.Show();
	Window.LoadAccelerator(Manager->GetInstance(),(LPCTSTR)IDR_ACCELERATORMAIN);
	int res = Window.Run();
	window::Unregister();
	delete Manager;
	return res;
}
