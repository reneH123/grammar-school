#ifndef XWINDOWS
#define XWINDOW

#include "glob.hpp"
#include "framebuf.hpp"					// draw into framebuffer
#include "DEFS"							// definitions, which OS/GL is present
#include "..\resource.h"
#include <windows.h>

scene* Scene;
BOOL bActive;							// is window active
#ifdef DIRECTDRAW
xddraw* DDraw;
#endif
#ifdef OPENGL
xgl* OpenGL;
#endif

class menu{								// managing menu handles
public:
	menu(void) { Menu = NULL; }			// constructor
	~menu(void) { DestroyMenu(Menu); }	// destructor
	void Check(UINT id)					// check menuitem
	{
		/*bool check = (GetMenuState(Menu,id,MF_BYCOMMAND) == MF_CHECKED)?false:true;
		if (!RegisteredID(id))
		{
			MenuItemActive newid;
			newid.activated = check;
			newid.ID=id;
			MenuItemList.push(newid);
		}
		if (check) CheckMenuItem(Menu,id,MF_CHECKED);
		else CheckMenuItem(Menu,id,MF_UNCHECKED);*/
		CheckMenuItem(Menu,id,MF_CHECKED); // UNCHECK misses
	}
	void SetMenuHandle(HWND window)			// retrieve MenuHandle of the window
	{
		Menu = GetMenu(window);
	}
	HMENU GetMenuHandle(void) const			// .. set it
	{
		return Menu;
	}
private:
	HMENU Menu;
/*	typedef struct tagMenuItemActive{		// type definition
		UINT ID;
		bool activated;
	}MenuItemActive;
	tlist<MenuItemActive> MenuItemList;
	bool RegisteredID(UINT id)				// is specified MenuItem on Menu
	{
		tle<MenuItemActive>* node = MenuItemList.getroot();
		int count = 0;
		for (;node!=NULL;count++,node = node->nxt)
		{
			if (node->data.ID == id) return true;
		}
	    return false;
	}*/
};

class window{								// managing window
	// windows exceptions for main window
	// charting all windows based messages
	friend LRESULT CALLBACK WindowProc(HWND wnd, UINT message, WPARAM wParam, LPARAM lParam )	// Interaktionen
	{
		static LPARAM lpii;
		MINMAXINFO *pMinMax;	// informations about window's maximized size, position, its minimum and maximum tracking size
		if (message == WM_CREATE)
		{
			CREATESTRUCT* createstruct;
			createstruct =(CREATESTRUCT*)lParam;
			SetWindowLong(wnd,0,(LONG)(createstruct->lpCreateParams));
			Scene = new (scene);			// init scene
			return 0;
		}
		window* object = (window*)GetWindowLong(wnd,0);
		if (object)
		{
			bool wmode;
			if (object->XGLX!=NULL)
			{
				wmode = object->XGLX->WindowedMode();  // get modus type: boole
			}
			switch(message) 
			{
				case WM_DESTROY:
				{
						delete Scene;
						return object->Destroy(wnd,message,wParam,lParam);
				}
				break;
				case WM_GETMINMAXINFO:
					// Fix the size of the window to ApWidthxApHeight (client size)
					pMinMax = (MINMAXINFO *)lParam;
					pMinMax->ptMinTrackSize.x = AppInfo->width+GetSystemMetrics(SM_CXSIZEFRAME)*2;
					pMinMax->ptMinTrackSize.y = AppInfo->height+GetSystemMetrics(SM_CYSIZEFRAME)*2
                                           +GetSystemMetrics(SM_CYMENU);
					pMinMax->ptMaxTrackSize.x = pMinMax->ptMinTrackSize.x;
					pMinMax->ptMaxTrackSize.y = pMinMax->ptMinTrackSize.y;
				break;
				case WM_ACTIVATEAPP:
					// Pause if minimized or not the top window
        			bActive = (wParam == WA_ACTIVE) || (wParam == WA_CLICKACTIVE);
					if (object->XGLX) object->XGLX->restoreSurfaces(wnd);	// successfull
				break;
				case WM_MOVE:
					// Retrieve the window position after a move
					if (bActive&&wmode)
					{
						if (object->XGLX)
						{
							GetWindowRect(wnd, &object->g_rcWindow);
				    		GetClientRect(wnd, object->XGLX->GetView());
							GetClientRect(wnd, object->XGLX->GetScreen());
							RECT *oldcoords = object->XGLX->GetScreen();
					    	ClientToScreen(wnd, (POINT*)&oldcoords->left);
							ClientToScreen(wnd, (POINT*)&oldcoords->right);
						}
					}
				break;
				case WM_SIZE:
					// Retrieve the window position after a move
					if (bActive&&wmode)
					{
						if (object->XGLX)
						{
							GetWindowRect(wnd, &object->g_rcWindow);
				    		GetClientRect(wnd, object->XGLX->GetView());
							GetClientRect(wnd, object->XGLX->GetScreen());
							RECT *oldcoords = object->XGLX->GetScreen();
				    		ClientToScreen(wnd, (POINT*)&oldcoords->left);
							ClientToScreen(wnd, (POINT*)&oldcoords->right);
						}
					}
				break;
				case WM_PAINT:
					// Update the screen if we need to refresh
					object->XGLX->refreshclient();
				break;
				case WM_KEYDOWN:
					switch( wParam ) 
					{
						case VK_ESCAPE:
						case VK_F12:
							PostMessage(wnd, WM_CLOSE, 0, 0);
						break;
					}
				break;
				// Tastatur-Steuerung:
				// Tastenabfrage: 'Hauptmen�'
				case WM_COMMAND:	
					switch(LOWORD(wParam))
					{
						case IDM_QUIT:
							PostMessage(wnd, WM_CLOSE, 0, 0);
						break;
						case IDM_CHANGERESOLUTIONUP:
							{
								AppInfo->MatchRes();     // get best matching vid res
								AppInfo->GetHigherRes(); // next higher resolution
								if (!MoveWindow(wnd,0,0,AppInfo->width,AppInfo->height,TRUE))
								{
									AppInfo->GetLowerRes();	// unable to set higher res mode
								}
								ShowWindow(wnd,SW_SHOW);
							}
						break;
						case IDM_CHANGERESOLUTIONDOWN:
							{
								AppInfo->MatchRes();     // get best matching vid resx
								AppInfo->GetLowerRes();  // next lower resolution
								if (!MoveWindow(wnd,0,0,AppInfo->width,AppInfo->height,TRUE))
								{
									AppInfo->GetHigherRes();	// unable to set lower res mode
								}
								ShowWindow(wnd,SW_SHOW);
							}
						break;
						case IDM_TOGGLEFULLSCREEN:
							{
								if (bActive)
								{
									if (object->XGLX->WindowedMode()) GetWindowRect(wnd,&object->g_rcWindow);
									object->XGLX->ChangeMode();
									// Change Cooperation Mode
									object->XGLX->ChangeCoop(wnd,object->g_rcWindow);
								}
							}
						break;
						case IDM_ROTALPHAUP:
							Scene->CameraAddRotAlpha(0.11f);
						break;
						case IDM_ROTALPHADOWN:
							Scene->CameraAddRotAlpha(-0.31f);
						break;
						case IDM_ROTBETAUP:
							Scene->CameraAddRotBeta(0.11f);
						break;
						case IDM_ROTBETADOWN:
							Scene->CameraAddRotBeta(-0.31f);
						break;
						case IDM_ROTGAMMAUP:
							Scene->CameraAddRotGamma(0.11f);
						break;
						case IDM_ROTGAMMADOWN:
							Scene->CameraAddRotGamma(-0.31f);
						break;
						case IDM_UP:
							Scene->CameraAddTrans(vertex(0.0f,-0.2f,0.0f));
						break;
						case IDM_DOWN:
							Scene->CameraAddTrans(vertex(0.0f,+0.2f,0.0f));
						break;
						case IDM_STEPLEFT:
							Scene->CameraAddTrans(vertex(-0.2f,0.0f,0.0f));
						break;
						case IDM_STEPRIGHT:
							Scene->CameraAddTrans(vertex(+0.2f,0.0f,0.0f));
						break;
						case IDM_FORWARD:
							Scene->CameraAddTrans(vertex(0.0f,0.0f,-0.2f));
						break;
						case IDM_BACKWARD:
							Scene->CameraAddTrans(vertex(0.0f,0.0f,+0.2f));
						break;
					}
			}
		}
		return DefWindowProc(wnd,message,wParam,lParam);
	}
public:
	int Run(void)								// runtime
	{
		MSG msg;
		while (1)
		{
			if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
			{
				if (msg.message == WM_QUIT) break;
				// Translate and dispatch the message
				if (!Window || !TranslateAccelerator(Window, AccelTable, &msg))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}			
			}
			else
			{
				if (bActive) 
				{ 
					DrawFrame();
					/*if (!*/XGLX->blt();//) return false;		// if unable to block
					/*if (!*/XGLX->flip();//) return false;	// if unable to unlock
				}
				// Make sure we go to sleep if we have nothing else to do
				else WaitMessage(); 
			}
		}
		return msg.wParam;
	}

	static void Register(void)					// register window-properties
	{
		WNDCLASS windowclass;
		LPCTSTR icon,cursor;
		HBRUSH brush;
		icon = MAKEINTRESOURCE(IDI_ICONINIT);
		cursor = MAKEINTRESOURCE(IDC_ARROW);
		brush = (HBRUSH)GetStockObject(BLACK_BRUSH);
		// Registrieren des Hauptfensters
		windowclass.lpszClassName = "IkarosClassName";
		windowclass.hInstance = Manager->GetInstance();
		windowclass.lpfnWndProc = WindowProc;	
		windowclass.style = CS_HREDRAW|CS_VREDRAW;
		windowclass.lpszMenuName = NULL;//MAKEINTRESOURCE(IDR_MAINMENU);
		windowclass.hIcon = LoadIcon(Manager->GetInstance(),icon);
		windowclass.hCursor = LoadCursor(NULL,cursor);
		windowclass.hbrBackground = brush;
	    windowclass.cbClsExtra = 0;
	    windowclass.cbWndExtra = 4;
		if (!RegisterClass(&windowclass)) 
		{
			Unregister(); 
		}
	}
	static void Unregister(void)				// ~ undo properties (end)
	{
		XGLX->~xglx();		// dynamic destructor call
		UnregisterClass("IkarosClassName",Manager->GetInstance());
	}
	LRESULT Create(LPCTSTR title) 				// creating
	{
		DWORD style = WS_POPUP | WS_SYSMENU | WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_SIZEBOX;
		HWND parent = NULL;
		LPVOID params = this;
		// initialize also important objects
		Menu = new (menu);
		AppInfo = new appdesc(BPP,RESX,RESY,title);
		XGLX = NULL;
		// create mainwindow
		Window = CreateWindow("IkarosClassName",AppInfo->title,style,CW_USEDEFAULT,CW_USEDEFAULT,
							  AppInfo->width,AppInfo->height,parent,Menu->GetMenuHandle(),
							  Manager->GetInstance(),params);
		Menu->SetMenuHandle(Window);
		// was window truely created?
		return ((!Window)?FALSE:TRUE);
	}
	void Show(void) 							// show window on desktop
	{
		UpdateWindow(Window);
		SetFocus(Window);
		ShowWindow(Window,SW_SHOW);
		// Save the window size/pos for switching modes
		if (Window)	
		{
			GetWindowRect(Window, &g_rcWindow);
			bool windowedmode;

			#ifdef DIRECTDRAW
				#ifdef DIRECTDRAWWINDOWEDMODE
				     windowedmode = true;
				#else 
					 windowedmode = false;
				#endif
				DDraw = new xddraw(Window,windowedmode); //= DirectX
				XGLX = DDraw;
			#endif
			#ifdef OPENGL
				#ifdef OPENGLWINDOWEDMODE
				     windowedmode = true;
				#else 
					 windowedmode = false;
				#endif
				OpenGL= new xgl(Window,windowedmode);   //= OpenGL;
				XGLX = OpenGL;
			#endif
		}
	}
	void LoadAccelerator(HINSTANCE instance,LPCTSTR lpTableName)	// load accelerators from resources
	{
		AccelTable = LoadAccelerators(instance, lpTableName);
	}
	HWND GetWndHandle(void) const				// window-Handle
	{
		return Window;
	}
	menu* GetMenuObj(void) const				// pointer to menu-object
	{
		return Menu;
	}
	void SetActivity(BOOL b)					// set activity
	{
		bActive = b;
	}
private:
	LRESULT Destroy(HWND wnd,UINT msg,WPARAM wParam,LPARAM lParam)	// destroying
	{
		delete Menu;
		delete AppInfo;
		PostQuitMessage(0);
		return 0;
	}
	HWND Window;								// windows-handle
	HACCEL AccelTable;							// accelerator handle
	menu* Menu;									// mainmenu handle
	RECT g_rcWindow;							// Pos. & size to blt from
	xglx* XGLX;									// independent polymorph graphic library support
};

#endif