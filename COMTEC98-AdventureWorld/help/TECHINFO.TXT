Willkommen in der Hilfe zu technischen Informationen zu ADVENTURE WORLD!

INHALTSVERZEICHNIS
-----------------
Einf�hrung..............
Vorraussetzungen......................
Tastenbelegung..................
Finden neuer Fehler.....................


==========================================
==     Einf�hrung   	                ==
==========================================

Nach dem Start von Adventure World wird ein Eingangsmenu sichtbar, in welchem man
die Auswahl zwischen den dort angezeigten Optionen hat. Um das richtige Spiel zu
starten stellen sie den Cursor (das "A" und das "W") auf den Schalter "Spiel" und
best�tigen sie mit ENTER.
Im Menu Optionen k�nnen sie beispielsweise den Schwierigkeitsmodus ihrem K�nnen anpassen.
Unter Hilfe k�nnen sie die Geschichte von Adventure World nachlesen, um das Spiel zu
verstehen.


==========================================
==         Vorraussetzungen             ==
==========================================

ADVENTURE WORLD ist eine DOS Anwendung, d.h. das das Programm auch aus einem
MS-DOS Prompt aufgerufen werden mu�.


Speicher
------------------

ADVENTURE WORLD ben�tigt unter Minimalvorraussetzung 4 MB an installiertem
Arbeitsspeicher. Es wird dringend mehr als 4 MB empfohlen. ADVENTURE WORLD 
ben�tigt au�erdem noch auf dem aktuellen Laufwerk etwas Massenspeicher, da
w�hrend des Programmablaufs st�ndig Zugriffe auf interne Dateien gemacht wird.


Laufzeit unter Win 95
----------------------------

ADVENTURE WORLD kann unter WIN 95 nur im Vollbildschirmmodus ausgef�hrt werden,
da dies eine DOS-Anwendung ist mit Routinen, welche auf MS-DOS Befehlsbibliotheken 
zugreift.


Grafikkarte
-----------------

F�r das Spiel wird eine EGA/VGA-Grafikkarte vorrausgesetzt, welche den SVGA-Modus mit
256 Farben unterst�tzt.


==========================================
==    Tastenbelegung                    ==
==========================================

Die folgenden Tasten k�nnen benutzt werden:

A-Z                     1-5
TAB			SPACE
ESC

Die konkreten Funktionen der Tasten sind in keys.bmp aufgef�hrt.

==========================================
==         Finden neuer Fehler	        ==
==========================================

Wenn sie im Umfang des Programms einen Fehler entdecken, wenden sie sich
an den Autor und f�llen sie die Fehlerregistration aus Help.txt aus.

