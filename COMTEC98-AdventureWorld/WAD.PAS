UNIT WAD;
INTERFACE
USES Graph, Initial, Crt, Dos, Help, Welt;

CONST Item_Anz=9;
      Enemy_Anz=12;
      Inventory_Anz=13;
      Weapons_Anz=4;
      Bild_Anz = Item_Anz+Enemy_Anz+Inventory_Anz+Weapons_Anz;
      Statement_Anz=1;
      Dat_Anz={Bild_Anz}2+Statement_Anz+BGI_Anz;
      Dateien: ARRAY[1..Dat_Anz] OF STRING =
      ('package.pcx','menu.pcx','mission.txt','SVGA256.BGI','EGAVGA.BGI');
      Geschwindigkeit: ARRAY[1..5] OF Word = (1200,800,20,1,0);
      Shot_Speed: ARRAY[1..5] OF Word= (50,100,200(*Standart*),300,350);
      MaxGegner = 5;
      MaxWelt = 5;
      MaxItems = 5;
TYPE Richt = (Hoch,Runter,Links,Rechts);
     ItemTyp = (EnergieMax,EnergieMin,Lives,Magazin,Kugeln,Granade,Bomben,ExitItem,
                MySelf_,Enemy_,Stufe,Wand,Grenze);
     Subject = OBJECT
       x_,y_: WORD;
       Hintergrund: BarType;
       Breite, Hoehe: WORD;
       tot: Boolean;
     END;
     Item = OBJECT(Subject)
       Typ: ItemTyp;
       CONSTRUCTOR Make(X_Start,Y_Start: WORD; typ_: ItemTyp);
       PROCEDURE Stelle_dar(Num: BYTE);
       DESTRUCTOR Kill;
       FUNCTION Kollision(x1,y1,x2,y2: WORD): BOOLEAN;
     END;
     MySelf = OBJECT(Subject)
       Schritt,
       Schritt2,
       Schritt2Lv: BYTE;
       a: Char;
       Direction: BOOLEAN;
       yDelta: INTEGER;
       BildTrue, BildFalse : BarType;
       Leben: BYTE;
       Energie: INTEGER;
       Weapon_Aktiv: T_Weapons_Anz;
       Schuss: ARRAY[T_Weapons_Anz] OF Byte;
       CONSTRUCTOR Make(X_Start,Y_Start: WORD);
       PROCEDURE Outfit(x,y: Word; Bild: BarType);
       PROCEDURE Fire(Waffe: Byte; x,y: WORD; Richtung: BOOLEAN);
       PROCEDURE Springen;
       PROCEDURE Tastenlesen;
       FUNCTION Erlaube(Key: CHAR; x,y: WORD; Level: Byte): BOOLEAN;
       FUNCTION AddItem(itm: Item): BOOLEAN;
       DESTRUCTOR Kill;
       PROCEDURE Treffer(Stark: BYTE);
     END;
     Enemy = OBJECT(MySelf)
       Richtung: Richt;
       CONSTRUCTOR Make(X_Start,Y_Start: Word);
       DESTRUCTOR Kill;
       PROCEDURE Tastenlesen;
       PROCEDURE Bewegen;
       FUNCTION Erlaube(Key: CHAR; x,y: WORD; Level: Byte): BOOLEAN;
       FUNCTION Kollision(x1,y1,x2,y2: WORD): BOOLEAN;
       PROCEDURE Treffer(Stark: BYTE);
     END;
     Game2 = OBJECT
         Time: LONGINT;
         Items_Anz: Byte;
         Items:  ARRAY [1..MaxItems] OF Item;
         Gegner_Anz: Byte;
         Feinde:  ARRAY [1..MaxGegner] OF Enemy;
         Akt_Level: Byte;
         FUNCTION Erlaube(a: CHAR; x1_,y1_,x2_,y2_: WORD; Level: Byte): BOOLEAN;
         PROCEDURE Kollision(x1,y1,x2,y2: WORD);
         PROCEDURE Tastenlesen;
         PROCEDURE Ablauf(Level: Byte);
         PROCEDURE Zeit;
       END;

VAR DateienMem: ARRAY[1..Bild_Anz] OF BarType;
    Fillinfo: FillSettingsType;
    Mensch: MySelf;
    Spiel: Game2;
    ErrResultat: Word;
    AbbruchDesProgramms: BOOLEAN;
    NaechstesLevel: BOOLEAN;

PROCEDURE Menu;
PROCEDURE Level;

IMPLEMENTATION
USES Begrenz;

CONST Bewege_Gegner_Pause: BYTE = 0;
TYPE TIdRec = RECORD
                x_,y_: WORD;
                Special: ItemTyp;
              END;
VAR AktPos: 1..Bottom_MaxAnz;
    Spiel_Ende: BOOLEAN;
    Max_Wand: 0..MaxWand;
    Max_Stufe: 0..MaxStufe;
    Max_Grenze: 0..MaxGrenze;

PROCEDURE Cursor(x,y: Word);
 VAR abgedeckt: Byte;
     grad,breit,hoch:Word;
     z: Integer;
     OldBar1, OldBar2: BarType;
     NoKey, vor: BOOLEAN;
 PROCEDURE Rotate(x,y: Word; z: SHORTINT; Bool: Boolean);
 BEGIN
   SetLineStyle(SolidLn, 0, NormWidth);
   SetWriteMode(NormalPut);
   SetColor(Red);
   IF Bool= True THEN BEGIN
     Line(x+z,y+30,x+z+15,y); Line(x+z+4,y+30,x+z+15,y+4); Line(x+z,y+30,x+z+4,y+30);
     Line(x+z+15,y,x+z+30,y+30); Line(x+z+15,y+4,x+z+26,y+30); Line(x+z+26,y+30,x+z+30,y+30);
     Line(x+7,y+15,x+23,y+15); Line(x+5,y+19,x+25,y+19);
   END
   ELSE IF Bool= False THEN BEGIN
     Line(x+z,y,x+z+4,y); Line(x+z,y,x+z+8,y+30); Line(x+z+4,y,x+z+12,y+30);
     Line(x+z+8,y+30,x+z+12,y+30); Line(x+z+8,y+30,x+z+15,y+5); Line(x+z+12,y+30,x+z+15,y+17);
     Line(x-z+15,y+5,x-z+22,y+30); Line(x-z+15,y+17,x-z+18,y+30); Line(x-z+18,y+30,x-z+22,y+30);
     Line(x-z+18,y+30,x-z+26,y); Line(x-z+22,y+30,x-z+30,y); Line(x-z+26,y,x-z+30,y);
   END;
 END;
BEGIN
  abgedeckt:=0; grad:=360; breit:=20; hoch:=20; z:=0;
  vor:=FALSE;
  REPEAT
    NoKey:=TRUE;
    vor:=NOT(vor);
    z:=0;
    WHILE (z >= -5) AND NoKey DO BEGIN
      SavePosition(x-5,y,x+35,y+30,Oldbar1);
      SavePosition(x+245,y,x+250+35,y+30,Oldbar2);
      IF vor THEN BEGIN
        Rotate(x+250,y,z,False);
        Rotate(x,y,z, True);
        z:=z-1;
        END
      ELSE BEGIN
        Rotate(x,y,-5-z,False);
        Rotate(x+250,y,-5-z,True);
        z:=z-1;
      END;
      Delay(100);
      ClearPosition(OldBar1);
      ClearPosition(OldBar2);
      IF KeyPressed THEN NoKey:=FALSE;
    END;
  UNTIL Keypressed;
END; (*Cursor*)
PROCEDURE Menu;
 PROCEDURE Taste;
  VAR a : Char;
      x,y: Word;
      OldBar1, OldBar2, OldBar3, OldBar4, OldBar5: BarType;
 BEGIN
     x:=X_Start-50;
     y:=Y_Delta+30;
     SetColor(White);
     REPEAT
       SavePosition(x-30,y-30,x+30,y+30,OldBar1);
       SavePosition(x+220,y-30,x+280,y+30,OldBar2);
       Cursor(x,y);
       a:=ReadKey;
       WITH Form.Bottoms DO BEGIN
         IF (a=KeyUp) AND (AktPos > 1) THEN BEGIN
            y:=y-(Y_Delta+Bottom_High);
            Dec(AktPos);
            END
         ELSE IF (a=KeyDown) AND (AktPos < 4) THEN BEGIN
            y:=y+(Y_Delta+Bottom_High);
            Inc(AktPos);
         END;
       END;
       ClearPosition(OldBar1);
       ClearPosition(OldBar2);
     UNTIL (a=KeyReturn);
     CASE AktPos OF
        1: Level;
        2: BEGIN
             Options(Config,OldBar3);
             ClearPosition(OldBar3);
             WITH Form.Bottoms DO
              AktPos:=1;
             Taste;
           END;
        3: BEGIN
             Help_Game(OldBar5);
             REPEAT UNTIL Keypressed;
             ClearPosition(OldBar5);
             WITH Form.Bottoms DO
               AktPos:=1;
             Taste;
           END;
        4: BEGIN
             Ende(OldBar4);
             ClearPosition(OldBar4);
             WITH Form.Bottoms DO
               AktPos:=1;
             Taste;
           END;
     END;
 END; (*TASTE*)
BEGIN (*Menu*)
  Menubild;
  WITH FillInfo DO SetFillStyle(Pattern, Color);
  WITH Form.Bottoms DO BEGIN
    Zeichnen(Bottom_Anz);
    Beschriften(B_Text);
  END;
  AktPos:=1;
  Taste;
END; (*Menu*)
CONSTRUCTOR MySelf.Make(X_Start,Y_Start: WORD);
BEGIN
  Hintergrund.Save:=NIL;
  tot:=FALSE;
  x_:=X_Start;
  y_:=Y_Start;
END;
DESTRUCTOR MySelf.Kill;
 VAR TmpBar1, TmpBar2: BarType;
BEGIN
  Write(#7);
  IF Integer(Leben) <= 0 THEN BEGIN
    IF SuddenDeath(TmpBar1)=Keyj THEN BEGIN
     ClearPosition(TmpBar1);
     Spiel.akt_Level:=1;
     Leben:=2;
     Weapon_Aktiv:=2;
     Energie:=100;
     Spiel_Ende:=FALSE;
     Mensch.Schuss[1]:=0;
     Mensch.Schuss[2]:=0;
     Mensch.Schuss[3]:=0;
     Mensch.Schuss[4]:=0;
     Mensch.Schuss[5]:=0;
    END
    ELSE BEGIN
      Ende(TmpBar2);
      ClearPosition(TmpBar2);
      Spiel.Ablauf(1); (*BUGS*)
    END;
  END;
  Dec(Leben);
  Energie:=100;
  WITH Form DO BEGIN
    Frames.Zeichne(10,Weltrahmen_ButtomZeit_Y,190,
     Weltrahmen_ButtomZeit_Y+Frames.ButtomHigh,
     1, DarkGray,'  Leben: ',Leben,'');
    Frames.Zeichne(10,445,250,470,1,DarkGray,' Energie: ',Energie,'');
  END;
END;
PROCEDURE MySelf.Outfit(x,y: Word; Bild: BarType);
BEGIN
  BildTrue.X:=x;
  BildTrue.Y:=y;
  BildFalse.X:=x;
  BildFalse.Y:=y;
  IF (BildTrue.Size > 0) AND (BildFalse.Size > 0) THEN BEGIN
     PutImage(BildTrue.X,BildTrue.Y,Bild.Save^,NOrMalPut);
  END;
END;
FUNCTION MySelf.AddItem(itm: Item): BOOLEAN;
BEGIN
  AddItem:=FALSE;
  CASE itm.Typ OF
    EnergieMax: IF Energie < 100 THEN BEGIN
                  IF Energie < 30 THEN
                    Energie:=Energie+70
                  ELSE
                    Energie:=100;
                  Form.Frames.Zeichne(10,445,250,470,1,DarkGray,' Energie: ',Energie,'');
                  AddItem:=TRUE;
                END;
    EnergieMin: IF Energie < 100 THEN BEGIN
                  IF Energie < 75 THEN
                    Energie:=Energie+15
                  ELSE
                    Energie:=100;
                  Form.Frames.Zeichne(10,445,250,470,1,DarkGray,' Energie: ',Energie,'');
                  AddItem:=TRUE;
                END;
    Lives:      BEGIN
                  IF Leben < 3 THEN BEGIN
                    Inc(Leben);
                    AddItem:=TRUE;
                  END;
                END;
    Magazin:    BEGIN
                  IF Schuss[2] < 5 THEN BEGIN
                    Inc(Schuss[2],5); (*pistole - MaxSchuss: 5*)
                    AddItem:=TRUE;
                  END;
                END;
    Kugeln:     BEGIN
                  IF Schuss[3] < 10 THEN BEGIN
                    Inc(Schuss[3],10); (*MP - MaxSchuss: 10*)
                    AddItem:=TRUE;
                  END;
                END;
    Granade:    BEGIN
                  IF Schuss[4] < 2 THEN BEGIN
                    Inc(Schuss[4],2); (*granade - MaxSchuss: 2*)
                    AddItem:=TRUE;
                  END;
                END;
    Bomben:     BEGIN
                  IF Schuss[5] < 1 THEN BEGIN
                    Inc(Schuss[5],1); (*bombe - MaxSchuss: 1*)
                    AddItem:=TRUE;
                  END;
                END;
    ExitItem:   BEGIN
                  NaechstesLevel:=TRUE;
                  AddItem:=TRUE;
                END;
  END;
END;
CONSTRUCTOR Enemy.Make(X_Start,Y_Start: Word);
BEGIN
  x_:=X_Start;
  y_:=Y_Start;
  tot:=FALSE;
  Richtung:=Links;
  Hintergrund.Save:=NIL;
END;
DESTRUCTOR Enemy.Kill;
BEGIN
  tot:=TRUE;
  IF (Spiel.Gegner_Anz > 0) THEN Dec(Spiel.Gegner_Anz);
END;
PROCEDURE Enemy.Treffer(Stark: BYTE);
BEGIN
  IF NOT(tot) THEN BEGIN
    IF Energie < Stark THEN BEGIN
      Kill;
      ClearPosition(Hintergrund);  (*Hintergrund wiederherstellen, Enemy verschwindet*)
     END
    ELSE
      Energie:=Energie-Stark;
  END;
END;
PROCEDURE MySelf.Treffer(Stark: BYTE);
BEGIN
  IF NOT(tot) THEN BEGIN
    IF Energie < Stark THEN
      Kill
    ELSE BEGIN
      Energie:=Energie-Stark;
      Form.Frames.Zeichne(10,445,250,470,1,DarkGray,' Energie: ',Energie,'');
    END;
  END;
END;
PROCEDURE  Enemy.Bewegen;
BEGIN
  IF NOT(tot) THEN BEGIN
    ClearPosition(Hintergrund);
    CASE Richtung OF
      Links:  IF Erlaube(KeyLeft,x_,y_,Spiel.Akt_Level) THEN x_:=x_-1 ELSE Richtung:=Rechts;
      Rechts: IF Erlaube(KeyRight,x_,y_,Spiel.Akt_Level) THEN x_:=x_+1 ELSE Richtung:=Links;
    END;
    SavePosition(x_,y_,x_+BildTrue.XSize,y_+BildTrue.YSize,Hintergrund);
    CASE Richtung OF
      Links:  Outfit(x_,y_,BildFalse);
      Rechts: Outfit(x_,y_,BildTrue);
    END;
  END;
END;
PROCEDURE Enemy.Tastenlesen;
BEGIN (* Spart Speicherplatz *)
END;
FUNCTION Enemy.Kollision(x1,y1,x2,y2: WORD): BOOLEAN;
 VAR ma,mb,mc,md: BOOLEAN;
BEGIN
  Kollision:=FALSE;
  IF (NOT tot) THEN BEGIN
    ma:=(x_ <= x1) AND (x1 <= x_+Breite);
    mb:=(y_ <= y1) AND (y1 <= y_+Hoehe);
    mc:=(x_ <= x2) AND (x2 <= x_+Breite);
    md:=(y_ <= y2) AND (y2 <= y_+Hoehe);
    IF (ma AND mb) OR (mc AND mb) OR (ma AND md) OR (mc AND md) THEN
      Kollision:=TRUE;
    ma:=(x1 <= x_) AND (x_ <= x2);
    mb:=(y1 <= y_) AND (y_ <= y2);
    mc:=(x1 <= x_+Breite) AND (x_+Breite <= x2);
    md:=(y1 <= y_+Hoehe) AND (y_+Hoehe <= y2);
    IF (ma AND mb) OR (mc AND mb) OR (ma AND md) OR (mc AND md) THEN
      Kollision:=TRUE;
  END;
END;
FUNCTION Enemy.Erlaube(Key: CHAR; x,y: WORD; Level: Byte): BOOLEAN;
 VAR ErlaubeTmp: BOOLEAN;
     Lv: BYTE;
BEGIN
  ErlaubeTmp:=MySelf.Erlaube(Key, x,y, Level);
  Lv:=1;
  WHILE ErlaubeTmp AND (Lv <= Max_Grenze) DO BEGIN
    ErlaubeTmp:=ErlaubeTmp AND Grenzen[Level][Lv].Erlaube(Key, x,y,x+Hoehe,y+Breite);
    Inc(Lv);
  END;
  Erlaube:=ErlaubeTmp;
END;
PROCEDURE Bewege_Gegner;
 VAR Lv: BYTE;
BEGIN
  IF Bewege_Gegner_Pause < 10 THEN
    Inc(Bewege_Gegner_Pause)
  ELSE BEGIN
    FOR Lv:=1 TO MaxGegner DO
      IF NOT(Spiel.Feinde[Lv].Tot) THEN
        Spiel.Feinde[Lv].Bewegen;
    Bewege_Gegner_Pause:=0;
  END;
END;
FUNCTION Item.Kollision(x1,y1,x2,y2: WORD): BOOLEAN;
 VAR ma,mb,mc,md: BOOLEAN;
BEGIN
  Kollision:=FALSE;
  IF (NOT tot) THEN BEGIN
    ma:=(x_ <= x1) AND (x1 <= x_+Breite);
    mb:=(y_ <= y1) AND (y1 <= y_+Hoehe);
    mc:=(x_ <= x2) AND (x2 <= x_+Breite);
    md:=(y_ <= y2) AND (y2 <= y_+Hoehe);
    IF (ma AND mb) OR (mc AND mb) OR (ma AND md) OR (mc AND md) THEN
      Kollision:=TRUE;
    ma:=(x1 <= x_) AND (x_ <= x2);
    mb:=(y1 <= y_) AND (y_ <= y2);
    mc:=(x1 <= x_+Breite) AND (x_+Breite <= x2);
    md:=(y1 <= y_+Hoehe) AND (y_+Hoehe <= y2);
    IF (ma AND mb) OR (mc AND mb) OR (ma AND md) OR (mc AND md) THEN
      Kollision:=TRUE;
  END;
END;
PROCEDURE Item.Stelle_dar(Num: BYTE);
BEGIN (*Hintergrund speichern*)
  SavePosition(x_,y_,x_+DateienMem[Num].XSize,y_+DateienMem[Num].YSize,Hintergrund);
  PutImage(x_,y_,DateienMem[Num].Save^,NormalPut);
END;
CONSTRUCTOR Item.Make(X_Start,Y_Start: Word; typ_: ItemTyp);
BEGIN
  x_:=X_Start;
  y_:=Y_Start;
  tot:=FALSE;
  Hintergrund.Save:=NIL;
  typ:=typ_;
END;
DESTRUCTOR Item.Kill;
BEGIN
  ClearPosition(Mensch.Hintergrund); (* Hintergrund wiederherstellen *)
  ClearPosition(Hintergrund);        (* Item verschwindet *)
  WITH Mensch DO BEGIN
    SavePosition(x_,y_,x_+BildTrue.XSize,y_+BildTrue.YSize,Hintergrund);
    IF Direction THEN Outfit(x_,y_,BildTrue)
    ELSE Outfit(x_,y_,BildFalse);
  END;
  tot:=TRUE;
END;
FUNCTION MySelf.Erlaube(Key: CHAR; x,y: WORD; Level: Byte): BOOLEAN;
 VAR ErlaubeTmp: BOOLEAN;
BEGIN
  ErlaubeTmp:=FALSE;
  (* Gesamtrahmen *)
  IF      (Key=KeyUp) AND (y > MinY) THEN ErlaubeTmp:=TRUE
  ELSE IF (Key=KeyLeft) AND (x > MinX) THEN ErlaubeTmp:=TRUE
  ELSE IF (Key=KeyRight) AND (x < MaxX-Breite) THEN ErlaubeTmp:=TRUE
  ELSE IF (Key=KeyDown) AND (y < MaxY-Hoehe-15) THEN ErlaubeTmp:=TRUE;
  (* Gegenst�nde *)
  IF NOT Spiel.Erlaube(Key,x,y,x+Breite,y+Hoehe,Level) THEN
    ErlaubeTmp:=FALSE;
  Erlaube:=ErlaubeTmp;
END;
PROCEDURE MySelf.Fire(Waffe: Byte; x,y: WORD; Richtung: BOOLEAN);
 VAR OldLine: BarType;
     SchussLaenge,SchussDauer,Lv: BYTE;
     Key: CHAR;
     Raus: BOOLEAN;
BEGIN
  IF (Waffe <> 0) AND (Schuss[Waffe] > 0) THEN BEGIN
    Dec(Schuss[Waffe]);
    IF Richtung THEN x:=x+20 ELSE x:=x-10;
    y:=y+32;
    SetColor(Weapon_Aktiv);
    SchussLaenge:=10;
    SchussDauer:=1;
    Raus:=FALSE;
    OldLine.Save:=NIL;
    IF Richtung THEN Key:=KeyRight ELSE Key:=KeyLeft; (* Kollision mit W�nden *)
    WHILE NOT(Raus) AND (x < (MaxX-10)) AND (x > MinX) AND (Weapon_Aktiv <> 1) DO BEGIN
      FOR Lv:=1 TO MaxGegner DO BEGIN (* Kollision mit Gegnern *)
        {IF ((Richtung) AND Spiel.Feinde[Lv].Kollision(x,y,x,y)) OR ((NOT Richtung) AND )}
        IF NOT(Spiel.Feinde[Lv].Tot) AND Spiel.Feinde[Lv].Kollision(x,y,x,y) THEN BEGIN
          IF OldLine.Save <> NIL THEN ClearPosition(OldLine);
          Spiel.Feinde[Lv].Treffer(Weapon_Aktiv*10); (* Gegner wird angeschossen *)
          Raus:=TRUE;
        END;
      END;
      IF NOT Spiel.Erlaube(Key,x,y,x+SchussLaenge,y,Spiel.Akt_Level) THEN Raus:=TRUE;
      IF OldLine.Save <> NIL THEN ClearPosition(OldLine);
      IF NOT(Raus) THEN BEGIN
        SavePosition(x,y,x+SchussLaenge,y,OldLine);
        Line(x,y,x+SchussLaenge,y);
        Delay(SchussDauer);
        IF Richtung THEN Inc(x) ELSE Dec(x);
        {Spiel.Zeit;}
      END;
    END;
    IF OldLine.Save <> NIL THEN ClearPosition(OldLine);
  END;
  Form.Frames.Zeichne(430,445,620,470,1,DarkGray,' Schu�: ',Schuss[Weapon_Aktiv],'');
END; (* Fire *)
PROCEDURE MySelf.Springen;
 CONST Hoch = TRUE;
 PROCEDURE Sprung(yDelta, xDelta: INTEGER);
  CONST SprungHoehe = 110;
  VAR   Lv: INTEGER;
        Taste: CHAR;
        JumpMove: BYTE;
 BEGIN  (* Sprung *)
   JumpMove:=0;
   FOR Lv:=1 TO SprungHoehe DO BEGIN
     ClearPosition(Hintergrund);
     IF ((yDelta > 0) AND (Erlaube(KeyDown,x_,y_,Spiel.Akt_Level))) OR
        ((yDelta < 0) AND (Erlaube(KeyUp,x_,y_,Spiel.Akt_Level))) THEN
       y_:=y_+yDelta;
     IF JumpMove > 0 THEN BEGIN
       IF (Taste = KeyLeft) AND Erlaube(KeyLeft,x_,y_,Spiel.Akt_Level) THEN
         x_:=x_-xDelta (* Sprungweite nach links *)
       ELSE IF (Taste = KeyRight) AND Erlaube(KeyRight,x_,y_,Spiel.Akt_Level) THEN
         x_:=x_+xDelta; (* Sprungweite nach rechts *)
       Dec(JumpMove);
     END;
     IF KeyPressed THEN BEGIN
       Taste:=ReadKey;
       JumpMove:=5;
       IF (Taste=KeyTab) THEN Fire(3,x_,y_,Direction);
     END;
     SavePosition(x_,y_,x_+BildTrue.XSize,y_+BildTrue.YSize,Hintergrund);
     IF Direction THEN
       Outfit(x_,y_,BildTrue)
     ELSE
       Outfit(x_,y_,BildFalse);
     Delay(1);
     Bewege_Gegner;
     (* Kollision an: *)
     Spiel.Kollision(x_,y_,x_+BildTrue.XSize,y_+BildTrue.YSize);
   END;
 END;  (* Sprung *)
BEGIN (* Springen *)
  IF Hoch THEN
    yDelta:=-1
  ELSE yDelta:=1;
  Sprung(yDelta,Abs(yDelta));  (* Hochspringen *)
  Sprung(-yDelta,Abs(yDelta)); (* Herunterfallen *)
END;  (* Springen *)
PROCEDURE MySelf.Tastenlesen;
 VAR i: Integer;
     OldBar: BarType;
     Lv: BYTE;
BEGIN
  AbbruchDesProgramms:=FALSE;
  NaechstesLevel:=FALSE;
  SavePosition(x_,y_,x_+BildTrue.XSize,y_+BildTrue.YSize,Hintergrund);
  Outfit(x_,y_,BildTrue);
  WITH Spiel.Feinde[Spiel.Akt_Level] DO BEGIN
    SavePosition(x_,y_,x_+BildTrue.XSize,y_+BildTrue.YSize,Hintergrund);
    Outfit(x_,y_,BildTrue);
  END;
  Direction:=FALSE;
  a:=#0;
  REPEAT
    Bewege_Gegner;
    Bewege_Gegner;
    Spiel.Kollision(x_,y_,x_+BildTrue.XSize,y_+BildTrue.YSize);
    IF Erlaube(KeyDown,x_,y_,Spiel.Akt_Level) THEN BEGIN (* Gravitation an: *)
      {Inc(Beschl);
      FOR BeschlLv:=1 TO Beschl DO BEGIN (*F�r Beschleunigung*)
        FOR Lv:=1 TO Schritt DO BEGIN
          ClearPosition(Hintergrund);
          y_:=y_+1;
          SavePosition(x_,y_,x_+BildFalse.XSize,y_+BildFalse.YSize,Hintergrund);
          IF Direction THEN Outfit(x_,y_,BildTrue)
          ELSE Outfit(x_,y_,BildFalse);
        END;
      END;}
      ClearPosition(Hintergrund);
      y_:=y_+1;
      SavePosition(x_,y_,x_+BildTrue.XSize,y_+BildTrue.YSize,Hintergrund);
      IF Direction THEN
        Outfit(x_,y_,BildTrue)
      ELSE
        Outfit(x_,y_,BildFalse);
      END;
{    ELSE
      Beschl:=0;}
    IF KeyPressed THEN BEGIN
      a:=ReadKey;
      CASE a OF
        KeyLeft:
         BEGIN
           Schritt2Lv:=0;
           WHILE Erlaube(a,x_,y_,Spiel.Akt_Level) AND (Schritt2Lv < Schritt2) DO BEGIN
             ClearPosition(Hintergrund);
             x_:=x_-Schritt; Direction:=FALSE;
             SavePosition(x_,y_,x_+BildFalse.XSize,y_+BildFalse.YSize,Hintergrund);
             IF Direction THEN
               Outfit(x_,y_,BildTrue)
             ELSE
               Outfit(x_,y_,BildFalse);
             Inc(Schritt2Lv);
           END;
         END;
        KeyRight:
         BEGIN
           Schritt2Lv:=0;
           WHILE Erlaube(a,x_,y_,Spiel.Akt_Level) AND (Schritt2Lv < Schritt2) DO BEGIN
             ClearPosition(Hintergrund);
             x_:=x_+Schritt; Direction:=TRUE;
             SavePosition(x_,y_,x_+BildTrue.XSize,y_+BildTrue.YSize,Hintergrund);
             IF Direction THEN
               Outfit(x_,y_,BildTrue)
             ELSE
               Outfit(x_,y_,BildFalse);
             Inc(Schritt2Lv);
           END;
         END;
        KeySpace:
         IF NOT Erlaube(KeyDown,x_,y_,Spiel.Akt_Level) THEN Springen;
        KeyTab:
         Fire(Weapon_Aktiv,x_,y_,Direction);
        KeyOne:
         Weapon_Aktiv:=2;
        KeyH:
         BEGIN
           Delay(500);
           IF Keypressed THEN BEGIN
             a:=ReadKey;
             IF (a=KeyE) THEN BEGIN
               Delay(500);
               IF Keypressed THEN BEGIN
                 a:=ReadKey;
                 IF (a=KeyL) THEN BEGIN
                   Delay(500);
                   IF Keypressed THEN BEGIN
                     a:=ReadKey;
                     IF (a=KeyP) THEN BEGIN
                       Mission(Spiel.Akt_Level,OldBar);
                       REPEAT UNTIL Keypressed;
                       ClearPosition(OldBar);
                     END;
                   END;
                 END;
               END;
             END;
           END;
         END;
        KeyA:
         BEGIN
           Delay(500);
           IF Keypressed THEN BEGIN
             a:=ReadKey;
             IF (a=KeyW) THEN BEGIN
               Delay(500);
               IF Keypressed THEN BEGIN
                 a:=ReadKey;
                 CASE a OF
                   KeyG: BEGIN
                           Delay(500);
                           IF Keypressed THEN BEGIN
                             a:=ReadKey;
                             IF (a=KeyO) THEN BEGIN
                               Delay(500);
                               IF Keypressed THEN BEGIN
                                 a:=ReadKey;
                                 IF (a=KeyD) THEN
                                   Energie:=9999;
                               END;
                             END;
                           END;
                         END;
                   KeyA: BEGIN
                           Delay(500);
                           IF Keypressed THEN BEGIN
                             a:=ReadKey;
                             IF (a=KeyL) THEN BEGIN
                               Delay(500);
                               IF Keypressed THEN BEGIN
                                 a:=ReadKey;
                                 IF (a=KeyL) THEN
                                   FOR i:=2 TO Weapons_Max DO
                                    Schuss[i]:=50;
                               END;
                             END;
                           END;
                         END;
                 END; (*CASE*)
                 (*Cheats: awgod, awall*)
               END;
             END;
           END;
         END;
        KeyEsc:
         BEGIN
           AbbruchDesProgramms:=TRUE;
         END;
        ELSE
          IF (a=Keytwo) AND (Schuss[2] > 0) THEN
            Weapon_Aktiv:=2 (*Pistole -Magazin*)
          ELSE IF (a=Keythree) AND (Schuss[3] > 0) THEN
            Weapon_Aktiv:=3 (*MP - Kugeln*)
          ELSE IF (a=Keyfour) AND (Schuss[4] > 0) THEN
            Weapon_Aktiv:=4 (*Granade - "------"*)
          ELSE IF (a=Keyfive) AND (Schuss[5] > 0) THEN
            Weapon_Aktiv:=5; (*Bombe - "-------"*)
      END;
      Form.Frames.Zeichne(260,445,620,470,1,DarkGray,' Waffe: ',Weapon_Aktiv,'');
      Form.Frames.Zeichne(430,445,620,470,1,DarkGray,' Schu�: ',Schuss[Weapon_Aktiv],'');
      Form.Frames.Zeichne(200,415,400,440,1,DarkGray,' Gegner: ',Spiel.Gegner_Anz,'');
      Form.Frames.Zeichne(10,445,250,470,1,DarkGray,' Energie: ',Energie,'');
    END;
    Spiel.Zeit;
  UNTIL (a = KeyEsc) OR NaechstesLevel OR AbbruchDesProgramms;
END; (*Tastenlesen*)

PROCEDURE WadInit;
BEGIN
  Spiel.akt_Level:=1;
  Spiel_Ende:=FALSE;
  Mensch.Leben:=2;
  Mensch.Weapon_Aktiv:=2;
  Mensch.Energie:=70;
  Mensch.Schuss[1]:=0;
  Mensch.Schuss[2]:=4;
  Mensch.Schuss[3]:=3;
  Mensch.Schuss[4]:=1;
  Mensch.Schuss[5]:=0;
  Mensch.Schritt:=2;
  Mensch.Schritt2:=2;
  GetFillSettings(FillInfo);
END;
FUNCTION Game2.Erlaube(a: CHAR; x1_,y1_,x2_,y2_: WORD; Level: BYTE): BOOLEAN;
 VAR  ErlaubeTmp: BOOLEAN;
      i: Integer;
BEGIN
  Erlaube:=TRUE;
  FOR i:=1 TO Max_Wand DO
   ErlaubeTmp:=ErlaubeTmp AND Waende[Level][i].Erlaube(a,x1_,y1_,x2_,y2_);
  FOR i:=1 TO Max_Stufe DO
   ErlaubeTmp:=ErlaubeTmp AND Stufen[Level][i].Erlaube(a,x1_,y1_,x2_,y2_);
  Erlaube:=ErlaubeTmp;
END;
PROCEDURE Game2.Kollision(x1,y1,x2,y2: WORD);
 CONST Verwundet: BYTE = 3;
 VAR Lv: BYTE;
BEGIN
  FOR Lv:=1 TO MaxGegner DO BEGIN (* Kollision mit Gegnern *)
    IF NOT(Spiel.Feinde[Lv].Tot) AND Spiel.Feinde[Lv].Kollision(x1,y1,x2,y2) THEN BEGIN
      IF Spiel.Feinde[Lv].Richtung = Links THEN (*Gegner kehrt um*)
        Spiel.Feinde[Lv].Richtung:=Rechts
      ELSE
        Spiel.Feinde[Lv].Richtung:=Links;
      Mensch.Treffer(Verwundet); (*Mensch wird verwundet*)
    END;
  END;
  FOR Lv:=1 TO MaxItems DO (* Kollision" mit Items *) (*BUGS*)
    IF NOT(Spiel.Items[Lv].Tot) AND Spiel.Items[Lv].Kollision(x1,y1,x2,y2) THEN (* Item aufnehmen + l�schen*)
      IF Mensch.AddItem(Spiel.Items[Lv]) THEN
        Spiel.Items[Lv].Kill;
END;
PROCEDURE Game2.Tastenlesen;
BEGIN
  Mensch.Tastenlesen; (*Anfangsposition*)
END;
PROCEDURE Game2.Ablauf(Level: Byte);
 VAR Buffer: ARRAY[0..30] OF TIdRec;
     BufferSize: LongInt;
     i: Integer;
 PROCEDURE LoadPicture(Filename: String);
  VAR Breite, Hoehe: WORD;
 BEGIN
   Bild_einlesen(Path[2]+Filename,Breite,Hoehe);
   (* Bei Fehlen des Bildes wird leerer Bildschirm gespeichert *)
   IF (Breite=640) AND (Hoehe=480) THEN BEGIN
     SavePosition(0+9,80+9,95+9,110+9,DateienMem[22]);  (*arrow*)
     SavePosition(0+9,110+9,20+9,130+9,DateienMem[1]);  (*atomic*)
     SavePosition(20+9,110+9,40+9,130+9,DateienMem[2]);  (*atomic1*)
     SavePosition(40+9,110+9,60+9,130+9,DateienMem[3]);  (*bluekey*)
     SavePosition(0+9,220+9,25+9,245+9,DateienMem[38]);  (*bombe*)
     SavePosition(0+9,220+9,50+9,250+9,DateienMem[5]); (*cells*)
     SavePosition(60+9,110+9,70+9,120+9,DateienMem[30]);  (*erde*)
     SavePosition(60+9,120+9,70+9,130+9,DateienMem[31]);  (*erde1*)
     SavePosition(60+9,130+9,70+9,140+9,DateienMem[32]);  (*erde2*)
     SavePosition(41+9,141+9,70+9,170+9,DateienMem[6]);  (*firstaid*)
     SavePosition(70+9,110+9,80+9,120+9,DateienMem[33]);  (*gras*)
     SavePosition(71+9,141+9,100+9,170+9,DateienMem[8]);  (*medkit*)
     SavePosition(101+9,141+9,135+9,170+9,DateienMem[36]);  (*mp*)
     SavePosition(0+9,246+9,25+9,270+9,DateienMem[35]);  (*pistole*)
     SavePosition(95+9,80+9,115+9,140+9,DateienMem[29]);  (*stop*)
     SavePosition(115+9,80+9,135+9,140+9,DateienMem[34]);  (*wand*)
     SavePosition(140+9,0+9,170+9,75+9,Mensch.BildTrue); (*Gremlin*)
     SavePosition(170+9,0+9,200+9,75+9,Mensch.BildFalse); (*Gremlin1*)
     Mensch.Breite:=Mensch.BildTrue.XSize; Mensch.Hoehe:=Mensch.BildTrue.YSize;
     SavePosition(200+9,0+9,230+9,75+9,Spiel.Feinde[1].BildTrue); (*Knight*)
     SavePosition(230+9,0+9,260+9,75+9,Spiel.Feinde[1].BildFalse); (*Knight1*)
     SavePosition(0+9,0+9,35+9,80+9,Spiel.Feinde[2].BildTrue); (*Alien*)
     SavePosition(35+9,0+9,70+9,80+9,Spiel.Feinde[2].BildFalse); (*Alien1*)
     SavePosition(260+9,0+9,290+9,80+9,Spiel.Feinde[3].BildTrue); (*Skelet*)
     SavePosition(290+9,0+9,320+9,80+9,Spiel.Feinde[3].BildFalse); (*Skelet1*)
     SavePosition(320+9,0+9,350+9,80+9,Spiel.Feinde[4].BildTrue); (*Zombie*)
     Spiel.Feinde[4].BildFalse:=Spiel.Feinde[4].BildTrue;
     SavePosition(136+9,81+9,350+9,140+9,Spiel.Feinde[5].BildTrue); (*Boss*)
     Spiel.Feinde[5].BildFalse:=Spiel.Feinde[5].BildTrue;
   END
   ELSE BEGIN
     CloseGraph;
     WriteLn('Konnte Bilddatei '+Filename+'nicht initialisieren !');
     Halt;
   END;
 END; (*LoadPicture*)
 PROCEDURE Filter(Name: String; VAR Buf: ARRAY OF TIdRec; VAR Size: LongInt);
  VAR FileName: File Of TIdRec;
      Message: TIdRec;
      i: Integer;
 BEGIN
   Name:=Name+'.map';
   Assign(Filename,Name);
   {$I-}
   Reset(Filename);
   {$I+}
   IF IOResult<>0 THEN BEGIN
     CloseGraph;
     WriteLn('Fehler beim �ffnen der Datei ',Name);
     Halt;
   END;
   Max_Wand:=0;
   Max_Stufe:=0;
   Max_Grenze:=0;
   i:=0;
   WHILE i < FileSize(Filename) DO BEGIN
     Read(FileName,Message);
     WITH Message DO BEGIN
       Buf[i].X_:=x_; Buf[i].Y_:=y_; Buf[i].Special:=Special;
       CASE Special OF
         Wand: Inc(Max_Wand);
         Stufe: Inc(Max_Stufe);
         Grenze: Inc(Max_Grenze);
       END;
       Inc(i);
     END;
   END;
   Size:=i;
 END; (*Filter*)
 PROCEDURE Build(Buffer: ARRAY OF TIdRec; BufferSize: LongInt);
  CONST Items_Anz: BYTE =1;
        Stufen_Anz: BYTE =1;
        Waende_Anz: BYTE =1;
  VAR i,Lv: Integer;
 BEGIN
   Items_Anz:=0;
   Stufen_Anz:=0;
   Waende_Anz:=0;
   FOR i:=0 TO Pred(BufferSize) DO BEGIN
     WITH Buffer[i] DO BEGIN
       CASE Special OF
         MySelf_: Mensch.Make(X_,Y_);
         Enemy_: FOR Lv:=1 TO MaxGegner DO BEGIN
                   Spiel.Feinde[Lv].Energie:=70;
                   IF (Lv = Spiel.Akt_Level) THEN Spiel.Feinde[Spiel.Akt_Level].Make(X_,Y_)
                   ELSE Spiel.Feinde[Lv].Kill;
                   WITH Spiel.Feinde[Lv] DO BEGIN
                     Breite:=30; (*gleich, bis auf Endgegner*)
                     Hoehe:=80;  (*soll gleich sein*)
                   END;
                 END;
         EnergieMax: BEGIN
                       Inc(Items_Anz);
                       Spiel.Items[Items_Anz].Breite:=20;
                       Spiel.Items[Items_Anz].Hoehe:=20;
                       Spiel.Items[Items_Anz].Make(X_,Y_,EnergieMax);
                       Spiel.Items[Items_Anz].Stelle_Dar(1); (*Atomic*)
                     END;
         EnergieMin: BEGIN
                       Inc(Items_Anz);
                       Spiel.Items[Items_Anz].Breite:=29;
                       Spiel.Items[Items_Anz].Hoehe:=29;
                       Spiel.Items[Items_Anz].Make(X_,Y_,EnergieMin);
                       Spiel.Items[Items_Anz].Stelle_Dar(6); (*Firstaid*)
                     END;
         Magazin: BEGIN
                    Inc(Items_Anz);
                    Spiel.Items[Items_Anz].Breite:=36;
                    Spiel.Items[Items_Anz].Hoehe:=29;
                    Spiel.Items[Items_Anz].Make(X_,Y_,Magazin);
                    Spiel.Items[Items_Anz].Stelle_Dar(36); (*MP*)
                  END;
         Bomben: BEGIN
                   Inc(Items_Anz);
                   Spiel.Items[Items_Anz].Breite:=25;
                   Spiel.Items[Items_Anz].Hoehe:=25;
                   Spiel.Items[Items_Anz].Make(X_,Y_,Bomben);
                   Spiel.Items[Items_Anz].Stelle_Dar(38); (*Bombe*)
                 END;
         ExitItem: BEGIN
                     Inc(Items_Anz);
                     Spiel.Items[Items_Anz].Breite:=20;
                     Spiel.Items[Items_Anz].Hoehe:=20;
                     Spiel.Items[Items_Anz].Make(X_,Y_,ExitItem);
                     Spiel.Items[Items_Anz].Stelle_Dar(3); (*BlueKey*)
                   END;
         Stufe: BEGIN
                 Inc(Stufen_Anz);
                 Stufen[Spiel.Akt_Level][Stufen_Anz].Make(X_,X_+10,Y_);
                 Stufen[Spiel.Akt_Level][Stufen_Anz].Stelle_Dar;
                END;
         Wand: BEGIN
                 Inc(Waende_Anz);
                 Waende[Spiel.Akt_Level][Waende_Anz].Make(X_,Y_);
                 Waende[Spiel.Akt_Level][Waende_Anz].Stelle_Dar;
               END;
       END;
     END;
   END;
   WorldGround;
 END; (*Build*)
BEGIN
  IF NOT AbbruchDesProgramms THEN BEGIN
    IF Level = 1 THEN BEGIN
      LoadPicture(Dateien[1]);
      ClearDevice;
      InitialInit;
      WadInit;
    END;
    IF Level = MaxWelt+1 THEN
      Spiel_Ende:=TRUE
    ELSE BEGIN
      ClearDevice;
      Spiel.Akt_Level:=Level;
      Spiel.Gegner_Anz:=0;
      Spiel.Time:=Config.Minutes[Akt_Level]*60*100;
      Filter('World'+StrWord(Level),Buffer,BufferSize);
      Build(Buffer,BufferSize);
      FOR i:=0 TO BufferSize DO
        IF Buffer[i].Special=Enemy_ THEN Inc(Spiel.Gegner_Anz);
      Form.Zeichne_Weltrahmen(Mensch.Leben,Spiel.Time,Mensch.Schuss[1],Mensch.Energie,Spiel.Gegner_Anz,Mensch.Weapon_Aktiv);
      SetTextStyle(TriplexFont, HorizDir, 2);
      SetTextJustify(LeftText,TopText);
      OutTextXY(10,5,'Kommandos jetzt eingabebereit: ');
      OutTextXY(250,5,'Hilfe durch <HELP>');
      Tastenlesen;
    END;
  END;
END;
PROCEDURE Level;
 VAR  Lv: BYTE;
     ErrRes: WORD;
     OldBar: BarType;
     TmpTaste: CHAR;
BEGIN
  ClearDevice;
  WITH Game2(Spiel) DO BEGIN
    ErrRes:=ErrResultat;
    FOR Lv:=6 DOWNTO 2 DO BEGIN
      IF ErrRes >= Pot2(Lv-2) THEN BEGIN
        ErrRes:=ErrRes-Pot2(Lv-2);
        Ablauf(Lv);
      END
      ELSE BEGIN
        Ablauf(7-Lv);
        Spiel.Akt_Level:=Lv;
      END;
    END;
    ClearDevice;
    IF Spiel_Ende THEN BEGIN
      Happy_End;
      TmpTaste:=ReadKey;
      ClearDevice;
      Menu;
    END
    ELSE BEGIN
      Ende(OldBar);
      ClearPosition(OldBar);
    END;
   END; (*FOR*)
END;
PROCEDURE Game2.Zeit;
 CONST OldTime: WORD = 0;
       NewTime: WORD = 0;
 VAR i: Integer;
     TmpBar1, TmpBar2: BarType;
     TmpTaste: CHAR;
BEGIN
  WITH Form DO BEGIN
    IF Spiel.Time <= 0 THEN BEGIN
      Time_is_up(TmpBar1);
      TmpTaste:=ReadKey;
      ClearPosition(TmpBar1);
      Dec(Mensch.Leben);
      Frames.Zeichne(10,Weltrahmen_ButtomZeit_Y,190,
       Weltrahmen_ButtomZeit_Y+Frames.ButtomHigh,
       1, DarkGray,'  Leben: ',Mensch.Leben,'');
      IF Mensch.Leben <= 0 THEN BEGIN
        IF (SuddenDeath(TmpBar1)=Keyj) THEN BEGIN
          ClearPosition(TmpBar1);
         END
        ELSE BEGIN
          Ende(OldBar2);
          ClearPosition(OldBar2);
        END;
        Ablauf(Akt_Level);
      END;
      Spiel.Time:=Config.Minutes[Akt_Level]*60*100;
    END; (*time=0*)
    Dec(Spiel.Time);
    Delay(Geschwindigkeit[3]);
    NewTime:=Spiel.Time DIV 1000;
    IF OldTime <> NewTime THEN BEGIN
      OldTime:=NewTime;
      Frames.Zeichne(Weltrahmen_ButtomZeit_X,Weltrahmen_ButtomZeit_Y,
              Weltrahmen_ButtomZeit_X+Frames.ButtomWidth,Weltrahmen_ButtomZeit_Y+Frames.ButtomHigh,
              1,DarkGray,'  Zeit: ',NewTime,'');
    END;
  END; (*WITH ENDE*)
END; (* Zeit ENDE*)


(*Initialisierung*)
BEGIN
  WadInit;
END.
