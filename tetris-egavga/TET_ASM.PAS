{$A+,B-,D-,E-,F+,G-,I-,L-,N-,O+,R-,S-,V-,X-}
{ ***************** }
{ *** zu Tetris *** }
{ *** V1.0      *** }

UNIT Tet_ASM;
 { *** Definitionen ******************* }
INTERFACE
  CONST {Dunkle Farben (Vordergrund & Hintergrund):}
        Black        =  0;
        Blue         =  1;
        Green        =  2;
        Cyan         =  3;
        Red          =  4;
        Magenta      =  5;
        Brown        =  6;
        LightGray    =  7;
        {Helle Farben (Vordergrund):}
        DarkGray     =  8;
        LightBlue    =  9;
        LightGreen   = 10;
        LightCyan    = 11;
        LightRed     = 12;
        LightMagenta = 13;
        Yellow       = 14;
        White        = 15;
        {Durch Addition von Blink zu einer Vordergrundfarbe werden Zeichen blinkend dargestellt:}
        Blink        = 128;

 PROCEDURE Cursor_OnOff(Ein: BOOLEAN);
 PROCEDURE PutAttr(X,Y: BYTE; Attr: BYTE);
 FUNCTION  GetAttr(X,Y: BYTE): BYTE;
 PROCEDURE PutZeich(X,Y: BYTE; Zeich: CHAR; Attr: BYTE);
 PROCEDURE Get_Cursor_Definition(Seitennummer: BYTE; VAR Spalte, Zeile, Start, Stop: BYTE);
 PROCEDURE Get_Cursor_Position(VAR Spalte, Zeile: BYTE);
 PROCEDURE Setze_Cursor_Position(Seitennummer, Spalte, Zeile: BYTE);
 PROCEDURE Set_Cursor_Position(Spalte, Zeile: BYTE);
 PROCEDURE ClrScr;
 PROCEDURE WriteS(X,Y: BYTE; VAR OText: STRING; LoR: BOOLEAN; Length: BYTE);
 PROCEDURE MoveFast(VAR Source, Dest; Count: WORD);
 FUNCTION  Check_Character_Available: BYTE;
 FUNCTION  No_Echo_Read: CHAR;
 PROCEDURE TextColor(C: BYTE);
 PROCEDURE TextBackground(C: BYTE);
 PROCEDURE Delay(ms: WORD);

 { ************************************ }
 { *** Hauptteil ********************** }
IMPLEMENTATION
  USES  Crt;
  VAR   ScreenOfs:  INTEGER;           {Bildschirmseiten}
        ScreenSeg: WORD;
        DeltaOfs:  WORD;
        ScrWidth, ScrHigh: INTEGER;    {Bildschirmgroesse}
        ScreenSize: WORD;
        {Seitennummer 0 wird vorausgesetzt:}
        Seitennummer: BYTE;
        ITextAttr: BYTE;               {Attribut}


 { ************************************
   Name:     Cursor_OnOff(Ein)
   Funktion: Schaltet den Cursor ein/aus.
    Ein (ein): gew�nschter Cursor-Status.
   Beispiel: Cursor_OnOff(FALSE);
   ************************************ }
PROCEDURE Cursor_OnOff(Ein: BOOLEAN); ASSEMBLER;
ASM      (* Cursor_OnOff *)
  MOV  AH,0Fh              {Funktion $0F, Videoadapter bestimmen}
  INT  10h                 {BIOS-Funktion ausfueren}
  MOV  AH,03h              {Funktion $03, Position und Groesse lesen}
  INT  10h                 {BIOS-Funktion ausfueren}
  MOV  AH,1h               {Funktion $01, Position udd Groesse setzen}
  CMP  Ein,0h
  JZ   @001                {einschalten}
   AND CH,NOT $20          {Bit 5 = 0 -> sichtbar}
   JMP @002
 @001:                     {ausschalten}
   OR  CH,$20;             {Bit 5 = 1 -> unsichtbar}
 @002:
  INT  10h                 {BIOS-Funktion ausfueren}
END;     (* Cursor_OnOff *)
 { *** Setzt ein Attribut ************* }
PROCEDURE PutAttr(X,Y: BYTE; Attr: BYTE); ASSEMBLER;
ASM      (* PutAttr *)
  {ES:[DI] = Bildschirmadresse}
  MOV  BH,0             {X nach BL}
  MOV  BL,X
  DEC  BL
  {$IFDEF Teste}
  {Bereichs�berschreitung:}
  MOV  AL,Y             {IF (Y >= ScrHigh) OR (X >= ScrWidth) THEN EXIT}
  DEC  AL
  MOV  AH,ScrHigh
  CMP  AL,AH
  JGE  @Raus
  MOV  AL,BL
  MOV  AH,ScrWidth
  CMP  AL,AH
  JGE  @Raus
  {$ENDIF}
  {Vorbereitungen:}
  MOV  AH,0             {(Y*ScrWidth+X)*2+ScreenOfs nach DI}
  MOV  AL,Y
  DEC  AL
  MUL  ScrWidth
  ADD  AX,BX
  SHL  AX,1
  ADD  AX,ScreenOfs
  MOV  DI,AX
  MOV  ES,ScreenSeg     {ScreenSeg nach ES}
  {Attribut ausgeben:}
  INC  DI
  MOV  AL,Attr
  MOV  ES:[DI],AL       {Attribut copieren}
@Raus:
END;     (* PutAttr *)
 { *** Liefert das Attribut auf X,Y *** }
FUNCTION GetAttr(X,Y: BYTE): BYTE; ASSEMBLER;
ASM      (* GetAttr *)
  {ES:[DI] = Bildschirmadresse}
  MOV  BH,0             {X nach BL}
  MOV  BL,X
  DEC  BL
  {$IFDEF Teste}
  {Bereichs�berschreitung:}
  MOV  AL,Y             {IF (Y >= ScrHigh) OR (X >= ScrWidth) THEN EXIT}
  DEC  AL
  MOV  AH,ScrHigh
  CMP  AL,AH
  JGE  @Err
  MOV  AL,BL
  MOV  AH,ScrWidth
  CMP  AL,AH
  JGE  @Err
  {$ENDIF}
  {Vorbereitungen:}
  MOV  AH,0             {(Y*ScrWidth+X)*2+ScreenOfs nach DI}
  MOV  AL,Y
  DEC  AL
  MUL  ScrWidth
  ADD  AX,BX
  SHL  AX,1
  ADD  AX,ScreenOfs
  MOV  DI,AX
  MOV  ES,ScreenSeg     {ScreenSeg nach ES}
  {Attribut einlesen:}
  INC  DI
  MOV  AL,ES:[DI]       {Attribut copieren}
  JMP  @Raus
@Err:
  MOV  AL,$1F
@Raus:
  INC  AL
END;     (* GetIAttr_Text *)
 { *** Schreibt ein Zeichen mit Attribut *** }
PROCEDURE PutZeich(X,Y: BYTE; Zeich: CHAR; Attr: BYTE); ASSEMBLER;
ASM      (* PutZeich *)
  {ES:[DI] = Bildschirmadresse}
  MOV  BH,0             {X nach BL}
  MOV  BL,X
  DEC  BL
  {$IFDEF Teste}
  {Bereichs�berschreitung:}
  MOV  AL,Y             {IF (Y >= ScrHigh) OR (X >= ScrWidth) THEN EXIT}
  DEC  AL
  MOV  AH,ScrHigh
  CMP  AL,AH
  JGE  @Raus
  MOV  AL,BL
  MOV  AH,ScrWidth
  CMP  AL,AH
  JGE  @Raus
  {$ENDIF}
  {Vorbereitungen:}
  MOV  AH,0             {(Y*ScrWidth+X)*2+ScreenOfs nach DI}
  MOV  AL,Y
  DEC  AL
  MUL  ScrWidth
  ADD  AX,BX
  SHL  AX,1
  ADD  AX,ScreenOfs
  MOV  DI,AX
  MOV  ES,ScreenSeg     {ScreenSeg nach ES}
  {Zeichen ausgeben:}
  MOV  AL,Zeich
  MOV  AH,Attr
  MOV  ES:[DI],AX       {Zeichen copieren}
@Raus:
END;     (* PutZeich *)

 { ************************************
   Name:     Get_Cursor_Definition(Seitennummer, Spalte, Zeile, Start, Stop)
   Funktion: Liest die Cursorinformationen der angegebenen Bildschirmseite.
    Seitennummer (ein): Nummer der Bildschirmseite, fuer die die Cursor-
    informationen gelesen werden sollen.
    Spalte (aus): Nummer der aktuellen Cursorspalte.
    Zeile  (aus): Nummer der aktuellen Cursorzeile.
    Start  (aus): Oberste  Scanzeile des Cursors.
    Stop   (aus): Unterste Scanzeile des Cursors.
   Beispiel: Get_Cursor_Definition(0, Spalte, Zeile, Start, Stop);
   ************************************ }
PROCEDURE Get_Cursor_Definition(Seitennummer: BYTE;
                                VAR Spalte, Zeile, Start, Stop: BYTE); ASSEMBLER;
ASM      (* Get_Cursor_Definition *)
  MOV  BH,Seitennummer
  MOV  AH,$03
  INT  10h
  LES  BX,Spalte
  MOV  ES:[BX],DL
  LES  BX,Zeile
  MOV  ES:[BX],DH
  LES  BX,Start
  MOV  ES:[BX],CH
  LES  BX,Stop
  MOV  ES:[BX],CL
END;     (* Get_Cursor_Definition *)

 { *** Liefert Koordinaten des Cursors *** }
PROCEDURE Get_Cursor_Position(VAR Spalte, Zeile: BYTE);
  VAR   Start, Stop: BYTE;
BEGIN    (* Get_Cursor_Position *)
  Get_Cursor_Definition(Seitennummer,Spalte,Zeile,Start,Stop);
END;     (* Get_Cursor_Position *)

 { ************************************
   Name:     Setze_Cursor_Position(Seitennummer, Spalte, Zeile)
   Funktion: Stellt den Cursor an die angegebene Zeile und Spalte der
    angegebenen Bildschirmseite.
    Seitennummer (ein): Nummer der Bildschirmseite.
    Spalte       (ein): Nummer der gewuenschten Spalte.
    Zeile        (ein): Nummer der gewuenschten Zeile.
   Beispiel: Setze_Cursor_Position(0,10,15);
   ************************************ }
PROCEDURE Setze_Cursor_Position(Seitennummer, Spalte, Zeile: BYTE); ASSEMBLER;
ASM      (* Setze_Cursor_Position *)
  MOV  BH,Seitennummer
  MOV  DL,Spalte
  MOV  DH,Zeile
  MOV  AH,2h
  INT  10h
END;     (* Setze_Cursor_Position *)

 { *** Setzt Koordinaten des Cursors *** }
PROCEDURE Set_Cursor_Position(Spalte, Zeile: BYTE);
BEGIN    (* Set_Cursor_Position *)
  Setze_Cursor_Position(Seitennummer,Pred(Spalte),Pred(Zeile));
END;     (* Set_Cursor_Position *)

 { *** L�scht schnell den Bildschirm *** }
PROCEDURE ClrScr; ASSEMBLER;
  LABEL Loop;
ASM      (* ClrScr *)
  {ES:=$B800:}
  MOV  AX,$B800
  MOV  ES,AX
  {BX:=Seitennummer*$1000:}
  MOV  BL,0
  MOV  BH,Seitennummer
  SHL  BH,1
  SHL  BH,1
  SHL  BH,1
  SHL  BH,1
  {CX:=BX+80*25*2:}
  MOV  CX,BX
  ADD  CX,4000
  {Bildschirm l�schen:}
  MOV  AH,ITextAttr     {ITextAttr nach AH}
  MOV  AL,' '
 Loop:
  MOV  ES:[BX],AX
  INC  BX
  INC  BX
  CMP  BX,CX
  JLE  Loop
  {Cursorposition korrigieren:}
  MOV  BH,Seitennummer
  MOV  DX,0           {0,0}
  MOV  AH,2h
  INT  10h
END;     (* ClrScr *)

 { *** Gibt einen String aus **********
   Ver�ndert nicht die Cursorposition
   Vorsicht bei �berschreitung der Grenzen !}
PROCEDURE WriteS(X,Y: BYTE; VAR OText: STRING; LoR: BOOLEAN; Length: BYTE); ASSEMBLER;
ASM      (* WriteS *)
  {Warnung !  Mit ver�nderten DS,SI kann zu Fehlern bei Variablenlesen kommen}
  {DS:[SI] = Zeichenkettenadresse
   ES:[DI] = Bildschirmadresse
   CL      = L�nge der Zeichenkette
   CH      = Anzahl der Leerzeichen um den String
   BL      = L�nge der auszugebenden Zeichenkette
   DL      = X
   DH      = Y}
  MOV  DH,Y
  DEC  DH
  MOV  AL,X
  DEC  AL
  MOV  DL,AL
  {$IFDEF Teste}
  {Bereichs�berschreitung:}
  MOV  AH,ScrWidth      {IF (Y >= ScrHigh) OR (X >= ScrWidth) THEN EXIT}
  CMP  AL,AH
  JGE  @Raus
  MOV  AL,DH
  MOV  AH,ScrHigh
  CMP  AL,AH
  JGE  @Raus
  {$ENDIF}
  {Vorbereitungen:}
  MOV  BX,DX            {BX:=X}
  MOV  BH,0
  MOV  AH,0             {(Y*ScrWidth+X)*2+ScreenOfs nach DI}
  MOV  AL,DH
  MUL  ScrWidth
  ADD  AX,BX
  SHL  AX,1
  ADD  AX,ScreenOfs
  MOV  DI,AX
  MOV  ES,ScreenSeg     {ScreenSeg nach ES}
  MOV  AH,ITextAttr     {ITextAttr nach AH}
  PUSH DS               {Register retten}
  LDS  SI,&OText        {OText nach DS:[SI]}
  MOV  AL,DS:[SI]       {L�nge copieren}
  CMP  AL,0
  JG   @Ok
  MOV  CH,Length        {Kein Text zum Ausgeben da}
  JMP  @LeerNach
 @Ok:
  INC  SI
  MOV  CL,AL
  MOV  AL,Length        {Length nach DL}
  CMP  AL,0
  JnE  @Leng            {IF L = 0 THEN L:=Length(S)}
  MOV  AL,CL
  JMP  @Lng
 @Leng:
  CMP  AL,CL
  JGE  @Lng
  MOV  CL,AL
 @Lng:
  MOV  BL,AL            {Zeichenanzahl nach DL}
  SUB  AL,CL            {Leerzeichenanzahl nach CH}
  MOV  CH,AL
  MOV  AL,LoR           {IF LoR THEN}
  CMP  AL,0
  JE   @OutTxt
  {Vorstehende Leerzeichen ausgeben:}
  CMP  CH,0             {WHILE Leer < L-Length(S) DO BEGIN}
  JE   @OutTxt
  MOV  AL,' '
  DEC  CH
 @LeerVorLoop:
  MOV  ES:[DI],AX       {Leerzeichen und Attribut schreiben}
  INC  DI
  INC  DI
  DEC  CH
  JnS  @LeerVorLoop     {END}
 @OutTxt:
  {Text Byte f�r Byte ausgeben:}
  CMP  CL,0
  JE   @EndOutTxt
  DEC  CL               {L�nge um 1 erniedrigen}
 @OutTxtLoop:
  MOV  AL,DS:[SI]       {Buchstabe und Farbe}
  MOV  ES:[DI],AX       { copieren}
  INC  SI               {N�chster Buchstabe}
  INC  DI               {N�chste Adresse}
  INC  DI
  DEC  CL               {Restliche L�nge k�rzen}
  JnS  @OutTxtLoop      {N�chster Buchstabe, wenn CL nicht unterlaufen}
 @EndOutTxt:
  MOV  AL,LoR           {IF NOT LoR THEN}
  CMP  AL,0
  JnE  @Curs
  {Nachfolgende Leerzeichen ausgeben:}
 @LeerNach:
  CMP  CH,0             {WHILE Leer < L-Length(S) DO BEGIN}
  JE   @Curs
  MOV  AL,' '
  DEC  CH
 @LeerNachLoop:
  MOV  ES:[DI],AX       {Leerzeichen und Attribut schreiben}
  INC  DI
  INC  DI
  DEC  CH
  JnS  @LeerNachLoop    {END}
 @Curs:
  POP  DS               {Register reparieren}
@Raus:
END;     (* WriteS *)

 { *** Copiert schnell Source nach Dest *** }
PROCEDURE MoveFast(VAR Source, Dest; Count: WORD); ASSEMBLER;
ASM      (* MoveFast *)
    MOV   BX, DS          {rette DS}
    LDS   SI, [Source]    {Source nach DS:[SI]}
    LES   DI, [Dest]      {Dest   nach ES:[DI]}
    MOV   CX, [Count]     {Anzahl der Bytes nach CX}
    CLD                   {???}
    SHR   CX, 1           {Anzahl halbieren,}
    JNC   @Out            { wenn ungerade gewesen, wird Carry gesetzt}
    MOVSB                 {ein Byte copieren}
  @Out:                   {jetzt nur Words �brig}
    REP   MOVSW           {copiere CX Words}
    MOV   DS, BX          {stelle DS wieder her}
END;     (* MoveFast *)

 { ************************************
   Name:     Check_Character_Available
   Funktion: Liefert den Wert 255, wenn an der Standardeingabe
    ein Zeichen verf�gbar ist, und ansonsten den Wert 0.
   Beispiel: Status:=Check_Character_Available;
   ************************************ }
FUNCTION Check_Character_Available: BYTE; ASSEMBLER;
ASM      (* Check_Character_Available *)
  MOV  AH,0Bh
  INT  21h
END;     (* Check_Character_Available *)

 { ************************************
   Name:     No_Echo_Read
   Funktion: Liest ein Zeichen von der Stanardeingabe,
    ohne es gleichzeitig am Bildschirm anzuzeigen.
   Beispiel: Zeichen:=No_Echo_Read;
    Wenn kein Zeichen im Tastaturpuffer bereit ist,
    wartet diese Routine, bis ein Zeichen verf�gbar wird.
    Wenn der Benutzer eine Funktionstaste dr�ckt, liefert No_Echo_Read
    beim ersten Aufruf den Wert Null. Mit einem weiteren Aufruf von
    No_Echo_Read kann der Scancode der gedr�ckten Taste bestimmt werden.
    Diese Routine zeigt das vom Benutzer eingegebene Zeichen am
    Bildschirm nicht an.
   ************************************ }
FUNCTION No_Echo_Read: CHAR; ASSEMBLER;
ASM      (* No_Echo_Read *)
  MOV  AH,07h
  INT  21h
END;     (* No_Echo_Read *)

{ *** Setzt die Vordergrundfarbe *** }
PROCEDURE TextColor(C: BYTE);
BEGIN    (* TextColor *)
  ITextAttr:=(ITextAttr AND $F0) OR C;
END;     (* TextColor *)

{ *** Setzt die Hintergrundfarbe *** }
PROCEDURE TextBackground(C: BYTE);
BEGIN    (* TextBackground *)
  ITextAttr:=(ITextAttr AND $0F) OR (C SHL 4);
END;     (* TextBackground *)

 { ************************************
   Name:     Delay(ms)
   Funktion: Wartet.
    ms (ein): Tausendstelsekunden.
   Beispiel: Delay(1000);
   ************************************ }
PROCEDURE Delay(ms: WORD);
BEGIN    (* Delay *)
  Crt.Delay(ms);
END;     (* Delay *)

  { *** Initialisierung *************** }
BEGIN    (* Tet_ASM *)
  {Farb-Karte wird vorausgesetzt:}
  ScreenSeg:=$B800;
  DeltaOfs:=$1000;
  ScrWidth:=80;
  ScrHigh:=25;
  ScreenOfs:=DeltaOfs*Seitennummer;
  ScreenSize:=2*ScrWidth*ScrHigh;
  Seitennummer:=0; {Seitennummer 0 wird vorausgesetzt:}
  ITextAttr:=0;
 { *** Hauptteil Ende ***************** }
END.     (* Tet_ASM *)
