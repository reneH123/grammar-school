All rights reserved, copyright to Rene Haberland, (C) 1997-2001, 2020

The COMTEC-relevant packages may have been protected further and may not be used whatsoever in any public circumstance without prior written consent by the author.
 - Except from that all other packages may be used under the conditions of the GNU GENERAL PUBLIC LICENSE version 3 (GPL3), namely under attribution of the author, etc. The license terms are added and can also be found under: http://www.gnu.org/licenses.

The primary goal of those packages is for educational purposes. Everyone is hereby invited to check out and play around with the purely experimental code bases under the conditions mentioned. Disclaimer: no guarantee for any crash/loss of information whatsoever, use at your full own risk and joy.


All program listings are as were written in the years mentioned. Word documents had been saved as PDF file (some macros, etc), hence some macros may have vanished or been disabled every since.


12th September 2020
