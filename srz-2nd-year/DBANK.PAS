{$A+,B-,D+,E-,F+,G+,I-,L+,N+,O-,P-,Q-,R-,S-,T-,V-,X+}
{$M 65520,0,655360}
PROGRAM Schuldatenbank;
USES Crt;
TYPE PLZZAHL= STRING[5];
     TBUCH= RECORD
       Name: STRING[30];
       Vorname: STRING[20];
       Klasse: STRING[4];
       Geschlecht: CHAR;
       G_Date: STRING[10];
       Strasse: STRING[30];
       PLZ: PLZZAHL;
       Wohnort: STRING[30];
     END;
VAR F: FILE OF TBUCH;
    Person: ARRAY[1..49] OF TBUCH;
    Name: ARRAY[1..49] OF STRING[30];
    a: Integer;

FUNCTION Start: Char;
  BEGIN
    WriteLn('Die Schuldatenbank                                Abbruch 2xESC');
    WriteLn;
    WriteLn('(N)eu');
    WriteLn('(O)effnen');
    WriteLn('(L)�schen');
    WriteLn('(B)eenden');
    WriteLn;
    Write('Ihre Wahl: ');
    Start:=ReadKey;
  END;

PROCEDURE Einlesen(i: Integer);
  BEGIN
    WriteLn;
    WriteLn;
    WriteLn('Neue Kartei:');
    WITH Person[i] DO BEGIN
      WriteLn;
      Write('Name: ');
      ReadLn(Name);
      Write('Vorname: ');
      ReadLn(Vorname);
      Write('Klasse: ');
      ReadLn(Klasse);
      Write('(m)�nnlich/(w)eiblich: ');
      REPEAT
        Geschlecht:=ReadKey;
      UNTIL UpCase(Geschlecht) IN ['M','W'];
      WriteLn(Geschlecht);
      Write('Geburtsdatum: ');
      ReadLn(G_Date);
      Write('Strasse: ');
      ReadLn(Strasse);
      Write('Postleitzahl: ');
      ReadLn(PLZ);
      Write('Wohnort: ');
      ReadLn(Wohnort);
    END;
    Assign(F,'Info.Txt');
    {I-}
    Reset(F);
    {I+}
    IF IOResult <> 0 THEN
      Rewrite(F)
    ELSE
      Seek(F,Filesize(F)); {Setzt Position ans Ende der Datei}
    Write(F,Person[i]);
    Close(F);
  END;

PROCEDURE Namen_Einlesen(VAR Count: WORD);
  VAR  Pers: TBuch;
BEGIN
  Assign(F,'Info.Txt');
  {I-}
  Reset(F);
  {I+}
  Count:=0;
  IF IOResult <> 0 THEN EXIT;
  WHILE NOT EoF(F) DO BEGIN
    Inc(Count);
    Read(F,Pers);
    Name[Count]:=Pers.Name
  END;
  Close(F);
END;

PROCEDURE Daten_Einlesen(VAR Count: WORD);
BEGIN
  Assign(F,'Info.Txt');
  {I-}
  Reset(F);
  {I+}
  Count:=0;
  IF IOResult <> 0 THEN EXIT;
  WHILE NOT EoF(F) DO BEGIN
    Inc(Count);
    Read(F,Person[Count]);
  END;
  Close(F);
END;

PROCEDURE Ausgeben(i: Integer);
  VAR Count: WORD;
BEGIN
  ClrScr;
  Daten_Einlesen(Count);
  WriteLn('Inhalt der Kartei: ');
  WriteLn;
  FOR i:=1 TO Count DO BEGIN
    WITH Person[i] DO BEGIN
      WriteLn('Name: ',Name);
      WriteLn('Vorname: ',Vorname);
      WriteLn('Klasse: ',Klasse);
      WriteLn('(m)�nnlich/(w)eiblich: ',Geschlecht);
      WriteLn('Geburtsdatum: ',G_Date);
      WriteLn('Strasse: ',Strasse);
      WriteLn('Postleitzahl: ',PLZ);
      WriteLn('Wohnort: ',Wohnort);
      WriteLn;
    END;
  END;
  Write('ESC -> Ende...');
END;

PROCEDURE Clear;
  VAR Count: WORD;
      i: WORD;
      Lv: WORD;
      Killed: BOOLEAN;
      Wahl: String[30];
      Position: Longint;
BEGIN
  Daten_Einlesen(Count);
  WriteLn('Welcher der genannten Karteien m�chten sie l�schen? ');
  WriteLn;
  FOR i:=1 TO Count DO
    Write(Person[i].Name:40);
  WriteLn;
  Write('Name: ?');
  ReadLn(Wahl);
  Killed:=FALSE;
  FOR i:=1 TO Count DO BEGIN
    IF Person[i].Name = Wahl THEN BEGIN
      Killed:=TRUE;
      Seek(F,i);
      Assign(F,'Info.Txt');
      Rewrite(F);
      FOR Lv:=1 TO Count DO
        IF Lv <> i THEN
          Write(F,Person[Lv]);
      Close(F);
      Daten_Einlesen(Count);
    END
    ELSE WriteLn('Eine solche Kartei existiert nicht!');
    WriteLn('ESC!');
  END;
  Close(F);
END;

BEGIN
  ClrScr;
  a:=1;
  REPEAT
    CASE UpCase(Start) OF
      'N': Einlesen(a);
      'O': Ausgeben(a);
      'L': Clear;
      'B': Halt(1);
    END;
    a:=a+1;
  UNTIL ReadKey=#27;
END.