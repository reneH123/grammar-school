program liesdbase;

uses crt;

type tfeld = record
               name : array[1..11] of char;
               feldtyp : char;
               addr : array[1..2] of word;
               laenge : byte;
               nachkomma : byte;
               res1 : word;
               arbbereich : byte;
               res2 : word;
               kennz : byte;
               res3 : array[1..8] of byte;
             end;

   theader = record
               version : byte;
               datum : array[1..3] of byte;
               anzds : longint;
               lenh : word;
               lends : word;
               res1 : word;
               flagtrans : byte;
               flagver : byte;
               res2 : array[1..12] of byte;
               mdx : byte;
               res3 : array[1..3] of byte;
             end;


var f : file;
    h : theader;
    felder : tfeld;
    i, anz : word;
    s : string;

procedure liesheader(var hd : theader);
begin
  blockread(f,hd,1)
end;

begin
  clrscr;
  writeln('Programm zum Lesen der Struktur einer dBASE-Datei');
  writeln;
  if paramcount=0 then s:='test.dbf'
  else s:=paramstr(1);
  assign(f,s);
  {$I-}
  reset(f,32);
  {$I+}
  if ioresult<>0 then begin
    writeln('Datei ',s,' existiert nicht.');
  end
  else begin
    liesheader(h);
    anz:=(h.lenh-33) div 32;
    writeln('Feldname':20,'Feldtyp':10);
    writeln;
    for i:=1 to anz do begin
      blockread(f,felder,1);
      write(felder.name:23);
      case felder.feldtyp of
        'C':writeln('Character');
    'N','F':writeln('Numerisch');
        'L':writeln('logisch');
        'D':writeln('Datum');
        'M':writeln('Memo');
      end
    end
  end;
  close(f);
  readln
end.
