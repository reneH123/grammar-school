Program Sortieren;
Uses Crt;
Const Anz=100;
Type Feld= Array[1..Anz+1] Of Integer;
Var n: Byte;
    Feld_alt: Feld;

Function Sort(Stelle: Byte): Integer;
Var x,i,hilf,a: Integer;
    Bool: Boolean;
Begin
 ClrScr;
 WriteLn;
 WriteLn('Die Zahlen im Feld lauten: ');
 For i:=1 To Anz Do Begin
  a:=Random(1000)+11;
  Feld_alt[i]:=a;
  Write(Feld_alt[i]:8);
 End;
 Repeat
  Bool:=False;
  For i:=1 To Anz Do Begin
   If Feld_alt[i] > Feld_alt[i+1] Then Begin
    Hilf:=Feld_alt[i];
    Feld_alt[i]:=Feld_alt[i+1];
    Feld_alt[i+1]:=Hilf;
    Bool:=True;
   End;
  End;
 Until Not Bool;
 WriteLn('Das geordnete Feld lautet: ');
 For i:=1 To Anz Do
 Write(Feld_alt[i]:8);
 Write('Die kleinste Zahl im Feld ist: ',Feld_alt[1]:2);
 Textcolor(black);
End;

Begin
 Repeat
  Textcolor(8);
  WriteLn;
  WriteLn('Minimumsuche in einem Feld: ',Sort(n));
 Until ReadKey=#27;
End.