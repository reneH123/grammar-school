PROGRAM Test;
USES Crt;
TYPE T_B= Record
    Titel:String;
    Autor: String[30];
    Seiten: Word;
    Preis: Real;
    ausgeliehen: String[4];
End;
VAR T_Buch: T_B;

PROCEDURE Frage;
BEGIN
 WITH T_Buch DO BEGIN
  Write('Der Titel des Buches hei�t: ');
  ReadLn(Titel);
  Write('Der Autor des Buches hei�t: ');
  ReadLn(Autor);
  Write('Das Buch hat Seiten: ');
  ReadLn(Seiten);
  Write('Das Buch kostet DM: ');
  ReadLn(Preis);
  Write('Das Buch ist ausgeliehen: ');
  ReadLn(ausgeliehen);
 END;
END;

PROCEDURE Antwort;
BEGIN
 WITH T_Buch DO BEGIN
  WriteLn('Der Titel des Buches hei�t: ',Titel);
  WriteLn('Der Autor des Buches hei�t: ',Autor);
  WriteLn('Das Buch hat Seiten: ',Seiten);
  WriteLn('Das Buch kostet DM: ',Preis:0:2);
  WriteLn('Das Buch ist ausgeliehen: ',ausgeliehen);
 END;
END;

BEGIN
 ClrScr;
 Frage;
 WriteLn;
 Antwort;
 Readln;
END.